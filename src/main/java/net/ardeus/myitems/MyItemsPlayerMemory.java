package net.ardeus.myitems;

import net.ardeus.myitems.manager.player.PlayerItemStatsManager;
import net.ardeus.myitems.manager.player.PlayerManager;
import net.ardeus.myitems.manager.player.PlayerPassiveEffectManager;
import net.ardeus.myitems.manager.player.PlayerPowerManager;
import net.ardeus.myitems.player.PlayerItemStatsMemory;
import net.ardeus.myitems.player.PlayerPassiveEffectMemory;
import net.ardeus.myitems.player.PlayerPowerMemory;

public final class MyItemsPlayerMemory extends PlayerManager {

	private final PlayerItemStatsManager playerItemStatsManager;
	private final PlayerPassiveEffectManager playerPassiveEffectManager;
	private final PlayerPowerManager playerPowerManager;
	
	protected MyItemsPlayerMemory(MyItems plugin) {
		super(plugin);
		
		this.playerItemStatsManager = PlayerItemStatsMemory.getInstance();
		this.playerPassiveEffectManager = PlayerPassiveEffectMemory.getInstance();
		this.playerPowerManager = PlayerPowerMemory.getInstance();
	}

	@Override
	public final PlayerItemStatsManager getPlayerItemStatsManager() {
		return this.playerItemStatsManager;
	}

	@Override
	public final PlayerPassiveEffectManager getPlayerPassiveEffectManager() {
		return this.playerPassiveEffectManager;
	}

	@Override
	public final PlayerPowerManager getPlayerPowerManager() {
		return this.playerPowerManager;
	}

}
