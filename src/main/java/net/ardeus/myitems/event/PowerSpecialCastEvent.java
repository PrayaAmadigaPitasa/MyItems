package net.ardeus.myitems.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerSpecial;
import net.ardeus.myitems.power.PowerType;

public class PowerSpecialCastEvent extends PowerPreCastEvent {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final PowerSpecial powerSpecial;
	
	private boolean cancel = false;
	private double cooldown;
	
	public PowerSpecialCastEvent(Player player, PowerType power, PowerClickType click, ItemStack item, String lore, PowerSpecial powerSpecial, double cooldown) {
		super(player, power, click, item, lore);
		
		if (powerSpecial == null) {
			throw new IllegalArgumentException();
		} else {
			this.powerSpecial = powerSpecial;
			this.cooldown = cooldown;
		}
	}

	public final PowerSpecial getPowerSpecial() {
		return this.powerSpecial;
	}
	
	public final double getCooldown() {
		return this.cooldown;
	}
	
	public final void setCooldown(double cooldown) {
		this.cooldown = cooldown;
	}

	public HandlerList getHandlers() {
        return handlers;
    }
	
	public static HandlerList getHandlerList() {
        return handlers;
    }
    
    @Override
	public boolean isCancelled() {
		return cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
}