package net.ardeus.myitems.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerType;

public class PowerPreCastEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	private boolean cancel = false;
	
	private final Player player;
	private final PowerType power;
	private final PowerClickType click;
	private final ItemStack item;
	private final String lore;

    public PowerPreCastEvent(Player player, PowerType power, PowerClickType click, ItemStack item, String lore) {
    	this.player = player;
        this.power = power;
        this.click = click;
        this.item = item;
        this.lore = lore;
    }
    
    public final Player getPlayer() {
    	return this.player;
    }
    
    public final PowerType getPower() {
    	return this.power;
    }
    
    public final PowerClickType getClick() {
    	return this.click;
    }
    
    public final ItemStack getItem() {
    	return this.item;
    }
    
    public final String getLore() {
    	return this.lore;
    }

    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

	@Override
	public boolean isCancelled() {
		return this.cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
}