package net.ardeus.myitems.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerCommand;
import net.ardeus.myitems.power.PowerType;

public class PowerCommandCastEvent extends PowerPreCastEvent {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final PowerCommand powerCommand;
	
	private boolean cancel = false;
	private double cooldown;
	
	public PowerCommandCastEvent(Player player, PowerType power, PowerClickType click, ItemStack item, String lore, PowerCommand powerCommand, double cooldown) {
		super(player, power, click, item, lore);
		
		this.powerCommand = powerCommand;
		this.cooldown = cooldown;
	}

	public final PowerCommand getPowerCommand() {
		return this.powerCommand;
	}
	
	public final double getCooldown() {
		return this.cooldown;
	}
	
	public final void setCooldown(double cooldown) {
		this.cooldown = cooldown;
	}

    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    @Override
	public boolean isCancelled() {
		return cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
}