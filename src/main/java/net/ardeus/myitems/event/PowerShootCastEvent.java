package net.ardeus.myitems.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.enums.branch.ProjectileEnum;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerType;

public class PowerShootCastEvent extends PowerPreCastEvent {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final ProjectileEnum projectile;
	
	private boolean cancel = false;
	private double cooldown;
	private double speed;
	
	public PowerShootCastEvent(Player player, PowerType power, PowerClickType click, ItemStack item, String lore, ProjectileEnum projectile, double cooldown) {
		super(player, power, click, item, lore);
		
		this.projectile = projectile;
		this.cooldown = cooldown;
		this.speed = 3D;
	}

	public final ProjectileEnum getProjectile() {
		return this.projectile;
	}
	
	public final double getCooldown() {
		return this.cooldown;
	}
	
	public final double getSpeed() {
		return this.speed;
	}
	
	public final void setCooldown(double cooldown) {
		this.cooldown = cooldown;
	}
	
	public final void setSpeed(double speed) {
		this.speed = speed;
	}

    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
    
    @Override
	public boolean isCancelled() {
		return cancel;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancel = cancel;
	}
}