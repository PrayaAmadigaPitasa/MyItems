package net.ardeus.myitems.lorestats;

public class LoreStatsModifier {
	
	private final LoreStatsWeapon weaponModifier;
	private final LoreStatsArmor armorModifier;
	private final LoreStatsUniversal universalModifier;
	
	public LoreStatsModifier() {
		this.weaponModifier = new LoreStatsWeapon(1, 1, 1, 1, 1, 1, 1, 1, 1);
		this.armorModifier = new LoreStatsArmor(1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
		this.universalModifier = new LoreStatsUniversal(1, 1);
	}
	
	public LoreStatsModifier(LoreStatsWeapon weaponModifier, LoreStatsArmor armorModifier, LoreStatsUniversal universalModifier) {
		this.weaponModifier = weaponModifier;
		this.armorModifier = armorModifier;
		this.universalModifier = universalModifier;
	}
	
	public final LoreStatsWeapon getWeaponModifier() {
		return this.weaponModifier;
	}
	
	public final LoreStatsArmor getArmorModifier() {
		return this.armorModifier;
	}
	
	public final LoreStatsUniversal getUniversalModifier() {
		return this.universalModifier;
	}
	
	public final double getModifier(LoreStatsEnum loreStats) {
		switch (loreStats) {
		case BLOCK_AMOUNT : return armorModifier.getBlockAmount();
		case BLOCK_RATE : return armorModifier.getBlockRate();
		case CRITICAL_CHANCE : return weaponModifier.getCriticalChance();
		case CRITICAL_DAMAGE : return weaponModifier.getCriticalDamage();
		case DAMAGE : return weaponModifier.getDamage();
		case DEFENSE : return armorModifier.getDefense();
		case DODGE_RATE : return armorModifier.getDodgeRate();
		case DURABILITY : return universalModifier.getDurability();
		case HEALTH : return armorModifier.getHealth();
		case HIT_RATE : return weaponModifier.getHitRate();
		case LEVEL : return universalModifier.getLeveL();
		case PENETRATION : return weaponModifier.getPenetration();
		case PVE_DAMAGE : return weaponModifier.getPvEDamage();
		case PVE_DEFENSE : return armorModifier.getPvEDefense();
		case PVP_DAMAGE : return weaponModifier.getPvPDamage();
		case PVP_DEFENSE : return armorModifier.getPvPDefense();
		default : return 1;
		}
	}
}
