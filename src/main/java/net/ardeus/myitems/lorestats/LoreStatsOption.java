package net.ardeus.myitems.lorestats;

import java.util.Arrays;
import java.util.List;

public enum LoreStatsOption {
	
	MIN("Min", Arrays.asList("Min", "Minimal", "Low", "Lowest")),
	MAX("Max", Arrays.asList("Max", "Maximal", "High", "Highest")),
	CURRENT("Current", Arrays.asList("Current", "Now"));
	
	private final String text;
	private final List<String> aliases;
	
	private LoreStatsOption(String text, List<String> aliases) {
		this.text = text;
		this.aliases = aliases;
	}
	
	public final String getText() {
		return this.text;
	}
	
	public final List<String> getAliases() {
		return this.aliases;
	}
	
	public static final LoreStatsOption get(String option) {
		for (LoreStatsOption key : LoreStatsOption.values()) {
			for (String aliase : key.getAliases()) {
				if (aliase.equalsIgnoreCase(option)) {
					return key;
				}
			}
		}
		
		return null;
	}
}
