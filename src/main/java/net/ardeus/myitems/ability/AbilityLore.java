package net.ardeus.myitems.ability;

public class AbilityLore {

	private final String keyLore;
	private final int grade;
	private final double chance;
	
	public AbilityLore(String keyLore, int grade, double chance) {
		if (keyLore == null) {
			throw new IllegalArgumentException();
		} else {
			this.keyLore = keyLore;
			this.grade = Math.max(1, grade);
			this.chance = Math.max(0, Math.min(100, chance));
		}
	}
	
	public final String getKeyLore() {
		return this.keyLore;
	}
	
	public final int getGrade() {
		return this.grade;
	}
	
	public final double getChance() {
		return this.chance;
	}
}
