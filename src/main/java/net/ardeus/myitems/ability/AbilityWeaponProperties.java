package net.ardeus.myitems.ability;

import com.praya.agarthalib.utility.MathUtil;

public class AbilityWeaponProperties {

	private final int maxGrade;
	private final int baseDurationEffect;
	private final int scaleDurationEffect;
	private final double scaleBaseBonusDamage;
	private final double scaleBasePercentDamage;
	private final double scaleCastBonusDamage;
	private final double scaleCastPercentDamage;
	
	public AbilityWeaponProperties(int maxGrade, int baseDurationEffect, int scaleDurationEffect, double scaleBaseBonusDamage, double scaleBasePercentDamage, double scaleCastBonusDamage, double scaleCastPercentDamage) {
		this.maxGrade = maxGrade;
		this.baseDurationEffect = baseDurationEffect;
		this.scaleDurationEffect = scaleDurationEffect;
		this.scaleBaseBonusDamage = scaleBaseBonusDamage;
		this.scaleBasePercentDamage = scaleBasePercentDamage;
		this.scaleCastBonusDamage = scaleCastBonusDamage;
		this.scaleCastPercentDamage = scaleCastPercentDamage;
	}
	
	public final int getMaxGrade() {
		return this.maxGrade;
	}
	
	public final int getBaseDurationEffect() {
		return this.baseDurationEffect;
	}
	
	public final int getScaleDurationEffect() {
		return this.scaleDurationEffect;
	}
	
	public final double getScaleBaseBonusDamage() {
		return this.scaleBaseBonusDamage;
	}
	
	public final double getScaleBasePercentDamage() {
		return this.scaleBasePercentDamage;
	}
	
	public final double getScaleCastBonusDamage() {
		return this.scaleCastBonusDamage;
	}
	
	public final double getScaleCastPercentDamage() {
		return this.scaleCastPercentDamage;
	}
	
	public final int getTotalDuration(int grade) {
		final int finalGrade = MathUtil.limitInteger(grade, 0, getMaxGrade());
		final int baseDuration = getBaseDurationEffect();
		final int scaleDuration = getScaleDurationEffect();
		final int totalDuration = baseDuration + (finalGrade * scaleDuration);
		
		return totalDuration;
	}
	
	public final double getTotalBaseDamage(int grade, double damage) {
		final int finalGrade = MathUtil.limitInteger(grade, 0, getMaxGrade());
		final double scaleBonusDamage = getScaleBaseBonusDamage();
		final double scalePercentDamage = getScaleBasePercentDamage();
		final double totalDamage = (finalGrade * scaleBonusDamage) + (damage * (finalGrade * scalePercentDamage / 100));
		
		return totalDamage;
	}
	
	public final double getTotalCastDamage(int grade, double damage) {
		final int finalGrade = MathUtil.limitInteger(grade, 0, getMaxGrade());
		final double scaleBonusDamage = getScaleCastBonusDamage();
		final double scalePercentDamage = getScaleCastPercentDamage();
		final double totalDamage = (finalGrade * scaleBonusDamage) + (damage * (finalGrade * scalePercentDamage / 100));
		
		return totalDamage;
	}
}
