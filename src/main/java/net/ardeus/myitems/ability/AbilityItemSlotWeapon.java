package net.ardeus.myitems.ability;

import java.util.ArrayList;
import java.util.List;

import core.praya.agarthalib.enums.main.Slot;

public final class AbilityItemSlotWeapon extends AbilityItemSlot {

	private final List<AbilityItemWeapon> listAbilityItemWeapon;
	
	public AbilityItemSlotWeapon(Slot slot, List<AbilityItemWeapon> listAbilityItemWeapon) {
		super(slot);
		
		this.listAbilityItemWeapon = listAbilityItemWeapon != null ? listAbilityItemWeapon : new ArrayList<AbilityItemWeapon>();
	}
	
	public final List<AbilityItemWeapon> getListAbilityItemWeapon() {
		return this.listAbilityItemWeapon;
	}
}
