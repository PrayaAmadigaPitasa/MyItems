package net.ardeus.myitems.ability;

import core.praya.agarthalib.enums.main.Slot;

public abstract class AbilityItemSlot {

	private final Slot slot;
	
	protected AbilityItemSlot(Slot slot) {
		if (slot == null) {
			throw new IllegalArgumentException();
		} else {
			this.slot = slot;
		}
	}
	
	public final Slot getSlot() {
		return this.slot;
	}
}
