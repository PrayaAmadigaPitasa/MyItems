package net.ardeus.myitems.ability;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.FileUtil;

public final class AbilityWeaponConfig extends HandlerConfig {

	protected final Map<String, AbilityWeaponProperties> mapAbilityWeaponProperties = new HashMap<String, AbilityWeaponProperties>();

	protected AbilityWeaponConfig(MyItems plugin) {
		super(plugin);
		
		setup();
	};
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapAbilityWeaponProperties.clear();
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String path = dataManager.getPath("Path_File_Ability_Weapon");
		final File file = FileUtil.getFile(plugin, path);
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, path);
		}
		
		for (int t = 0; t < 2; t++) {
			final FileConfiguration config;
			
			if (t == 0) {
				config = FileUtil.getFileConfigurationResource(plugin, path);
			} else {
				config = FileUtil.getFileConfiguration(file);
			}
			
			for (String key : config.getKeys(false)) {
				final ConfigurationSection mainDataSection = config.getConfigurationSection(key);
				
				int maxGrade = 1;
				int baseDurationEffect = 1;
				int scaleDurationEffect = 1;
				
				double scaleBaseBonusDamage = 0;
				double scaleBasePercentDamage = 0;
				double scaleCastBonusDamage = 0;
				double scaleCastPercentDamage = 0;
				
				for (String keySection : mainDataSection.getKeys(false)) {
					if (keySection.equalsIgnoreCase("Max_Grade")) {
						maxGrade = mainDataSection.getInt(keySection);
					} else if (keySection.equalsIgnoreCase("Base_Duration_Effect")) {
						baseDurationEffect = mainDataSection.getInt(keySection);
					} else if (keySection.equalsIgnoreCase("Scale_Duration_Effect")) {
						scaleDurationEffect = mainDataSection.getInt(keySection);
					} else if (keySection.equalsIgnoreCase("Scale_Base_Bonus_Damage")) {
						scaleBaseBonusDamage = mainDataSection.getDouble(keySection);
					} else if (keySection.equalsIgnoreCase("Scale_Base_Percent_Damage")) {
						scaleBasePercentDamage = mainDataSection.getDouble(keySection);
					} else if (keySection.equalsIgnoreCase("Scale_Cast_Bonus_Damage")) {
						scaleCastBonusDamage = mainDataSection.getDouble(keySection);
					} else if (keySection.equalsIgnoreCase("Scale_Cast_Percent_Damage")) {
						scaleCastPercentDamage = mainDataSection.getDouble(keySection);
					}
				}
				
				maxGrade = Math.max(1, maxGrade);
				baseDurationEffect = Math.max(0, baseDurationEffect);
				scaleDurationEffect = Math.max(0, scaleDurationEffect);
				
				final AbilityWeaponProperties abilityWeaponProperties = new AbilityWeaponProperties(maxGrade, baseDurationEffect, scaleDurationEffect, scaleBaseBonusDamage, scaleBasePercentDamage, scaleCastBonusDamage, scaleCastPercentDamage);
			
				this.mapAbilityWeaponProperties.put(key, abilityWeaponProperties);
			}
		}
	}
	
	private final void moveOldFile() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathSource = "Configuration/ability.yml";
		final String pathTarget = dataManager.getPath("Path_File_Ability_Weapon");
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}
