package net.ardeus.myitems.ability;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.game.AbilityWeaponManager;
import net.ardeus.myitems.manager.game.GameManager;

public final class AbilityItemWeapon extends AbilityItem {

	public AbilityItemWeapon(String ability, int grade, double chance) {
		super(ability, grade, chance);
	}
	
	public final AbilityWeapon getAbilityWeapon() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponManager abilityWeaponManager = gameManager.getAbilityWeaponManager();
		
		return abilityWeaponManager.getAbilityWeapon(getAbility());
	}
}