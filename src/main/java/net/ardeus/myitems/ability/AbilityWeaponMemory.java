package net.ardeus.myitems.ability;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.enums.main.VersionNMS;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.weapon.AbilityWeaponAirShock;
import net.ardeus.myitems.ability.weapon.AbilityWeaponBadLuck;
import net.ardeus.myitems.ability.weapon.AbilityWeaponBlind;
import net.ardeus.myitems.ability.weapon.AbilityWeaponBubbleDeflector;
import net.ardeus.myitems.ability.weapon.AbilityWeaponCannibalism;
import net.ardeus.myitems.ability.weapon.AbilityWeaponConfuse;
import net.ardeus.myitems.ability.weapon.AbilityWeaponCurse;
import net.ardeus.myitems.ability.weapon.AbilityWeaponDarkFlame;
import net.ardeus.myitems.ability.weapon.AbilityWeaponDarkImpact;
import net.ardeus.myitems.ability.weapon.AbilityWeaponFlame;
import net.ardeus.myitems.ability.weapon.AbilityWeaponFlameWheel;
import net.ardeus.myitems.ability.weapon.AbilityWeaponFreeze;
import net.ardeus.myitems.ability.weapon.AbilityWeaponHarm;
import net.ardeus.myitems.ability.weapon.AbilityWeaponHungry;
import net.ardeus.myitems.ability.weapon.AbilityWeaponLevitation;
import net.ardeus.myitems.ability.weapon.AbilityWeaponLightning;
import net.ardeus.myitems.ability.weapon.AbilityWeaponPoison;
import net.ardeus.myitems.ability.weapon.AbilityWeaponRoots;
import net.ardeus.myitems.ability.weapon.AbilityWeaponSlow;
import net.ardeus.myitems.ability.weapon.AbilityWeaponTired;
import net.ardeus.myitems.ability.weapon.AbilityWeaponVampirism;
import net.ardeus.myitems.ability.weapon.AbilityWeaponVenomBlast;
import net.ardeus.myitems.ability.weapon.AbilityWeaponVenomSpread;
import net.ardeus.myitems.ability.weapon.AbilityWeaponWeak;
import net.ardeus.myitems.ability.weapon.AbilityWeaponWither;
import net.ardeus.myitems.manager.game.AbilityWeaponManager;

import com.praya.agarthalib.utility.ServerUtil;
import com.praya.agarthalib.utility.TextUtil;

public final class AbilityWeaponMemory extends AbilityWeaponManager {

	protected final Map<String, AbilityWeapon> mapAbilityWeapon = new HashMap<String, AbilityWeapon>();
	
	private AbilityWeaponMemory(MyItems plugin) {
		super(plugin);
	
		loadAbility();
	}
	
	private static class AbilityWeaponMemorySingleton {
		private static final AbilityWeaponMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new AbilityWeaponMemory(plugin);
		}
	}
	
	public static final AbilityWeaponMemory getInstance() {
		return AbilityWeaponMemorySingleton.INSTANCE;
	}
	
	@Override
	public final Collection<String> getAbilityIds() {
		return new ArrayList<String>(this.mapAbilityWeapon.keySet());
	}
	
	@Override
	public final Collection<AbilityWeapon> getAllAbilityWeapon() {
		return new ArrayList<AbilityWeapon>(this.mapAbilityWeapon.values());
	}
	
	@Override
	public final AbilityWeapon getAbilityWeapon(String ability) {
		if (ability != null) {
			final Collection<String> abilityIds = this.mapAbilityWeapon.keySet();
			
			for (String key : abilityIds) {
				if (key.equalsIgnoreCase(ability)) {
					return this.mapAbilityWeapon.get(key);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final AbilityWeapon getAbilityWeaponByKeyLore(String keyLore) {
		if (keyLore != null) {
			final String coloredKeyLore = TextUtil.colorful(keyLore);
			final Collection<AbilityWeapon> allAbilityWeapon = this.mapAbilityWeapon.values();
			
			for (AbilityWeapon key : allAbilityWeapon) {
				if (TextUtil.colorful(key.getKeyLore()).equalsIgnoreCase(coloredKeyLore)) {
					return key;
				}
			}
		}
		
		return null;
	}
	
	protected final boolean register(AbilityWeapon abilityWeapon) {
		if (abilityWeapon != null) {
			final String id = abilityWeapon.getId();
			
			if (!isExists(id)) {
				
				this.mapAbilityWeapon.put(id, abilityWeapon);
				
				return true;
			}
		}
		
		return false;
	}
	
	protected final boolean unregister(AbilityWeapon abilityWeapon) {
		if (abilityWeapon != null && this.mapAbilityWeapon.containsValue(abilityWeapon)) {
			final String id = abilityWeapon.getId();
			
			this.mapAbilityWeapon.remove(id);
			
			return true;
		} else {
			return false;
		}
	}
	
	private final void loadAbility() {
		final AbilityWeapon abilityWeaponAirShock = AbilityWeaponAirShock.getInstance();
		final AbilityWeapon abilityWeaponBlind = AbilityWeaponBlind.getInstance();
		final AbilityWeapon abilityWeaponBubbleDeflector = AbilityWeaponBubbleDeflector.getInstance();
		final AbilityWeapon abilityWeaponCannibalism = AbilityWeaponCannibalism.getInstance();
		final AbilityWeapon abilityWeaponConfuse = AbilityWeaponConfuse.getInstance();
		final AbilityWeapon abilityWeaponCurse = AbilityWeaponCurse.getInstance();
		final AbilityWeapon abilityWeaponDarkFlame = AbilityWeaponDarkFlame.getInstance();
		final AbilityWeapon abilityWeaponDarkImpact = AbilityWeaponDarkImpact.getInstance();
		final AbilityWeapon abilityWeaponFlame = AbilityWeaponFlame.getInstance();
		final AbilityWeapon abilityWeaponFlameWheel = AbilityWeaponFlameWheel.getInstance();
		final AbilityWeapon abilityWeaponFreeze = AbilityWeaponFreeze.getInstance();
		final AbilityWeapon abilityWeaponHarm = AbilityWeaponHarm.getInstance();
		final AbilityWeapon abilityWeaponHungry = AbilityWeaponHungry.getInstance();
		final AbilityWeapon abilityWeaponLightning = AbilityWeaponLightning.getInstance();
		final AbilityWeapon abilityWeaponPoison = AbilityWeaponPoison.getInstance();
		final AbilityWeapon abilityWeaponRoots = AbilityWeaponRoots.getInstance();
		final AbilityWeapon abilityWeaponSlowness = AbilityWeaponSlow.getInstance();
		final AbilityWeapon abilityWeaponTired = AbilityWeaponTired.getInstance();
		final AbilityWeapon abilityWeaponVampirism = AbilityWeaponVampirism.getInstance();
		final AbilityWeapon abilityWeaponVenomBlast = AbilityWeaponVenomBlast.getInstance();
		final AbilityWeapon abilityWeaponVenomSpread = AbilityWeaponVenomSpread.getInstance();
		final AbilityWeapon abilityWeaponWeakness = AbilityWeaponWeak.getInstance();
		final AbilityWeapon abilityWeaponWither = AbilityWeaponWither.getInstance();
		
		register(abilityWeaponAirShock);
		register(abilityWeaponBlind);
		register(abilityWeaponBubbleDeflector);
		register(abilityWeaponCannibalism);
		register(abilityWeaponConfuse);
		register(abilityWeaponCurse);
		register(abilityWeaponDarkFlame);
		register(abilityWeaponDarkImpact);
		register(abilityWeaponFlame);
		register(abilityWeaponFlameWheel);
		register(abilityWeaponFreeze);
		register(abilityWeaponHarm);
		register(abilityWeaponHungry);
		register(abilityWeaponLightning);
		register(abilityWeaponPoison);
		register(abilityWeaponRoots);
		register(abilityWeaponSlowness);
		register(abilityWeaponTired);
		register(abilityWeaponVampirism);
		register(abilityWeaponVenomBlast);
		register(abilityWeaponVenomSpread);
		register(abilityWeaponWeakness);
		register(abilityWeaponWither);
		
		if (ServerUtil.isCompatible(VersionNMS.V1_9_R1)) {
			final AbilityWeapon abilityWeaponBadLuck = AbilityWeaponBadLuck.getInstance();
			final AbilityWeapon abilityWeaponLevitation = AbilityWeaponLevitation.getInstance();
			
			register(abilityWeaponBadLuck);
			register(abilityWeaponLevitation);
		}
	}
}
