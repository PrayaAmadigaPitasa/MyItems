package net.ardeus.myitems.ability;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;

public final class AbilityWeaponPropertiesMemory extends AbilityWeaponPropertiesManager {

	private final AbilityWeaponConfig abilityWeaponConfig;
	
	private AbilityWeaponPropertiesMemory(MyItems plugin) {
		super(plugin);
		
		this.abilityWeaponConfig = new AbilityWeaponConfig(plugin);
	}
	
	private static class AbilityWeaponPropertiesMemorySingleton {
		private static final AbilityWeaponPropertiesMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new AbilityWeaponPropertiesMemory(plugin);
		}
	}
	
	public static final AbilityWeaponPropertiesMemory getInstance() {
		return AbilityWeaponPropertiesMemorySingleton.INSTANCE;
	}
	
	protected final AbilityWeaponConfig getAbilityWeaponConfig() {
		return this.abilityWeaponConfig;
	}
	
	@Override
	public final Collection<String> getAbilityWeaponPropertiesIds() {
		final Map<String, AbilityWeaponProperties> mapAbilityWeaponProperties = getAbilityWeaponConfig().mapAbilityWeaponProperties;
		
		return new ArrayList<String>(mapAbilityWeaponProperties.keySet());
	}
	
	@Override
	public final Collection<AbilityWeaponProperties> getAllAbilityWeaponProperties() {
		final Map<String, AbilityWeaponProperties> mapAbilityWeaponProperties = getAbilityWeaponConfig().mapAbilityWeaponProperties;
		
		return new ArrayList<AbilityWeaponProperties>(mapAbilityWeaponProperties.values());
	}
	
	@Override
	public final AbilityWeaponProperties getAbilityWeaponProperties(String ability) {
		if (ability != null) {
			final Map<String, AbilityWeaponProperties> mapAbilityWeaponProperties = getAbilityWeaponConfig().mapAbilityWeaponProperties;
			
			for (String key : mapAbilityWeaponProperties.keySet()) {
				if (key.equalsIgnoreCase(ability)) {
					return mapAbilityWeaponProperties.get(key);
				}
			}
		}
		
		return null;
	}
}
