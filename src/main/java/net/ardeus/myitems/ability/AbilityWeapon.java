package net.ardeus.myitems.ability;

import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;

public abstract class AbilityWeapon extends Ability {

	public AbilityWeapon(Plugin plugin, String id) {
		super(plugin, id);
	}
	
	public double getBaseBonusDamage(int grade) {
		return 0;
	}
	
	public double getBasePercentDamage(int grade) {
		return 0;
	}
	
	public double getCastBonusDamage(int grade) {
		return 0;
	}
	
	public double getCastPercentDamage(int grade) {
		return 0;
	}
	
	public void onDamage(Entity entity, Entity victims, double damage) {
		
	}
	
	public final boolean register() {
		final AbilityWeaponMemory abilityWeaponMemory = AbilityWeaponMemory.getInstance();
		
		return abilityWeaponMemory.register(this);
	}
}