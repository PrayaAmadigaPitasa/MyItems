package net.ardeus.myitems.ability.weapon;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;

import com.praya.agarthalib.utility.BlockUtil;
import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.MetadataUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.PotionUtil;

public final class AbilityWeaponVenomBlast extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Venom_Blast";
	
	private AbilityWeaponVenomBlast(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityVenomBlastSingleton {
		private static final AbilityWeaponVenomBlast instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponVenomBlast(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponVenomBlast getInstance() {
		return AbilityVenomBlastSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierVenomBlast();
	}

	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}
	
	@Override
	public final double getCastBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double castBonusDamage = grade * abilityWeaponProperties.getScaleCastBonusDamage();
		
		return castBonusDamage;
	}
	
	@Override
	public final double getCastPercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double castPercentDamage = grade * abilityWeaponProperties.getScaleCastPercentDamage();
		
		return castPercentDamage;
	}
	
	@Override
	public final int getEffectDuration(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getTotalDuration(grade);
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		final LivingEntity attacker;
		
		if (caster instanceof Projectile) {
			final Projectile projectile = (Projectile) caster;
			final ProjectileSource projectileSource = projectile.getShooter();
			
			if (projectileSource != null && projectileSource instanceof LivingEntity) {
				attacker = (LivingEntity) projectileSource;
			} else {
				return;
			}
		} else {
			attacker = (LivingEntity) caster;
		}
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = victims.getLocation().add(0, 0.5, 0);
			final PotionEffectType potionType = PotionUtil.getPoisonType(victims);
			final double spreadDamage = getCastBonusDamage(grade) + (damage * (getCastPercentDamage(grade) / 100));;
			final double blastDamage = spreadDamage * 2;
			final double decrease = 1;
			final int limit = 4;
			final int duration = getEffectDuration(grade);
			final int amplifier = potionType.equals(PotionEffectType.WITHER) ? 2 : 1;
			final PotionEffect potion = PotionUtil.createPotion(potionType, duration, amplifier);
			final Set<LivingEntity> units = new HashSet<LivingEntity>();
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
			
			victims.addPotionEffect(potion);
			
			new BukkitRunnable() {
				
				int time = 0;
				
				double radius = 3;
				double x;
				double z;
				
				@Override
				public void run() {
					if (time > limit) {
						this.cancel();
						return;
					} else {
						if (time == limit) {
							
							Bridge.getBridgeParticle().playParticle(players, ParticleEnum.EXPLOSION_HUGE, location, 10, 0, 0.2, 0.0, 0.05F);
							Bridge.getBridgeSound().playSound(players, location, SoundEnum.ENTITY_GENERIC_EXPLODE, 1, 1);
							
							for (LivingEntity unit : CombatUtil.getNearbyUnits(location, 3)) {
								if (!unit.equals(attacker) && !unit.equals(victims) && !units.contains(unit)) {
									CombatUtil.areaDamage(attacker, unit, blastDamage);
									units.add(unit);
								}
							}
						} else {
							final Material materialDandelion = MaterialEnum.DANDELION.getMaterial();
							
							Bridge.getBridgeSound().playSound(players, location, SoundEnum.BLOCK_GRAVEL_BREAK, 1, 1);
							
							for (LivingEntity unit : CombatUtil.getNearbyUnits(location, radius+1)) {
								if (!unit.equals(attacker) && !unit.equals(victims) && !units.contains(unit)) {
									final PotionEffectType potionType = PotionUtil.getPoisonType(unit);
									final int amplifier = potionType.equals(PotionEffectType.WITHER) ? 2 : 1;
									final PotionEffect potion = PotionUtil.createPotion(potionType, duration, amplifier);
									
									unit.addPotionEffect(potion);
									CombatUtil.areaDamage(attacker, unit, spreadDamage);
									units.add(unit);
								}
							}
							
							for (int i = 0; i < 360; i += 30) {
								
								x = Math.sin(i) * radius;
								z = Math.cos(i) * radius;
								
								location.add(x, 0, z);
								Bridge.getBridgeParticle().playParticle(players, ParticleEnum.VILLAGER_HAPPY, location, 3, 0.05, 0.05, 0.05, 0.05F);
								
								final Block block = location.getBlock();
								final Material material = block.getType();
								
								if (material.equals(Material.AIR)) {
									final Location locationBlock = block.getLocation();
									
									BlockUtil.set(locationBlock);
									block.setType(materialDandelion);
									block.setMetadata("Anti_Block_Physic", MetadataUtil.createMetadata(true));
									
									new BukkitRunnable() {
										final Location locationFlower = location.clone();
										
										@Override
										public void run() {
											final Block blockFlower = locationFlower.getBlock();
											final Material materialFlower = blockFlower.getType();
											final Location locationBlockFlower = blockFlower.getLocation();
											
											BlockUtil.remove(locationBlockFlower);
											
											if (materialFlower.equals(materialDandelion)) {
												blockFlower.setType(Material.AIR);
											}
											
										}
									}.runTaskLater(plugin, 5);
								}
								
								location.subtract(x, 0, z);
							}
							
							radius = radius - decrease;
						}
						
						time++;
					}
				}
			}.runTaskTimer(plugin, 0, 5);
		}
	}
}