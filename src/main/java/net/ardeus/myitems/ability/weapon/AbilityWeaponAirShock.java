package net.ardeus.myitems.ability.weapon;

import java.util.Collection;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;

import com.praya.agarthalib.utility.PlayerUtil;

public final class AbilityWeaponAirShock extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Air_Shock";
	
	private AbilityWeaponAirShock(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityAirShockSingleton {
		private static final AbilityWeaponAirShock instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponAirShock(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponAirShock getInstance() {
		return AbilityAirShockSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierAirShock();
	}

	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		final LivingEntity attacker;
		
		if (caster instanceof Projectile) {
			final Projectile projectile = (Projectile) caster;
			final ProjectileSource projectileSource = projectile.getShooter();
			
			if (projectileSource != null && projectileSource instanceof LivingEntity) {
				attacker = (LivingEntity) projectileSource;
			} else {
				return;
			}
		} else {
			attacker = (LivingEntity) caster;
		}
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = victims.getLocation().add(0, 0.2, 0);
			final int maxGrade = getMaxGrade();
			final int duration = 2 + grade;
			final double speed = 1 + (grade * 2 / maxGrade);
			final Vector vector = attacker.getLocation().getDirection().setY(0).normalize();
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
			
			vector.setY(2).normalize().multiply(speed);
			victims.teleport(location);
			victims.setVelocity(vector);
			Bridge.getBridgeSound().playSound(players, location, SoundEnum.ENTITY_WITHER_SHOOT, 1, 1);			
			
			new BukkitRunnable() {
				
				int time = 0;
				
				@Override
				public void run() {
					if (time >= duration) {
						this.cancel();
						return;
					} else if (victims.isDead())  {
						this.cancel();
						return;
					} else {
						final Location location = victims.getLocation();
						
						Bridge.getBridgeParticle().playParticle(players, ParticleEnum.CLOUD, location, 10, 0.5, 0.25, 0.5, 0.15F);
						time++;
					}
				}
			}.runTaskTimer(plugin, 1, 1);
		}
	}
}