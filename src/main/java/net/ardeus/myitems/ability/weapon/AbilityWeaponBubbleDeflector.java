package net.ardeus.myitems.ability.weapon;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.PotionUtil;
import com.praya.agarthalib.utility.ProjectileUtil;

public final class AbilityWeaponBubbleDeflector extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Bubble_Deflector";
	
	private AbilityWeaponBubbleDeflector(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityBubbleDeflectorSingleton {
		private static final AbilityWeaponBubbleDeflector instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponBubbleDeflector(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponBubbleDeflector getInstance() {
		return AbilityBubbleDeflectorSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierBubbleDeflector();
	}

	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}
	
	@Override
	public final double getCastBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double castBonusDamage = grade * abilityWeaponProperties.getScaleCastBonusDamage();
		
		return castBonusDamage;
	}
	
	@Override
	public final double getCastPercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double castPercentDamage = grade * abilityWeaponProperties.getScaleCastPercentDamage();
		
		return castPercentDamage;
	}
	
	@Override
	public final int getEffectDuration(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getTotalDuration(grade);
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		final LivingEntity attacker;
		
		if (caster instanceof Projectile) {
			final Projectile projectile = (Projectile) caster;
			final ProjectileSource projectileSource = projectile.getShooter();
			
			if (projectileSource != null && projectileSource instanceof LivingEntity) {
				attacker = (LivingEntity) projectileSource;
			} else {
				return;
			}
		} else {
			attacker = (LivingEntity) caster;
		}
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = ProjectileUtil.isProjectile(caster) ? ProjectileUtil.getDirectLocation(caster) : attacker.getEyeLocation();
			final Vector vector = location.getDirection();
			final int limit = 10 + grade;
			final int duration = getEffectDuration(grade);
			final double range = 1.5;
			final double bubbleDamage = getCastBonusDamage(grade) + (damage * (getCastPercentDamage(grade) / 100));;
			final PotionEffect potionEffect = PotionUtil.createPotion(PotionEffectType.SLOW, duration, 4);
			final Set<LivingEntity> units = new HashSet<LivingEntity>();
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
			
			victims.addPotionEffect(potionEffect);
			Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_SPLASH, location, 40, 0.25, 0.25, 0.25, 0F);
			Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_WAKE, location, 40, 0.25, 0.25, 0.25, 0F);
			Bridge.getBridgeSound().playSound(players, location, SoundEnum.ENTITY_GUARDIAN_FLOP, 5, 1);
			
			new BukkitRunnable() {
				
				int time = 0;
				
				@Override
				public void run() {
					if (time >= limit) {
						this.cancel();
						return;
					} else {
						for (LivingEntity unit : CombatUtil.getNearbyUnits(location, range)) {
							if (!unit.equals(attacker) && !unit.equals(victims) && !units.contains(unit)) {
								CombatUtil.areaDamage(attacker, unit, bubbleDamage);
								unit.addPotionEffect(potionEffect);
								units.add(unit);
							}
						}
						
						if (time % 2 == 0) {
							Bridge.getBridgeSound().playSound(players, location, SoundEnum.BLOCK_WATER_AMBIENT, 2, 1);
						}
						
						Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_DROP, location, 25, 0.2, 0.2, 0.2, 0F);
						Bridge.getBridgeParticle().playParticle(players, ParticleEnum.WATER_BUBBLE, location, 25, 0.2, 0.2, 0.2, 0F);
						
						location.add(vector);
						time++;
					}
				}
			}.runTaskTimer(plugin, 0, 1);
		}
	}
}