package net.ardeus.myitems.ability.weapon;

import java.util.Collection;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.passive.PassiveEffectTypeEnum;
import net.ardeus.myitems.utility.main.CustomEffectUtil;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class AbilityWeaponDarkFlame extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Dark_Flame";
	
	private AbilityWeaponDarkFlame(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityDarkFlameSingleton {
		private static final AbilityWeaponDarkFlame instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponDarkFlame(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponDarkFlame getInstance() {
		return AbilityDarkFlameSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierDarkFlame();
	}

	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}
	
	@Override
	public final double getCastBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double castBonusDamage = grade * abilityWeaponProperties.getScaleCastBonusDamage();
		
		return castBonusDamage;
	}
	
	@Override
	public final double getCastPercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double castPercentDamage = grade * abilityWeaponProperties.getScaleCastPercentDamage();
		
		return castPercentDamage;
	}
	
	@Override
	public final int getEffectDuration(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getTotalDuration(grade);
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		final LivingEntity attacker;
		
		if (caster instanceof Projectile) {
			final Projectile projectile = (Projectile) caster;
			final ProjectileSource projectileSource = projectile.getShooter();
			
			if (projectileSource != null && projectileSource instanceof LivingEntity) {
				attacker = (LivingEntity) projectileSource;
			} else {
				return;
			}
		} else {
			attacker = (LivingEntity) caster;
		}
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			
			if (!CustomEffectUtil.isRunCustomEffect(victims, PassiveEffectTypeEnum.DARK_FLAME)) {
				final Location location = victims.getLocation();
				final int effectDuration = getEffectDuration(grade);
				final int period = 2;
				final int limit = effectDuration / period;
				final long milis = effectDuration * 50;
				final double flameDamage = getCastBonusDamage(grade) + (damage * (getCastPercentDamage(grade) / 100));;
				final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
				
				Bridge.getBridgeSound().playSound(players, location, SoundEnum.ITEM_FIRECHARGE_USE, 5, 1);
				CustomEffectUtil.setCustomEffect(victims, milis, PassiveEffectTypeEnum.DARK_FLAME);
				
				new BukkitRunnable() {
					
					int time = 0;			
					
					@Override
					public void run() {					
						if (time >= limit) {
							this.cancel();
							return;
						} else if (victims.isDead())  {
							this.cancel();
							return;
						} else {
							final Location location = victims.getLocation().add(0, 0.5, 0);
							
							if (time % 5 == 0) {
								CombatUtil.areaDamage(attacker, victims, flameDamage);
							}
							
							Bridge.getBridgeParticle().playParticle(players, ParticleEnum.SPELL_MOB, location, 20, 0.25, 0.5, 0.25, 0F);
							Bridge.getBridgeSound().playSound(players, location, SoundEnum.BLOCK_FIRE_AMBIENT, 5, 1);
							
							time++;
						}
					}
					
				}.runTaskTimer(plugin, 2, 2);
			}
		}
	}
}