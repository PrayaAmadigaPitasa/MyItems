package net.ardeus.myitems.ability.weapon;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.projectiles.ProjectileSource;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EntityUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class AbilityWeaponCannibalism extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Cannibalism";
	
	private AbilityWeaponCannibalism(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityCannibalismSingleton {
		private static final AbilityWeaponCannibalism instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponCannibalism(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponCannibalism getInstance() {
		return AbilityCannibalismSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierCannibalism();
	}

	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final LivingEntity attacker;
		
		if (caster instanceof Projectile) {
			final Projectile projectile = (Projectile) caster;
			final ProjectileSource projectileSource = projectile.getShooter();
			
			if (projectileSource != null && projectileSource instanceof LivingEntity) {
				attacker = (LivingEntity) projectileSource;
			} else {
				return;
			}
		} else {
			attacker = (LivingEntity) caster;
		}
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = attacker.getEyeLocation();
			final MessageBuild messageAttacker = lang.getMessage(attacker, "Ability_Cannibalism_Attacker");
			final MessageBuild messageVictims = lang.getMessage(victims, "Ability_Cannibalism_Victims");
			final int maxGrade = getMaxGrade();
			
			int food = 10 * (grade/maxGrade);
			
			if (victims instanceof Player) {
				final Player playerVictims = PlayerUtil.parse(victims);
				final int foodVictims = playerVictims.getFoodLevel();
				
				food = Math.max(foodVictims, food);
				
				playerVictims.setFoodLevel(foodVictims - food);
				messageVictims.sendMessage(victims, "food", String.valueOf(food));
				Bridge.getBridgeSound().playSound(playerVictims, location, SoundEnum.ENTITY_PLAYER_BURP, 0.7F, 1);
			}
			
			if (EntityUtil.isPlayer(attacker)) {
				final Player playerAttacker = PlayerUtil.parse(attacker);
				final int foodAttacker = playerAttacker.getFoodLevel();
				
				food = foodAttacker + food > 20 ? 20 - foodAttacker : food;
				
				playerAttacker.setFoodLevel(foodAttacker + food);
				messageAttacker.sendMessage(playerAttacker, "food", String.valueOf(food));
				Bridge.getBridgeSound().playSound(playerAttacker, location, SoundEnum.ENTITY_PLAYER_BURP, 0.7F, 1);
			}
		}
	}
}