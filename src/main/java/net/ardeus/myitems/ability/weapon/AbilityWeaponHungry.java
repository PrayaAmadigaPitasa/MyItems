package net.ardeus.myitems.ability.weapon;

import java.util.Collection;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class AbilityWeaponHungry extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Hungry";
	
	private AbilityWeaponHungry(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityHungrySingleton {
		private static final AbilityWeaponHungry instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponHungry(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponHungry getInstance() {
		return AbilityHungrySingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierHungry();
	}
	
	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}
	
	@Override
	public final int getEffectDuration(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final int scaleDuration = abilityWeaponProperties.getScaleDurationEffect();
		
		int duration = scaleDuration * (1+grade);
		int amplifier = 0;
		
		while (duration > scaleDuration * (7 + amplifier)) {
			duration = duration - (scaleDuration*2);
			amplifier = amplifier + 1;
		}
		
		return duration;
	}
	
	private final int getEffectAmplifier(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final int scaleDuration = abilityWeaponProperties.getScaleDurationEffect();
		
		int duration = scaleDuration * (1+grade);
		int amplifier = 0;
		
		while (duration > scaleDuration * (7 + amplifier)) {
			duration = duration - (scaleDuration*2);
			amplifier = amplifier + 1;
		}
		
		return amplifier;
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = victims.getEyeLocation();
			final int duration = getEffectDuration(grade);
			final int amplifier = getEffectAmplifier(grade);
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
			
			Bridge.getBridgeParticle().playParticle(players, ParticleEnum.SPELL_INSTANT, location, 25, 0.5, 0.5, 0.5, 0.5F);
			Bridge.getBridgeSound().playSound(players, location, SoundEnum.ENTITY_PLAYER_BURP, 0.7F, 1);
			CombatUtil.applyPotion(victims, PotionEffectType.HUNGER, duration, amplifier);
		}
	}
}