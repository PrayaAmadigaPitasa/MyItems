package net.ardeus.myitems.ability.weapon;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.passive.PassiveEffectTypeEnum;
import net.ardeus.myitems.utility.main.CustomEffectUtil;

import com.praya.agarthalib.utility.BlockUtil;
import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class AbilityWeaponFreeze extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Freeze";
	
	private AbilityWeaponFreeze(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityFreezeSingleton {
		private static final AbilityWeaponFreeze instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponFreeze(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponFreeze getInstance() {
		return AbilityFreezeSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierFreeze();
	}

	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}
	
	@Override
	public int getEffectDuration(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getTotalDuration(grade);
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final LivingEntity attacker;
		
		if (caster instanceof Projectile) {
			final Projectile projectile = (Projectile) caster;
			final ProjectileSource projectileSource = projectile.getShooter();
			
			if (projectileSource != null && projectileSource instanceof LivingEntity) {
				attacker = (LivingEntity) projectileSource;
			} else {
				return;
			}
		} else {
			attacker = (LivingEntity) caster;
		}
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = victims.getLocation();
			final int duration = getEffectDuration(grade);
			final long milis = duration * 50;
			final double seconds = (duration) / 20;
			final MessageBuild messageAttacker = lang.getMessage(attacker, "Ability_Freeze_Attacker");
			final MessageBuild messageVictims = lang.getMessage(victims, "Ability_Freeze_Victims");
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			mapPlaceholder.put("seconds", String.valueOf(MathUtil.roundNumber(seconds)));
			
			victims.setVelocity(victims.getVelocity().multiply(0D));
			messageAttacker.sendMessage(attacker, mapPlaceholder);
			messageVictims.sendMessage(victims, mapPlaceholder);
			Bridge.getBridgeParticle().playParticle(players, ParticleEnum.CLOUD, location, 10, 0.25, 0.25, 0.25, 0.1F);
			Bridge.getBridgeSound().playSound(players, location, SoundEnum.BLOCK_GLASS_BREAK, 1, 1);
					
			if (victims instanceof Player) {
				CustomEffectUtil.setCustomEffect(victims, milis, PassiveEffectTypeEnum.FREEZE);
			} else {
				CombatUtil.applyPotion(victims, PotionEffectType.SLOW, duration, 100, true, mainConfig.isMiscEnableParticlePotion());
			}
			
			new BukkitRunnable() {
				
				@Override
				public void run() {
					final Collection<Location> locationIce = new ArrayList<Location>();
					final Location location = victims.getLocation();
					
					for (int i = 0; i < 2; i++) {
						if (i > 0) {
							location.add(0, 1, 0);
						}
						
						final Block block = location.getBlock();
						final Material material = block.getType();
						
						if (material.equals(Material.AIR)) {
							final Location locationBlock = block.getLocation();
							
							locationIce.add(locationBlock);
							block.setType(Material.PACKED_ICE);
							BlockUtil.set(locationBlock);
						}
					}
					
					new BukkitRunnable() {
						
						@Override
						public void run() {
				    		for (Location location : locationIce) {
				    			final Block block = location.getBlock();
				    			final Material material = block.getType();
				    			final Location locationBlock = block.getLocation();
				    			
				    			BlockUtil.remove(locationBlock);
				    			
				    			if (material.equals(Material.PACKED_ICE)) {
				    				block.setType(Material.AIR);
				    			}
				    		}
						}
					}.runTaskLater(plugin, duration);
				}
			}.runTaskLater(plugin, 1);
		}
	}
}