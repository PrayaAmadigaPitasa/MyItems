package net.ardeus.myitems.ability.weapon;

import java.util.Collection;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;

import com.praya.agarthalib.utility.PlayerUtil;

public final class AbilityWeaponFlameWheel extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Flame_Wheel";
	
	private AbilityWeaponFlameWheel(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityFlameWheelSingleton {
		private static final AbilityWeaponFlameWheel instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponFlameWheel(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponFlameWheel getInstance() {
		return AbilityFlameWheelSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierFlameWheel();
	}

	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}
	
	@Override
	public final int getEffectDuration(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getTotalDuration(grade);
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = victims.getLocation();
			final int duration = getEffectDuration(grade);
			final int limit = 12;
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
			
			victims.setFireTicks(duration);
			Bridge.getBridgeParticle().playParticle(players, ParticleEnum.LAVA, location, 5, 0.2, 0.05, 0.2, 0.1F);
			Bridge.getBridgeSound().playSound(players, location, SoundEnum.ITEM_FIRECHARGE_USE, 1, 1);
			
			new BukkitRunnable() {
				
				double horizontal = 0;
				double vertical = 0.25;
				int time = 0;
				
				double x;
				double y;
				double z;
				
				@Override
				public void run() {				
					if (time >= limit) {
						this.cancel();
						return;
					} else {
						for (int i = 0;  i < 3; i ++) {
							
							x = 0.8 * Math.sin(horizontal);
							y = vertical;
							z = 0.8 * Math.cos(horizontal);
							
							location.add(x, y, z);
							Bridge.getBridgeParticle().playParticle(players, ParticleEnum.FLAME, location, 1, 0, 0, 0, 0);
							location.subtract(x, y, z);
							
							location.add(-x, y, -z);
							Bridge.getBridgeParticle().playParticle(players, ParticleEnum.FLAME, location, 1, 0, 0, 0, 0);
							location.subtract(-x, y, -z);
							
							horizontal = horizontal + Math.PI/24;
							vertical = vertical + 0.05;
						}
						
						time++;
					}
				}
			}.runTaskTimer(plugin, 0, 1);	
		}
	}
}