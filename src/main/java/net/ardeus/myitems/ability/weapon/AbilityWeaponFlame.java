package net.ardeus.myitems.ability.weapon;

import java.util.Collection;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;

import com.praya.agarthalib.utility.PlayerUtil;

public final class AbilityWeaponFlame extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Flame";
	
	private AbilityWeaponFlame(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityFlameSingleton {
		private static final AbilityWeaponFlame instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponFlame(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponFlame getInstance() {
		return AbilityFlameSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierFlame();
	}
	
	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}
	
	@Override
	public final int getEffectDuration(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getTotalDuration(grade);
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = victims.getEyeLocation();
			final int duration = getEffectDuration(grade);
			final int durationVictims = victims.getFireTicks();
			final int durationTotal = duration + durationVictims;
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
			
			Bridge.getBridgeParticle().playParticle(players, ParticleEnum.FLAME, location, 20, 0.3, 0.5, 0.3, 0);
			Bridge.getBridgeSound().playSound(players, location, SoundEnum.ENTITY_BLAZE_SHOOT, 0.7F, 1);
			victims.setFireTicks(durationTotal);
		}
	}
}