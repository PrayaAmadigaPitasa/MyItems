package net.ardeus.myitems.ability.weapon;

import java.util.Collection;
import java.util.HashMap;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.projectiles.ProjectileSource;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EntityUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class AbilityWeaponVampirism extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Vampirism";
	
	private AbilityWeaponVampirism(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityVampirismSingleton {
		private static final AbilityWeaponVampirism instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponVampirism(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponVampirism getInstance() {
		return AbilityVampirismSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierVampirism();
	}

	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final LivingEntity attacker;
		
		if (caster instanceof Projectile) {
			final Projectile projectile = (Projectile) caster;
			final ProjectileSource projectileSource = projectile.getShooter();
			
			if (projectileSource != null && projectileSource instanceof LivingEntity) {
				attacker = (LivingEntity) projectileSource;
			} else {
				return;
			}
		} else {
			attacker = (LivingEntity) caster;
		}
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = attacker.getLocation();
			final double drain = getDrain(attacker, victims, grade, damage);
			final double attackerNewHealth = getAttackerNewHealth(attacker, victims, drain);
			final MessageBuild messageAttacker = lang.getMessage(attacker, "Ability_Vampirism_Attacker");
			final MessageBuild messageVictims = lang.getMessage(victims, "Ability_Vampirism_Victims");
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			mapPlaceholder.put("health", String.valueOf(MathUtil.roundNumber(drain)));
			
			Bridge.getBridgeParticle().playParticle(players, ParticleEnum.HEART, location, 10, 0.25, 0.25, 0.25, 0.1F);
			
			if (EntityUtil.isPlayer(attacker)) {
				final Player playerAttacker = PlayerUtil.parse(attacker);
				
				messageAttacker.sendMessage(playerAttacker, mapPlaceholder);
				Bridge.getBridgeSound().playSound(playerAttacker, location, SoundEnum.ENTITY_WITCH_DRINK, 1, 1);
			}			
			
			if (EntityUtil.isPlayer(victims)) {				
				final Player playerVictims = PlayerUtil.parse(victims);
								
				messageVictims.sendMessage(playerVictims, mapPlaceholder);
				Bridge.getBridgeSound().playSound(playerVictims, location, SoundEnum.ENTITY_WITCH_DRINK, 1, 1);
			}
			
			EntityUtil.setHealth(attacker, attackerNewHealth);
		}
	}
	
	@SuppressWarnings("deprecation")
	private final double getDrain(LivingEntity attacker, LivingEntity victims, int grade, double damage) {
		final double attackerHealth = attacker.getHealth();
		final double attackerMaxHealth = attacker.getMaxHealth();
		final int maxGrade = getMaxGrade();
		
		double drain = damage * grade / maxGrade;
		
		if (victims.getHealth() - drain < 0) {
			drain = victims.getHealth();
		}
		
		if (!(victims instanceof Player)) {
			drain = drain / 2;
		}
		
		if (attackerHealth + drain > attackerMaxHealth) {
			drain = attackerMaxHealth - attackerHealth;
		}
		
		return drain;
	}
	
	@SuppressWarnings("deprecation")
	private final double getAttackerNewHealth(LivingEntity attacker, LivingEntity victims, double drain) {
		final double attackerHealth = attacker.getHealth();
		final double attackerMaxHealth = attacker.getMaxHealth();
		
		if (attackerHealth + drain > attackerMaxHealth) {
			return attackerMaxHealth;
		} else {
			return attackerHealth + drain;
		}
	}
}