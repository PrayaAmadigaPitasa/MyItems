package net.ardeus.myitems.ability.weapon;

import java.util.Collection;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.AbilityWeaponPropertiesManager;
import net.ardeus.myitems.manager.game.GameManager;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.PotionUtil;

public final class AbilityWeaponVenomSpread extends AbilityWeapon {
	
	private static final String ABILITY_ID = "Venom_Spread";
	
	private AbilityWeaponVenomSpread(MyItems plugin, String id) {
		super(plugin, id);
	}
	
	private static class AbilityVenomSpreadSingleton {
		private static final AbilityWeaponVenomSpread instance;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			instance = new AbilityWeaponVenomSpread(plugin, ABILITY_ID);
		}
	}
	
	public static final AbilityWeaponVenomSpread getInstance() {
		return AbilityVenomSpreadSingleton.instance;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getAbilityWeaponIdentifierVenomSpread();
	}

	@Override
	public final int getMaxGrade() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getMaxGrade();
	}
	
	@Override
	public final double getBaseBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double baseBonusDamage = grade * abilityWeaponProperties.getScaleBaseBonusDamage();
		
		return baseBonusDamage;
	}
	
	@Override
	public final double getBasePercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double basePercentDamage = grade * abilityWeaponProperties.getScaleBasePercentDamage();
		
		return basePercentDamage;
	}
	
	@Override
	public final double getCastBonusDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double castBonusDamage = grade * abilityWeaponProperties.getScaleCastBonusDamage();
		
		return castBonusDamage;
	}
	
	@Override
	public final double getCastPercentDamage(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		final double castPercentDamage = grade * abilityWeaponProperties.getScaleCastPercentDamage();
		
		return castPercentDamage;
	}
	
	@Override
	public final int getEffectDuration(int grade) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager = gameManager.getAbilityWeaponPropertiesManager();
		final AbilityWeaponProperties abilityWeaponProperties = abilityWeaponPropertiesManager.getAbilityWeaponProperties(ABILITY_ID);
		
		return abilityWeaponProperties.getTotalDuration(grade);
	}

	@Override
	public final void onDamage(Entity caster, Entity target, int grade, double damage) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		final LivingEntity attacker;
		
		if (caster instanceof Projectile) {
			final Projectile projectile = (Projectile) caster;
			final ProjectileSource projectileSource = projectile.getShooter();
			
			if (projectileSource != null && projectileSource instanceof LivingEntity) {
				attacker = (LivingEntity) projectileSource;
			} else {
				return;
			}
		} else {
			attacker = (LivingEntity) caster;
		}
		
		if (target instanceof LivingEntity) {
			final LivingEntity victims = (LivingEntity) target;
			final Location location = victims.getLocation();
			final int duration = getEffectDuration(grade);
			final int limit = 5;
			final double dividen = 2;
			final double spreadDamage = getCastBonusDamage(grade) + (damage * (getCastPercentDamage(grade) / 100));;
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, mainConfig.getEffectRange());
			final PotionEffectType potionType = PotionUtil.getPoisonType(victims);
			final int amplifier = potionType.equals(PotionEffectType.WITHER) ? 3 : 2;
			final PotionEffect potion = PotionUtil.createPotion(potionType, duration, amplifier);
			
			victims.addPotionEffect(potion);
			Bridge.getBridgeParticle().playParticle(players, ParticleEnum.SLIME, location, 40, 0.25, 0.5, 0.25, 0F);
			Bridge.getBridgeSound().playSound(players, location, SoundEnum.BLOCK_SLIME_HIT, 5, 1);			
			
			new BukkitRunnable() {
				
				double range = 0.5;
				int time = 0;
				
				@Override
				public void run() {				
					if (time >= limit) {
						this.cancel();
						return;
					} else {
						
						Bridge.getBridgeSound().playSound(players, location, SoundEnum.BLOCK_GRAVEL_BREAK, 5, 1);
						
						for (double i = 0; i <= 2 * Math.PI; i = i + (Math.PI / (dividen * (1+time)))) {
							final double x = range * Math.sin(i);
							final double z = range * Math.cos(i);
							
							location.add(x, 0, z);
							Bridge.getBridgeParticle().playParticle(players, ParticleEnum.VILLAGER_HAPPY, location, 1, 0, 0, 0, 0);
							
							if (time < 3) {
								Bridge.getBridgeParticle().playParticle(players, ParticleEnum.CLOUD, location, 1, 0, 0, 0, 0);
							}
							
							location.subtract(x, 0, z);
						}
						
						for (LivingEntity unit : CombatUtil.getNearbyUnits(location, range)) {
							if (!unit.equals(attacker) && !unit.equals(victims)) {
								final PotionEffectType potionType = PotionUtil.getPoisonType(victims);
								final int grade = potionType.equals(PotionEffectType.WITHER) ? 3 : 2;
								final PotionEffect potion = PotionUtil.createPotion(potionType, grade, duration);
								
								unit.addPotionEffect(potion);
								CombatUtil.areaDamage(attacker, unit, spreadDamage);
							}
						}
						
						range = range + 0.5;
						time++;
					}
				}
			}.runTaskTimer(plugin, 0, 1);
		}
	}
}