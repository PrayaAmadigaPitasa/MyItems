package net.ardeus.myitems.ability;

import java.util.List;

import org.bukkit.entity.Entity;
import org.bukkit.plugin.Plugin;

public abstract class Ability {

	private final Plugin plugin;
	private final String id;
	
	protected Ability(Plugin plugin, String id) {
		this.plugin = plugin;
		this.id = id;
	}
	
	public abstract String getKeyLore();
	public abstract int getMaxGrade();
	
	public final Plugin getPlugin() {
		return this.plugin;
	}
	
	public final String getId() {
		return this.id;
	}
	
	public List<String> getDescription() {
		return null;
	}
	
	public int getEffectDuration(int grade) {
		return 0;
	}
	
	public void onDamage(Entity caster, Entity target, int grade, double damage) {
		
	}
}