package net.ardeus.myitems.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

public class PlayerPassiveEffect {

	private final UUID playerId;
	private final PlayerPassiveEffectCooldown playerPassiveEffectCooldown;
	
	public PlayerPassiveEffect(OfflinePlayer player) {
		this(player.getUniqueId());
	}
	
	protected PlayerPassiveEffect(UUID playerId) {
		if (playerId == null) {
			throw new IllegalArgumentException();
		} else {
			this.playerId = playerId;
			this.playerPassiveEffectCooldown = new PlayerPassiveEffectCooldown();
		}
	}
	
	public final UUID getPlayerId() {
		return this.playerId;
	}
	
	public final PlayerPassiveEffectCooldown getCooldown() {
		return this.playerPassiveEffectCooldown;
	}
}
