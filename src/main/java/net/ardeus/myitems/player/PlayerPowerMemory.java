package net.ardeus.myitems.player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.player.PlayerPowerManager;

public final class PlayerPowerMemory extends PlayerPowerManager {

	protected final Map<UUID, PlayerPowerCooldown> mapPlayerPowerCooldown = new HashMap<UUID, PlayerPowerCooldown>();
	
	private PlayerPowerMemory(MyItems plugin) {
		super(plugin);
	};
	
	private static class PlayerPowerMemorySingleton {
		private static final PlayerPowerMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PlayerPowerMemory(plugin);
		}
	}
	
	public static final PlayerPowerMemory getInstance() {
		return PlayerPowerMemorySingleton.INSTANCE;
	}
	
	@Override
	public final PlayerPowerCooldown getPlayerPowerCooldown(UUID playerId) {
		if (!this.mapPlayerPowerCooldown.containsKey(playerId)) {
			final PlayerPowerCooldown playerPowerCooldown = new PlayerPowerCooldown();
			
			this.mapPlayerPowerCooldown.put(playerId, playerPowerCooldown);
		}
		
		return this.mapPlayerPowerCooldown.get(playerId);
	}
	
	@Override
	public final void removePlayerPowerCooldown(UUID playerId) {
		this.mapPlayerPowerCooldown.remove(playerId);
	}
}
