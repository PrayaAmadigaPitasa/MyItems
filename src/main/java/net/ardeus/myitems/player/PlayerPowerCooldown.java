package net.ardeus.myitems.player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import core.praya.agarthalib.enums.branch.ProjectileEnum;
import net.ardeus.myitems.power.PowerType;
import net.ardeus.myitems.power.PowerCommand;
import net.ardeus.myitems.power.PowerSpecial;

public class PlayerPowerCooldown {

	protected final Map<String, Long> mapPowerCommandCooldown;
	protected final Map<ProjectileEnum, Long> mapPowerShootCooldown;
	protected final Map<String, Long> mapPowerSpecialCooldown;
	
	public PlayerPowerCooldown() {
		this(null, null, null);
	}
	
	public PlayerPowerCooldown(Map<String, Long> mapPowerCommandCooldown, Map<ProjectileEnum, Long> mapPowerShootCooldown, Map<String, Long> mapPowerSpecialCooldown) {
		this.mapPowerCommandCooldown = mapPowerCommandCooldown != null ? mapPowerCommandCooldown : new HashMap<String, Long>();
		this.mapPowerShootCooldown = mapPowerShootCooldown != null ? mapPowerShootCooldown : new HashMap<ProjectileEnum, Long>();
		this.mapPowerSpecialCooldown = mapPowerSpecialCooldown != null ? mapPowerSpecialCooldown : new HashMap<String, Long>();
	}
	
	public final Set<String> getCooldownCommandKeySet() {
		return new HashSet<String>(this.mapPowerCommandCooldown.keySet());
	}
	
	public final Set<ProjectileEnum> getCooldownProjectileKeySet() {
		return new HashSet<ProjectileEnum>(this.mapPowerShootCooldown.keySet());
	}
	
	public final Set<String> getCooldownSpecialKeySet() {
		return new HashSet<String>(this.mapPowerSpecialCooldown.keySet());
	}
	
	public final Long getPowerCommandExpired(PowerCommand powerCommand) {
		return powerCommand != null ? getPowerCommandExpired(powerCommand.getId()) : null;
	}
	
	public final Long getPowerCommandExpired(String command) {
		return command != null ? this.mapPowerCommandCooldown.get(command) : null;
	}
	
	public final Long getPowerShootExpired(ProjectileEnum projectile) {
		return projectile != null ? this.mapPowerShootCooldown.get(projectile) : null;
	}
	
	public final Long getPowerSpecialExpired(PowerSpecial powerSpecial) {
		return powerSpecial != null ? getPowerSpecialExpired(powerSpecial.getId()) : null;
	}
	
	public final Long getPowerSpecialExpired(String powerSpecialId) {
		return powerSpecialId != null ? this.mapPowerSpecialCooldown.get(powerSpecialId) : null;
	}
	
	public final void setPowerCommandCooldown(PowerCommand powerCommand, long cooldown) {
		if (powerCommand != null) {
			final String powerCommandId = powerCommand.getId();
			
			setPowerCommandCooldown(powerCommandId, cooldown);
		}
	}
	
	public final void setPowerCommandCooldown(String command, long cooldown) {
		if (command != null) {
			final long expired = System.currentTimeMillis() + cooldown;
			
			this.mapPowerCommandCooldown.put(command, expired);
		}
	}
	
	public final void setPowerShootCooldown(ProjectileEnum projectile, long cooldown) {
		if (projectile != null) {
			final long expired = System.currentTimeMillis() + cooldown;
			
			this.mapPowerShootCooldown.put(projectile, expired);
		}
	}
	
	public final void setPowerSpecialCooldown(PowerSpecial powerSpecial, long cooldown) {
		if (powerSpecial != null) {
			final String powerSpecialId = powerSpecial.getId();
			
			setPowerSpecialCooldown(powerSpecialId, cooldown);
		}
	}
	
	public final void setPowerSpecialCooldown(String powerSpecialId, long cooldown) {
		if (powerSpecialId != null) {
			final long expired = System.currentTimeMillis() + cooldown;
			
			this.mapPowerSpecialCooldown.put(powerSpecialId, expired);
		}
	}
	
	public final boolean isPowerCommandCooldown(PowerCommand powerCommand) {
		return powerCommand != null ? isPowerCommandCooldown(powerCommand.getId()) : false;
	}
	
	public final boolean isPowerCommandCooldown(String powerCommandId) {
		final Long expired = getPowerCommandExpired(powerCommandId);
		
		if (expired != null) {
			final long now = System.currentTimeMillis();
			
			return now < expired;
		} else {
			return false;
		}
	}
	
	public final boolean isPowerShootCooldown(ProjectileEnum projectile) {
		final Long expired = getPowerShootExpired(projectile);
		
		if (expired != null) {
			final long now = System.currentTimeMillis();
			
			return now < expired;
		} else {
			return false;
		}
	}
	
	public final boolean isPowerSpecialCooldown(PowerSpecial powerSpecial) {
		return powerSpecial != null ? isPowerSpecialCooldown(powerSpecial.getId()) : false;
	}
	
	public final boolean isPowerSpecialCooldown(String powerSpecialId) {
		final Long expired = getPowerSpecialExpired(powerSpecialId);
		
		if (expired != null) {
			final long now = System.currentTimeMillis();
			
			return now < expired;
		} else {
			return false;
		}
	}
	
	public final void removePowerCommandCooldown(PowerCommand powerCommand) {
		if (powerCommand != null) {
			final String powerCommandId = powerCommand.getId();
			
			removePowerCommandCooldown(powerCommandId);
		}
	}
	
	public final void removePowerCommandCooldown(String powerCommandId) {
		this.mapPowerCommandCooldown.remove(powerCommandId);
	}
	
	public final void removePowerShootCooldown(ProjectileEnum projectile) {
		this.mapPowerShootCooldown.remove(projectile);
	}
	
	public final void removePowerSpecialCooldown(PowerSpecial powerSpecial) {
		if (powerSpecial != null) {
			final String powerSpecialId = powerSpecial.getId();
			
			removePowerSpecialCooldown(powerSpecialId);
		}
	}
	
	public final void removePowerSpecialCooldown(String powerSpecialId) {
		this.mapPowerSpecialCooldown.remove(powerSpecialId);
	}
	
	public final Long getPowerCommandTimeLeft(PowerCommand powerCommand) {
		return powerCommand != null ? getPowerCommandTimeLeft(powerCommand.getId()) : null;
	}
	
	public final Long getPowerCommandTimeLeft(String powerCommandId) {
		final Long expired = getPowerCommandExpired(powerCommandId);
		
		if (expired != null) {
			final long now = System.currentTimeMillis();
			
			return Math.max(0, expired - now);
		} else {
			return null;
		} 
	}
	
	public final Long getPowerShootTimeLeft(ProjectileEnum projectile) {
		final Long expired = getPowerShootExpired(projectile);
		
		if (expired != null) {
			final long now = System.currentTimeMillis();
			
			return Math.max(0, expired - now);
		} else {
			return null;
		}  
	}
	
	public final Long getPowerSpecialTimeLeft(PowerSpecial powerSpecial) {
		return powerSpecial != null ? getPowerSpecialTimeLeft(powerSpecial.getId()) : null;
	}
	
	public final Long getPowerSpecialTimeLeft(String powerSpecialId) {
		final Long expired = getPowerSpecialExpired(powerSpecialId);
		
		if (expired != null) {
			final long now = System.currentTimeMillis();
			
			return Math.max(0, expired - now);
		} else {
			return null;
		}  
	}
	
	public final void clearPowerCooldown(PowerType power) {
		switch (power) {
		case COMMAND: 
			this.mapPowerCommandCooldown.clear(); 
			return;
		case SHOOT: 
			this.mapPowerShootCooldown.clear(); 
			return;
		case SPECIAL: 
			this.mapPowerSpecialCooldown.clear();
			return;
		default: 
			return;
		}
	}
	
	public final void clearAllCooldown() {
		this.mapPowerCommandCooldown.clear();
		this.mapPowerShootCooldown.clear();
		this.mapPowerSpecialCooldown.clear();
	}
}
