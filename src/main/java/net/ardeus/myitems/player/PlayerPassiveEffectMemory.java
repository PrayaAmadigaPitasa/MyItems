package net.ardeus.myitems.player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.player.PlayerPassiveEffectManager;

public final class PlayerPassiveEffectMemory extends PlayerPassiveEffectManager {

	protected final Map<UUID, PlayerPassiveEffect> mapPassiveEffect = new HashMap<UUID, PlayerPassiveEffect>();
	
	private PlayerPassiveEffectMemory(MyItems plugin) {
		super(plugin);
	};
	
	private static class PlayerPassiveEffectMemorySingleton {
		private static final PlayerPassiveEffectMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PlayerPassiveEffectMemory(plugin);
		}
	}
	
	public static final PlayerPassiveEffectMemory getInstance() {
		return PlayerPassiveEffectMemorySingleton.INSTANCE;
	}
	
	@Override
	public final PlayerPassiveEffect getPlayerPassiveEffect(UUID playerId) {
		if (!this.mapPassiveEffect.containsKey(playerId)) {
			final PlayerPassiveEffect playerPassiveEffect = new PlayerPassiveEffect(playerId);
			
			this.mapPassiveEffect.put(playerId, playerPassiveEffect);
		}
		
		return this.mapPassiveEffect.get(playerId);
	}
	
	@Override
	public final void removePlayerPassiveEffect(UUID playerId) {
		this.mapPassiveEffect.remove(playerId);
	}
}
