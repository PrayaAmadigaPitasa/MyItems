package net.ardeus.myitems.player;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.player.PlayerItemStatsManager;

public final class PlayerItemStatsMemory extends PlayerItemStatsManager {

	private PlayerItemStatsMemory(MyItems plugin) {
		super(plugin);
	}
	
	private static class PlayerItemStatsMemorySingleton {
		private static final PlayerItemStatsMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PlayerItemStatsMemory(plugin);
		}
	}
	
	public static final PlayerItemStatsMemory getInstance() {
		return PlayerItemStatsMemorySingleton.INSTANCE;
	}
}
