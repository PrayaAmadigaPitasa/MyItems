package net.ardeus.myitems.player;

import java.util.HashMap;

import net.ardeus.myitems.passive.PassiveEffectEnum;

public class PlayerPassiveEffectCooldown {

	private final HashMap<PassiveEffectEnum, Long> mapPassiveEffectCooldown;
	
	public PlayerPassiveEffectCooldown() {
		this(null);
	}
	
	public PlayerPassiveEffectCooldown(HashMap<PassiveEffectEnum, Long> mapPassiveEffectCooldown) {
		this.mapPassiveEffectCooldown = mapPassiveEffectCooldown != null ? mapPassiveEffectCooldown : new HashMap<PassiveEffectEnum, Long>();
	}
	
	public final Long getPassiveEffectExpired(PassiveEffectEnum effect) {
		return this.mapPassiveEffectCooldown.get(effect);
	}
	
	public final void setPassiveEffectCooldown(PassiveEffectEnum effect, long cooldown) {
		if (effect != null) {
			final long expired = System.currentTimeMillis() + cooldown;
			
			this.mapPassiveEffectCooldown.put(effect, expired);
		}
	}
	
	public final boolean isPassiveEffectCooldown(PassiveEffectEnum effect) {
		final Long expired = getPassiveEffectExpired(effect);
		
		if (expired != null) {
			final long now = System.currentTimeMillis();
			
			return now < expired;
		} else {
			return false;
		}
	}
	
	public final void removePassiveEffectCooldown(PassiveEffectEnum effect) {
		this.mapPassiveEffectCooldown.remove(effect);
	}
	
	public final Long getPassiveEffectTimeLeft(PassiveEffectEnum effect) {
		final Long expired = getPassiveEffectExpired(effect);
		
		if (expired != null) {
			final long now = System.currentTimeMillis();
			
			return Math.max(0, expired - now);
		} else {
			return null;
		} 
	}
}
