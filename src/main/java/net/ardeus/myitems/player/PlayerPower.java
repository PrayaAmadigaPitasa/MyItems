package net.ardeus.myitems.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

import com.praya.agarthalib.utility.PlayerUtil;

public class PlayerPower {

	private final UUID playerId;
	private final PlayerPowerCooldown playerPowerCooldown;
	
	public PlayerPower(OfflinePlayer player) {
		this(player.getUniqueId());
	}
	
	protected PlayerPower(UUID playerId) {
		if (playerId == null) {
			throw new IllegalArgumentException();
		} else {
			this.playerId = playerId;
			this.playerPowerCooldown = new PlayerPowerCooldown();
		}
	}
	
	public UUID getPlayerId() {
		return this.playerId;
	}
	
	public PlayerPowerCooldown getCooldown() {
		return this.playerPowerCooldown;
	}
	
	public OfflinePlayer getOfflinePlayer() {
		return PlayerUtil.getPlayer(getPlayerId());
	}
}
