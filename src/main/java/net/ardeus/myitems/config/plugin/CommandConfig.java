package net.ardeus.myitems.config.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import core.praya.agarthalib.builder.command.CommandBuild;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.FileUtil;

public class CommandConfig extends HandlerCommand {
	
	private final HashMap<String, CommandBuild> mapCommand = new HashMap<String, CommandBuild>(); 
	
	public CommandConfig(MyItems plugin) {
		super(plugin);
		
		setup();
	};
	
	public final Collection<String> getCommandIDs() {
		return this.mapCommand.keySet();
	}
	
	public final Collection<CommandBuild> getCommandBuilds() {
		return this.mapCommand.values();
	}
	
	public final CommandBuild getCommand(String id) {
		for (String key : getCommandIDs()) {
			if (key.equalsIgnoreCase(id)) {
				return this.mapCommand.get(key);
			}
		}
		
		return null;
	}
	
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapCommand.clear();
	}
	
	@SuppressWarnings("deprecation")
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String path = dataManager.getPath("Path_File_Command");
		final FileConfiguration config = FileUtil.getFileConfigurationResource(plugin, path);
		
		for (String key : config.getKeys(false)) {
			if (key.equalsIgnoreCase("Command")) {
				final ConfigurationSection idSection = config.getConfigurationSection(key);
				
				for (String id : idSection.getKeys(false)) {
					final ConfigurationSection mainDataSection = idSection.getConfigurationSection(id);
					final List<String> aliases = new ArrayList<String>();
					
					String permission = null;
					
					for (String mainData : mainDataSection.getKeys(false)) {
						if (mainData.equalsIgnoreCase("Permission")) {
							permission = mainDataSection.getString(mainData);
						} else if (mainData.equalsIgnoreCase("Aliases")) {
							if (mainDataSection.isString(mainData)) {
								aliases.add(mainDataSection.getString(mainData));
							} else if (mainDataSection.isList(mainData)) {
								aliases.addAll(mainDataSection.getStringList(mainData));
							}
						}
					}
					
					final CommandBuild commandBuild = new CommandBuild(id, permission, aliases);
					
					mapCommand.put(id, commandBuild);
				}
			}
		}
	}
}
