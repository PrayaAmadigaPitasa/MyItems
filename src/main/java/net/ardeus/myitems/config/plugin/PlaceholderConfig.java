package net.ardeus.myitems.config.plugin;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.praya.agarthalib.utility.FileUtil;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

public class PlaceholderConfig extends HandlerConfig {
	
	private final HashMap<String, String> mapPlaceholder = new HashMap<String, String>(); 
	
	public PlaceholderConfig(MyItems plugin) {
		super(plugin);
		
		setup();
	};
	
	public final Collection<String> getPlaceholderIDs() {
		return this.mapPlaceholder.keySet();
	}
	
	public final Collection<String> getPlaceholders() {
		return this.mapPlaceholder.values();
	}
	
	public final String getPlaceholder(String id) {
		for (String key : getPlaceholderIDs()) {
			if (key.equalsIgnoreCase(id)) {
				return this.mapPlaceholder.get(key);
			}
		}
		
		return null;
	}
	
	public final HashMap<String, String> getPlaceholderCopy() {
		return new HashMap<String, String>(mapPlaceholder);
	}
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapPlaceholder.clear();
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String path = dataManager.getPath("Path_File_Placeholder");
		final File file = FileUtil.getFile(plugin, path);
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, path);
		}
		
		for (int t = 0; t < 2; t++) {
			final FileConfiguration config = t == 0 ? FileUtil.getFileConfigurationResource(plugin, path) : FileUtil.getFileConfiguration(file);
			
			for (String key : config.getKeys(false)) {
				if (key.equalsIgnoreCase("Placeholder")) {
					final ConfigurationSection placeholderSection = config.getConfigurationSection(key);
					
					for (String placeholder : placeholderSection.getKeys(false)) {
						mapPlaceholder.put(placeholder, placeholderSection.getString(placeholder));
					}
				}
			}
		}
	}
	
	private final void moveOldFile() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathSource = "placeholder.yml";
		final String pathTarget = dataManager.getPath("Path_File_Placeholder");
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}
