package net.ardeus.myitems.config.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.bukkit.configuration.file.FileConfiguration;

import core.praya.agarthalib.builder.main.LanguageBuild;
import core.praya.agarthalib.builder.message.MessageBuild;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PlaceholderManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.FileUtil;

public class LanguageConfig extends HandlerConfig {
	
	private final HashMap<String, LanguageBuild> mapLanguage = new HashMap<String, LanguageBuild>();
		
	public LanguageConfig(MyItems plugin) {
		super(plugin);
		
		setup();
	}
	
	public final Collection<String> getLanguageIDs() {
		return this.mapLanguage.keySet();
	}
	
	public final Collection<LanguageBuild> getLanguageBuilds() {
		return this.mapLanguage.values();
	}
	
	public final LanguageBuild getLanguageBuild(String id) {
		if (id != null) {
			for (String key : getLanguageIDs()) {
				if (key.equalsIgnoreCase(id)) {
					return this.mapLanguage.get(key);
				}
			}
		}
		
		return null;
	}
	
	public final LanguageBuild getLanguage(String id) {
		if (id != null) {
			final String[] parts = id.split("_");
			final int length = parts.length;
			
			for (int size = length; size > 0; size--) {
				final StringBuilder builder = new StringBuilder();
				
				for (int index = 0; index < size; index++) {
					final String component = parts[index];
					
					builder.append(component);
					
					if (index != (size-1)) {
						builder.append("_");
					}
					
					final String identifier = builder.toString();
					final LanguageBuild languageBuild = getLanguageBuild(identifier);
					
					if (languageBuild != null) {
						return languageBuild;
					}
				}
			}
		}
		
		return getLanguageBuild("en");
	}
	
	public final MessageBuild getMessageBuild(String id, String key) {
		if (id != null && key != null) {
			final LanguageBuild languageBuild = getLanguageBuild(id);
			
			if (languageBuild != null) {
				return languageBuild.getMessage(key);
			}
		}
		
		return new MessageBuild();
	}
	
	public final MessageBuild getMessage(String id, String key) {
		if (id != null) {
			final String[] parts = id.split("_");
			final int length = parts.length;
			
			for (int size = length; size > 0; size--) {
				final StringBuilder builder = new StringBuilder();
				
				for (int index = 0; index < size; index++) {
					final String component = parts[index];
					
					builder.append(component);
					
					if (index != (size-1)) {
						builder.append("_");
					}
					
					final String identifier = builder.toString();
					final LanguageBuild languageBuild = getLanguageBuild(identifier);
					
					if (languageBuild != null) {
						final MessageBuild message = languageBuild.getMessage(key);
						
						if (message.isSet()) {
							return message;
						}
					}
				}
			}
		}
		
		return getMessageBuild("en", key);
	}
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapLanguage.clear();
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathFolder = dataManager.getPath("Path_Folder_Language");
		final File folder = FileUtil.getFile(plugin, pathFolder);
		final List<String> listPath = dataManager.getListPath("Path_File_Language");
		
		for (String pathFile : listPath) {
			final File file = FileUtil.getFile(plugin, pathFile);
			final String name = file.getName().toLowerCase();
			final String id = name.split(Pattern.quote("."))[0];
			final String locale = id.startsWith("lang_") ? id.replaceFirst("lang_", "") : "en";
			
			if (!file.exists()) {
				FileUtil.saveResource(plugin, pathFile);
			}
			
			final FileConfiguration config = FileUtil.getFileConfigurationResource(plugin, pathFile);
			final LanguageBuild language = loadLanguage(locale, config);
			
			this.mapLanguage.put(locale, language);
		}
		
		for (File file : folder.listFiles()) {
			final String name = file.getName().toLowerCase();
			final String id = name.split(Pattern.quote("."))[0];
			final String locale = id.startsWith("lang_") ? id.replaceFirst("lang_", "") : "en";
			final FileConfiguration config = FileUtil.getFileConfiguration(file);
			final LanguageBuild language = loadLanguage(locale, config);
			final LanguageBuild localeLang = getLanguage(locale);
			
			if (localeLang != null) {
				localeLang.mergeLanguage(language);
			} else {
				this.mapLanguage.put(id, language);
			}
		}
	}
	
	private final LanguageBuild loadLanguage(String locale, FileConfiguration config) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final HashMap<String, MessageBuild> mapLanguage = new HashMap<String, MessageBuild>();
		
		for (String path : config.getKeys(true)) {
			final String key = path.replace(".", "_");
			
			if (config.isString(path)) {
				final String text = config.getString(path);
				final List<String> list = new ArrayList<String>();
				
				list.add(text);
				
				final List<String> listPlaceholder = placeholderManager.localPlaceholder(list);
				final MessageBuild messages = new MessageBuild(listPlaceholder);
				
				mapLanguage.put(key, messages);
			} else if (config.isList(path)) {
				final List<String> list = config.getStringList(path);
				final List<String> listPlaceholder = placeholderManager.localPlaceholder(list);
				final MessageBuild messages = new MessageBuild(listPlaceholder);
				
				mapLanguage.put(key, messages);
			}
		}
		
		return new LanguageBuild(locale, mapLanguage);
	}
	
	private final void moveOldFile() {
		final String pathSource = "language.yml";
		final String pathTarget = "Language/lang_en.yml";
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}