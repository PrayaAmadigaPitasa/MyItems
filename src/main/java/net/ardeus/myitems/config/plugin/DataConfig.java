package net.ardeus.myitems.config.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import core.praya.agarthalib.builder.main.DataBuild;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerConfig;

import com.praya.agarthalib.utility.FileUtil;

public class DataConfig extends HandlerConfig {
	
	private final HashMap<String, DataBuild> mapData = new HashMap<String, DataBuild>();

	public DataConfig(MyItems plugin) {
		super(plugin);
		
		setup();
	};
	
	public final Collection<String> getDataIDs() {
		return this.mapData.keySet();
	}
	
	public final Collection<DataBuild> getDataBuilds() {
		return this.mapData.values();
	}
	
	public final DataBuild getData(String id) {
		if (id != null) {
			for (String key : getDataIDs()) {
				if (key.equalsIgnoreCase(id)) {
					return this.mapData.get(key);
				}
			}
		}
		
		return null;
	}

	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapData.clear();
	}
	
	private final void loadConfig() {
		final FileConfiguration config = FileUtil.getFileConfigurationResource(plugin, "Resources/data.yml");
		
		for (String path : config.getKeys(true)) {
			final String key = path.replace(".", "_");
			
			if (config.isString(path)) {
				final String text = config.getString(path);
				final List<String> list = new ArrayList<String>();
				
				list.add(text);
				
				final DataBuild dataBuild = new DataBuild(key, list);
				
				this.mapData.put(key, dataBuild);
			} else if (config.isList(path)) {
				final List<String> list = config.getStringList(path);
				final DataBuild dataBuild = new DataBuild(key, list);
				
				mapData.put(key, dataBuild);
			}
		}
	}
}
