package net.ardeus.myitems.placeholder;

import org.bukkit.entity.Player;

import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.PlaceholderHook;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.plugin.PlaceholderManager;

public class ReplacerPlaceholderAPIBuild extends PlaceholderHook {
	
	private final String placeholder;
	private final MyItems plugin;
	
	public ReplacerPlaceholderAPIBuild(MyItems plugin, String placeholder) {
		this.plugin = plugin;
		this.placeholder = placeholder;
	}
	
	public final String getPlaceholder() {
		return this.placeholder;
	}
	
	public final boolean hook() {
		return PlaceholderAPI.registerPlaceholderHook(this.placeholder, this);
	}

	@Override
	public String onPlaceholderRequest(Player player, String identifier) {
		final PlaceholderManager placeholderManager = plugin.getPluginManager().getPlaceholderManager();
		
		return placeholderManager.getReplacement(player, identifier);
	}
}