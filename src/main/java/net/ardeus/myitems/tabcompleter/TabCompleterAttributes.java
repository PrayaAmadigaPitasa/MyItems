package net.ardeus.myitems.tabcompleter;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import core.praya.agarthalib.enums.branch.ProjectileEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.TagsAttribute;
import core.praya.agarthalib.enums.main.VersionNMS;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerTabCompleter;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.manager.game.AbilityWeaponManager;
import net.ardeus.myitems.manager.game.ElementManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.PowerCommandManager;
import net.ardeus.myitems.manager.game.PowerSpecialManager;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.passive.PassiveTypeEnum;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerType;
import net.ardeus.myitems.requirement.RequirementEnum;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerUtil;
import com.praya.agarthalib.utility.TabCompleterUtil;

public class TabCompleterAttributes extends HandlerTabCompleter implements TabCompleter {

	public TabCompleterAttributes(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final ElementManager elementManager = gameManager.getElementManager();
		final PowerCommandManager powerCommandManager = gameManager.getPowerCommandManager();
		final PowerSpecialManager powerSpecialManager = gameManager.getPowerSpecialManager();
		final AbilityWeaponManager abilityWeaponManager = gameManager.getAbilityWeaponManager();
		final List<String> tabList = new ArrayList<String>();
		
		SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
		
		if (args.length == 1) {
			if (commandManager.checkPermission(sender, "Attribute_Stats")) {
				tabList.add("Stats");
			}
			if (commandManager.checkPermission(sender, "Attribute_Buff")) {
				tabList.add("Buff");
			}
			if (commandManager.checkPermission(sender, "Attribute_Debuff")) {
				tabList.add("Debuff");
			}
			if (commandManager.checkPermission(sender, "Attribute_Ability")) {
				tabList.add("Ability");
			}
			if (commandManager.checkPermission(sender, "Attribute_Power")) {
				tabList.add("Power");
			}
			if (commandManager.checkPermission(sender, "Attribute_NBT")) {
				tabList.add("NBT");
			}
			if (commandManager.checkPermission(sender, "Attribute_Element")) {
				tabList.add("Element");
			}
			if (commandManager.checkPermission(sender, "Attribute_Requirement")) {
				tabList.add("Requirement");
			}
		} else if (args.length == 2) {
			final String argument1 = args[0];
			
			if (commandManager.checkCommand(argument1, "Attribute_Stats")) {
				if (commandManager.checkPermission(sender, "Attribute_Stats")) {
					for (LoreStatsEnum stats : LoreStatsEnum.values()) {
						if (stats.isAllowed()) {
							tabList.add(String.valueOf(stats));
						}
					}
				}
			} else if (commandManager.checkCommand(argument1, "Attribute_Element")) {
				if (commandManager.checkPermission(sender, "Attribute_Element")) {
					for (String element : elementManager.getElementIds()) {
						tabList.add(element);			
					}
				}
			} else if (commandManager.checkCommand(argument1, "Attribute_Buff")) {
				if (commandManager.checkPermission(sender, "Attribute_Buff")) {
					for (PassiveEffectEnum buff : PassiveEffectEnum.values()) {
						if (buff.getType().equals(PassiveTypeEnum.BUFF) ) {
							if (!ServerUtil.isCompatible(VersionNMS.V1_9_R1)) {	
								if (buff.equals(PassiveEffectEnum.LUCK)) {
									continue;
								}
							}
							
							tabList.add(buff.toString());
						}
					}
				}
			} else if (commandManager.checkCommand(argument1, "Attribute_Debuff")) {
				if (commandManager.checkPermission(sender, "Attribute_Debuff")) {
					for (PassiveEffectEnum debuff : PassiveEffectEnum.values()) {
						if (debuff.getType().equals(PassiveTypeEnum.DEBUFF) ) {
							if (!ServerUtil.isCompatible(VersionNMS.V1_9_R1)) {
								if (debuff.equals(PassiveEffectEnum.UNLUCK) || debuff.equals(PassiveEffectEnum.GLOW)) {
									continue;
								}
							}
							
							tabList.add(debuff.toString());
						}
					}
				}
			} else if (commandManager.checkCommand(argument1, "Attribute_NBT")) {
				if (commandManager.checkPermission(sender, "Attribute_NBT")) {
					for (TagsAttribute tags : TagsAttribute.values()) {
						tabList.add(String.valueOf(tags));			
					}
				}
			} else if (commandManager.checkCommand(argument1, "Attribute_Ability")) {
				if (commandManager.checkPermission(sender, "Attribute_Ability")) {
					for (String ability : abilityWeaponManager.getAbilityIds()) {
						tabList.add(ability);
					}
				}
			} else if (commandManager.checkCommand(argument1, "Attribute_Power")) {
				if (commandManager.checkPermission(sender, "Attribute_Power")) {
					for (PowerType power : PowerType.values()) {
						tabList.add(String.valueOf(power));			
					}
				}
			} else if (commandManager.checkCommand(argument1, "Attribute_Requirement")) {
				if (commandManager.checkPermission(sender, "Attribute_Requirement")) {
					for (RequirementEnum requirement : RequirementEnum.values()) {
						tabList.add(String.valueOf(requirement.getName()));			
					}
				}
			}
		} else if (args.length == 3) {
			final String argument1 = args[0];
			
			if (commandManager.checkCommand(argument1, "Attribute_Power")) {
				if (commandManager.checkPermission(sender, "Attribute_Power")) {
					for (PowerClickType click : PowerClickType.values()) {
						tabList.add(String.valueOf(click));
					}
				}
			}
		} else if (args.length == 4) {
			final String argument1 = args[0];
			final String argument2 = args[1];
				
			if (commandManager.checkCommand(argument1, "Attribute_Power")) {
				if (commandManager.checkPermission(sender, "Attribute_Power")) {
					final PowerType power = PowerType.getPowerType(argument2);
					
					if (power != null) {
						if (power.equals(PowerType.COMMAND)) {
							final List<String> powerCommandIds = powerCommandManager.getPowerCommandIds();
							
							tabList.addAll(powerCommandIds);
						} else if (power.equals(PowerType.SHOOT)) {
							for (ProjectileEnum projectile : ProjectileEnum.values()) {
								tabList.add(String.valueOf(projectile));
							}
						} else if (power.equals(PowerType.SPECIAL)) {
							final List<String> powerSpecialIds = powerSpecialManager.getPowerSpecialIds();
							
							tabList.addAll(powerSpecialIds);
						}
					}
				}
			}
		}
		
		return TabCompleterUtil.returnList(tabList, args);
	}
}
