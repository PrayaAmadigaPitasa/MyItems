package net.ardeus.myitems.tabcompleter;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerTabCompleter;
import net.ardeus.myitems.manager.plugin.LanguageManager;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TabCompleterUtil;

public class TabCompleterNotCompatible extends HandlerTabCompleter implements TabCompleter {

	public TabCompleterNotCompatible(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		final LanguageManager lang = plugin.getPluginManager().getLanguageManager();
		final String message = lang.getText(sender, "MyItems_Not_Compatible");
		final List<String> tabList = new ArrayList<String>();
		
		SenderUtil.sendMessage(sender, message);
		SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
		return TabCompleterUtil.returnList(tabList, args);
	}
}
