package net.ardeus.myitems.tabcompleter;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerTabCompleter;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TabCompleterUtil;

public class TabCompleterFlagRemove extends HandlerTabCompleter implements TabCompleter {

	public TabCompleterFlagRemove(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final List<String> tabList = new ArrayList<String>();
		
		SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
		
		if (SenderUtil.isPlayer(sender)) {
			final Player player = PlayerUtil.parse(sender);
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			final ItemMeta meta = item.getItemMeta();
			
			if (args.length == 1) {
				if (commandManager.checkPermission(sender, "Flag_Remove")) {
					for (ItemFlag flag : ItemFlag.values()) {
						if (meta.hasItemFlag(flag)) {
							tabList.add(flag.toString());
						}
					}
				}
			}
		}
		
		return TabCompleterUtil.returnList(tabList, args);
	}
}
