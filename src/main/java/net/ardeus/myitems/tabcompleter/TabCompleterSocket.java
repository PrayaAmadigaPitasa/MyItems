package net.ardeus.myitems.tabcompleter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerTabCompleter;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.ListUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TabCompleterUtil;

public class TabCompleterSocket extends HandlerTabCompleter implements TabCompleter {

	public TabCompleterSocket(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final List<String> tabList = new ArrayList<String>();
		
		SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
		
		if (args.length == 1) {
			if (commandManager.checkPermission(sender, "Socket_Add")) {
				tabList.add("Add");
			}
			if (commandManager.checkPermission(sender, "Socket_Load")) {
				tabList.add("Load");
			}
			if (commandManager.checkPermission(sender, "Socket_Drop")) {
				tabList.add("Drop");
			}
			if (commandManager.checkPermission(sender, "Socket_List")) {
				tabList.add("List");
			}
		}  else if (args.length == 2) {
			final String argument1 = args[0];
			
			if (commandManager.checkCommand(argument1, "Socket_Add")) {
				if (commandManager.checkPermission(sender, "Socket_Add")) {
					tabList.add("Empty");
					tabList.add("Locked");
				}
			} else if (commandManager.checkCommand(argument1, "Socket_Load")) {
				if (commandManager.checkPermission(sender, "Socket_Load")) {
					tabList.add("Gems");
					tabList.add("Rod");
				}
			} else if (commandManager.checkCommand(argument1, "Socket_Drop")) {
				if (commandManager.checkPermission(sender, "Socket_Drop")) {
					tabList.add("Gems");
					tabList.add("Rod");
				}
			}
		} else if (args.length == 3) {
			final String argument1 = args[0];
			final String argument2 = args[1];
			
			if (commandManager.checkCommand(argument1, "Socket_Load")) {
				if (commandManager.checkPermission(sender, "Socket_Load")) {
					if (commandManager.checkCommand(argument2, "Socket_Load_Gems")) {
						if (commandManager.checkPermission(sender, "Socket_Load_Gems")) {
							final Collection<String> gems = socketManager.getSocketGemIds();
							
							if (gems.isEmpty()) {
								tabList.add("");
							} else {
								tabList.addAll(gems);
							}
						}
					} else if (commandManager.checkCommand(argument2, "Socket_Load_Rod")) {
						if (commandManager.checkPermission(sender, "Socket_Load_Rod")) {
							tabList.add("Unlock");
							tabList.add("Remove");
						} 
					}
				}
			} else if (commandManager.checkCommand(argument1, "Socket_Drop")) {
				if (commandManager.checkPermission(sender, "Socket_Drop")) {
					if (commandManager.checkCommand(argument2, "Socket_Drop_Gems")) {
						if (commandManager.checkPermission(sender, "Socket_Drop_Gems")) {
							final Collection<String> gems = socketManager.getSocketGemIds();
							
							if (gems.isEmpty()) {
								tabList.add("");
							} else {
								tabList.addAll(gems);
							}
						}
					} else if (commandManager.checkCommand(argument2, "Socket_Drop_Rod")) {
						if (commandManager.checkPermission(sender, "Socket_Drop_Rod")) {
							tabList.add("Unlock");
							tabList.add("Remove");
						} 
					}
				}
			}
		} else if (args.length == 4) {
			final String argument1 = args[0];
			final String argument2 = args[1];
			final String argument4 = args[3];
			
			if (commandManager.checkCommand(argument1, "Socket_Drop")) {
				if (commandManager.checkPermission(sender, "Socket_Drop")) {
					if (commandManager.checkCommand(argument2, "Socket_Drop_Rod")) {
						if (commandManager.checkPermission(sender, "Socket_Drop_Rod")) {
							final int page = MathUtil.isNumber(argument4) ? MathUtil.parseInteger(argument4) : 1;
							final List<String> keyList = new ArrayList<String>();
							
							for (World world : Bukkit.getWorlds()) {
								keyList.add(world.getName());
							}
							
							return ListUtil.sendList(sender, keyList, page);
						}
					}
				}
			}
		} else if (args.length == 5) {
			final String argument1 = args[0];
			final String argument2 = args[1];
			final String argument5 = args[4];
			
			if (commandManager.checkCommand(argument1, "Socket_Drop")) {
				if (commandManager.checkPermission(sender, "Socket_Drop")) {
					if (commandManager.checkCommand(argument2, "Socket_Drop_Gems")) {
						if (commandManager.checkPermission(sender, "Socket_Drop_Gems")) {
							final int page = MathUtil.isNumber(argument5) ? MathUtil.parseInteger(argument5) : 1;
							final List<String> keyList = new ArrayList<String>();
							
							for (World world : Bukkit.getWorlds()) {
								keyList.add(world.getName());
							}
							
							return ListUtil.sendList(sender, keyList, page);
						}
					}
				}
			} 
		}
		
		return TabCompleterUtil.returnList(tabList, args);
	} 
}
