package net.ardeus.myitems.tabcompleter;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.enchantments.Enchantment;

import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerTabCompleter;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TabCompleterUtil;

public class TabCompleterEnchantmentAdd extends HandlerTabCompleter implements TabCompleter {

	public TabCompleterEnchantmentAdd(MyItems plugin) {
		super(plugin);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final List<String> tabList = new ArrayList<String>();
		
		SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
		
		if (SenderUtil.isPlayer(sender)) {
			if (args.length == 1) {
				if (commandManager.checkPermission(sender, "Enchant_Add")) {
					for (Enchantment enchantment : Enchantment.values()) {
						tabList.add(enchantment.getName());
					}
				}
			}
		}
		
		return TabCompleterUtil.returnList(tabList, args);
	}
}
