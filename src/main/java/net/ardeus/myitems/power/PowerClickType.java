package net.ardeus.myitems.power;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.ardeus.myitems.config.plugin.MainConfig;

public enum PowerClickType {
	
	LEFT(Arrays.asList("Left", "LeftClick", "Left Click", "Left_Click")),
	SHIFT_LEFT(Arrays.asList("Shift Left", "ShiftLeft", "Shift_Left", "Shift Left Click")),
	RIGHT(Arrays.asList("Right", "RightClick,", "Right Click", "Right_Click")),
	SHIFT_RIGHT(Arrays.asList("Shift Right", "ShiftRight", "Shift_Right", "Shift Right Click"));
	
	private final List<String> aliases;
	
	private PowerClickType(List<String> aliases) {
		this.aliases = aliases;
	}
	
	public final List<String> getAliases() {
		return new ArrayList<String>(this.aliases);
	}
	
	public final String getText() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		switch (this) {
		case LEFT: 
			return mainConfig.getPowerClickLeft();
		case RIGHT: 
			return mainConfig.getPowerClickRight();
		case SHIFT_LEFT: 
			return mainConfig.getPowerClickShiftLeft();
		case SHIFT_RIGHT: 
			return mainConfig.getPowerClickShiftRight();
		default: 
			return null;
		}
	}
	
	public static final PowerClickType getPowerClickType(String clickType) {
		if (clickType != null) {
			for (PowerClickType key : PowerClickType.values()) {
				for (String aliase : key.aliases) {
					if (aliase.equalsIgnoreCase(clickType)) {
						return key;
					}
				}
			}
		}
		
		return null;
	}
}
