package net.ardeus.myitems.power;

import java.util.ArrayList;
import java.util.List;

public class PowerCommand {

	private final String id;
	private final String keyLore;
	private final boolean consume;
	private final List<String> commandOP;
	private final List<String> commandConsole;
	
	public PowerCommand(String id, String keyLore) {
		this(id, keyLore, false);
	}
	
	public PowerCommand(String id, String keyLore, boolean consume) {
		this(id, keyLore, consume, null, null);
	}
	
	public PowerCommand(String id, String keyLore, boolean consume, List<String> commandOP, List<String> commandConsole) {
		if (id == null || keyLore == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.keyLore = keyLore;
			this.consume = consume;
			this.commandOP = commandOP != null ? commandOP : new ArrayList<String>();
			this.commandConsole = commandConsole != null ? commandConsole : new ArrayList<String>();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getKeyLore() {
		return this.keyLore;
	}
	
	public final boolean isConsume() {
		return this.consume;
	}
	
	public final List<String> getCommandOP() {
		return this.commandOP;
	}
	
	public final List<String> getCommandConsole() {
		return this.commandConsole;
	}
}
