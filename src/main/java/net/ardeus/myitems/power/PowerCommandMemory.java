package net.ardeus.myitems.power;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.game.PowerCommandManager;
import net.ardeus.myitems.power.PowerCommand;

public final class PowerCommandMemory extends PowerCommandManager {
	
	private final PowerCommandConfig powerCommandConfig;
	
	private PowerCommandMemory(MyItems plugin) {
		super(plugin);
		
		this.powerCommandConfig = new PowerCommandConfig(plugin);
	};
	
	private static class PowerCommandMemorySingleton {
		private static final PowerCommandMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerCommandMemory(plugin);
		}
	}
	
	public static final PowerCommandMemory getInstance() {
		return PowerCommandMemorySingleton.INSTANCE;
	}
	
	protected final PowerCommandConfig getPowerCommandConfig() {
		return this.powerCommandConfig;
	}
	
	@Override
	public final List<String> getPowerCommandIds() {
		final Map<String, PowerCommand> mapPowerCommand = getPowerCommandConfig().mapPowerCommand;
		
		return new ArrayList<String>(mapPowerCommand.keySet());
	}
	
	@Override
	public final List<PowerCommand> getAllPowerCommandProperties() {
		final Map<String, PowerCommand> mapPowerCommand = getPowerCommandConfig().mapPowerCommand;
		
		return new ArrayList<PowerCommand>(mapPowerCommand.values());
	}
	
	@Override
	public final PowerCommand getPowerCommand(String powerCommandId) {
		if (powerCommandId != null) {
			final Map<String, PowerCommand> mapPowerCommand = getPowerCommandConfig().mapPowerCommand;
			
			for (String key : mapPowerCommand.keySet()) {
				if (powerCommandId.equalsIgnoreCase(key)) {
					return mapPowerCommand.get(key);
				}
			}
		}
		
		return null;
	}
	
	public final PowerCommand getPowerCommandByKeyLore(String keyLore) {
		if (keyLore != null) {
			final Map<String, PowerCommand> mapPowerCommand = getPowerCommandConfig().mapPowerCommand;
			
			for (PowerCommand key : mapPowerCommand.values()) {
				if (key.getKeyLore().equalsIgnoreCase(keyLore)) {
					return key;
				}
			}
		}
		
		return null;
	}
}
