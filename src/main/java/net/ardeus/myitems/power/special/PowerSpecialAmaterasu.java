package net.ardeus.myitems.power.special;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.lorestats.LoreStatsWeapon;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PowerSpecialPropertiesManager;
import net.ardeus.myitems.power.PowerSpecial;
import net.ardeus.myitems.power.PowerSpecialProperties;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.LocationUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class PowerSpecialAmaterasu extends PowerSpecial {

	private static final String POWER_SPECIAL_ID = "Amaterasu";
	
	private PowerSpecialAmaterasu(MyItems plugin, String id) {
		super(plugin, id);
	};
	
	private static class PowerSpecialAmaterasuSingleton {
		private static final PowerSpecialAmaterasu INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerSpecialAmaterasu(plugin, POWER_SPECIAL_ID);
		}
	}
	
	public static final PowerSpecialAmaterasu getInstance() {
		return PowerSpecialAmaterasuSingleton.INSTANCE;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String keyLore = mainConfig.getPowerSpecialIdentifierAmaterasu();
		
		return keyLore;
	}

	@Override
	public final void cast(LivingEntity caster) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		final Location locationEye = caster.getEyeLocation();
		final Location locationSight = LocationUtil.getLineLocation(caster, locationEye, 0.5D, 2, 20, 20, false);
		final double effectRange = mainConfig.getEffectRange();
		final Collection<Player> players = PlayerUtil.getNearbyPlayers(locationSight, effectRange);
		final PowerSpecialAmaterasuTask powerSpecialAmaterasuTask = new PowerSpecialAmaterasuTask(caster, locationSight, players);
				
		Bridge.getBridgeSound().playSound(players, locationSight, SoundEnum.ITEM_FIRECHARGE_USE, 5, 1);
		
		powerSpecialAmaterasuTask.runTaskTimer(plugin, 0, 2);
	}
	
	private static class PowerSpecialAmaterasuTask extends BukkitRunnable {
		
		private final LivingEntity caster;
		private final Location locationSight;
		private final int limit;
		private final double range;
		private final double skillDamage;
		private final Collection<Player> players; 
		private final Set<LivingEntity> listEntity;
		
		private int count = 0;
		
		private PowerSpecialAmaterasuTask(LivingEntity caster, Location locationSight, Collection<Player> players) {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			final GameManager gameManager = plugin.getGameManager();
			final LoreStatsManager statsManager = gameManager.getStatsManager();
			final PowerSpecialPropertiesManager powerSpecialPropertiesManager = gameManager.getPowerSpecialPropertiesManager();
			final PowerSpecialProperties powerSpecialProperties = powerSpecialPropertiesManager.getPowerSpecialProperties(POWER_SPECIAL_ID);
			final LoreStatsWeapon loreStatsWeapon = statsManager.getLoreStatsWeapon(caster);
			final int duration = powerSpecialProperties.getDurationEffect();
			final int limit = duration / 2;
			final double range = 3D;
			final double baseAdditionalDamage = powerSpecialProperties.getBaseAdditionalDamage();
			final double basePercentDamage = powerSpecialProperties.getBasePercentDamage();
			final double weaponDamage = loreStatsWeapon.getDamage();
			final double skillDamage = baseAdditionalDamage + ((basePercentDamage * weaponDamage) / 100);
			
			this.caster = caster;
			this.locationSight = locationSight;
			this.limit = limit;
			this.range = range;
			this.skillDamage = skillDamage;
			this.players = players;
			this.listEntity = new HashSet<LivingEntity>();
		}

		@Override
		public void run() {
			if (count >= limit) {
				this.cancel();
				return;
			} else {
				for (LivingEntity unit : CombatUtil.getNearbyUnits(locationSight, range)) {
					if (!unit.equals(caster) && !listEntity.contains(unit)) {
						listEntity.add(unit);
					}
				}
				
				for (LivingEntity victim : listEntity) {
					if (!victim.isDead()) {
						final Location locationVictim = victim.getLocation().add(0, 0.5, 0);
					
						Bridge.getBridgeParticle().playParticle(players, ParticleEnum.SPELL_MOB, locationVictim, 12, 0.25, 0.5, 0.25, 0F);
						Bridge.getBridgeSound().playSound(players, locationVictim, SoundEnum.BLOCK_FIRE_AMBIENT, 0.75F, 1);
						
						if (count % 10 == 0) {
							CombatUtil.areaDamage(caster, victim, skillDamage);
						}
					}
				}
				
				Bridge.getBridgeParticle().playParticle(players, ParticleEnum.SPELL_MOB, locationSight, 25, 1.5, 0.75, 1.5, 0F);
				Bridge.getBridgeSound().playSound(players, locationSight, SoundEnum.BLOCK_FIRE_AMBIENT, 5, 1);
				
				count++;
			}
		}
	}
}