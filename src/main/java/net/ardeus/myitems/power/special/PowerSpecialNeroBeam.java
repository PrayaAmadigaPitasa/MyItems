package net.ardeus.myitems.power.special;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.lorestats.LoreStatsWeapon;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PowerSpecialPropertiesManager;
import net.ardeus.myitems.power.PowerSpecial;
import net.ardeus.myitems.power.PowerSpecialProperties;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class PowerSpecialNeroBeam extends PowerSpecial {
	
	private static final String POWER_SPECIAL_ID = "Nero_Beam";
	
	private PowerSpecialNeroBeam(MyItems plugin, String id) {
		super(plugin, id);
	};
	
	private static class PowerSpecialNeroBeamSingleton {
		private static final PowerSpecialNeroBeam INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerSpecialNeroBeam(plugin, POWER_SPECIAL_ID);
		}
	}
	
	public static final PowerSpecialNeroBeam getInstance() {
		return PowerSpecialNeroBeamSingleton.INSTANCE;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String keyLore = mainConfig.getPowerSpecialIdentifierNeroBeam();
		
		return keyLore;
	}

	@Override
	public final void cast(LivingEntity caster) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		final World world = caster.getWorld();
		final Location location = caster.getEyeLocation();
		final Location locationSide = new Location(world, 0, 0, 0, location.getYaw()-90, location.getPitch());
		final Location locationUp = new Location(world, 0, 0, 0, location.getYaw(), location.getPitch()-90);
		final Vector vectorSide = locationSide.getDirection();
		final Vector vectorUp = locationUp.getDirection();		
		final Vector vector = location.getDirection();	
		final double effectRange = mainConfig.getEffectRange();
		final Collection<Player> players = PlayerUtil.getNearbyPlayers(location, effectRange);
		final PowerSpecialNeroBeamTask powerSpecialNeroBeamTask = new PowerSpecialNeroBeamTask(caster, locationUp, vector, vectorSide, vectorUp, players);
		
		powerSpecialNeroBeamTask.runTaskTimer(plugin, 0, 1);
	}
	
	private static class PowerSpecialNeroBeamTask extends BukkitRunnable {

		private final LivingEntity caster;
		private final Location location;
		private final Vector vector;
		private final Vector vectorSide;
		private final Vector vectorUp;
		private final int limit;
		private final int duration;
		private final double rangeScale;
		private final double radiusStart;
		private final double radiusScale;
		private final double skillDamage;
		private final Collection<Player> players; 
		private final Set<LivingEntity> listEntity;
		
		private int count = 0;
		
		private double degree;
		private double range;
		private double radius;
		
		private PowerSpecialNeroBeamTask(LivingEntity caster, Location location, Vector vector, Vector vectorSide, Vector vectorUp, Collection<Player> players) {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			final GameManager gameManager = plugin.getGameManager();
			final LoreStatsManager loreStatsManager = gameManager.getStatsManager();
			final PowerSpecialPropertiesManager powerSpecialPropertiesManager = gameManager.getPowerSpecialPropertiesManager();
			final PowerSpecialProperties powerSpecialProperties = powerSpecialPropertiesManager.getPowerSpecialProperties(POWER_SPECIAL_ID);
			final LoreStatsWeapon loreStatsWeapon = loreStatsManager.getLoreStatsWeapon(caster);
			final int duration = powerSpecialProperties.getDurationEffect();
			final double weaponDamage = loreStatsWeapon.getDamage();
			final double baseAdditionalDamage = powerSpecialProperties.getBaseAdditionalDamage();
			final double basePercentDamage = powerSpecialProperties.getBasePercentDamage();
			final double skillDamage = baseAdditionalDamage + ((basePercentDamage * weaponDamage) / 100);
			final int limit = 15;
			final double rangeBase = 1.5D;
			final double rangeScale = 0.05D;
			final double radiusStart = 0.2D;
			final double radiusScale = 0.05D;
			
			this.caster = caster;
			this.location = location;
			this.vector = vector;
			this.vectorSide = vectorSide;
			this.vectorUp = vectorUp;
			this.limit = limit;
			this.duration = duration;
			this.rangeScale = rangeScale;
			this.radiusStart = radiusStart;
			this.radiusScale = radiusScale;
			this.skillDamage = skillDamage;
			this.players = players;
			this.listEntity = new HashSet<LivingEntity>();
			this.range = rangeBase;
			this.radius = radiusStart;
		}
						
		@Override
		public void run() {
			if (count >= limit) {
				this.cancel();
				return;
			} else {
				final Location partLoc = location.clone();
				
				degree = Math.PI / (2 * (radius / radiusStart));
				
				location.add(vector);
				
				for (double math = 0; math <= 2 * Math.PI; math += degree) {
					final double calcHorizontal = Math.sin(math)*radius;
					final double calVertical = Math.cos(math)*radius;
					final double xSide = vectorSide.getX();
					final double ySide = vectorSide.getY();
					final double zSide = vectorSide.getZ();
					final double xUp = vectorUp.getX();
					final double yUp = vectorUp.getY();
					final double zUp = vectorUp.getZ();
					
					partLoc.add(xSide * calcHorizontal, ySide * calcHorizontal, zSide * calcHorizontal);
					partLoc.add(xUp * calVertical, yUp * calVertical, zUp * calVertical);
					Bridge.getBridgeParticle().playParticle(players, ParticleEnum.SPELL_WITCH, partLoc, 1, 0, 0, 0, 0);
					partLoc.subtract(xSide * calcHorizontal, ySide * calcHorizontal, zSide * calcHorizontal);
					partLoc.subtract(xUp * calVertical, yUp * calVertical, zUp * calVertical);
				}
				
				Bridge.getBridgeSound().playSound(players, location, SoundEnum.ENTITY_WITHER_SHOOT, 5, 1);
				
				for (LivingEntity unit : CombatUtil.getNearbyUnits(location, range)) {
					if (!unit.equals(caster) && !listEntity.contains(unit)) {
						listEntity.add(unit);
						CombatUtil.applyPotion(unit, PotionEffectType.SLOW, duration, 10);
						CombatUtil.skillDamage(caster, unit, skillDamage);
					}
				}
				
				radius += radiusScale;
				range += rangeScale;
				
				count++;
			}
		}
	}
}
