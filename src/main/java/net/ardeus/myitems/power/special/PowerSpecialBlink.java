package net.ardeus.myitems.power.special;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.power.PowerSpecial;
import com.praya.agarthalib.utility.LocationUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class PowerSpecialBlink extends PowerSpecial {
	
	private static final String POWER_SPECIAL_ID = "Blink";
	
	private PowerSpecialBlink(MyItems plugin, String id) {
		super(plugin, id);
	};
	
	private static class PowerSpecialBlinkSingleton {
		private static final PowerSpecialBlink INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerSpecialBlink(plugin, POWER_SPECIAL_ID);
		}
	}
	
	public static final PowerSpecialBlink getInstance() {
		return PowerSpecialBlinkSingleton.INSTANCE;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String keyLore = mainConfig.getPowerSpecialIdentifierBlink();
		
		return keyLore;
	}

	@Override
	public final void cast(LivingEntity caster) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final Location locationCasterEye = caster.getEyeLocation();
		final Location locationBlink = LocationUtil.getLineBlock(locationCasterEye, 20, 20);
		final double height = caster.getEyeHeight();
		final double effectRange = mainConfig.getEffectRange();
		final Collection<Player> players = PlayerUtil.getNearbyPlayers(locationCasterEye, effectRange);
		
		locationBlink.setYaw(locationCasterEye.getYaw());
		locationBlink.setPitch(locationCasterEye.getPitch());
		locationBlink.subtract(0, height, 0);
		
		if (locationBlink.getBlock().getType().isSolid()) {
			locationBlink.add(0, height, 0);
		}
		
		caster.teleport(locationBlink);
		Bridge.getBridgeParticle().playParticle(players, ParticleEnum.PORTAL, locationBlink, 25, 0.5, 0.25, 0.5F);
		Bridge.getBridgeSound().playSound(players, locationBlink, SoundEnum.BLOCK_PORTAL_TRAVEL, 0.6F, 1F);
	}
}
