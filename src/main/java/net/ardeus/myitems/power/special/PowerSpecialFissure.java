package net.ardeus.myitems.power.special;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.lorestats.LoreStatsWeapon;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PowerSpecialPropertiesManager;
import net.ardeus.myitems.power.PowerSpecial;
import net.ardeus.myitems.power.PowerSpecialProperties;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class PowerSpecialFissure extends PowerSpecial {
	
	private static final String POWER_SPECIAL_ID = "Fissure";
	
	private PowerSpecialFissure(MyItems plugin, String id) {
		super(plugin, id);
	};
	
	private static class PowerSpecialFissureSingleton {
		private static final PowerSpecialFissure INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerSpecialFissure(plugin, POWER_SPECIAL_ID);
		}
	}
	
	public static final PowerSpecialFissure getInstance() {
		return PowerSpecialFissureSingleton.INSTANCE;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String keyLore = mainConfig.getPowerSpecialIdentifierFissure();
		
		return keyLore;
	}

	@Override
	public final void cast(LivingEntity caster) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		final World world = caster.getWorld();
		final Location locationCaster = caster.getLocation();
		final Location locationHorizontal = new Location(world, 0, 0, 0, locationCaster.getYaw(), 0F);
		final Vector aim = locationHorizontal.getDirection().normalize();
		final Collection<Player> players = PlayerUtil.getNearbyPlayers(locationCaster, mainConfig.getEffectRange());
		final PowerSpecialFissureTask powerSpecialFissureTask = new PowerSpecialFissureTask(caster, locationCaster, aim, players);
		
		Bridge.getBridgeSound().playSound(players, locationCaster, SoundEnum.ITEM_FIRECHARGE_USE, 1, 1);
		
		powerSpecialFissureTask.runTaskTimer(plugin, 0, 1);
	}
	
	private static class PowerSpecialFissureTask extends BukkitRunnable {

		private final LivingEntity caster;
		private final Location location;
		private final Vector vector;
		private final int limit;
		private final int duration;
		private final double range;
		private final double skillDamage;
		private final Collection<Player> players; 
		private final Set<LivingEntity> listEntity;
		
		private int count = 0;
		
		private PowerSpecialFissureTask(LivingEntity caster, Location location, Vector vector, Collection<Player> players) {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			final GameManager gameManager = plugin.getGameManager();
			final LoreStatsManager loreStatsManager = gameManager.getStatsManager();
			final PowerSpecialPropertiesManager powerSpecialPropertiesManager = gameManager.getPowerSpecialPropertiesManager();
			final PowerSpecialProperties powerSpecialProperties = powerSpecialPropertiesManager.getPowerSpecialProperties(POWER_SPECIAL_ID);
			final LoreStatsWeapon loreStatsWeapon = loreStatsManager.getLoreStatsWeapon(caster);
			final int duration = powerSpecialProperties.getDurationEffect();
			final int limit = 12;
			final double range = 3D;
			final double weaponDamage = loreStatsWeapon.getDamage();
			final double baseAdditionalDamage = powerSpecialProperties.getBaseAdditionalDamage();
			final double basePercentDamage = powerSpecialProperties.getBasePercentDamage();
			final double skillDamage = baseAdditionalDamage + ((basePercentDamage * weaponDamage) / 100);
			
			this.caster = caster;
			this.location = location;
			this.vector = vector;
			this.limit = limit;
			this.duration = duration;
			this.range = range;
			this.skillDamage = skillDamage;
			this.players = players;
			this.listEntity = new HashSet<LivingEntity>();
		}
		
		@Override
		public void run() {
			if (count >= limit) {
				this.cancel();
				return;
			} else {
			
				location.add(vector);
				
				Bridge.getBridgeParticle().playParticle(players, ParticleEnum.FLAME, location, 25, 0.15, 0.25, 0.15, 0.02F);
				Bridge.getBridgeParticle().playParticle(players, ParticleEnum.LAVA, location, 10, 0.2, 0.15, 0.2, 0.05F);
				Bridge.getBridgeSound().playSound(players, location, SoundEnum.BLOCK_FIRE_AMBIENT, 0.8F, 1);
				
				for (LivingEntity unit : CombatUtil.getNearbyUnits(location, range)) {
					if (!unit.equals(caster) && !listEntity.contains(unit)) {
						listEntity.add(unit);
						unit.setFireTicks(duration);
						CombatUtil.skillDamage(caster, unit, skillDamage);
					}
				}
				
				count++;
			}
		}
	}
}