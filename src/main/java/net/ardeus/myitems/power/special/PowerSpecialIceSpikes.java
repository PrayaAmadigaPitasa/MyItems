package net.ardeus.myitems.power.special;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.lorestats.LoreStatsWeapon;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PowerSpecialPropertiesManager;
import net.ardeus.myitems.power.PowerSpecial;
import net.ardeus.myitems.power.PowerSpecialProperties;

import com.praya.agarthalib.utility.BlockUtil;
import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public final class PowerSpecialIceSpikes extends PowerSpecial {
	
	private static final String POWER_SPECIAL_ID = "Ice_Spikes";
	
	private PowerSpecialIceSpikes(MyItems plugin, String id) {
		super(plugin, id);
	};
	
	private static class PowerSpecialIceSpikesSingleton {
		private static final PowerSpecialIceSpikes INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerSpecialIceSpikes(plugin, POWER_SPECIAL_ID);
		}
	}
	
	public static final PowerSpecialIceSpikes getInstance() {
		return PowerSpecialIceSpikesSingleton.INSTANCE;
	}
	
	@Override
	public final String getKeyLore() {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String keyLore = mainConfig.getPowerSpecialIdentifierIceSpikes();
		
		return keyLore;
	}

	@Override
	public final void cast(LivingEntity caster) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final MainConfig mainConfig = MainConfig.getInstance();
		final World world = caster.getWorld();
		final Location locationCaster = caster.getLocation();
		final Location locationHorizontal = new Location(world, 0, 0, 0, locationCaster.getYaw(), 0F);
		final Vector aim = locationHorizontal.getDirection().multiply(2);
		final double effectRange = mainConfig.getEffectRange();
		final Collection<Player> players = PlayerUtil.getNearbyPlayers(locationCaster, effectRange);
		final PowerSpecialIceSpikesTask powerSpecialIceSpikesTask = new PowerSpecialIceSpikesTask(caster, locationCaster, aim, players);
		
		powerSpecialIceSpikesTask.runTaskTimer(plugin, 0, 3);
	}
	
	private static class PowerSpecialIceSpikesTask extends BukkitRunnable {

		private final LivingEntity caster;
		private final Location location;
		private final Vector vector;
		private final int limit;
		private final int duration;
		private final double range;
		private final double skillDamage;
		private final Collection<Player> players; 
		private final Set<LivingEntity> listEntity;
		
		private int count = 0;
		
		private PowerSpecialIceSpikesTask(LivingEntity caster, Location location, Vector vector, Collection<Player> players) {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			final GameManager gameManager = plugin.getGameManager();
			final LoreStatsManager loreStatsManager = gameManager.getStatsManager();
			final PowerSpecialPropertiesManager powerSpecialPropertiesManager = gameManager.getPowerSpecialPropertiesManager();
			final PowerSpecialProperties powerSpecialProperties = powerSpecialPropertiesManager.getPowerSpecialProperties(POWER_SPECIAL_ID);
			final LoreStatsWeapon loreStatsWeapon = loreStatsManager.getLoreStatsWeapon(caster);
			final int duration = powerSpecialProperties.getDurationEffect();
			final int limit = 5;
			final double range = 2D;
			final double weaponDamage = loreStatsWeapon.getDamage();
			final double baseAdditionalDamage = powerSpecialProperties.getBaseAdditionalDamage();
			final double basePercentDamage = powerSpecialProperties.getBasePercentDamage();
			final double skillDamage = baseAdditionalDamage + ((basePercentDamage * weaponDamage) / 100);
			
			this.caster = caster;
			this.location = location;
			this.vector = vector;
			this.limit = limit;
			this.duration = duration;
			this.range = range;
			this.skillDamage = skillDamage;
			this.players = players;
			this.listEntity = new HashSet<LivingEntity>();
		}
		
		@Override
		public void run() {
			if (count >= limit) {
				this.cancel();
				return;
			} else {
				final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
				
				location.add(vector);
				
				Bridge.getBridgeSound().playSound(players, location, SoundEnum.BLOCK_GLASS_BREAK, 0.8F, 1);
				
				for (int indexSpikes = 0; indexSpikes < 3; indexSpikes++) {
					
					location.add(0, indexSpikes, 0);
					
					if (location.getBlock().getType().equals(Material.AIR)) {
						final Block block = location.getBlock();
						
						BlockUtil.set(block);
						block.setType(Material.PACKED_ICE);
						
						for (LivingEntity unit : CombatUtil.getNearbyUnits(location, range)) {
							if (!unit.equals(caster) && !listEntity.contains(unit)) {
								listEntity.add(unit);
								CombatUtil.applyPotion(unit, PotionEffectType.SLOW, duration, 4);
								CombatUtil.skillDamage(caster, unit, skillDamage);
								Bridge.getBridgeParticle().playParticle(players, ParticleEnum.SNOW_SHOVEL, location, 10, 0.2, 0.2, 0.2, 0.1F);
							}
						}
						
						new BukkitRunnable() {
							
							final Location iceLoc = location.clone();
							
							@Override
							public void run() {
								final Block block = iceLoc.getBlock();
								
								BlockUtil.remove(block);
								
								if (block.getType().equals(Material.PACKED_ICE)) {
									block.setType(Material.AIR);
								}
								
							}
						}.runTaskLater(plugin, 9);
					}
					
					location.subtract(0, indexSpikes, 0);
				}
				
				count++;
			}
		}
		
	}
}
