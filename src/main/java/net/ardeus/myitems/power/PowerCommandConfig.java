package net.ardeus.myitems.power;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.TextUtil;

public class PowerCommandConfig extends HandlerConfig {
	
	protected final Map<String, PowerCommand> mapPowerCommand = new HashMap<String, PowerCommand>();
	
	protected PowerCommandConfig(MyItems plugin) {
		super(plugin);
	};
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapPowerCommand.clear();
	}
	
	public final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String path = dataManager.getPath("path_File_Power_Command");
		final File file = FileUtil.getFile(plugin, path);		
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, path);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection mainDataSection = config.getConfigurationSection(key);
			final List<String> commandOP = new ArrayList<String>();
			final List<String> commandConsole = new ArrayList<String>();
			
			String keyLore = null;
			boolean consume = false;
			
			for (String mainData : mainDataSection.getKeys(false)) {
				if (mainData.equalsIgnoreCase("KeyLore")) {
					keyLore = TextUtil.colorful(mainDataSection.getString(mainData));
				} else if (mainData.equalsIgnoreCase("Consume")) {
					consume = mainDataSection.getBoolean(mainData);
				} else if (mainData.equalsIgnoreCase("command")) {
					if (mainDataSection.isList(mainData)) {
						commandOP.addAll(mainDataSection.getStringList(mainData));
					} else if (mainDataSection.isString(mainData)) {
						commandOP.add(mainDataSection.getString(mainData));
					} else if (mainDataSection.isConfigurationSection(mainData)) {
						final ConfigurationSection commandDataSection = mainDataSection.getConfigurationSection(mainData);
						
						for (String commandData : commandDataSection.getKeys(false)) {
							if (commandData.equalsIgnoreCase("OP")) {
								if (commandDataSection.isList(commandData)) {
									commandOP.addAll(commandDataSection.getStringList(commandData));
								} else if (commandDataSection.isString(commandData)) {
									commandOP.add(commandDataSection.getString(commandData));
								}
							} else if (commandData.equalsIgnoreCase("Console")) {
								if (commandDataSection.isList(commandData)) {
									commandConsole.addAll(commandDataSection.getStringList(commandData));
								} else if (commandDataSection.isString(commandData)) {
									commandConsole.add(commandDataSection.getString(commandData));
								}
							}
						}
					}
				}
			}
			
			if (keyLore != null) {
				final PowerCommand powerCommand = new PowerCommand(key, keyLore, consume, commandOP, commandConsole);
				
				this.mapPowerCommand.put(key, powerCommand);
			}
		}
	}
	
	private final void moveOldFile() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathSource = "command.yml";
		final String pathTarget = dataManager.getPath("path_File_Power_Command");
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}
