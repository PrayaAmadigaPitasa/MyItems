package net.ardeus.myitems.power;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.game.PowerSpecialManager;

public final class PowerSpecialMemory extends PowerSpecialManager {

	protected final Map<String, PowerSpecial> mapPowerSpecial = new HashMap<String, PowerSpecial>();
	
	private PowerSpecialMemory(MyItems plugin) {
		super(plugin);
	}
	
	private static class PowerSpecialMemorySingleton {
		private static final PowerSpecialMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerSpecialMemory(plugin);
		}
	}
	
	public static final PowerSpecialMemory getInstance() {
		return PowerSpecialMemorySingleton.INSTANCE;
	}
	
	@Override
	public final List<String> getPowerSpecialIds() {
		return new ArrayList<String>(this.mapPowerSpecial.keySet());
	}
	
	@Override
	public final List<PowerSpecial> getAllPowerSpecials() {
		return new ArrayList<PowerSpecial>(this.mapPowerSpecial.values());
	}
	
	@Override
	public final PowerSpecial getPowerSpecial(String id) {
		if (id != null) {
			for (String key : this.mapPowerSpecial.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return this.mapPowerSpecial.get(key);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final PowerSpecial getPowerSpecialByKeyLore(String keyLore) {
		if (keyLore != null) {
			for (PowerSpecial key : this.mapPowerSpecial.values()) {
				if (key.getKeyLore().equalsIgnoreCase(keyLore)) {
					return key;
				}
			}
		}
		
		return null;
	}
	
	protected final boolean register(PowerSpecial powerSpecial) {
		if (powerSpecial != null) {
			final String id = powerSpecial.getId();
			
			if (!isExists(id)) {
				
				this.mapPowerSpecial.put(id, powerSpecial);
				
				return true;
			}
		}
		
		return false;
	}
	
	protected final boolean unregister(PowerSpecial powerSpecial) {
		if (powerSpecial != null && this.mapPowerSpecial.containsValue(powerSpecial)) {
			final String id = powerSpecial.getId();
			
			this.mapPowerSpecial.remove(id);
			
			return true;
		} else {
			return false;
		}
	}
}
