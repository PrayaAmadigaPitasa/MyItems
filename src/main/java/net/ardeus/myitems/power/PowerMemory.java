package net.ardeus.myitems.power;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.game.PowerManager;

public final class PowerMemory extends PowerManager {

	private PowerMemory(MyItems plugin) {
		super(plugin);
	};
	
	private static class PowerMemorySingleton {
		private static final PowerMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerMemory(plugin);
		}
	}
	
	public static final PowerMemory getInstance() {
		return PowerMemorySingleton.INSTANCE;
	}
}
