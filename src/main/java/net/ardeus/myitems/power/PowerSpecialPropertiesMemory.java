package net.ardeus.myitems.power;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.game.PowerSpecialPropertiesManager;

public final class PowerSpecialPropertiesMemory extends PowerSpecialPropertiesManager {

	private final PowerSpecialConfig powerSpecialConfig;
	
	private PowerSpecialPropertiesMemory(MyItems plugin) {
		super(plugin);
		
		this.powerSpecialConfig = new PowerSpecialConfig(plugin);
	}

	private static class PowerSpecialPropertiesMemorySingleton {
		private static final PowerSpecialPropertiesMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerSpecialPropertiesMemory(plugin);
		}
	}
	
	public static final PowerSpecialPropertiesMemory getInstance() {
		return PowerSpecialPropertiesMemorySingleton.INSTANCE;
	}
	
	protected final PowerSpecialConfig getPowerSpecialConfig() {
		return this.powerSpecialConfig;
	}
	
	@Override
	public final Collection<String> getPowerSpecialPropertiesIds() {
		final Map<String, PowerSpecialProperties> mapPowerSpecialProperties = getPowerSpecialConfig().mapPowerSpecialProperties;
		
		return new ArrayList<String>(mapPowerSpecialProperties.keySet());
	}
	
	@Override
	public final Collection<PowerSpecialProperties> getAllPowerSpecialProperties() {
		final Map<String, PowerSpecialProperties> mapPowerSpecialProperties = getPowerSpecialConfig().mapPowerSpecialProperties;
		
		return new ArrayList<PowerSpecialProperties>(mapPowerSpecialProperties.values());
	}
	
	@Override
	public final PowerSpecialProperties getPowerSpecialProperties(String powerSpecialId) {
		if (powerSpecialId != null) {
			final Map<String, PowerSpecialProperties> mapPowerSpecialProperties = getPowerSpecialConfig().mapPowerSpecialProperties;
			
			for (String key : mapPowerSpecialProperties.keySet()) {
				if (key.equalsIgnoreCase(powerSpecialId)) {
					return mapPowerSpecialProperties.get(key);
				}
			}
		}
		
		return null;
	}
}