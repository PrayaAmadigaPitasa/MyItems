package net.ardeus.myitems.power;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.MathUtil;

public final class PowerSpecialConfig extends HandlerConfig {
	
	protected final Map<String, PowerSpecialProperties> mapPowerSpecialProperties = new HashMap<String, PowerSpecialProperties>();
	
	protected PowerSpecialConfig(MyItems plugin) {
		super(plugin);
	};
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapPowerSpecialProperties.clear();
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String path = dataManager.getPath("Path_File_Power_Special");
		final File file = FileUtil.getFile(plugin, path);		
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, path);
		}
		
		for (int t = 0; t < 2; t++) {
			final FileConfiguration config = t == 0 ? FileUtil.getFileConfigurationResource(plugin, path) : FileUtil.getFileConfiguration(file);
			
			for (String key : config.getKeys(false)) {
				final ConfigurationSection section = config.getConfigurationSection(key);
				
				int durationEffect = 1;
				double baseAdditionalDamage = 0;
				double basePercentDamage = 100;
									
				for (String keySection : section.getKeys(false)) {
					if (keySection.equalsIgnoreCase("Duration_Effect")) {
						durationEffect = section.getInt(keySection);
					} else if (keySection.equalsIgnoreCase("Base_Additional_Damage")) {
						baseAdditionalDamage = section.getDouble(keySection);
					} else if (keySection.equalsIgnoreCase("Base_Percent_Damage")) {
						basePercentDamage = section.getDouble(keySection);
					}
				}
				
				durationEffect = MathUtil.limitInteger(durationEffect, 1, durationEffect);
				baseAdditionalDamage = MathUtil.limitDouble(baseAdditionalDamage, 0, baseAdditionalDamage);
				basePercentDamage = MathUtil.limitDouble(basePercentDamage, 0, basePercentDamage);
				
				final PowerSpecialProperties powerSpecialProperties = new PowerSpecialProperties(key, durationEffect, baseAdditionalDamage, basePercentDamage);
				
				mapPowerSpecialProperties.put(key, powerSpecialProperties);
			}
		}
	}
	
	private final void moveOldFile() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathSource = "specialpower.yml";
		final String pathTarget = dataManager.getPath("Path_File_Power_Special");
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}
