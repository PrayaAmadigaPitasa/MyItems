package net.ardeus.myitems.power;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum PowerType {
	
	COMMAND(Arrays.asList("Command", "Cmd")),
	SHOOT(Arrays.asList("Shoot", "Launch", "Projectile")),
	SPECIAL(Arrays.asList("Special"));
	
	private final List<String> aliases;
	
	private PowerType(List<String> aliases) {
		this.aliases = aliases;
	}
	
	public List<String> getAliases() {
		return new ArrayList<String>(this.aliases);
	}
	
	public static final PowerType getPowerType(String power) {
		if (power != null) {
			for (PowerType key : PowerType.values()) {
				for (String aliase : key.aliases) {
					if (aliase.equalsIgnoreCase(power)) {
						return key;
					}
				}
			}
		}
		
		return null;
	}
}