package net.ardeus.myitems.power;

import org.bukkit.entity.LivingEntity;
import org.bukkit.plugin.Plugin;

public abstract class PowerSpecial {
	
	private final Plugin plugin;
	private final String id;
	
	public PowerSpecial(Plugin plugin, String id) {
		if (plugin == null || id == null) {
			throw new IllegalArgumentException();
		} else {
			this.plugin = plugin;
			this.id = id;
		}
	}
	
	public abstract String getKeyLore();
	public abstract void cast(LivingEntity caster);
	
	public final Plugin getPlugin() {
		return this.plugin;
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final boolean register() {
		final PowerSpecialMemory powerSpecialMemory = PowerSpecialMemory.getInstance();
		
		return powerSpecialMemory.register(this);
	}
	
	public final boolean unregister() {
		final PowerSpecialMemory powerSpecialMemory = PowerSpecialMemory.getInstance();
		
		return powerSpecialMemory.unregister(this);
	}
}