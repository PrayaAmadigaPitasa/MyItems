package net.ardeus.myitems.power;

public class PowerSpecialProperties {

	private final String id;
	private final int durationEffect;
	private final double baseAdditionalDamage;
	private final double basePercentDamage;
	
	public PowerSpecialProperties(String id, int durationEffect, double baseAdditionalDamage, double basePercentDamage) {
		if (id == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.durationEffect = durationEffect;
			this.baseAdditionalDamage = baseAdditionalDamage;
			this.basePercentDamage = basePercentDamage;
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final int getDurationEffect() {
		return this.durationEffect;
	}
	
	public final double getBaseAdditionalDamage() {
		return this.baseAdditionalDamage;
	}
	
	public final double getBasePercentDamage() {
		return this.basePercentDamage;
	}
}