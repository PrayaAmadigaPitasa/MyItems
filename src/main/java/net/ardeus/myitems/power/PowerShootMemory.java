package net.ardeus.myitems.power;

import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.enums.branch.ProjectileEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.game.PowerShootManager;

public final class PowerShootMemory extends PowerShootManager {

	private PowerShootMemory(MyItems plugin) {
		super(plugin);
	};
	
	private static class PowerShootMemorySingleton {
		private static final PowerShootMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new PowerShootMemory(plugin);
		}
	}
	
	public static final PowerShootMemory getInstance() {
		return PowerShootMemorySingleton.INSTANCE;
	}
	
	@Override
	public final String getPowerShootKeyLore(ProjectileEnum projectileEnum) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (projectileEnum != null) {
			switch (projectileEnum) {
			case ARROW: 
				return mainConfig.getPowerProjectileIdentifierArrow();
			case SNOWBALL: 
				return mainConfig.getPowerProjectileIdentifierSnowBall();
			case EGG: 
				return mainConfig.getPowerProjectileIdentifierEgg();
			case FLAME_ARROW: 
				return mainConfig.getPowerProjectileIdentifierFlameArrow();
			case FLAME_BALL: 
				return mainConfig.getPowerProjectileIdentifierFlameBall();
			case FLAME_EGG: 
				return mainConfig.getPowerProjectileIdentifierFlameEgg();
			case SMALL_FIREBALL: 
				return mainConfig.getPowerProjectileIdentifierSmallFireball();
			case LARGE_FIREBALL: 
				return mainConfig.getPowerProjectileIdentifierLargeFireball();
			case WITHER_SKULL: 
				return mainConfig.getPowerProjectileIdentifierWitherSkull();
			default: 
				return null;
			}
		} else {
			return null;
		}
	}
	
	@Override
	public final ProjectileEnum getProjectileEnumByKeyLore(String keyLore) {
		if (keyLore != null) {
			for (ProjectileEnum key : ProjectileEnum.values()) {
				if (getPowerShootKeyLore(key).equalsIgnoreCase(keyLore)) {
					return key;
				}
			}
		}
		
		return null;
	}
}
