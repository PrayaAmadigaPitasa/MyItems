package net.ardeus.myitems.item;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import core.praya.agarthalib.enums.branch.ProjectileEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityItemWeapon;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.element.Element;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsOption;
import net.ardeus.myitems.manager.game.AbilityWeaponManager;
import net.ardeus.myitems.manager.game.ElementManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.manager.game.PowerCommandManager;
import net.ardeus.myitems.manager.game.PowerManager;
import net.ardeus.myitems.manager.game.PowerShootManager;
import net.ardeus.myitems.manager.game.PowerSpecialManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerCommand;
import net.ardeus.myitems.power.PowerSpecial;
import net.ardeus.myitems.power.PowerType;
import net.ardeus.myitems.socket.SocketGem;
import net.ardeus.myitems.socket.SocketGemTree;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public final class ItemConfig extends HandlerConfig {
	
	protected final Map<String, ItemStack> mapItem = new HashMap<String, ItemStack>();
	
	protected ItemConfig(MyItems plugin) {
		super(plugin);
	};
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapItem.clear();
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String path = dataManager.getPath("Path_File_Item");
		final File file = FileUtil.getFile(plugin, path);		
		
		convertOldDatabase();
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, path);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ItemStack item = config.getItemStack(key);
			
			if (item != null) {
				final ItemStack gameItem = convertToGame(item);
				
				EquipmentUtil.colorful(gameItem);
				
				this.mapItem.put(key, gameItem);
			}
		}
	}	
	
	protected final ItemStack convertToFile(ItemStack convertItem) {
		final GameManager gameManager = plugin.getGameManager();
		final PowerManager powerManager = gameManager.getPowerManager();
		final PowerCommandManager powerCommandManager = gameManager.getPowerCommandManager();
		final PowerShootManager powerShootManager = gameManager.getPowerShootManager();
		final PowerSpecialManager powerSpecialManager = gameManager.getPowerSpecialManager();;
		final AbilityWeaponManager abilityWeaponmanager = gameManager.getAbilityWeaponManager();
		final ElementManager elementManager = gameManager.getElementManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final ItemStack item = new ItemStack(convertItem);
		
		if (EquipmentUtil.hasDisplayName(item)) {
			EquipmentUtil.setDisplayName(item, EquipmentUtil.getDisplayName(item).replaceAll("�", "&"));
		}
		
		if (EquipmentUtil.loreCheck(item)) {
			final List<String> lores = EquipmentUtil.getLores(item);
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			for (int i = 0; i < lores.size(); i++) {
				final int line = i+1;
				final String lore = lores.get(i);
				
				if (statsManager.isLoreStats(lore)) {
					final LoreStatsEnum loreStats = statsManager.getLoreStats(lore);
					final double minValue = loreStats.equals(LoreStatsEnum.CRITICAL_DAMAGE) ? statsManager.getLoreValue(loreStats, LoreStatsOption.MIN, lore) + 1 : statsManager.getLoreValue(loreStats, LoreStatsOption.MIN, lore);
					final double maxValue = loreStats.equals(LoreStatsEnum.CRITICAL_DAMAGE) ? statsManager.getLoreValue(loreStats, LoreStatsOption.MAX, lore) + 1 : statsManager.getLoreValue(loreStats, LoreStatsOption.MAX, lore);
					
					if (minValue == maxValue) {
						
						String replacement = "@MyItems, LoreStats = {lorestats}, Value = {value}";
						
						mapPlaceholder.clear();
						mapPlaceholder.put("lorestats", String.valueOf(loreStats));
						mapPlaceholder.put("value", String.valueOf(minValue));
						replacement = TextUtil.placeholder(mapPlaceholder, replacement);
						
						EquipmentUtil.setLore(item, line, replacement);
					} else {
						
						String replacement = "@MyItems, LoreStats = {lorestats}, MinValue = {minvalue}, MaxValue = {maxvalue}";
						
						mapPlaceholder.clear();
						mapPlaceholder.put("lorestats", String.valueOf(loreStats));
						mapPlaceholder.put("minvalue", String.valueOf(minValue));
						mapPlaceholder.put("maxvalue", String.valueOf(maxValue));
						replacement = TextUtil.placeholder(mapPlaceholder, replacement);
						
						EquipmentUtil.setLore(item, line, replacement);
					}
				} else if (socketManager.isSocketEmptyLore(lore)){
					final String replacement = "@MyItems, SlotSocket = Empty";
					
					EquipmentUtil.setLore(item, line, replacement);
				} else if (socketManager.isSocketGemLore(lore)) {
					final SocketGem socketGem = socketManager.getSocketGemByLore(lore);
					final String socketGemId = socketGem.getGem();
					
					String replacement = "@MyItems, Socket = {socket}";
					
					mapPlaceholder.clear();
					mapPlaceholder.put("socket", String.valueOf(socketGemId));
					replacement = TextUtil.placeholder(mapPlaceholder, replacement);
					
					EquipmentUtil.setLore(item, line, replacement);
				} else if (abilityWeaponmanager.isAbilityItemWeapon(lore)) {
					final AbilityItemWeapon abilityItemWeapon = abilityWeaponmanager.getAbilityItemWeapon(lore);
					final String ability = abilityItemWeapon.getAbility();
					final int grade = abilityItemWeapon.getGrade();
					final double chance = abilityItemWeapon.getChance();
					
					String replacement = "@MyItems, Ability = {ability}, Grade = {grade}, Chance = {chance}";
					
					mapPlaceholder.clear();
					mapPlaceholder.put("ability", String.valueOf(ability));
					mapPlaceholder.put("grade", String.valueOf(grade));
					mapPlaceholder.put("chance", String.valueOf(chance));
					replacement = TextUtil.placeholder(mapPlaceholder, replacement);
					
					EquipmentUtil.setLore(item, line, replacement);
				} else if (elementManager.isElementLore(lore)) {
					final Element element = elementManager.getElementByLore(lore);
					final String elementId = element.getId();
					final double value = elementManager.getElementValue(lore);
					
					String replacement = "@MyItems, Element = {element}, Value = {value}";
					
					mapPlaceholder.clear();
					mapPlaceholder.put("element", elementId);
					mapPlaceholder.put("value", String.valueOf(value));
					replacement = TextUtil.placeholder(mapPlaceholder, replacement);
					
					EquipmentUtil.setLore(item, line, replacement);
				} else if (passiveEffectManager.isPassiveEffect(lore)) {
					final PassiveEffectEnum buff = passiveEffectManager.getPassiveEffect(lore);
					final int grade = passiveEffectManager.passiveEffectGrade(buff, lore);
					
					String replacement = "@MyItems, Buff = {buff}, Grade = {grade}";
					
					mapPlaceholder.clear();
					mapPlaceholder.put("buff", String.valueOf(buff));
					mapPlaceholder.put("grade", String.valueOf(grade));
					replacement = TextUtil.placeholder(mapPlaceholder, replacement);
					
					EquipmentUtil.setLore(item, line, replacement);
				} else if (powerManager.isLorePower(lore)) {
					final PowerType power = powerManager.getPowerTypeByLore(lore);
					final PowerClickType click = powerManager.getPowerClickTypeByLore(lore);
					final double cooldown = powerManager.getPowerCooldownByLore(lore);
					
					if (click != null) {
						if (power.equals(PowerType.COMMAND)) {
							final PowerCommand powerCommand = powerCommandManager.getPowerCommandByLore(lore);
							
							if (powerCommand != null) {
								final String powerCommandId = powerCommand.getId();
								
								String replacement = "@MyItems, Power = {power}, Click = {click}, Type = {type}, Cooldown = {cooldown}";
								
								mapPlaceholder.clear();
								mapPlaceholder.put("power", String.valueOf(power));
								mapPlaceholder.put("click", String.valueOf(click));
								mapPlaceholder.put("type", powerCommandId);
								mapPlaceholder.put("cooldown", String.valueOf(cooldown));
								replacement = TextUtil.placeholder(mapPlaceholder, replacement);
								
								EquipmentUtil.setLore(item, line, replacement);
							}
						} else if (power.equals(PowerType.SHOOT)) {
							final ProjectileEnum projectileEnum = powerShootManager.getProjectileEnumByLore(lore);
							
							if (projectileEnum != null) {
								final String projectileEnumId = String.valueOf(projectileEnum);
								
								String replacement = "@MyItems, Power = {power}, Click = {click}, Type = {type}, Cooldown = {cooldown}";
								
								mapPlaceholder.clear();
								mapPlaceholder.put("power", String.valueOf(power));
								mapPlaceholder.put("click", String.valueOf(click));
								mapPlaceholder.put("type", projectileEnumId);
								mapPlaceholder.put("cooldown", String.valueOf(cooldown));
								replacement = TextUtil.placeholder(mapPlaceholder, replacement);
								
								EquipmentUtil.setLore(item, line, replacement);
							}
						} else if (power.equals(PowerType.SPECIAL)) {
							final PowerSpecial powerSpecial = powerSpecialManager.getPowerSpecialByLore(lore);
							
							if (powerSpecial != null) {
								final String powerSpecialId = powerSpecial.getId();
								
								String replacement = "@MyItems, Power = {power}, Click = {click}, Type = {type}, Cooldown = {cooldown}";
								
								mapPlaceholder.clear();
								mapPlaceholder.put("power", String.valueOf(power));
								mapPlaceholder.put("click", String.valueOf(click));
								mapPlaceholder.put("type", powerSpecialId);
								mapPlaceholder.put("cooldown", String.valueOf(cooldown));
								replacement = TextUtil.placeholder(mapPlaceholder, replacement);
								
								EquipmentUtil.setLore(item, line, replacement);
							}
						}
					}
				}
			}
		}
		
		return item;
	}
	
	protected final ItemStack convertToGame(ItemStack convertItem) {
		final GameManager gameManager = plugin.getGameManager();
		final PowerCommandManager powerCommandManager = gameManager.getPowerCommandManager();
		final PowerShootManager powerShootManager = gameManager.getPowerShootManager();
		final PowerSpecialManager powerSpecialManager = gameManager.getPowerSpecialManager();
		final AbilityWeaponManager abilityWeaponManager = gameManager.getAbilityWeaponManager();
		final ElementManager elementManager = gameManager.getElementManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final ItemStack item = new ItemStack(convertItem);
		
		if (EquipmentUtil.loreCheck(item)) {
			final List<String> lores = EquipmentUtil.getLores(item);
			
			for (int i = 0; i < lores.size(); i++) {
				final int line = i+1;
				final String lore = lores.get(i).replaceAll(" ", "");
				
				if (lore.contains("@MyItems")) {
					final String[] part = lore.split(",");
					
					if (part.length > 1) {
						if (lore.contains("LoreStats=") || lore.contains("Stats=")) {
							
							LoreStatsEnum loreStats = null;
							double minValue = -1;
							double maxValue = -1;
							
							for (int t = 0; t < part.length; t++) {
								final String fullElement = part[t];
								final String[] element = fullElement.split("=");
								
								if (element.length == 2) {
									final String key = element[0];
									final String value = element[1];
									
									if (key.equalsIgnoreCase("LoreStats") || key.equalsIgnoreCase("Stats")) {
										
										loreStats = LoreStatsEnum.get(value);
												
									} else if (key.equalsIgnoreCase("MinValue") || key.equalsIgnoreCase("Value")) {
										if (MathUtil.isNumber(value)) {
											minValue = MathUtil.parseDouble(value);
										}
									} else if (key.equalsIgnoreCase("MaxValue") || key.equalsIgnoreCase("Max")) {
										if (MathUtil.isNumber(value)) {
											maxValue = MathUtil.parseDouble(value);
										}
									}
								}
							}
							
							if (loreStats != null) {
								if (minValue != -1) {					
									final String replacement = maxValue != -1 ? statsManager.getTextLoreStats(loreStats, minValue, maxValue) : statsManager.getTextLoreStats(loreStats, minValue);
										
									EquipmentUtil.setLore(item, line, replacement);
								}
							}
						} else if (lore.contains("SlotSocket=") || lore.contains("SlotSockets=")) {
								
								String slot = null;
								
								for (int t = 0; t < part.length; t++) {
									final String fullElement = part[t];
									final String[] element = fullElement.split("=");
									
									if (element.length == 2) {
										final String key = element[0];
										final String value = element[1];
										
										if (key.equalsIgnoreCase("SlotSocket") || key.equalsIgnoreCase("SlotSockets")) {
											
											slot = value;
										}
									}
								}
								
								if (slot != null) {
									if (slot.equalsIgnoreCase("Empty")) {
										final String replacement = socketManager.getTextSocketGemSlotEmpty();
										
										EquipmentUtil.setLore(item, line, replacement);
									}
								}
						} else if (lore.contains("Socket=") || lore.contains("Sockets=")) {
							
							SocketGem socket = null;
							
							for (int t = 0; t < part.length; t++) {
								final String fullElement = part[t];
								final String[] element = fullElement.split("=");
								
								if (element.length >= 2) {
									final String key = element[0];
									final String value = element[1];
									final String gradeText = element.length > 2 ? element[2] : String.valueOf(1);
									
									if (key.equalsIgnoreCase("Socket") || key.equalsIgnoreCase("Sockets")) {
										final int grade = MathUtil.isNumber(gradeText) ? MathUtil.parseInteger(gradeText) : 1;
										final SocketGemTree socketGemTree = socketManager.getSocketGemTree(value);
										
										if (socketGemTree != null) {
											socket = socketGemTree.getSocketGem(grade);
										}
									}
								}
							}
							
							if (socket != null) {
								final String gems = socket.getGem();
								final int grade = socket.getGrade();
								final String replacement = socketManager.getTextSocketGemsLore(gems, grade);
								
								EquipmentUtil.setLore(item, line, replacement);
							}
						} else if (lore.contains("Element=") || lore.contains("Elements=")) {
							
							String keyElement = null;
							double valueElement = 1;
							
							for (int t = 0; t < part.length; t++) {
								final String fullElement = part[t];
								final String[] element = fullElement.split("=");
								
								if (element.length == 2) {
									final String key = element[0];
									final String value = element[1];
									
									if (key.equalsIgnoreCase("Element") || key.equalsIgnoreCase("Elements")) {
										
										keyElement = value;
												
									} else if (key.equalsIgnoreCase("Value") || key.equalsIgnoreCase("Values")) {
										if (MathUtil.isNumber(value)) {
											valueElement = MathUtil.parseDouble(value);
										}
									}
								}
							}
							
							if (keyElement != null) {
								final String replacement = elementManager.getTextElement(keyElement, valueElement);
								
								EquipmentUtil.setLore(item, line, replacement);
							}
						} else if (lore.contains("Buff=") || lore.contains("Buffs=")) {
							
							PassiveEffectEnum effect = null;
							int grade = -1;
							
							for (int t = 0; t < part.length; t++) {
								final String fullElement = part[t];
								final String[] element = fullElement.split("=");
								
								if (element.length == 2) {
									final String key = element[0];
									final String value = element[1];
									
									if (key.equalsIgnoreCase("Buff") || key.equalsIgnoreCase("Buffs")) {
										
										effect = PassiveEffectEnum.get(value);
												
									} else if (key.equalsIgnoreCase("Grade") || key.equalsIgnoreCase("Grades")) {
										if (MathUtil.isNumber(value)) {
											grade = MathUtil.parseInteger(value);
										}
									}
								}
							}
							
							if (effect != null) {
								if (grade != -1) {
									final String replacement = passiveEffectManager.getTextPassiveEffect(effect, grade);
									
									EquipmentUtil.setLore(item, line, replacement);
								}
							}
						} else if (lore.contains("Ability=")) {
							
							String ability = null;
							int grade = -1;
							double chance = 0;
							
							for (int t = 0; t < part.length; t++) {
								final String fullElement = part[t];
								final String[] element = fullElement.split("=");
								
								if (element.length == 2) {
									final String key = element[0];
									final String value = element[1];
									
									if (key.equalsIgnoreCase("Ability")) {
										ability = value;
									} else if (key.equalsIgnoreCase("Grade")) {
										if (MathUtil.isNumber(value)) {
											grade = MathUtil.parseInteger(value);
										}
									} else if (key.equalsIgnoreCase("chance")) {
										if (MathUtil.isNumber(value)) {
											chance = MathUtil.parseDouble(value);
										}
									}
								}
							}
							
							if (ability != null) {
								final AbilityWeapon abilityWeapon = abilityWeaponManager.getAbilityWeapon(ability);
								
								if (abilityWeapon != null && grade != -1 && chance != 0) {
									final String replacement = abilityWeaponManager.getTextAbility(abilityWeapon, grade, chance);
									
									EquipmentUtil.setLore(item, line, replacement);
								}
							}
						} else if (lore.contains("Power=")) {
							
							PowerType power = null;
							PowerClickType click = null;
							double cooldown = 0;
							String type = null;
							
							for (int t = 0; t < part.length; t++) {
								final String fullElement = part[t];
								final String[] element = fullElement.split("=");
								
								if (element.length == 2) {
									final String key = element[0];
									final String value = element[1];
									
									if (key.equalsIgnoreCase("Power")) {
										
										power = PowerType.getPowerType(value);
												
									} else if (key.equalsIgnoreCase("Click")) {
										
										click = PowerClickType.getPowerClickType(value);
										
									} else if (key.equalsIgnoreCase("Cooldown")) {
										if (MathUtil.isNumber(value)) {
											cooldown = MathUtil.parseDouble(value);
										}
									} else if (key.equalsIgnoreCase("Type")) {
										
										type = value;
									}
								}
							}
							
							if (power != null) {
								if (click != null) {
									if (type != null) {
										if (power.equals(PowerType.COMMAND)) {
											final PowerCommand powerCommand = powerCommandManager.getPowerCommand(type);
											
											if (powerCommand != null) {
												final String replacement = powerCommandManager.getTextPowerCommand(click, powerCommand, cooldown);
												
												EquipmentUtil.setLore(item, line, replacement);
											}
										} else if (power.equals(PowerType.SHOOT)){
											final ProjectileEnum projectile = ProjectileEnum.getProjectileEnum(type);
											
											if (projectile != null) {
												final String replacement = powerShootManager.getTextPowerShoot(click, projectile, cooldown);
												
												EquipmentUtil.setLore(item, line, replacement);
											}
										} else if (power.equals(PowerType.SPECIAL)){
											final PowerSpecial powerSpecial = powerSpecialManager.getPowerSpecialByLore(lore);
											
											if (powerSpecial != null) {
												final String replacement = powerSpecialManager.getTextPowerSpecial(click, powerSpecial, cooldown);
												
												EquipmentUtil.setLore(item, line, replacement);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		return item;
	}
	
	
	
	public final void convertOldDatabase() {
		final File dirDatabase = FileUtil.getFile(plugin, "database");
		final ItemMemory itemMemory = ItemMemory.getInstance();
		
		if (dirDatabase.exists()) {
			final File oldItemsFile = FileUtil.getFile(plugin, "database/items.dat");
			
			if (oldItemsFile.exists()) {
				final HashMap<String, Object> oldItems = (HashMap<String, Object>) FileUtil.loadObjectSilent(oldItemsFile);
				
				for (String key : oldItems.keySet()) {
					final Object rawItem = oldItems.get(key);
					final ItemStack item = deserializeItemStack((HashMap<Map<String, Object>, Map<String, Object>>) rawItem);
					
					convertToFile(item);
					itemMemory.saveItem(item, key);
				}
				
				oldItemsFile.delete();
				dirDatabase.delete();
			}
		}
	}
	
	private ItemStack deserializeItemStack(HashMap<Map<String, Object>, Map<String, Object>> serializedItemStackMap) {
		final Entry<Map<String, Object>, Map<String, Object>> serializedItemStack = serializedItemStackMap.entrySet().iterator().next();
		final ItemStack itemStack = ItemStack.deserialize(serializedItemStack.getKey());
		
		if (serializedItemStack.getValue() != null) {
			final ItemMeta itemMeta = (ItemMeta) ConfigurationSerialization.deserializeObject(serializedItemStack.getValue(), ConfigurationSerialization.getClassByAlias("ItemMeta"));
			
			itemStack.setItemMeta(itemMeta);
		}
		
		return itemStack;
	}
	
	private final void moveOldFile() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathSource = "item.yml";
		final String pathTarget = dataManager.getPath("Path_File_Item");
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}
