package net.ardeus.myitems.item;

import net.ardeus.myitems.item.ItemSetBonusEffectStats.ItemSetBonusEffectStatsBuilder;

public class ItemSetBonusEffect {

	private final ItemSetBonusEffectStats effectStats;
	private final ItemSetBonusEffectAbilityWeapon effectAbilityWeapon;
	
	public ItemSetBonusEffect(ItemSetBonusEffectStats effectStats, ItemSetBonusEffectAbilityWeapon effectAbilityWeapon) {
		this.effectStats = effectStats != null ? effectStats : new ItemSetBonusEffectStatsBuilder().build();
		this.effectAbilityWeapon = effectAbilityWeapon != null ? effectAbilityWeapon : new ItemSetBonusEffectAbilityWeapon();
	}
	
	public final ItemSetBonusEffectStats getEffectStats() {
		return this.effectStats;
	}
	
	public final ItemSetBonusEffectAbilityWeapon getEffectAbilityWeapon() {
		return this.effectAbilityWeapon;
	}
}
