package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.item.ItemSet;
import net.ardeus.myitems.item.ItemSetComponent;
import net.ardeus.myitems.manager.game.ItemSetManager;

public final class ItemSetMemory extends ItemSetManager {

	private final ItemSetConfig itemSetConfig;
	
	private ItemSetMemory(MyItems plugin) {
		super(plugin);
		
		this.itemSetConfig = new ItemSetConfig(plugin);
	}
	
	private static class ItemSetMemorySingleton {
		private static final ItemSetMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new ItemSetMemory(plugin);
		}
	}
	
	public static final ItemSetMemory getInstance() {
		return ItemSetMemorySingleton.INSTANCE;
	}
	
	protected final ItemSetConfig getItemSetConfig() {
		return this.itemSetConfig;
	}
	
	@Override
	public final Collection<String> getItemSetIds() {
		final Map<String, ItemSet> mapItemSet = getItemSetConfig().mapItemSet;
		
		return new ArrayList<String>(mapItemSet.keySet());
	}
	
	@Override
	public final Collection<ItemSet> getAllItemSet() {
		final Map<String, ItemSet> mapItemSet = getItemSetConfig().mapItemSet;
		
		return new ArrayList<ItemSet>(mapItemSet.values());
	}
	
	@Override
	public final ItemSet getItemSet(String id) {
		if (id != null) {
			final Map<String, ItemSet> mapItemSet = getItemSetConfig().mapItemSet;
			
			for (String key : mapItemSet.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return mapItemSet.get(key);
				}
			}
		}
		
		return null;
	}
	
	
	@Override
	public final Collection<String> getItemComponentIds() {
		final Collection<String> itemComponentIds = new ArrayList<String>();
		final Map<String, ItemSet> mapItemSet = getItemSetConfig().mapItemSet;
		
		for (ItemSet itemSet : mapItemSet.values()) {
			for (ItemSetComponent itemSetComponent : itemSet.getAllItemSetComponent()) {
				final String itemComponentId = itemSetComponent.getId();
				
				itemComponentIds.add(itemComponentId);
			}
		}
		
		return itemComponentIds;
	}
	
	@Override
	public final ItemSetComponent getItemComponentByKeyLore(String keyLore) {
		if (keyLore != null) {
			final Map<String, ItemSet> mapItemSet = getItemSetConfig().mapItemSet;
			
			for (ItemSet itemSet : mapItemSet.values()) {
				for (ItemSetComponent key : itemSet.getAllItemSetComponent()) {
					if (key.getKeyLore().equalsIgnoreCase(keyLore)) {
						return key;
					}
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final ItemSet getItemSetByComponentId(String componentId) {
		if (componentId != null) {
			final Map<String, ItemSet> mapItemSet = getItemSetConfig().mapItemSet;
			
			for (ItemSet key : mapItemSet.values()) {
				for (ItemSetComponent itemSetComponent : key.getAllItemSetComponent()) {
					if (itemSetComponent.getId().equalsIgnoreCase(componentId)) {
						return key;
					}
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final ItemSet getItemSetByKeyLore(String keyLore) {
		if (keyLore != null) {
			final Map<String, ItemSet> mapItemSet = getItemSetConfig().mapItemSet;
			
			for (ItemSet key : mapItemSet.values()) {
				for (ItemSetComponent itemSetComponent : key.getAllItemSetComponent()) {
					if (itemSetComponent.getKeyLore().equalsIgnoreCase(keyLore)) {
						return key;
					}
				}
			}
		}
		
		return null;
	}
}