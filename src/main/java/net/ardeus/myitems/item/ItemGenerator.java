package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.TagsAttribute;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsModifier;
import net.ardeus.myitems.lorestats.LoreStatsUniversal;
import net.ardeus.myitems.lorestats.LoreStatsWeapon;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemTierManager;
import net.ardeus.myitems.manager.game.ItemTypeManager;
import net.ardeus.myitems.manager.plugin.PlaceholderManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MapUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public class ItemGenerator {

	private final String id;
	private final String displayName;
	private final boolean unbreakable;
	private final List<String> lores;
	private final List<String> flags;
	private final Map<ItemType, ItemGeneratorType> mapType;
	private final Map<ItemTier, ItemGeneratorTier> mapTier;
	
	public ItemGenerator(String id) {
		this(id, null);
	}
	
	public ItemGenerator(String id, String displayName) {
		this(id, displayName, false);
	}
	
	public ItemGenerator(String id, String displayName, boolean unbreakable) {
		this(id, displayName, unbreakable, null);
	}
	
	public ItemGenerator(String id, String displayName, boolean unbreakable, List<String> lores) {
		this(id, displayName, unbreakable, lores, null);
	}
	
	public ItemGenerator(String id, String displayName, boolean unbreakable, List<String> lores, List<String> flags) {
		this(id, displayName, unbreakable, lores, flags, null, null);
	}
	
	public ItemGenerator(String id, String displayName, boolean unbreakable, List<String> lores, List<String> flags, HashMap<ItemType, ItemGeneratorType> mapType, HashMap<ItemTier, ItemGeneratorTier> mapTier) {
		if (id == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.displayName = displayName;
			this.unbreakable = unbreakable;
			this.lores = lores != null ? flags : new ArrayList<String>();
			this.flags = flags != null ? lores : new ArrayList<String>();
			this.mapType = mapType != null ? mapType : new HashMap<ItemType, ItemGeneratorType>();
			this.mapTier = mapTier != null ? mapTier : new HashMap<ItemTier, ItemGeneratorTier>();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getDisplayName() {
		return this.displayName;
	}
	
	public final boolean isUnbreakable() {
		return this.unbreakable;
	}
	
	public final List<String> getLores() {
		return this.lores;
	}
	
	public final List<String> getFlags() {
		return this.flags;
	}
	
	public final Collection<ItemType> getAllItemType() {
		return new ArrayList<ItemType>(this.mapType.keySet());
	}
	
	public final Collection<ItemTier> getAllItemTier() {
		return new ArrayList<ItemTier>(this.mapTier.keySet());
	}
	
	public final ItemGeneratorType getItemGeneratorType(ItemType itemType) {
		return this.mapType.get(itemType);
	}
	
	public final ItemGeneratorTier getItemGeneratorTier(ItemTier itemTier) {
		return this.mapTier.get(itemTier);
	}
	
	public final HashMap<String, Integer> getMapPossibilityType() {
		return getMapPossibilityType(null);
	}
	
	public final HashMap<String, Integer> getMapPossibilityType(Slot slot) {
		final HashMap<String, Integer> mapPossibilityType = new HashMap<String, Integer>();
		
		for (ItemType itemType : this.mapType.keySet()) {
			final Slot slotDefault = itemType.getDefaultSlot();
			
			if (slot == null || slot.equals(slotDefault)) {
				final String id = itemType.getId();
				final ItemGeneratorType itemGeneratorType = getItemGeneratorType(itemType);
				final int possibility = itemGeneratorType.getPossibility();
				
				mapPossibilityType.put(id, possibility);
			}
		}
		
		return mapPossibilityType;
	}
	
	public final HashMap<String, Integer> getMapPossibilityTier() {
		final HashMap<String, Integer> mapPossibilityTier = new HashMap<String, Integer>();
		
		for (ItemTier key : getAllItemTier()) {
			final String id = key.getId();
			final ItemGeneratorTier itemGeneratorTier = getItemGeneratorTier(key);
			final int possibility = itemGeneratorTier.getPossibility();
			
			mapPossibilityTier.put(id, possibility);
		}
		
		return mapPossibilityTier;
	}
	
	public final ItemStack generateItem() {
		return generateItem(null, null);
	}
	
	public final ItemStack generateItem(Slot slot) {
		return generateItem(slot, null);
	}
	
	public final ItemStack generateItem(ItemTier itemTier) {
		return generateItem(null, itemTier);
	}
	
	public final ItemStack generateItem(Slot slot, ItemTier itemTier) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final ItemTypeManager itemTypeManager = gameManager.getItemTypeManager();
		final ItemTierManager itemTierManager = gameManager.getItemTierManager();
		final HashMap<String, Integer> mapPossibilityType = getMapPossibilityType(slot);
		
		if (!mapPossibilityType.isEmpty()) {
			final String idType = MapUtil.getRandomIdByInteger(mapPossibilityType);
			final ItemType itemType = itemTypeManager.getItemType(idType);
			
			if (itemTier == null) {
				final HashMap<String, Integer> mapPossibilityTier = getMapPossibilityTier();
				
				if (!mapPossibilityTier.isEmpty()) {
					final String idTier = MapUtil.getRandomIdByInteger(mapPossibilityTier);
					
					itemTier = itemTierManager.getItemTier(idTier);
				}
			}
			
			if (itemType != null && itemType != null) {
				final Material material = itemType.getMaterial();
				final short data = itemType.getData();
				final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(material, data);
				
				if (materialEnum != null) {
					final boolean shiny = itemType.isShiny();
					final ItemGeneratorType itemGeneratorType = getItemGeneratorType(itemType);
					final ItemGeneratorTier itemGeneratorTier = getItemGeneratorTier(itemTier);
					final LoreStatsModifier typeModifier = itemType.getStatsModifier();
					final LoreStatsModifier tierModifier = itemTier.getStatsModifier();
					final List<String> description = itemGeneratorType.getDescription();
					final List<String> names = itemGeneratorType.getNames();
					final List<String> additionalLores = itemGeneratorTier.getAdditionalLores();
					final Collection<Enchantment> enchantments = itemType.getEnchantments();
					final Collection<Slot> allSlotNBT = itemType.getAllSlotNBT();
					final int random = (int) (Math.random() * names.size());
					final String divider = "\n";
					final String lineDescription = TextUtil.convertListToString(description, divider);
					final String lineLore = TextUtil.convertListToString(lores, divider);
					final String lineAdditionalLores = TextUtil.convertListToString(additionalLores, divider);
					final String name = !names.isEmpty() ? names.get(MathUtil.limitInteger(random, 0, names.size()-1)) : null;
					final HashMap<String, String> map = new HashMap<String, String>();
					final HashMap<LoreStatsEnum, Double> mapStatsModifier = new HashMap<LoreStatsEnum, Double>();
					
					for (LoreStatsEnum loreStats : LoreStatsEnum.values()) {
						final double typeValue = typeModifier.getModifier(loreStats);
						final double tierValue = tierModifier.getModifier(loreStats);
						final double finalValue = typeValue * tierValue;
						
						mapStatsModifier.put(loreStats, finalValue);
					}
					
					final double damage = mapStatsModifier.get(LoreStatsEnum.DAMAGE);
					final double penetration = mapStatsModifier.get(LoreStatsEnum.PENETRATION);
					final double pvpDamage = mapStatsModifier.get(LoreStatsEnum.PVP_DAMAGE);
					final double pveDamage = mapStatsModifier.get(LoreStatsEnum.PVE_DAMAGE);
					final double criticalChance = mapStatsModifier.get(LoreStatsEnum.CRITICAL_CHANCE);
					final double criticalDamage = mapStatsModifier.get(LoreStatsEnum.CRITICAL_DAMAGE);
					final double hitRate = mapStatsModifier.get(LoreStatsEnum.HIT_RATE);
					final double defense = mapStatsModifier.get(LoreStatsEnum.DEFENSE);
					final double pvpDefense = mapStatsModifier.get(LoreStatsEnum.PVP_DEFENSE);
					final double pveDefense = mapStatsModifier.get(LoreStatsEnum.PVE_DEFENSE);
					final double health = mapStatsModifier.get(LoreStatsEnum.HEALTH);
					final double healthRegen = mapStatsModifier.get(LoreStatsEnum.HEALTH_REGEN);
					final double staminaMax = mapStatsModifier.get(LoreStatsEnum.STAMINA_MAX);
					final double staminaRegen = mapStatsModifier.get(LoreStatsEnum.STAMINA_REGEN);
					final double attackAoERadius = mapStatsModifier.get(LoreStatsEnum.ATTACK_AOE_RADIUS);
					final double attackAoEDamage = mapStatsModifier.get(LoreStatsEnum.ATTACK_AOE_DAMAGE);
					final double blockAmount = mapStatsModifier.get(LoreStatsEnum.BLOCK_AMOUNT);
					final double blockRate = mapStatsModifier.get(LoreStatsEnum.BLOCK_RATE);
					final double dodgeRate = mapStatsModifier.get(LoreStatsEnum.DODGE_RATE);
					final double durability = mapStatsModifier.get(LoreStatsEnum.DURABILITY);
					final double level = mapStatsModifier.get(LoreStatsEnum.LEVEL);
					
					final LoreStatsWeapon weaponModifier = new LoreStatsWeapon(damage, penetration, pvpDamage, pveDamage, attackAoERadius, attackAoEDamage, criticalChance, criticalDamage, hitRate);
					final LoreStatsArmor armorModifier = new LoreStatsArmor(defense, pvpDefense, pveDefense, health, healthRegen, staminaMax, staminaRegen, blockAmount, blockRate, dodgeRate);
					final LoreStatsUniversal universalModifier = new LoreStatsUniversal(durability, level);
					final LoreStatsModifier statsModifier = new LoreStatsModifier(weaponModifier, armorModifier, universalModifier);
					
					String loreBuilder = TextUtil.placeholder(lineLore, "description", lineDescription);
					String display = displayName; 
									
					map.put("Name", name);
					map.put("Type_ID", idType);
					map.put("Tier_ID", itemTier.getId());
					map.put("Tier_Name", itemTier.getName());
					map.put("Tier_Prefix", itemTier.getPrefix());
					
					display = TextUtil.placeholder(map, display);
					display = TextUtil.colorful(display);
					loreBuilder = !lineAdditionalLores.isEmpty() ? loreBuilder + divider + lineAdditionalLores : loreBuilder;
					loreBuilder = TextUtil.placeholder(map, loreBuilder);
					loreBuilder = placeholderManager.placeholder(null, loreBuilder, statsModifier);
					loreBuilder = TextUtil.colorful(loreBuilder);
					
					final String[] finalLores = loreBuilder.split(divider);
					final ItemStack item = EquipmentUtil.createItem(materialEnum, display, 1, finalLores);
					
					for (String flag : flags) {
						EquipmentUtil.addFlag(item, flag);
					}
					
					for (Enchantment enchantment : enchantments) {
						final int grade = itemType.getEnchantmentGrade(enchantment);
						
						EquipmentUtil.addEnchantment(item, enchantment, grade);
					}
					
					for (Slot slotNBT : allSlotNBT) {
						final ItemTypeNBT itemTypeNBT = itemType.getSlotNBT(slotNBT);
						
						for (TagsAttribute tagsAttribute : itemTypeNBT.getAllTagsAttribute()) {
							final double tagsValue = itemTypeNBT.getTagsAttributeValue(tagsAttribute);
							
							Bridge.getBridgeTagsNBT().addNBT(item, tagsAttribute, tagsValue, slotNBT);
						}
					}
					
					if (shiny) {
						EquipmentUtil.shiny(item);
					}
					
					if (unbreakable) {
						Bridge.getBridgeTagsNBT().setUnbreakable(item, true);
					}
					
					return item;
				}
			}
		}
		
		return null;
	}
}
