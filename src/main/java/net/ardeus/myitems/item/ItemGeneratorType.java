package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.List;

public class ItemGeneratorType {

	private final int possibility;
	private final List<String> description;
	private final List<String> names;
	
	public ItemGeneratorType(int possibility) {
		this(possibility, null);
	}
	
	public ItemGeneratorType(int possibility, List<String> description) {
		this(possibility, description, null);
	}
	
	public ItemGeneratorType(int possibility, List<String> description, List<String> names) {
		this.possibility = possibility;
		this.description = description != null ? description : new ArrayList<String>();
		this.names = names != null ? names : new ArrayList<String>();
	}
	
	public final int getPossibility() {
		return this.possibility;
	}
	
	public final List<String> getDescription() {
		return this.description;
	}
	
	public final List<String> getNames() {
		return this.names;
	}
}
