package net.ardeus.myitems.item;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;

import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityItemWeapon;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.item.ItemSetBonusEffectStats.ItemSetBonusEffectStatsBuilder;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EnchantmentUtil;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.MaterialUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public final class ItemSetConfig extends HandlerConfig {
	
	protected final Map<String, ItemSet> mapItemSet = new HashMap<String, ItemSet>();
	
	protected ItemSetConfig(MyItems plugin) {
		super(plugin);
	};
	
	public final void setup() {
		reset();
		loadConfig();
	}
	
	private final void reset() {		
		this.mapItemSet.clear();
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathDefault = dataManager.getPath("Path_File_Item_Set");
		final String pathFolder = dataManager.getPath("Path_Folder_Item_Set");
		final File fileDefault = FileUtil.getFile(plugin, pathDefault);		
		final File fileFolder = FileUtil.getFile(plugin, pathFolder);
		
		if (!fileDefault.exists()) {
			FileUtil.saveResource(plugin, pathDefault);
		}
		
		for (File file : fileFolder.listFiles()) {
			final FileConfiguration config = FileUtil.getFileConfiguration(file);
			
			for (String key : config.getKeys(false)) {
				final ConfigurationSection mainDataSection = config.getConfigurationSection(key);
				final HashMap<Integer, ItemSetBonus> mapBonus = new HashMap<Integer, ItemSetBonus>();
				final HashMap<String, ItemSetComponent> mapComponent = new HashMap<String, ItemSetComponent>();
				
				String name = key.replace("_", " ");
				
				for (String mainData : mainDataSection.getKeys(false)) {
					if (mainData.equalsIgnoreCase("Name")) {
						name = mainDataSection.getString(mainData);
					} else if (mainData.equalsIgnoreCase("Bonus")) {
						final ConfigurationSection bonusAmountSection = mainDataSection.getConfigurationSection(mainData);
						
						for (String bonusAmount : bonusAmountSection.getKeys(false)) {
							if (MathUtil.isNumber(bonusAmount)) {
								final ConfigurationSection bonusDataSection = bonusAmountSection.getConfigurationSection(bonusAmount);
								final int amountID = MathUtil.parseInteger(bonusAmount);
								final List<String> description = new ArrayList<String>();
								
								ItemSetBonusEffectStats bonusEffectStats = new ItemSetBonusEffectStatsBuilder().build();
								ItemSetBonusEffectAbilityWeapon bonusEffectAbilityWeapon = new ItemSetBonusEffectAbilityWeapon();
								
								for (String bonusData : bonusDataSection.getKeys(false)) {
									if (bonusData.equalsIgnoreCase("Description")) {
										if (bonusDataSection.isList(bonusData)) {
											for (String descriptionLine : bonusDataSection.getStringList(bonusData)) {
												description.add(ChatColor.stripColor(TextUtil.colorful(descriptionLine)));
											}
										} else if (bonusDataSection.isString(bonusData)) {
											description.add(ChatColor.stripColor(TextUtil.colorful(bonusDataSection.getString(bonusData))));
										}
									} else if (bonusData.equalsIgnoreCase("Effect")) {
										final ConfigurationSection effectDataSection = bonusDataSection.getConfigurationSection(bonusData);
										
										for (String effectData : effectDataSection.getKeys(false)) {
											if (effectData.equalsIgnoreCase("Stats")) {
												final ConfigurationSection effectStatsSection = effectDataSection.getConfigurationSection(effectData);
												
												double additionalDamage = 0;
												double percentDamage = 0;
												double penetration = 0;
												double pvpDamage = 0;
												double pveDamage = 0;
												double additionalDefense = 0;
												double percentDefense = 0;
												double health = 0;
												double healthRegen = 0;
												double staminaMax = 0;
												double staminaRegen = 0;
												double attackAoERadius = 0;
												double attackAoEDamage = 0;
												double pvpDefense = 0;
												double pveDefense = 0;
												double criticalChance = 0;
												double criticalDamage = 0;
												double blockAmount = 0;
												double blockRate = 0;
												double hitRate = 0;
												double dodgeRate = 0;
												
												for (String effectStats : effectStatsSection.getKeys(false)) {
													if (effectStats.equalsIgnoreCase("Additional_Damage") || effectStats.equalsIgnoreCase("Damage")) {
														additionalDamage = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Percent_Damage")) {
														percentDamage = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Penetration")) {
														penetration = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("PvP_Damage")) {
														pvpDamage = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("PvE_Damage")) {
														pveDamage = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Additional_Defense") || effectStats.equalsIgnoreCase("Defense")) {
														additionalDefense = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Percent_Defense")) {
														percentDefense = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Health")) {
														health = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Health_Regen") || effectStats.equalsIgnoreCase("Regen") || effectStats.equalsIgnoreCase("Regeneration")) {
														healthRegen = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Stamina_Max") || effectStats.equalsIgnoreCase("Max_Stamina")) {
														staminaMax = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Stamina_Regen") || effectStats.equalsIgnoreCase("Regen_Stamina")) {
														staminaRegen = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Attack_AoE_Radius")) {
														attackAoERadius = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Attack_AoE_Damage")) {
														attackAoEDamage = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("PvP_Defense")) {
														pvpDefense = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("PvE_Defense")) {
														pveDefense = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Critical_Chance")) {
														criticalChance = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Critical_Damage")) {
														criticalDamage = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Block_Amount")) {
														blockAmount = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Block_Rate")) {
														blockRate = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Hit_Rate")) {
														hitRate = effectStatsSection.getDouble(effectStats);
													} else if (effectStats.equalsIgnoreCase("Dodge_Rate")) {
														dodgeRate = effectStatsSection.getDouble(effectStats);
													}
												}
												
												final ItemSetBonusEffectStatsBuilder itemSetBonusEffectStatsBuilder = new ItemSetBonusEffectStatsBuilder();
												
												itemSetBonusEffectStatsBuilder.setAdditionalDamage(additionalDamage);
												itemSetBonusEffectStatsBuilder.setPercentDamage(percentDamage);
												itemSetBonusEffectStatsBuilder.setPenetration(penetration);
												itemSetBonusEffectStatsBuilder.setPvPDamage(pvpDamage);
												itemSetBonusEffectStatsBuilder.setPvEDamage(pveDamage);
												itemSetBonusEffectStatsBuilder.setAdditionalDefense(additionalDefense);
												itemSetBonusEffectStatsBuilder.setPercentDefense(percentDefense);
												itemSetBonusEffectStatsBuilder.setHealth(health);
												itemSetBonusEffectStatsBuilder.setHealthRegen(healthRegen);
												itemSetBonusEffectStatsBuilder.setStaminaMax(staminaMax);
												itemSetBonusEffectStatsBuilder.setStaminaRegen(staminaRegen);
												itemSetBonusEffectStatsBuilder.setAttackAoERadius(attackAoERadius);
												itemSetBonusEffectStatsBuilder.setAttackAoEDamage(attackAoEDamage);
												itemSetBonusEffectStatsBuilder.setPvPDefense(pvpDefense);
												itemSetBonusEffectStatsBuilder.setPvEDefense(pveDefense);
												itemSetBonusEffectStatsBuilder.setCriticalChance(criticalChance);
												itemSetBonusEffectStatsBuilder.setCriticalDamage(criticalDamage);
												itemSetBonusEffectStatsBuilder.setBlockAmount(blockAmount);
												itemSetBonusEffectStatsBuilder.setBlockRate(blockRate);
												itemSetBonusEffectStatsBuilder.setHitRate(hitRate);
												itemSetBonusEffectStatsBuilder.setDodgeRate(dodgeRate);
												
												bonusEffectStats = itemSetBonusEffectStatsBuilder.build();
											} else if (effectData.equalsIgnoreCase("Ability_Weapon")) {
												final ConfigurationSection effectAbilityWeaponSection = effectDataSection.getConfigurationSection(effectData);
												final HashMap<String, AbilityItemWeapon> mapAbilityItem = new HashMap<String, AbilityItemWeapon>();
												
												for (String effectAbilityWeapon : effectAbilityWeaponSection.getKeys(false)) {
													final ConfigurationSection effectAbilityWeaponDataSection = effectAbilityWeaponSection.getConfigurationSection(effectAbilityWeapon);
													
													double chance = 100;
													int grade = 0;
													
													for (String effectAbilityWeaponData : effectAbilityWeaponDataSection.getKeys(false)) {
														if (effectAbilityWeaponData.equalsIgnoreCase("Chance")) {
															chance = MathUtil.limitDouble(effectAbilityWeaponDataSection.getDouble(effectAbilityWeaponData), 0, 100);
														} else if (effectAbilityWeaponData.equalsIgnoreCase("Grade")) {
															grade = effectAbilityWeaponDataSection.getInt(effectAbilityWeaponData);
														}
													}
													
													if (grade > 0) {
														final AbilityItemWeapon abilityItemWeapon = new AbilityItemWeapon(effectAbilityWeapon, grade, chance);
														
														mapAbilityItem.put(effectAbilityWeapon, abilityItemWeapon);
													}
												}
												
												bonusEffectAbilityWeapon = new ItemSetBonusEffectAbilityWeapon(mapAbilityItem);
											}
										}
									}
								}
								
								final ItemSetBonusEffect itemSetBonusEffect = new ItemSetBonusEffect(bonusEffectStats, bonusEffectAbilityWeapon);
								final ItemSetBonus itemSetBonus = new ItemSetBonus(amountID, description, itemSetBonusEffect);
								
								mapBonus.put(amountID, itemSetBonus);
							}
						}
					} else if (mainData.equalsIgnoreCase("Component")) {
						final ConfigurationSection componentSection = mainDataSection.getConfigurationSection(mainData);
						
						for (String component : componentSection.getKeys(false)) {
							final ConfigurationSection componentDataSection = componentSection.getConfigurationSection(component);
							final String componentID = key + "_" + component;
							final List<String> lores = new ArrayList<String>();
							final List<String> flags = new ArrayList<String>();
							final Set<Slot> slots = new HashSet<Slot>();
							final HashMap<Enchantment, Integer> mapEnchantment = new HashMap<Enchantment, Integer>();
							
							String keyLore = componentID.replace("_", " ");
							String displayName = null;
							Material material = null;
							boolean shiny = false;
							boolean unbreakable = false;
							short data = 0;
							
							for (String componentData : componentDataSection.getKeys(false)) {
								if (componentData.equalsIgnoreCase("KeyLore")) {
									keyLore = ChatColor.stripColor(TextUtil.colorful(componentDataSection.getString(componentData)));
								} else if (componentData.equalsIgnoreCase("Display_Name") || componentData.equalsIgnoreCase("Display") || componentData.equalsIgnoreCase("Name")) {
									displayName = componentDataSection.getString(componentData);
								} else if (componentData.equalsIgnoreCase("Material")) {
									material = MaterialUtil.getMaterial(componentDataSection.getString(componentData));
								} else if (componentData.equalsIgnoreCase("Data")) {
									data = (short) componentDataSection.getInt(componentData);
								} else if (componentData.equalsIgnoreCase("Shiny")) {
									shiny = componentDataSection.getBoolean(componentData);
								} else if (componentData.equalsIgnoreCase("Unbreakable")) {
									unbreakable = componentDataSection.getBoolean(componentData);
								} else if (componentData.equalsIgnoreCase("Lores") || componentData.equalsIgnoreCase("Lore")) {
									lores.addAll(componentDataSection.getStringList(componentData));
								} else if (componentData.equalsIgnoreCase("Flags") || componentData.equalsIgnoreCase("ItemFlags")) {
									flags.addAll(componentDataSection.getStringList(componentData));
								} else if (componentData.equalsIgnoreCase("Slots") || componentData.equalsIgnoreCase("Slot")) {
									if (componentDataSection.isString(componentData)) {
										final String slotData = componentDataSection.getString(componentData);
										final Slot slot = Slot.get(slotData);
										
										if (slot != null) {
											slots.add(slot);
										}
									} else if (componentDataSection.isList(componentData)) {
										final List<String> listSlotData = componentDataSection.getStringList(componentData);
										
										for (String slotData : listSlotData) {
											final Slot slot = Slot.get(slotData);
											
											if (slot != null) {
												slots.add(slot);
											}
										}
									}
								} else if (componentData.equalsIgnoreCase("Enchantments") || componentData.equalsIgnoreCase("Enchantment")) {
									final ConfigurationSection enchantmentDataSection = componentDataSection.getConfigurationSection(componentData);
									
									for (String enchantmentData : enchantmentDataSection.getKeys(false)) {
										final Enchantment enchantment = EnchantmentUtil.getEnchantment(enchantmentData);
										
										if (enchantment != null) {
											final int grade = enchantmentDataSection.getInt(enchantmentData);
											
											mapEnchantment.put(enchantment, grade);
										}
									}
								}
							}
							
							if (slots.isEmpty()) {
								for (Slot slot : Slot.values()) {
									slots.add(slot);
								}
							}
							
							if (material != null) {
								final ItemSetComponentItem itemSetComponentItem = new ItemSetComponentItem(material, displayName, data, shiny, unbreakable, lores, flags, mapEnchantment);
								final ItemSetComponent itemSetComponent = new ItemSetComponent(key, componentID, keyLore, itemSetComponentItem, slots);
								
								mapComponent.put(componentID, itemSetComponent);
							}
						}
					}
				}
				
				final ItemSet itemSet = new ItemSet(key, name, mapBonus, mapComponent);
				
				this.mapItemSet.put(key, itemSet);
			}
		}
	}
}