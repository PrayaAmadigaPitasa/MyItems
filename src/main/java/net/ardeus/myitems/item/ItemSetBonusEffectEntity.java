package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.item.ItemSetBonusEffectStats.ItemSetBonusEffectStatsBuilder;

public class ItemSetBonusEffectEntity {

	private final ItemSetBonusEffectStats effectStats;
	private final Map<AbilityWeapon, Integer> mapAbilityWeapon;
	
	public ItemSetBonusEffectEntity(ItemSetBonusEffectStats effectStats, Map<AbilityWeapon, Integer> mapAbilityWeapon) {
		this.effectStats = effectStats != null ? effectStats : new ItemSetBonusEffectStatsBuilder().build();
		this.mapAbilityWeapon = mapAbilityWeapon != null ? mapAbilityWeapon : new HashMap<AbilityWeapon, Integer>();
	}
	
	public final ItemSetBonusEffectStats getEffectStats() {
		return this.effectStats;
	}
	
	public final Collection<AbilityWeapon> getAllAbilityWeapon() {
		return new ArrayList<AbilityWeapon>(this.mapAbilityWeapon.keySet());
	}
	
	public final int getGradeAbilityWeapon(AbilityWeapon abilityWeapon) {
		return this.mapAbilityWeapon.get(abilityWeapon);
	}
}
