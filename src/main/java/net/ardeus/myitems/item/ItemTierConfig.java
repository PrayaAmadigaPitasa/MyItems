package net.ardeus.myitems.item;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.lorestats.LoreStatsModifier;
import net.ardeus.myitems.lorestats.LoreStatsUniversal;
import net.ardeus.myitems.lorestats.LoreStatsWeapon;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.TextUtil;

public final class ItemTierConfig extends HandlerConfig {
	
	protected final Map<String, ItemTier> mapTier = new HashMap<String, ItemTier>();
	
	protected ItemTierConfig(MyItems plugin) {
		super(plugin);
	};
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapTier.clear();
		
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathDefault = dataManager.getPath("Path_File_Item_Tier");
		final String pathFolder = dataManager.getPath("Path_Folder_Item_Tier");
		final File fileDefault = FileUtil.getFile(plugin, pathDefault);
		final File fileFolder = FileUtil.getFile(plugin, pathFolder);		
		
		if (!fileDefault.exists()) {
			FileUtil.saveResource(plugin, pathDefault);
		}
		
		for (File file : fileFolder.listFiles()) {
			final FileConfiguration config = FileUtil.getFileConfiguration(file);
			
			for (String key : config.getKeys(false)) {
				final ConfigurationSection mainDataSection = config.getConfigurationSection(key);
				
				String name = "";
				String prefix = "";
				LoreStatsModifier statsModifier = new LoreStatsModifier();
				
				for (String mainData : mainDataSection.getKeys(false)) {
					if (mainData.equalsIgnoreCase("Name")) {
						name = TextUtil.colorful(mainDataSection.getString(mainData));
					} else if (mainData.equalsIgnoreCase("Prefix")) {
						prefix = TextUtil.colorful(mainDataSection.getString(mainData));
					} else if (mainData.equalsIgnoreCase("Modifier")) {
						final ConfigurationSection modifierDataSection = mainDataSection.getConfigurationSection(mainData);
						
						double damage = 1;
						double penetration = 1;
						double pvpDamage = 1;
						double pveDamage = 1;
						double criticalChance = 1;
						double criticalDamage = 1;
						double hitRate = 1;
						double defense = 1;
						double pvpDefense = 1;
						double pveDefense = 1;
						double health = 1;
						double healthRegen = 1;
						double staminaMax = 1;
						double staminaRegen = 1;
						double attackAoERadius = 1;
						double attackAoEDamage = 1;
						double blockAmount = 1;
						double blockRate = 1;
						double dodgeRate = 1;
						double durability = 1;
						double level = 1;
						
						for (String modifierData : modifierDataSection.getKeys(false)) {
							if (modifierData.equalsIgnoreCase("Damage")) {
								damage = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Penetration")) {
								penetration = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("PvP_Damage")) {
								pvpDamage = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("PvE_Damage")) {
								pveDamage = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Critical_Chance")) {
								criticalChance = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Critical_Damage")) {
								criticalDamage = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Hit_Rate")) {
								hitRate = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Defense")) {
								defense = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("PvP_Defense")) {
								pvpDefense = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("PvE_Defense")) {
								pveDefense = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Health")) {
								health = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Health_Regen")) {
								healthRegen = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Stamina_Max")) {
								staminaMax = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Stamina_Regen")) {
								staminaRegen = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Attack_AoE_Radius")) {
								attackAoERadius = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Attack_AoE_Damage")) {
								attackAoEDamage = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Block_Amount")) {
								blockAmount = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Block_Rate")) {
								blockRate = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Dodge_Rate")) {
								dodgeRate = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Durability")) {
								durability = modifierDataSection.getDouble(modifierData);
							} else if (modifierData.equalsIgnoreCase("Level")) {
								level = modifierDataSection.getDouble(modifierData);
							}
						}
						
						final LoreStatsWeapon weaponModifier = new LoreStatsWeapon(damage, penetration, pvpDamage, pveDamage, attackAoERadius, attackAoEDamage, criticalChance, criticalDamage, hitRate);
						final LoreStatsArmor armorModifier = new LoreStatsArmor(defense, pvpDefense, pveDefense, health, healthRegen, staminaMax, staminaRegen, blockAmount, blockRate, dodgeRate);
						final LoreStatsUniversal universalModifier = new LoreStatsUniversal(durability, level);
						
						statsModifier = new LoreStatsModifier(weaponModifier, armorModifier, universalModifier);
					}
				}
				
				final ItemTier itemTier = new ItemTier(key, name, prefix, statsModifier);
				
				mapTier.put(key, itemTier);
			}
		}
	}
	
	private final void moveOldFile() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathSource_1 = "item_tier.yml";
		final String pathSource_2 = "Configuration/item_tier.yml";
		final String pathTarget = dataManager.getPath("Path_File_Item_Tier");
		final File fileSource_1 = FileUtil.getFile(plugin, pathSource_1);
		final File fileSource_2 = FileUtil.getFile(plugin, pathSource_2);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource_1.exists()) {
			FileUtil.moveFileSilent(fileSource_1, fileTarget);
		} else if (fileSource_2.exists()) {
			FileUtil.moveFileSilent(fileSource_2, fileTarget);
		}
	}
}