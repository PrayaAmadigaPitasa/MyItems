package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.ardeus.myitems.ability.AbilityItemWeapon;

public class ItemSetBonusEffectAbilityWeapon {

	private final Map<String, AbilityItemWeapon> mapAbilityItemWeapon;
	
	public ItemSetBonusEffectAbilityWeapon() {
		this(null);
	}
	
	public ItemSetBonusEffectAbilityWeapon(Map<String, AbilityItemWeapon> mapAbilityItemWeapon) {
		this.mapAbilityItemWeapon = mapAbilityItemWeapon != null ? mapAbilityItemWeapon : new HashMap<String, AbilityItemWeapon>();
	}
	
	public final Collection<String> getAbilityIds() {
		return new ArrayList<String>(this.mapAbilityItemWeapon.keySet());
	}
	
	public final Collection<AbilityItemWeapon> getAllAbilityItemWeapon() {
		return new ArrayList<AbilityItemWeapon>(this.mapAbilityItemWeapon.values());
	}
	
	public final AbilityItemWeapon getAbilityItemWeapon(String ability) {
		if (ability != null) {
			for (String key : this.mapAbilityItemWeapon.keySet()) {
				if (key.equalsIgnoreCase(ability)) {
					return this.mapAbilityItemWeapon.get(key);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isAbilityExists(String ability) {
		return getAbilityItemWeapon(ability) != null;
	}
}
