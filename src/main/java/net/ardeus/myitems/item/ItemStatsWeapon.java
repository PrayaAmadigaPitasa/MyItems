package net.ardeus.myitems.item;

import com.praya.agarthalib.utility.MathUtil;

public class ItemStatsWeapon {

	private final double totalDamageMin;
	private final double totalDamageMax;
	private final double totalPenetration;
	private final double totalPvPDamage;
	private final double totalPvEDamage;
	private final double totalAttackAoERadius;
	private final double totalAttackAoEDamage;
	private final double totalCriticalChance;
	private final double totalCriticalDamage;
	private final double totalHitRate;
	private final double socketDamage;
	private final double elementDamage;
		
	private ItemStatsWeapon(double totalDamageMin, double totalDamageMax, double totalPenetration, double totalPvPDamage, double totalPvEDamage, double totalAttackAoERadius, double totalAttackAoEDamage, double totalCriticalChance, double totalCriticalDamage, double totalHitRate, double socketDamage, double elementDamage) {
		this.totalDamageMin = totalDamageMin;
		this.totalDamageMax = totalDamageMax;
		this.totalPenetration = totalPenetration;
		this.totalPvPDamage = totalPvPDamage;
		this.totalPvEDamage = totalPvEDamage;
		this.totalAttackAoERadius = totalAttackAoERadius;
		this.totalAttackAoEDamage = totalAttackAoEDamage;
		this.totalCriticalChance = MathUtil.limitDouble(totalCriticalChance, 0, 100);
		this.totalCriticalDamage = totalCriticalDamage;
		this.totalHitRate = totalHitRate;
		this.socketDamage = socketDamage;
		this.elementDamage = elementDamage;
	}
	
	public final double getTotalDamageMin() {
		return this.totalDamageMin;
	}
	
	public final double getTotalDamageMax() {
		return this.totalDamageMax;
	}
	
	public final double getTotalPenetration() {
		return this.totalPenetration;
	}
	
	public final double getTotalPvPDamage() {
		return this.totalPvPDamage;
	}
	
	public final double getTotalPvEDamage() {
		return this.totalPvEDamage;
	}
	
	public final double getTotalAttackAoERadius() {
		return this.totalAttackAoERadius;
	}
	
	public final double getTotalAttackAoEDamage() {
		return this.totalAttackAoEDamage;
	}
	
	public final double getTotalCriticalChance() {
		return this.totalCriticalChance;
	}
	
	public final double getTotalCriticalDamage() {
		return this.totalCriticalDamage;
	}
	
	public final double getTotalHitRate() {
		return this.totalHitRate;
	}
	
	public final double getSocketDamage() {
		return this.socketDamage;
	}
	
	public final double getElementDamage() {
		return this.elementDamage;
	}
	
	public static class ItemStatsWeaponBuilder {
		private double totalDamageMin;
		private double totalDamageMax;
		private double totalPenetration;
		private double totalPvPDamage;
		private double totalPvEDamage;
		private double totalAttackAoERadius;
		private double totalAttackAoEDamage;
		private double totalCriticalChance;
		private double totalCriticalDamage;
		private double totalHitRate;
		private double socketDamage;
		private double elementDamage;
		
		public ItemStatsWeaponBuilder() {
			this.totalDamageMin = 0;
			this.totalDamageMax = 0;
			this.totalPenetration = 0;
			this.totalPvPDamage = 0;
			this.totalPvEDamage = 0;
			this.totalAttackAoERadius = 0;
			this.totalAttackAoEDamage = 0;
			this.totalCriticalChance = 0;
			this.totalCriticalDamage = 0;
			this.totalHitRate = 0;
			this.socketDamage = 0;
			this.elementDamage = 0;
		}
		
		public final ItemStatsWeaponBuilder setTotalDamageMin(double totalDamageMin) {
			this.totalDamageMin = totalDamageMin;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalDamageMax(double totalDamageMax) {
			this.totalDamageMax = totalDamageMax;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalPenetration(double totalPenetration) {
			this.totalPenetration = totalPenetration;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalPvPDamage(double totalPvPDamage) {
			this.totalPvPDamage = totalPvPDamage;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalPvEDamage(double totalPvEDamage) {
			this.totalPvEDamage = totalPvEDamage;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalAttackAoERadius(double totalAttackAoERadius) {
			this.totalAttackAoERadius = totalAttackAoERadius;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalAttackAoEDamage(double totalAttackAoEDamage) {
			this.totalAttackAoEDamage = totalAttackAoEDamage;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalCriticalChance(double totalCriticalChance) {
			this.totalCriticalChance = Math.max(0, Math.min(100, totalCriticalChance));
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalCriticalDamage(double totalCriticalDamage) {
			this.totalCriticalDamage = totalCriticalDamage;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalHitRate(double totalHitRate) {
			this.totalHitRate = totalHitRate;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalSocketDamage(double socketDamage) {
			this.socketDamage = socketDamage;
			return this;
		}
		
		public final ItemStatsWeaponBuilder setTotalElementDamage(double elementDamage) {
			this.elementDamage = elementDamage;
			return this;
		}
		
		public final double getTotalDamageMin() {
			return this.totalDamageMin;
		}
		
		public final double getTotalDamageMax() {
			return this.totalDamageMax;
		}
		
		public final double getTotalPenetration() {
			return this.totalPenetration;
		}
		
		public final double getTotalPvPDamage() {
			return this.totalPvPDamage;
		}
		
		public final double getTotalPvEDamage() {
			return this.totalPvEDamage;
		}
		
		public final double getTotalAttackAoERadius() {
			return this.totalAttackAoERadius;
		}
		
		public final double getTotalAttackAoEDamage() {
			return this.totalAttackAoEDamage;
		}
		
		public final double getTotalCriticalChance() {
			return this.totalCriticalChance;
		}
		
		public final double getTotalCriticalDamage() {
			return this.totalCriticalDamage;
		}
		
		public final double getTotalHitRate() {
			return this.totalHitRate;
		}
		
		public final double getSocketDamage() {
			return this.socketDamage;
		}
		
		public final double getElementDamage() {
			return this.elementDamage;
		}
		
		public final ItemStatsWeapon build() {
			return new ItemStatsWeapon(totalDamageMin, totalDamageMax, totalPenetration, totalPvPDamage, totalPvEDamage, totalAttackAoERadius, totalAttackAoEDamage, totalCriticalChance, totalCriticalDamage, totalHitRate, socketDamage, elementDamage);
		}
	}
}
