package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

public class ItemSetComponentItem {

	private final Material material;
	private final String displayName;
	private final int data;
	private final boolean shiny;
	private final boolean unbreakable;
	private final List<String> lores;
	private final List<String> flags;
	private final Map<Enchantment, Integer> mapEnchantment;
	
	public ItemSetComponentItem(Material material, String displayName, int data, boolean shiny, boolean unbreakable) {
		this(material, displayName, data, shiny, unbreakable, null);
	}
	
	public ItemSetComponentItem(Material material, String displayName, int data, boolean shiny, boolean unbreakable, List<String> lores) {
		this(material, displayName, data, shiny, unbreakable, lores, null);
	}
	
	public ItemSetComponentItem(Material material, String displayName, int data, boolean shiny, boolean unbreakable, List<String> lores, List<String> flags) {
		this(material, displayName, data, shiny, unbreakable, lores, flags, null);
	}
	
	public ItemSetComponentItem(Material material, String displayName, int data, boolean shiny, boolean unbreakable, List<String> lores, List<String> flags, Map<Enchantment, Integer> mapEnchantment) {
		if (material == null) {
			throw new IllegalArgumentException();
		} else {
			this.material = material;
			this.displayName = displayName;
			this.data = data;
			this.shiny = shiny;
			this.unbreakable = unbreakable;
			this.lores = lores != null ? lores : new ArrayList<String>();;
			this.flags = flags != null ? flags : new ArrayList<String>();
			this.mapEnchantment = mapEnchantment != null ? mapEnchantment : new HashMap<Enchantment, Integer>();;
		}
	}
	
	public final String getDisplayName() {
		return this.displayName;
	}
	
	public final Material getMaterial() {
		return this.material;
	}
	
	public final int getData() {
		return this.data;
	}
	
	public final boolean isShiny() {
		return this.shiny;
	}
	
	public final boolean isUnbreakable() {
		return this.unbreakable;
	}
	
	public final List<String> getLores() {
		return this.lores;
	}
	
	public final List<String> getFlags() {
		return this.flags;
	}
	
	public final Collection<Enchantment> getEnchantments() {
		return this.mapEnchantment.keySet();
	}
	
	public final int getEnchantmentGrade(Enchantment enchantment) {
		return this.mapEnchantment.get(enchantment);
	}
	
	public final boolean hasEnchantment(Enchantment enchantment) {
		return enchantment != null ? this.mapEnchantment.containsKey(enchantment) : false;
	}
}
