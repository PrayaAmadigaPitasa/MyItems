package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.List;

public class ItemSetBonus {

	private final int amountId;
	private final List<String> description;
	private final ItemSetBonusEffect effect;
	
	public ItemSetBonus(int amountId, List<String> description, ItemSetBonusEffect effect) {
		if (effect == null) {
			throw new IllegalArgumentException();
		} else {
			this.amountId = amountId;
			this.description = description != null ? description : new ArrayList<String>();
			this.effect = effect;
		}
	}
	
	public final int getAmountId() {
		return this.amountId;
	}
	
	public final List<String> getDescription() {
		return this.description;
	}
	
	public final ItemSetBonusEffect getEffect() {
		return this.effect;
	}
}
