package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.item.ItemTier;
import net.ardeus.myitems.manager.game.ItemTierManager;

public final class ItemTierMemory extends ItemTierManager {

	private final ItemTierConfig itemTierConfig;
	
	private ItemTierMemory(MyItems plugin) {
		super(plugin);
		
		this.itemTierConfig = new ItemTierConfig(plugin);
	};
	
	private static class ItemTierMemorySingleton {
		private static final ItemTierMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new ItemTierMemory(plugin);
		}
	}
	
	public static final ItemTierMemory getInstance() {
		return ItemTierMemorySingleton.INSTANCE;
	}
	
	protected final ItemTierConfig getItemTierConfig() {
		return this.itemTierConfig;
	}
	
	@Override
	public final Collection<String> getItemTierIds() {
		final Map<String, ItemTier> mapItemTier = getItemTierConfig().mapTier;
		
		return new ArrayList<String>(mapItemTier.keySet());
	}
	
	@Override
	public final Collection<ItemTier> getItemTiers() {
		final Map<String, ItemTier> mapItemTier = getItemTierConfig().mapTier;
		
		return new ArrayList<ItemTier>(mapItemTier.values());
	}
	
	@Override
	public final ItemTier getItemTier(String id) {
		if (id != null) {
			final Map<String, ItemTier> mapItemTier = getItemTierConfig().mapTier;
			
			for (String key : mapItemTier.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return mapItemTier.get(key);
				}
			}
		}
		
		return null;
	}
}