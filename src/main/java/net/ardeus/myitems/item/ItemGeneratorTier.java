package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.List;

public class ItemGeneratorTier {

	private final int possibility;
	private final List<String> additionalLores;
	
	public ItemGeneratorTier(int possibility) {
		this(possibility, null);
	}
	
	public ItemGeneratorTier(int possibility, List<String> additionalLores) {
		this.possibility = possibility;
		this.additionalLores = additionalLores != null ? additionalLores : new ArrayList<String>();
	}
	
	public final int getPossibility() {
		return this.possibility;
	}
	
	public final List<String> getAdditionalLores() {
		return this.additionalLores;
	}
}
