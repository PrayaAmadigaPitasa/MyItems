package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.plugin.PlaceholderManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.TextUtil;

public class ItemSet {

	private final String id;
	private final String name;
	private final Map<Integer, ItemSetBonus> mapBonus;
	private final Map<String, ItemSetComponent> mapComponent;
	
	public ItemSet(String id, String name, Map<Integer, ItemSetBonus> mapBonus, Map<String, ItemSetComponent> mapComponent) {
		if (id == null || name == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.name = name;
			this.mapBonus = mapBonus != null ? mapBonus : new HashMap<Integer, ItemSetBonus>();
			this.mapComponent = mapComponent != null ? mapComponent : new HashMap<String, ItemSetComponent>();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getName() {
		return this.name;
	}
	
	public final Collection<Integer> getBonusAmountIds() {
		return new ArrayList<Integer>(this.mapBonus.keySet());
	}
	
	public final Collection<String> getComponentIds() {
		return new ArrayList<String>(this.mapComponent.keySet());
	}
	
	public final Collection<ItemSetBonus> getAllItemSetBonus() {
		return new ArrayList<ItemSetBonus>(this.mapBonus.values());
	}
	
	public final Collection<ItemSetComponent> getAllItemSetComponent() {
		return new ArrayList<ItemSetComponent>(this.mapComponent.values());
	}
	
	public final ItemSetBonus getItemSetBonus(int amountId) {
		return this.mapBonus.get(amountId);
	}
	
	public final boolean isBonusExists(int amountId) {
		return getItemSetBonus(amountId) != null;
	}
	
	public final ItemSetComponent getItemSetComponent(String componentId) {
		if (componentId != null) {
			for (String key : this.mapComponent.keySet()) {
				if (key.equalsIgnoreCase(componentId)) {
					return this.mapComponent.get(key);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isComponentExists(String componentId) {
		return getItemSetComponent(componentId) != null;
	}
	
	public final int getTotalComponent() {
		return this.mapComponent.size();
	}
	
	public final ItemStack generateItem(String componentId) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final ItemSetComponent itemSetComponent = getItemSetComponent(componentId);
		
		if (itemSetComponent != null) {
			final ItemSetComponentItem componentItem = itemSetComponent.getComponentItem();
			final Material material = componentItem.getMaterial();
			final short data = (short) componentItem.getData();
			final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(material, data);
			
			if (materialEnum != null) {
				final String divider = "\n";
				final String keyLine = MainConfig.KEY_SET_LINE;
				final String keySetComponentSelf = MainConfig.KEY_SET_COMPONENT_SELF;
				final String keySetComponentOther = MainConfig.KEY_SET_COMPONENT_OTHER;
				final String loreBonusInactive = mainConfig.getSetLoreBonusInactive();
				final String loreComponentInactive = mainConfig.getSetLoreComponentInactive();
				final boolean shiny = componentItem.isShiny();
				final boolean unbreakable = componentItem.isUnbreakable();
				final List<String> flags = componentItem.getFlags();
				final List<String> lores = new ArrayList<String>(componentItem.getLores());
				final List<String> loresBonus = new ArrayList<String>();
				final List<String> loresComponent = new ArrayList<String>();
				final List<Integer> bonusAmountIds = new ArrayList<Integer>(this.mapBonus.keySet());
				final Collection<Enchantment> enchantments = componentItem.getEnchantments();
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				String display = componentItem.getDisplayName();
				List<String> loresSet = mainConfig.getSetFormat();
				
				Collections.sort(bonusAmountIds);
				
				for (ItemSetComponent partComponent : getAllItemSetComponent()) {
					final String partComponentId = partComponent.getId();
					final String keyLore = partComponent.getKeyLore();
					final String replacementKeyLore;
					
					String formatComponent = loreComponentInactive + mainConfig.getSetFormatComponent();
					
					if (partComponent.equals(itemSetComponent)) {
						replacementKeyLore = keySetComponentSelf + loreComponentInactive + keyLore + keySetComponentSelf + loreComponentInactive;
					} else {
						replacementKeyLore = keySetComponentOther + loreComponentInactive + keyLore + keySetComponentOther + loreComponentInactive;
					}
					
					mapPlaceholder.clear();
					mapPlaceholder.put("item_set_component_id", partComponentId);
					mapPlaceholder.put("item_set_component_keylore", replacementKeyLore);
					
					formatComponent = TextUtil.placeholder(mapPlaceholder, formatComponent, "<", ">");
					
					loresComponent.add(formatComponent);
				}
				
				for (int bonusAmountId : bonusAmountIds) {
					final ItemSetBonus partBonus = getItemSetBonus(bonusAmountId);
					
					for (String description : partBonus.getDescription()) {
					
						String formatBonus = loreBonusInactive + mainConfig.getSetFormatBonus();
						
						mapPlaceholder.clear();
						mapPlaceholder.put("item_set_bonus_amount", String.valueOf(bonusAmountId));
						mapPlaceholder.put("item_set_bonus_description", String.valueOf(description));
						
						formatBonus = TextUtil.placeholder(mapPlaceholder, formatBonus, "<", ">");
						
						loresBonus.add(formatBonus);
					}
				}
				
				final String lineBonus = TextUtil.convertListToString(loresBonus, divider);
				final String lineComponent = TextUtil.convertListToString(loresComponent, divider);
				
				mapPlaceholder.clear();
				mapPlaceholder.put("item_set_name", getName());
				mapPlaceholder.put("item_set_total", String.valueOf("0"));
				mapPlaceholder.put("item_set_max", String.valueOf(getTotalComponent()));
				mapPlaceholder.put("list_item_set_component", lineComponent);
				mapPlaceholder.put("list_item_set_bonus", lineBonus);
				
				loresSet = TextUtil.placeholder(mapPlaceholder, loresSet, "<", ">");
				loresSet = TextUtil.expandList(loresSet, divider);
				
				final ListIterator<String> iteratorLoresSet = loresSet.listIterator();
				
				while (iteratorLoresSet.hasNext()) {
					final String lore = keyLine + iteratorLoresSet.next();
					
					iteratorLoresSet.set(lore);
				}
				
				lores.addAll(loresSet);
				
				final List<String> loresFinal = placeholderManager.placeholder(null, lores);
				final ItemStack item = EquipmentUtil.createItem(materialEnum, display, 1, loresFinal);
				
				for (String flag : flags) {
					EquipmentUtil.addFlag(item, flag);
				}
				
				for (Enchantment enchantment : enchantments) {
					final int grade = componentItem.getEnchantmentGrade(enchantment);
					
					EquipmentUtil.addEnchantment(item, enchantment, grade);
				}
				
				if (shiny) {
					EquipmentUtil.shiny(item);
				}
				
				if (unbreakable) {
					Bridge.getBridgeTagsNBT().setUnbreakable(item, true);
				}
			
				return item;
			}
		}
		
		return null;
	}
}