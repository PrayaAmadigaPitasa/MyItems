package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.FileUtil;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.game.ItemManager;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

public final class ItemMemory extends ItemManager {

	private final ItemConfig itemConfig;
	
	private ItemMemory(MyItems plugin) {
		super(plugin);
		
		this.itemConfig = new ItemConfig(plugin);
	};
	
	private static class ItemMemorySingleton {
		private static final ItemMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new ItemMemory(plugin);
		}
	}
	
	public static final ItemMemory getInstance() {
		return ItemMemorySingleton.INSTANCE;
	}
	
	protected final ItemConfig getItemConfig() {
		return this.itemConfig;
	}
	
	@Override
	public final Collection<String> getItemIds() {
		final Map<String, ItemStack> mapItem = getItemConfig().mapItem;
		
		return new ArrayList<String>(mapItem.keySet());
	}
	
	@Override
	public final Collection<ItemStack> getAllItems() {
		final Map<String, ItemStack> mapItem = getItemConfig().mapItem;
		
		return new ArrayList<ItemStack>(mapItem.values());
	}
	
	@Override
	public final ItemStack getItem(String id) {
		if (id != null) {
			final Map<String, ItemStack> mapItem = getItemConfig().mapItem;
			
			for (String key : mapItem.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return mapItem.get(key);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final String getRawName(String id) {
		if (id != null) {
			final Map<String, ItemStack> mapItem = getItemConfig().mapItem;
			
			for (String key : mapItem.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return key;
				}
			}
		}
		
		return null;
	}
	
	public final boolean saveItem(ItemStack item, String id) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String path = dataManager.getPath("Path_File_Item");
		final ItemConfig config = getItemConfig();
		
		if (item != null && id != null) {
			final ItemStack fileItem = config.convertToFile(item);
			final String rawId = getRawName(id);
			final Map<String, ItemStack> mapItem = config.mapItem;
			
			if (rawId != null) {
				mapItem.remove(rawId);
			}
			
			mapItem.put(id, item);
			FileUtil.addObject(plugin, path, id, fileItem);
			return true;
		} else {
			return false;
		}
	}
	
	public final boolean removeItem(String id) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String path = dataManager.getPath("Path_File_Item");
		final ItemConfig config = getItemConfig();
		
		if (id != null && isExist(id)) {
			final String rawId = getRawName(id);
			final Map<String, ItemStack> mapItem = config.mapItem;
			
			mapItem.remove(rawId);
			FileUtil.removeObject(plugin, path, id);
			return true;
		} else {
			return false;
		}
	}
}
