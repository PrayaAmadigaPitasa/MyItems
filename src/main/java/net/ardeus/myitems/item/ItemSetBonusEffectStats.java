package net.ardeus.myitems.item;

public class ItemSetBonusEffectStats {

	private final double additionalDamage;
	private final double percentDamage;
	private final double penetration;
	private final double pvpDamage;
	private final double pveDamage;
	private final double additionalDefense;
	private final double percentDefense;
	private final double health;
	private final double healthRegen;
	private final double staminaMax;
	private final double staminaRegen;
	private final double attackAoERadius;
	private final double attackAoEDamage;
	private final double pvpDefense;
	private final double pveDefense;
	private final double criticalChance;
	private final double criticalDamage;
	private final double blockAmount;
	private final double blockRate;
	private final double hitRate;
	private final double dodgeRate;
	
	private ItemSetBonusEffectStats(double additionalDamage, double percentDamage, double penetration, double pvpDamage, double pveDamage, double additionalDefense, double percentDefense, double health, double healthRegen, double staminaMax, double staminaRegen, double attackAoERadius, double attackAoEDamage, double pvpDefense, double pveDefense, double criticalChance, double criticalDamage, double blockAmount, double blockRate, double hitRate, double dodgeRate) {
		this.additionalDamage = additionalDamage;
		this.percentDamage = percentDamage;
		this.penetration = penetration;
		this.pvpDamage = pvpDamage;
		this.pveDamage = pveDamage;
		this.additionalDefense = additionalDefense;
		this.percentDefense = percentDefense;
		this.health = health;
		this.healthRegen = healthRegen;
		this.staminaMax = staminaMax;
		this.staminaRegen = staminaRegen;
		this.attackAoERadius = attackAoERadius;
		this.attackAoEDamage = attackAoEDamage;
		this.pvpDefense = pvpDefense;
		this.pveDefense = pveDefense;
		this.criticalChance = criticalChance;
		this.criticalDamage = criticalDamage;
		this.blockAmount = blockAmount;
		this.blockRate = blockRate;
		this.hitRate = hitRate;
		this.dodgeRate = dodgeRate;	
	}
	
	public final double getAdditionalDamage() {
		return this.additionalDamage;
	}
	
	public final double getPercentDamage() {
		return this.percentDamage;
	}
	
	public final double getPenetration() {
		return this.penetration;
	}
	
	public final double getPvPDamage() {
		return this.pvpDamage;
	}
	
	public final double getPvEDamage() {
		return this.pveDamage;
	}
	
	public final double getAdditionalDefense() {
		return this.additionalDefense;
	}
	
	public final double getPercentDefense() {
		return this.percentDefense;
	}
	
	public final double getHealth() {
		return this.health;
	}
	
	public final double getHealthRegen() {
		return this.healthRegen;
	}
	
	public final double getStaminaMax() {
		return this.staminaMax;
	}
	
	public final double getStaminaRegen() {
		return this.staminaRegen;
	}
	
	public final double getAttackAoERadius() {
		return this.attackAoERadius;
	}
	
	public final double getAttackAoEDamage() {
		return this.attackAoEDamage;
	}
	
	public final double getPvPDefense() {
		return this.pvpDefense;
	}
	
	public final double getPvEDefense() {
		return this.pveDefense;
	}
	
	public final double getCriticalChance() {
		return this.criticalChance;
	}
	
	public final double getCriticalDamage() {
		return this.criticalDamage;
	}
	
	public final double getBlockAmount() {
		return this.blockAmount;
	}
	
	public final double getBlockRate() {
		return this.blockRate;
	}
	
	public final double getHitRate() {
		return this.hitRate;
	}
	
	public final double getDodgeRate() {
		return this.dodgeRate;
	}
	
	public static class ItemSetBonusEffectStatsBuilder {
		
		private double additionalDamage;
		private double percentDamage;
		private double penetration;
		private double pvpDamage;
		private double pveDamage;
		private double additionalDefense;
		private double percentDefense;
		private double health;
		private double healthRegen;
		private double staminaMax;
		private double staminaRegen;
		private double attackAoERadius;
		private double attackAoEDamage;
		private double pvpDefense;
		private double pveDefense;
		private double criticalChance;
		private double criticalDamage;
		private double blockAmount;
		private double blockRate;
		private double hitRate;
		private double dodgeRate;
		
		public ItemSetBonusEffectStatsBuilder() {
			this.additionalDamage = 0;
			this.percentDamage = 0;
			this.penetration = 0;
			this.pvpDamage = 0;
			this.pveDamage = 0;
			this.additionalDefense = 0;
			this.percentDefense = 0;
			this.health = 0;
			this.healthRegen = 0;
			this.staminaMax = 0;
			this.staminaRegen = 0;
			this.attackAoERadius = 0;
			this.attackAoEDamage = 0;
			this.pvpDefense = 0;
			this.pveDefense = 0;
			this.criticalChance = 0;
			this.criticalDamage = 0;
			this.blockAmount = 0;
			this.blockRate = 0;
			this.hitRate = 0;
			this.dodgeRate = 0;
		}
		
		public final double getAdditionalDamage() {
			return this.additionalDamage;
		}
		
		public final double getPercentDamage() {
			return this.percentDamage;
		}
		
		public final double getPenetration() {
			return this.penetration;
		}
		
		public final double getPvPDamage() {
			return this.pvpDamage;
		}
		
		public final double getPvEDamage() {
			return this.pveDamage;
		}
		
		public final double getAdditionalDefense() {
			return this.additionalDefense;
		}
		
		public final double getPercentDefense() {
			return this.percentDefense;
		}
		
		public final double getHealth() {
			return this.health;
		}
		
		public final double getHealthRegen() {
			return this.healthRegen;
		}
		
		public final double getStaminaMax() {
			return this.staminaMax;
		}
		
		public final double getStaminaRegen() {
			return this.staminaRegen;
		}
		
		public final double getAttackAoERadius() {
			return this.attackAoERadius;
		}
		
		public final double getAttackAoEDamage() {
			return this.attackAoEDamage;
		}
		
		public final double getPvPDefense() {
			return this.pvpDefense;
		}
		
		public final double getPvEDefense() {
			return this.pveDefense;
		}
		
		public final double getCriticalChance() {
			return this.criticalChance;
		}
		
		public final double getCriticalDamage() {
			return this.criticalDamage;
		}
		
		public final double getBlockAmount() {
			return this.blockAmount;
		}
		
		public final double getBlockRate() {
			return this.blockRate;
		}
		
		public final double getHitRate() {
			return this.hitRate;
		}
		
		public final double getDodgeRate() {
			return this.dodgeRate;
		}
		
		public final ItemSetBonusEffectStatsBuilder setAdditionalDamage(double additionalDamage) {
			this.additionalDamage = additionalDamage;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setPercentDamage(double percentDamage) {
			this.percentDamage = percentDamage;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setPenetration(double penetration) {
			this.penetration = penetration;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setPvPDamage(double pvpDamage) {
			this.pvpDamage = pvpDamage;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setPvEDamage(double pveDamage) {
			this.pveDamage = pveDamage;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setAdditionalDefense(double additionalDefense) {
			this.additionalDefense = additionalDefense;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setPercentDefense(double percentDefense) {
			this.percentDefense = percentDefense;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setHealth(double health) {
			this.health = health;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setHealthRegen(double healthRegen) {
			this.healthRegen = healthRegen;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setStaminaMax(double staminaMax) {
			this.staminaMax = staminaMax;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setStaminaRegen(double staminaRegen) {
			this.staminaRegen = staminaRegen;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setAttackAoERadius(double attackAoERadius) {
			this.attackAoERadius = attackAoERadius;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setAttackAoEDamage(double attackAoEDamage) {
			this.attackAoEDamage = attackAoEDamage;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setPvPDefense(double pvpDefense) {
			this.pvpDefense = pvpDefense;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setPvEDefense(double pveDefense) {
			this.pveDefense = pveDefense;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setCriticalChance(double criticalChance) {
			this.criticalChance = criticalChance;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setCriticalDamage(double criticalDamage) {
			this.criticalDamage = criticalDamage;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setBlockAmount(double blockAmount) {
			this.blockAmount = blockAmount;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setBlockRate(double blockRate) {
			this.blockRate = blockRate;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setHitRate(double hitRate) {
			this.hitRate = hitRate;
			return this;
		}
		
		public final ItemSetBonusEffectStatsBuilder setDodgeRate(double dodgeRate) {
			this.dodgeRate = dodgeRate;
			return this;
		}
		
		public final ItemSetBonusEffectStats build() {
			return new ItemSetBonusEffectStats(additionalDamage, percentDamage, penetration, pvpDamage, pveDamage, additionalDefense, percentDefense, health, healthRegen, staminaMax, staminaRegen, attackAoERadius, attackAoEDamage, pvpDefense, pveDefense, criticalChance, criticalDamage, blockAmount, blockRate, hitRate, dodgeRate);
		}
	}
}
