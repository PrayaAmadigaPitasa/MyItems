package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.item.ItemGenerator;
import net.ardeus.myitems.manager.game.ItemGeneratorManager;

public final class ItemGeneratorMemory extends ItemGeneratorManager {

	private final ItemGeneratorConfig itemGeneratorConfig;
	
	private ItemGeneratorMemory(MyItems plugin) {
		super(plugin);
		
		this.itemGeneratorConfig = new ItemGeneratorConfig(plugin);
	};
	
	private static class ItemGeneratorMemorySingleton {
		private static final ItemGeneratorMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new ItemGeneratorMemory(plugin);
		}
	}
	
	public static final ItemGeneratorMemory getInstance() {
		return ItemGeneratorMemorySingleton.INSTANCE;
	}
	
	protected final ItemGeneratorConfig getItemGeneratorConfig() {
		return this.itemGeneratorConfig;
	}
	
	@Override
	public final Collection<String> getItemGeneratorIds() {
		final Map<String, ItemGenerator> mapItemGenerator = getItemGeneratorConfig().mapItemGenerator;
		
		return new ArrayList<String>(mapItemGenerator.keySet());
	}
	
	@Override
	public final Collection<ItemGenerator> getAllItemGenerators() {
		final Map<String, ItemGenerator> mapItemGenerator = getItemGeneratorConfig().mapItemGenerator;
		
		return new ArrayList<ItemGenerator>(mapItemGenerator.values());
	}
	
	@Override
	public final ItemGenerator getItemGenerator(String id) {
		if (id != null) {
			final Map<String, ItemGenerator> mapItemGenerator = getItemGeneratorConfig().mapItemGenerator;
			
			for (String key : mapItemGenerator.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return mapItemGenerator.get(key);
				}
			}
		}
		
		return null;
	}
}
