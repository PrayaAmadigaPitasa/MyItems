package net.ardeus.myitems.item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.item.ItemType;
import net.ardeus.myitems.manager.game.ItemTypeManager;

public final class ItemTypeMemory extends ItemTypeManager {

	private final ItemTypeConfig itemTypeConfig;
	
	private ItemTypeMemory(MyItems plugin) {
		super(plugin);
		
		this.itemTypeConfig = new ItemTypeConfig(plugin);
	};
	
	private static class ItemTypeMemorySingleton {
		private static final ItemTypeMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new ItemTypeMemory(plugin);
		}
	}
	
	public static final ItemTypeMemory getInstance() {
		return ItemTypeMemorySingleton.INSTANCE;
	}
	
	protected final ItemTypeConfig getItemTypeConfig() {
		return this.itemTypeConfig;
	}
	
	@Override
	public final Collection<String> getItemTypeIds() {
		final Map<String, ItemType> mapItemType = getItemTypeConfig().mapType;
		
		return new ArrayList<String>(mapItemType.keySet());
	}
	
	@Override
	public final Collection<ItemType> getItemTypes() {
		final Map<String, ItemType> mapItemType = getItemTypeConfig().mapType;
		
		return new ArrayList<ItemType>(mapItemType.values());
	}
	
	@Override
	public final ItemType getItemType(String id) {
		if (id != null) {
			final Map<String, ItemType> mapItemType = getItemTypeConfig().mapType;
			
			for (String key : mapItemType.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return mapItemType.get(key);
				}
			}
		}
		
		return null;
	}
}