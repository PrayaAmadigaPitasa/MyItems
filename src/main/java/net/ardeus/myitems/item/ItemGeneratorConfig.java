package net.ardeus.myitems.item;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemTierManager;
import net.ardeus.myitems.manager.game.ItemTypeManager;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.TextUtil;

public final class ItemGeneratorConfig extends HandlerConfig {
	
	protected final Map<String, ItemGenerator> mapItemGenerator = new HashMap<String, ItemGenerator>();
	
	protected ItemGeneratorConfig(MyItems plugin) {
		super(plugin);
	};
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {		
		this.mapItemGenerator.clear();
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final ItemTypeManager itemTypeManager = gameManager.getItemTypeManager();
		final ItemTierManager itemTierManager = gameManager.getItemTierManager();
		final String pathDefault = dataManager.getPath("Path_File_Item_Generator");
		final String pathFolder = dataManager.getPath("Path_Folder_Item_Generator");
		final File fileDefault = FileUtil.getFile(plugin, pathDefault);
		final File fileFolder = FileUtil.getFile(plugin, pathFolder);		
		
		if (!fileDefault.exists()) {
			FileUtil.saveResource(plugin, pathDefault);
		}
		
		for (File file : fileFolder.listFiles()) {
			final FileConfiguration config = FileUtil.getFileConfiguration(file);
			
			for (String key : config.getKeys(false)) {
				final ConfigurationSection dataSection = config.getConfigurationSection(key);
				final List<String> lores = new ArrayList<String>();
				final List<String> flags = new ArrayList<String>();
				final HashMap<ItemType, ItemGeneratorType> mapType = new HashMap<ItemType, ItemGeneratorType>();
				final HashMap<ItemTier, ItemGeneratorTier> mapTier = new HashMap<ItemTier, ItemGeneratorTier>();
				
				String displayName = null;
				boolean unbreakable = false;
				
				for (String data : dataSection.getKeys(false)) {
					if (data.equalsIgnoreCase("Display_Name")) {
						displayName = TextUtil.colorful(dataSection.getString(data));
					} else if (data.equalsIgnoreCase("Unbreakable")) {
						unbreakable = dataSection.getBoolean(data);
					} else if (data.equalsIgnoreCase("Flags") || data.equalsIgnoreCase("ItemFlags")) {
						flags.addAll(dataSection.getStringList(data));
					} else if (data.equalsIgnoreCase("Lores")) {
						lores.addAll(dataSection.getStringList(data));
					} else if (data.equalsIgnoreCase("Type")) {
						final ConfigurationSection typeSection = dataSection.getConfigurationSection(data);
						
						for (String type : typeSection.getKeys(false)) {
							final ItemType itemType = itemTypeManager.getItemType(type);
							
							if (itemType != null) {
								final ConfigurationSection typePropertiesSection = typeSection.getConfigurationSection(type);
								final List<String> description = new ArrayList<String>();
								final List<String> names = new ArrayList<String>();
								
								int possibility = 1;
								
								for (String typeProperties : typePropertiesSection.getKeys(false)) {
									if (typeProperties.equalsIgnoreCase("Possibility")) {
										possibility = typePropertiesSection.getInt(typeProperties);
									} else if (typeProperties.equalsIgnoreCase("Description")) {
										for (String desc : typePropertiesSection.getStringList(typeProperties)) {
											description.add(desc);
										}
									} else if (typeProperties.equalsIgnoreCase("Name") || typeProperties.equalsIgnoreCase("Names")) {
										for (String name : typePropertiesSection.getStringList(typeProperties)) {
											names.add(name);
										}
									}
								}
								
								final ItemGeneratorType itemTypeProperties = new ItemGeneratorType(possibility, description, names);
								
								mapType.put(itemType, itemTypeProperties);
							}
						}
					} else if (data.equalsIgnoreCase("Tier")) {
						final ConfigurationSection tierSection = dataSection.getConfigurationSection(data);
						
						for (String tier : tierSection.getKeys(false)) {
							final ItemTier itemTier = itemTierManager.getItemTier(tier);
							
							if (itemTier != null) {
								final ConfigurationSection tierPropertiesSection = tierSection.getConfigurationSection(tier);
								final List<String> additionalLores = new ArrayList<String>();
								
								int possibility = 1;
								
								for (String tierProperties : tierPropertiesSection.getKeys(false)) {
									if (tierProperties.equalsIgnoreCase("Possibility")) {
										possibility = tierPropertiesSection.getInt(tierProperties);
									} else if (tierProperties.equalsIgnoreCase("Additional_Lores")) {
										for (String additionalLore : tierPropertiesSection.getStringList(tierProperties)) {
											additionalLores.add(additionalLore);
										}
									}
								}
								
								final ItemGeneratorTier itemTierProperties = new ItemGeneratorTier(possibility, additionalLores);
								
								mapTier.put(itemTier, itemTierProperties);
							}
						}
					}
				}
				
				if (displayName != null) {
					final ItemGenerator itemGenerator = new ItemGenerator(key, displayName, unbreakable, lores, flags, mapType, mapTier);
					
					mapItemGenerator.put(key, itemGenerator);
				}
			}
		}
	}
	
	private final void moveOldFile() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathSource_1 = "item_generator.yml";
		final String pathSource_2 = "Configuration/item_generator.yml";
		final String pathTarget = dataManager.getPath("Path_File_Item_Generator");
		final File fileSource_1 = FileUtil.getFile(plugin, pathSource_1);
		final File fileSource_2 = FileUtil.getFile(plugin, pathSource_2);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource_1.exists()) {
			FileUtil.moveFileSilent(fileSource_1, fileTarget);
		} else if (fileSource_2.exists()) {
			FileUtil.moveFileSilent(fileSource_2, fileTarget);
		}
	}
}