package net.ardeus.myitems.item;

import com.praya.agarthalib.utility.MathUtil;

public class ItemStatsArmor {

	private final double totalDefense;
	private final double totalPvPDefense;
	private final double totalPvEDefense;
	private final double totalHealth;
	private final double totalHealthRegen;
	private final double totalStaminaMax;
	private final double totalStaminaRegen;
	private final double totalBlockAmount;
	private final double totalBlockRate;
	private final double totalDodgeRate;
	private final double socketDefense;
		
	private ItemStatsArmor(double totalDefense, double totalPvPDefense, double totalPvEDefense, double totalHealth, double totalHealthRegen, double totalStaminaMax, double totalStaminaRegen, double totalBlockAmount, double totalBlockRate, double totalDodgeRate, double socketDefense) {
		this.totalDefense = totalDefense;
		this.totalPvPDefense = totalPvPDefense;
		this.totalPvEDefense = totalPvEDefense;
		this.totalHealth = totalHealth;
		this.totalHealthRegen = totalHealthRegen;
		this.totalStaminaMax = totalStaminaMax;
		this.totalStaminaRegen = totalStaminaRegen;
		this.totalBlockAmount = MathUtil.limitDouble(totalBlockAmount, 0, 100);
		this.totalBlockRate = MathUtil.limitDouble(totalBlockRate, 0, 100);
		this.totalDodgeRate = totalDodgeRate;
		this.socketDefense = socketDefense;
	}
	
	public final double getTotalDefense() {
		return this.totalDefense;
	}
	
	public final double getTotalPvPDefense() {
		return this.totalPvPDefense;
	}
	
	public final double getTotalPvEDefense() {
		return this.totalPvEDefense;
	}
	
	public final double getTotalHealth() {
		return this.totalHealth;
	}
	
	public final double getTotalHealthRegen() {
		return this.totalHealthRegen;
	}
	
	public final double getTotalStaminaMax() {
		return this.totalStaminaMax;
	}
	
	public final double getTotalStaminaRegen() {
		return this.totalStaminaRegen;
	}
	
	public final double getTotalBlockAmount() {
		return this.totalBlockAmount;
	}
	
	public final double getTotalBlockRate() {
		return this.totalBlockRate;
	}
	
	public final double getTotalDodgeRate() {
		return this.totalDodgeRate;
	}
	
	public final double getSocketDefense() {
		return this.socketDefense;
	}
	
	public static class ItemStatsArmorBuilder {
		
		private double totalDefense;
		private double totalPvPDefense;
		private double totalPvEDefense;
		private double totalHealth;
		private double totalHealthRegen;
		private double totalStaminaMax;
		private double totalStaminaRegen;
		private double totalBlockAmount;
		private double totalBlockRate;
		private double totalDodgeRate;
		private double socketDefense;
		
		public ItemStatsArmorBuilder() {
			this.totalDefense = 0;
			this.totalPvPDefense = 0;
			this.totalPvEDefense = 0;
			this.totalHealth = 0;
			this.totalHealthRegen = 0;
			this.totalStaminaMax = 0;
			this.totalStaminaRegen = 0;
			this.totalBlockAmount = 0;
			this.totalBlockRate = 0;
			this.totalDodgeRate = 0;
			this.socketDefense = 0;
		}
		
		public final double getTotalDefense() {
			return this.totalDefense;
		}
		
		public final double getTotalPvPDefense() {
			return this.totalPvPDefense;
		}
		
		public final double getTotalPvEDefense() {
			return this.totalPvEDefense;
		}
		
		public final double getTotalHealth() {
			return this.totalHealth;
		}
		
		public final double getTotalHealthRegen() {
			return this.totalHealthRegen;
		}
		
		public final double getTotalStaminaMax() {
			return this.totalStaminaMax;
		}
		
		public final double getTotalStaminaRegen() {
			return this.totalStaminaRegen;
		}
		
		public final double getTotalBlockAmount() {
			return this.totalBlockAmount;
		}
		
		public final double getTotalBlockRate() {
			return this.totalBlockRate;
		}
		
		public final double getTotalDodgeRate() {
			return this.totalDodgeRate;
		}
		
		public final double getSocketDefense() {
			return this.socketDefense;
		}
		
		public final ItemStatsArmorBuilder setTotalDefense(double totalDefense) {
			this.totalDefense = totalDefense;
			return this;
		}
		
		public final ItemStatsArmorBuilder setTotalPvPDefense(double totalPvPDefense) {
			this.totalPvPDefense = totalPvPDefense;
			return this;
		}
		
		public final ItemStatsArmorBuilder setTotalPvEDefense(double totalPvEDefense) {
			this.totalPvEDefense = totalPvEDefense;
			return this;
		}
		
		public final ItemStatsArmorBuilder setTotalHealth(double totalHealth) {
			this.totalHealth = totalHealth;
			return this;
		}
		
		public final ItemStatsArmorBuilder setTotalHealthRegen(double totalHealthRegen) {
			this.totalHealthRegen = totalHealthRegen;
			return this;
		}
		
		public final ItemStatsArmorBuilder setTotalStaminaMax(double totalStaminaMax) {
			this.totalStaminaMax = totalStaminaMax;
			return this;
		}
		
		public final ItemStatsArmorBuilder setTotalStaminaRegen(double totalStaminaRegen) {
			this.totalStaminaRegen = totalStaminaRegen;
			return this;
		}
		
		public final ItemStatsArmorBuilder setTotalBlockAmount(double totalBlockAmount) {
			this.totalBlockAmount = Math.max(0, Math.min(100, totalBlockAmount));
			return this;
		}
		
		public final ItemStatsArmorBuilder setTotalBlockRate(double totalBlockRate) {
			this.totalBlockRate = Math.max(0, Math.min(100, totalBlockRate));
			return this;
		}
		
		public final ItemStatsArmorBuilder setTotalDodgeRate(double totalDodgeRate) {
			this.totalDodgeRate = totalDodgeRate;
			return this;
		}
		
		public final ItemStatsArmorBuilder setSocketDefense(double socketDefense) {
			this.socketDefense = socketDefense;
			return this;
		}
		
		public final ItemStatsArmor build() {
			return new ItemStatsArmor(totalDefense, totalPvPDefense, totalPvEDefense, totalHealth, totalHealthRegen, totalStaminaMax, totalStaminaRegen, totalBlockAmount, totalBlockRate, totalDodgeRate, socketDefense);
		}
	}
}