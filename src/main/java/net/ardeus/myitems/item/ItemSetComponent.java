package net.ardeus.myitems.item;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;

public class ItemSetComponent {

	private final String itemSetId;
	private final String id;
	private final String keyLore;
	private final ItemSetComponentItem componentItem;
	private final Set<Slot> slots;
	
	public ItemSetComponent(String itemSetId, String id, String keyLore, ItemSetComponentItem componentItem, Set<Slot> slots) {
		if (itemSetId == null || id == null || keyLore == null || componentItem == null || slots == null) {
			throw new IllegalArgumentException();
		} else {
			this.itemSetId = itemSetId;
			this.id = id;
			this.keyLore = keyLore;
			this.componentItem = componentItem;
			this.slots = slots;
		}
	}
	
	public final String getItemSetId() {
		return this.itemSetId;
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getKeyLore() {
		return this.keyLore;
	}
	
	public final ItemSetComponentItem getComponentItem() {
		return this.componentItem;
	}
	
	public final Set<Slot> getSlots() {
		return new HashSet<Slot>(this.slots);
	}
	
	public final boolean isMatchSlot(Slot slot) {
		return slot != null ? getSlots().contains(slot) : false;
	}
	
	public final ItemSet getItemSet() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final ItemSet itemSet = itemSetManager.getItemSet(getItemSetId());
		
		return itemSet;
	}
}
