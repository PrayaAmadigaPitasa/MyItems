package net.ardeus.myitems.manager.plugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.metrics.BStats;

public class MetricsManager extends HandlerManager {

	private BStats metricsBStats;
	
	protected MetricsManager(MyItems plugin) {
		super(plugin);
		
		this.metricsBStats = new BStats(plugin);
	}
	
	public final BStats getMetricsBStats() {
		return this.metricsBStats;
	}
}
