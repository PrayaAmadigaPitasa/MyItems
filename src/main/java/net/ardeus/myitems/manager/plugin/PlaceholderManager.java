package net.ardeus.myitems.manager.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.regex.Pattern;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.enums.branch.ProjectileEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.config.plugin.PlaceholderConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsModifier;
import net.ardeus.myitems.manager.game.AbilityWeaponManager;
import net.ardeus.myitems.manager.game.ElementManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.manager.game.PowerCommandManager;
import net.ardeus.myitems.manager.game.PowerShootManager;
import net.ardeus.myitems.manager.game.PowerSpecialManager;
import net.ardeus.myitems.manager.game.RequirementManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.placeholder.ReplacerMVDWPlaceholderAPIBuild;
import net.ardeus.myitems.placeholder.ReplacerPlaceholderAPIBuild;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerCommand;
import net.ardeus.myitems.power.PowerSpecial;
import net.ardeus.myitems.power.PowerType;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.ListUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.TextUtil;

public class PlaceholderManager extends HandlerManager {
	
	private final PlaceholderConfig placeholderConfig;
	
	protected PlaceholderManager(MyItems plugin) {
		super(plugin);
		
		this.placeholderConfig = new PlaceholderConfig(plugin);
	};
	
	public final PlaceholderConfig getPlaceholderConfig() {
		return this.placeholderConfig;
	}
	
	public final Collection<String> getPlaceholderIDs() {
		return getPlaceholderConfig().getPlaceholderIDs();
	}
	
	public final Collection<String> getPlaceholders() {
		return getPlaceholderConfig().getPlaceholders();
	}
	
	public final String getPlaceholder(String id) {
		return getPlaceholderConfig().getPlaceholder(id);
	}
	
	public final HashMap<String, String> getPlaceholderCopy() {
		return getPlaceholderConfig().getPlaceholderCopy();
	}
	
	public final boolean isPlaceholderExists(String id) {
		return getPlaceholder(id) != null;
	}
	
	public final void registerAll() {
        final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
        final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
        final String placeholder = plugin.getPluginPlaceholder();
        
        if (supportManagerAPI.isSupportPlaceholderAPI()) {
            new ReplacerPlaceholderAPIBuild(plugin, placeholder).hook();
        }
        if (supportManagerAPI.isSupportMVdWPlaceholder()) {
            new ReplacerMVDWPlaceholderAPIBuild(plugin, placeholder).register();
        }
    }
	
	public final List<String> localPlaceholder(List<String> list) {
		final String divider = "\n";
		final String builder = TextUtil.convertListToString(list, divider);
		final String text = localPlaceholder(builder);
		
		return ListUtil.convertStringToList(text, divider);
	}
	
	public final String localPlaceholder(String text) {
		return TextUtil.placeholder(getPlaceholderCopy(), text);
	}
	
	public final List<String> pluginPlaceholder(List<String> list, String... identifiers) {
		return pluginPlaceholder(list, null, identifiers);
	}
	
	public final List<String> pluginPlaceholder(List<String> list, Player player, String... identifiers) {
		final String divider = "\n";
		final String builder = TextUtil.convertListToString(list, divider);
		final String text = pluginPlaceholder(builder, player, identifiers);
		
		return ListUtil.convertStringToList(text, divider);
	}
	
	public final String pluginPlaceholder(String text, String... identifiers) {
		return pluginPlaceholder(text, null, identifiers);
	}
	
	public final String pluginPlaceholder(String text, Player player, String... identifiers) {
		final HashMap<String, String> map = getMapPluginPlaceholder(player, identifiers);
		
		return TextUtil.placeholder(map, text);
	}
	
	public final HashMap<String, String> getMapPluginPlaceholder(String... identifiers) {
		return getMapPluginPlaceholder(null, identifiers);
	}
	
	public final HashMap<String, String> getMapPluginPlaceholder(Player player, String... identifiers) {
		final String placeholder = plugin.getPluginPlaceholder();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		for (String identifier : identifiers) {
			final String replacement = getReplacement(player, identifier);
			
			if (replacement != null) {
				final String key = placeholder + "_" + identifier;
				
				map.put(key, replacement);
			}
		}
		
		return map;
	}
    
    public final ItemStack parseItem(Player player, ItemStack item) {
        final String divider = "\n";
        
        if (EquipmentUtil.hasDisplayName(item)) {
            final String oldDisplayName = EquipmentUtil.getDisplayName(item);
            final String newDisplayName = placeholder(player, oldDisplayName);
            
            EquipmentUtil.setDisplayName(item, newDisplayName);
        }
        
        if (EquipmentUtil.hasLore(item)) {
            final List<String> oldLores = (List<String>)EquipmentUtil.getLores(item);
            final String oldLineLore = TextUtil.convertListToString(oldLores, divider);
            final String newLineLore = placeholder(player, oldLineLore);
            final String[] split = newLineLore.split(divider);
            final List<String> newLores = new ArrayList<String>();
            
            for (String lore : split) {
            	newLores.add(lore);
            }
            
            EquipmentUtil.setLores(item, newLores);
        }
        return item;
    }
    
    public final List<String> placeholder(Player player, List<String> listText) {
    	return placeholder(player, listText, null);
    }
    
    public final List<String> placeholder(Player player, List<String> listText, LoreStatsModifier modifier) {
    	return placeholder(player, listText, modifier, "{", "}");
    }
    
    public final List<String> placeholder(Player player, List<String> listText, LoreStatsModifier modifier, String leftKey, String rightKey) {
    	final List<String> list = new ArrayList<String>();
    	
    	if (listText != null) {
    		final ListIterator<String> iteratorText = listText.listIterator();
    		
    		while (iteratorText.hasNext()) {
    			final String text = iteratorText.next();
    			final String textPlaceholder = placeholder(player, text, modifier, leftKey, rightKey);
    			
    			iteratorText.set(textPlaceholder);
    		}
    		
    		list.addAll(listText);
    	}
    	
    	return listText;
    }
    
    public final String placeholder(Player player, String text) {
        return placeholder(player, text, null);
    }
    
    public final String placeholder(Player player, String text, LoreStatsModifier modifier) {
        return placeholder(player, text, modifier, "{", "}");
    }
    
    public final String placeholder(Player player, String text, LoreStatsModifier modifier, String leftKey, String rightKey) {
        final String placeholder = plugin.getPluginPlaceholder();
        
        if (text.contains(leftKey)) {
            final String[] fullPartFirst = text.split(Pattern.quote(leftKey));
            
            for (String checkPartFirst : fullPartFirst) {				
				if (checkPartFirst.contains(rightKey)) {
                    final String check = checkPartFirst.split(Pattern.quote(rightKey))[0];
                    
                    if (check.contains("_")) {
                        final String[] elements = check.split("_", 2);
                        final String textholder = elements[0];
                        final String identifier = elements[1];
                        
                        if (textholder.equalsIgnoreCase(placeholder)) {
                            final CharSequence replacement = this.getReplacement(player, identifier, modifier);
                            final CharSequence sequence = String.valueOf(leftKey) + check + rightKey;
                            
                            if (replacement != null) {
                                text = text.replace(sequence, replacement);
                            }
                        }
                    }
                }
            }
        }
        
        return text;
    }
    
    public final String getReplacement(Player player, String identifier) {
        return getReplacement(player, identifier, null);
    }
    
    public final String getReplacement(Player player, String identifier, LoreStatsModifier statsModifier) {
        final GameManager gameManager = plugin.getGameManager();
        final PowerCommandManager powerCommandManager = gameManager.getPowerCommandManager();
        final PowerShootManager powerShootManager = gameManager.getPowerShootManager();
        final PowerSpecialManager powerSpecialManager = gameManager.getPowerSpecialManager();
        final AbilityWeaponManager abilityWeaponManager = gameManager.getAbilityWeaponManager();
        final ElementManager elementManager = gameManager.getElementManager();
        final SocketGemManager socketManager = gameManager.getSocketManager();
        final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
        final LoreStatsManager statsManager = gameManager.getStatsManager();
        final RequirementManager requirementManager = gameManager.getRequirementManager();
        final String[] parts = identifier.split(":");
        final int length = parts.length;
        
        if (length > 0) {
            final String key = parts[0];
            
            if (key.equalsIgnoreCase("text_lorestats")) {
                if (length >= 3) {
                    final String textLoreStats = parts[1];
                    final LoreStatsEnum loreStats = LoreStatsEnum.get(textLoreStats);
                    
                    if (loreStats != null) {
                        final String textMinValue = parts[2];
                        final double modifier = (statsModifier != null) ? statsModifier.getModifier(loreStats) : 1.0;
                        
                        double minValue3;
                        double maxValue;
                        
                        if (textMinValue.contains("~")) {
                            final String[] componentsMinValue = textMinValue.split("~");
                            final String textMinValue2 = componentsMinValue[0];
                            final String textMinValue3 = componentsMinValue[1];
                            
                            if (!MathUtil.isNumber(textMinValue2) || !MathUtil.isNumber(textMinValue3)) {
                                return null;
                            } else {
	                            final double minValue1 = MathUtil.parseDouble(textMinValue2);
	                            final double minValue2 = MathUtil.parseDouble(textMinValue3);
	                            
	                            minValue3 = MathUtil.valueBetween(minValue1, minValue2);
                            }
                        } else {
                            if (!MathUtil.isNumber(textMinValue)) {
                                return null;
                            } else {
                            	minValue3 = MathUtil.roundNumber(MathUtil.parseDouble(textMinValue));
                            }
                        }
                        
                        if (length == 3) {
                            maxValue = minValue3;
                        } else {
                            final String textMaxValue = parts[3];
                            
                            if (textMaxValue.contains("~")) {
                                final String[] componentsMaxValue = textMaxValue.split("~");
                                final String textMaxValue2 = componentsMaxValue[0];
                                final String textMaxValue3 = componentsMaxValue[1];
                                
                                if (!MathUtil.isNumber(textMaxValue2) || !MathUtil.isNumber(textMaxValue3)) {
                                    return null;
                                } else {
	                                final double maxValue2 = MathUtil.parseDouble(textMaxValue2);
	                                final double maxValue3 = MathUtil.parseDouble(textMaxValue3);
	                                
	                                maxValue = MathUtil.valueBetween(maxValue2, maxValue3);
                                }
                            } else {
                                if (!MathUtil.isNumber(textMaxValue)) {
                                    return null;
                                } else {
                                	maxValue = MathUtil.roundNumber(MathUtil.parseDouble(textMaxValue));
                                }
                            }
                        }
                        
                        return statsManager.getTextLoreStats(loreStats, MathUtil.roundNumber(minValue3 * modifier, 2), MathUtil.roundNumber(maxValue * modifier, 2));
                    }
                }
            } else if (key.equalsIgnoreCase("text_ability")) {
                if (length >= 4) {
                    final String ability = parts[1];
                    final AbilityWeapon abilityWeapon = abilityWeaponManager.getAbilityWeapon(ability);
                    
                    if (abilityWeapon != null) {
                        final String textGrade = parts[2];
                        final String textChance = parts[3];
                        
                        int grade3;
                        double chance3;
                        
                        if (textGrade.contains("~")) {
                            final String[] componentsGrade = textGrade.split("~");
                            final String textGrade2 = componentsGrade[0];
                            final String textGrade3 = componentsGrade[1];
                            
                            if (!MathUtil.isNumber(textGrade2) || !MathUtil.isNumber(textGrade3)) {
                                return null;
                            } else {
	                            final int grade1 = MathUtil.parseInteger(textGrade2);
	                            final int grade2 = MathUtil.parseInteger(textGrade3);
	                            final int rawGrade = (int)MathUtil.valueBetween((double)grade1, (double)grade2);
	                            
	                            grade3 = MathUtil.limitInteger(rawGrade, 1, rawGrade);
                            }
                        } else {
                            if (!MathUtil.isNumber(textGrade)) {
                                return null;
                            } else {
	                            final int rawGrade2 = MathUtil.parseInteger(textGrade);
	                            
	                            grade3 = MathUtil.limitInteger(rawGrade2, 1, rawGrade2);
                            }
                        }
                        
                        
                        if (textChance.contains("~")) {
                            final String[] componentsChance = textChance.split("~");
                            final String textChance2 = componentsChance[0];
                            final String textChance3 = componentsChance[1];
                            
                            if (!MathUtil.isNumber(textChance2) || !MathUtil.isNumber(textChance3)) {
                                return null;
                            } else {
                            	final double chance1 = MathUtil.parseDouble(textChance2);
                            	final double chance2 = MathUtil.parseDouble(textChance3);
                            	
                                chance3 = MathUtil.valueBetween(chance1, chance2);
                            }
                        } else {
                            if (!MathUtil.isNumber(textChance)) {
                                return null;
                            } else {
                            	chance3 = MathUtil.roundNumber(MathUtil.parseDouble(textChance));
                            }
                        }
                        
                        return abilityWeaponManager.getTextAbility(abilityWeapon, grade3, chance3);
                    }
                }
            }
            else if (key.equalsIgnoreCase("text_buff")) {
                if (length >= 3) {
                    final String textBuff = parts[1];
                    final PassiveEffectEnum effect = PassiveEffectEnum.get(textBuff);
                    
                    if (effect != null) {
                        final String textGrade = parts[2];
                        
                        int grade6;
                        
                        if (textGrade.contains("~")) {
                            final String[] componentsGrade2 = textGrade.split("~");
                            final String textGrade4 = componentsGrade2[0];
                            final String textGrade5 = componentsGrade2[1];
                            
                            if (!MathUtil.isNumber(textGrade4) || !MathUtil.isNumber(textGrade5)) {
                                return null;
                            } else {
                            	final int grade4 = MathUtil.parseInteger(textGrade4);
                            	final int grade5 = MathUtil.parseInteger(textGrade5);
                                grade6 = (int)MathUtil.valueBetween((double)grade4, (double)grade5);
                            }
                        } else {
                            if (!MathUtil.isNumber(textGrade)) {
                                return null;
                            } else {
                            	grade6 = MathUtil.parseInteger(textGrade);
                            }
                        }
                        
                        return passiveEffectManager.getTextPassiveEffect(effect, grade6);
                    }
                }
            }
            else if (key.equalsIgnoreCase("text_power")) {
                if (length >= 4) {
                    final String textPower = parts[1];
                    final PowerType powerType = PowerType.getPowerType(textPower);
                    
                    if (powerType != null) {
                        final String textClick = parts[2];
                        final PowerClickType click = PowerClickType.getPowerClickType(textClick);
                        
                        if (click != null) {
                            final String textType = parts[3];
                            
                            double cooldown;
                            
                            if (length == 4) {
                                cooldown = 0.0;
                            } else {
                                final String textCooldown = parts[4];
                                
                                if (textCooldown.contains("~")) {
                                    final String[] componentsCooldown = textCooldown.split("~");
                                    final String textCooldown2 = componentsCooldown[0];
                                    final String textCooldown3 = componentsCooldown[1];
                                    
                                    if (!MathUtil.isNumber(textCooldown2) || !MathUtil.isNumber(textCooldown3)) {
                                        return null;
                                    } else {
	                                    final double cooldown2 = MathUtil.parseDouble(textCooldown2);
	                                    final double cooldown3 = MathUtil.parseDouble(textCooldown3);
	                                    
	                                    cooldown = MathUtil.valueBetween(cooldown2, cooldown3);
                                    }
                                } else {
                                    if (!MathUtil.isNumber(textCooldown)) {
                                        return null;
                                    } else {
                                    	cooldown = MathUtil.roundNumber(MathUtil.parseDouble(textCooldown));
                                    }
                                }
                            }
                            
                            if (powerType.equals(PowerType.COMMAND)) {
                                final PowerCommand powerCommand = powerCommandManager.getPowerCommand(textType);
                                
                                if (powerCommand != null) {
                                    return powerCommandManager.getTextPowerCommand(click, powerCommand, cooldown);
                                }
                            } else if (powerType.equals(PowerType.SHOOT)) {
                                final ProjectileEnum type = ProjectileEnum.getProjectileEnum(textType);
                                
                                if (type != null) {
                                    return powerShootManager.getTextPowerShoot(click, type, cooldown);
                                }
                            } else if (powerType.equals(PowerType.SPECIAL)) {
                            	final PowerSpecial powerSpecial = powerSpecialManager.getPowerSpecial(textType);
                                
                                if (powerSpecial != null) {
                                    return powerSpecialManager.getTextPowerSpecial(click, powerSpecial, cooldown);
                                }
                            }
                        }
                    }
                }
            } else if (key.equalsIgnoreCase("text_socket_empty")) {
                if (length >= 1) {
                    return socketManager.getTextSocketGemSlotEmpty();
                }
            } else if (key.equalsIgnoreCase("text_socket_fill")) {
                if (length >= 3) {
                    final String gems = parts[1];
                    
                    if (socketManager.isExist(gems)) {
                        final String textGrade6 = parts[2];
                        
                        int grade9;
                        
                        if (textGrade6.contains("~")) {
                            final String[] componentsGrade3 = textGrade6.split("~");
                            final String textGrade7 = componentsGrade3[0];
                            final String textGrade8 = componentsGrade3[1];
                            
                            if (!MathUtil.isNumber(textGrade7) || !MathUtil.isNumber(textGrade8)) {
                                return null;
                            } else {
	                            final int grade7 = MathUtil.parseInteger(textGrade7);
	                            final int grade8 = MathUtil.parseInteger(textGrade8);
	                            final int rawGrade3 = (int)MathUtil.valueBetween((double)grade7, (double)grade8);
	                            grade9 = MathUtil.limitInteger(rawGrade3, 1, rawGrade3);
                            }
                        } else {
                            if (!MathUtil.isNumber(textGrade6)) {
                                return null;
                            } else {
                            	grade9 = MathUtil.parseInteger(textGrade6);
                            }
                        }
                        
                        return socketManager.getTextSocketGemsLore(gems, grade9);
                    }
                }
            } else if (key.equalsIgnoreCase("text_element") && length >= 3) {
                final String textElement = parts[1];
                
                if (elementManager.isExists(textElement)) {
                    final String textValue = parts[2];
                    
                    double value3;
                    
                    if (textValue.contains("~")) {
                        final String[] componentsValue = textValue.split("~");
                        final String textValue2 = componentsValue[0];
                        final String textValue3 = componentsValue[1];
                        
                        if (!MathUtil.isNumber(textValue2) || !MathUtil.isNumber(textValue3)) {
                            return null;
                        } else {
	                        final double value1 = MathUtil.parseDouble(textValue2);
	                        final double value2 = MathUtil.parseDouble(textValue3);
	                        
	                        value3 = MathUtil.valueBetween(value1, value2);
                        }
                    } else {
                        if (!MathUtil.isNumber(textValue)) {
                            return null;
                        } else {
                        	value3 = MathUtil.roundNumber(MathUtil.parseDouble(textValue));
                        }
                    }
                    
                    return elementManager.getTextElement(textElement, value3);
                }
            } else if (key.equalsIgnoreCase("text_requirement_unbound")) {
            	return requirementManager.getTextSoulUnbound();
            } else if (key.equalsIgnoreCase("text_requirement_bound") && length >= 2) {
            	final String textBound = parts[1];
            	final OfflinePlayer bound = PlayerUtil.getPlayer(textBound);
            	
            	if (bound != null) {
            		return bound.getName();
            	}
            } else if (key.equalsIgnoreCase("text_requirement_level") && length >= 2) {
            	final String textLevel = parts[1];
            	
            	int level;
            	
            	if (textLevel.contains("~")) {
                    final String[] componentsLevel = textLevel.split("~");
                    final String textLevel1 = componentsLevel[0];
                    final String textLevel2 = componentsLevel[1];
                    
                    if (!MathUtil.isNumber(textLevel1) || !MathUtil.isNumber(textLevel2)) {
                        return null;
                    } else {
                        final int level1 = MathUtil.parseInteger(textLevel1);
                        final int level2 = MathUtil.parseInteger(textLevel2);
                        final int rawLevel = (int)MathUtil.valueBetween((double)level1, (double)level2);
                        
                        level = MathUtil.limitInteger(rawLevel, 0, rawLevel);
                    }
            	} else {
            		if (!MathUtil.isNumber(textLevel)) {
            			return null;
            		} else {
	                	level = MathUtil.parseInteger(textLevel);
	                	level = MathUtil.limitInteger(level, 0, level);
            		}
                }
            	
            	return requirementManager.getTextLevel(level);
            } else if (key.equalsIgnoreCase("text_requirement_permission") && length >= 2) {
            	final String textPermission = parts[1];
            	
            	return requirementManager.getTextPermission(textPermission);
            } else if (key.equalsIgnoreCase("text_requirement_class") && length >= 2) {
            	final String textClass = parts[1];
            	
            	return requirementManager.getTextClass(textClass);
            }
        }
        
        return null;
    }
}
	