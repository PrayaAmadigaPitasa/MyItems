package net.ardeus.myitems.manager.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.command.CommandSender;

import core.praya.agarthalib.builder.command.CommandBuild;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.CommandConfig;
import net.ardeus.myitems.handler.HandlerManager;

public class CommandManager extends HandlerManager {

	private final CommandConfig commandConfig;
	
	protected CommandManager(MyItems plugin) {
		super(plugin);
		
		this.commandConfig = new CommandConfig(plugin);
	};
	
	public final CommandConfig getCommandConfig() {
		return this.commandConfig;
	}
	
	public final Collection<String> getCommandIDs() {
		return getCommandConfig().getCommandIDs();
	}
	
	public final Collection<CommandBuild> getCommandBuilds() {
		return getCommandConfig().getCommandBuilds();
	}
	
	public final CommandBuild getCommand(String id) {
		return getCommandConfig().getCommand(id);
	}
	
	public final boolean isCommandExists(String id) {
		return getCommand(id) != null;
	}
	
	public final boolean checkCommand(String arg, String id) {
		final CommandBuild commandBuild = getCommand(id);
		
		if (commandBuild != null) {
			for (String aliases : commandBuild.getAliases()) {
				if (aliases.equalsIgnoreCase(arg)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	public final boolean checkPermission(CommandSender sender, String id) {
		final CommandBuild commandBuild = getCommand(id);
		
		if (commandBuild != null) {
			final String permission = commandBuild.getPermission();
			
			return permission != null ? sender.hasPermission(permission) : true;
		}
		
		return true;
	}
	
	public final List<String> getAliases(String id) {
		final CommandBuild commandBuild = getCommand(id);
		
		return commandBuild != null ? commandBuild.getAliases() : new ArrayList<String>();
	}
	
	public final String getPermission(String id) {
		final CommandBuild commandBuild = getCommand(id);
		
		return commandBuild != null ? commandBuild.getPermission() : null;
	}
}
