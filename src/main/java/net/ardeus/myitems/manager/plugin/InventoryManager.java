package net.ardeus.myitems.manager.plugin;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import core.praya.agarthalib.builder.inventory.InventoryBuild;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public class InventoryManager extends HandlerManager {

	private final HashMap<UUID, InventoryBuild> mapInventoryBuild = new HashMap<UUID, InventoryBuild>();
	private final HashMap<UUID, Long> mapInventoryCooldown = new HashMap<UUID, Long>();
	private final HashMap<UUID, Long> mapInventoryCloseCooldown = new HashMap<UUID, Long>();
	
	protected InventoryManager(MyItems plugin) {
		super(plugin);
	};
	
	public final HashMap<UUID, InventoryBuild> getMapInventoryBuild() {
		return mapInventoryBuild;
	}
	
	public final HashMap<UUID, Long> getMapInventoryCooldown() {
		return mapInventoryCooldown;
	}
	
	public final HashMap<UUID, Long> getMapInventoryCloseCooldown() {
		return mapInventoryCloseCooldown;
	}
	
	public final long getInventoryCooldown(Player player) {
		return hasInventoryCooldown(player) ? getMapInventoryCooldown().get(player.getUniqueId()) : 0;
	}
	
	public final long getInventoryCloseCooldown(Player player) {
		return hasInventoryCloseCooldown(player) ? getMapInventoryCloseCooldown().get(player.getUniqueId()) : 0;
	}
	
	public final InventoryBuild getInventoryBuild(Player player) {
		return hasInventoryBuild(player) ? getMapInventoryBuild().get(player.getUniqueId()) : null;
	}
	
	public final void setInventoryCooldown(Player player, long cooldown) {
		final long expired = System.currentTimeMillis() + cooldown;
		
		getMapInventoryCooldown().put(player.getUniqueId(), expired);
	}
	
	public final void setInventoryCloseCooldown(Player player, long cooldown) {
		final long expired = System.currentTimeMillis() + cooldown;
		
		getMapInventoryCloseCooldown().put(player.getUniqueId(), expired);
	}
	
	public final boolean isCooldown(Player player) {
		if (hasInventoryCooldown(player)) {
			final long expired = getInventoryCooldown(player);
			final long time = System.currentTimeMillis();
			
			if (time < expired) {
				return true;
			} else {
				removeInventoryCooldown(player);
				return false;
			}
		} else {
			return false;
		}
	}
	
	public final boolean isCloseCooldown(Player player) {
		if (hasInventoryCloseCooldown(player)) {
			final long expired = getInventoryCloseCooldown(player);
			final long time = System.currentTimeMillis();
			
			if (time < expired) {
				return true;
			} else {
				removeInventoryCloseCooldown(player);
				return false;
			}
		} else {
			return false;
		}
	}
	
	public final void openInventory(Player player, InventoryBuild inventoryBuild) {
		final UUID playerID = player.getUniqueId();
		final Inventory inventory = inventoryBuild.getInventory();
		
		if (inventoryBuild.hasSoundOpen()) {
			SenderUtil.playSound(player, inventoryBuild.getSoundOpen());
		}
		
		setInventoryCooldown(player, 50);
		player.openInventory(inventory);
		getMapInventoryBuild().put(playerID, inventoryBuild);
	}
	
	public final InventoryBuild createInventory(Player player, String title, InventoryType type, int row) {
		return createInventory(player, title, type, row, false, false);
	}
	
	public final InventoryBuild createInventory(Player player, String title, InventoryType type, int row, boolean isEditable, boolean isOwned) {
			
		Inventory inventory;
		
		if (type.equals(InventoryType.CHEST)) {
			final int size = MathUtil.limitInteger(row, 1, 6) * 9;
			
			title = title.isEmpty() ? "Menu" : (title.length() > 32 ? title.substring(0, 32) : title);
			title = TextUtil.colorful(title);
			inventory = Bukkit.createInventory((isOwned ? player : null), size, title);
		} else {
			
			title = TextUtil.colorful(title);
			inventory = Bukkit.createInventory((isOwned ? player : null), type, title);
		}
		
		return new InventoryBuild(inventory, isEditable);
	}
	
	public final boolean hasInventoryBuild(Player player) {		
		return getMapInventoryBuild().containsKey(player.getUniqueId());
	}
	
	public final boolean hasInventoryCooldown(Player player) {		
		return getMapInventoryCooldown().containsKey(player.getUniqueId());
	}
	
	public final boolean hasInventoryCloseCooldown(Player player) {		
		return getMapInventoryCloseCooldown().containsKey(player.getUniqueId());
	}
	
	public final void removeInventoryBuild(Player player) {
		getMapInventoryBuild().remove(player.getUniqueId());
	}
	
	public final void removeInventoryCooldown(Player player) {
		getMapInventoryCooldown().remove(player.getUniqueId());
	}
	
	public final void removeInventoryCloseCooldown(Player player) {
		getMapInventoryCloseCooldown().remove(player.getUniqueId());
	}
}
