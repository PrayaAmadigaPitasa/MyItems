package net.ardeus.myitems.manager.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.main.LanguageBuild;
import core.praya.agarthalib.builder.message.MessageBuild;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.LanguageConfig;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;

public class LanguageManager extends HandlerManager {

	private final LanguageConfig langConfig;
	
	protected LanguageManager(MyItems plugin) {
		super(plugin);
		
		this.langConfig = new LanguageConfig(plugin);
	};
	
	public final LanguageConfig getLangConfig() {
		return this.langConfig;
	}
	
	public final Collection<String> getLanguageIDs() {
		return getLangConfig().getLanguageIDs();
	}
	
	public final Collection<LanguageBuild> getLanguageBuilds() {
		return getLangConfig().getLanguageBuilds();
	}
	
	public final LanguageBuild getLanguageBuild(String id) {
		return getLangConfig().getLanguageBuild(id);
	}
	
	public final LanguageBuild getLanguage(String id) {
		return getLangConfig().getLanguage(id);
	}
	
	public final boolean isLanguageExists(String id) {
		return getLanguageBuild(id) != null;
	}
	
	public final List<String> getListText(LivingEntity entity, String key) {
		if (entity != null && entity instanceof Player) {
			final Player player = (Player) entity;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getListText(locale, key);
		} else {
			return getListText(key);
		}
	}
	
	public final List<String> getListText(CommandSender sender, String key) {
		if (sender != null && sender instanceof Player) {
			final Player player = (Player) sender;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getListText(locale, key);
		} else {
			return getListText(key);
		}
	}
	
	public final List<String> getListText(String key) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String locale = mainConfig.getGeneralLocale();
		
		return getListText(locale, key);
	}
	
	public final List<String> getListText(String id, String key) {
		final MessageBuild message = getMessage(id, key);
		
		return message != null ? message.getListText() : new ArrayList<String>();
	}
	
	public final String getText(LivingEntity entity, String key) {
		if (entity != null && entity instanceof Player) {
			final Player player = (Player) entity;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getText(locale, key);
		} else {
			return getText(key);
		}
	}
	
	public final String getText(CommandSender sender, String key) {
		if (sender != null && sender instanceof Player) {
			final Player player = (Player) sender;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getText(locale, key);
		} else {
			return getText(key);
		}
	}
	
	public final String getText(String key) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String locale = mainConfig.getGeneralLocale();
		
		return getText(locale, key);
	}
	
	public final String getText(String id, String key) {
		final MessageBuild message = getMessage(id, key);
		
		return message != null ? message.getText() : "";
	}
	
	public final MessageBuild getMessage(LivingEntity entity, String key) {
		if (entity != null && entity instanceof Player) {
			final Player player = (Player) entity;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getMessage(locale, key);
		} else {
			return getMessage(key);
		}
	}
	
	public final MessageBuild getMessage(CommandSender sender, String key) {
		if (sender != null && sender instanceof Player) {
			final Player player = (Player) sender;
			final String locale = Bridge.getBridgePlayer().getLocale(player);
			
			return getMessage(locale, key);
		} else {
			return getMessage(key);
		}
	}
	
	public final MessageBuild getMessage(String key) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String locale = mainConfig.getGeneralLocale();
		
		return getMessage(locale, key);
	}
	
	public final MessageBuild getMessage(String id, String key) {
		return getLangConfig().getMessage(id, key);
	}
}