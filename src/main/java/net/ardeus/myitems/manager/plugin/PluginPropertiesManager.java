package net.ardeus.myitems.manager.plugin;

import java.util.ArrayList;
import java.util.List;

import core.praya.agarthalib.builder.plugin.PluginBannedPropertiesBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesResourceBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesStreamBuild;
import core.praya.agarthalib.builder.plugin.PluginTypePropertiesBuild;
import core.praya.agarthalib.utility.ServerUtil;
import core.praya.agarthalib.utility.SortUtil;
import core.praya.agarthalib.utility.SystemUtil;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;

@SuppressWarnings("deprecation")
public class PluginPropertiesManager extends HandlerManager {
	
	private final PluginPropertiesResourceBuild pluginPropertiesResource;
	private final PluginPropertiesStreamBuild pluginPropertiesStream;
	
	public PluginPropertiesManager(MyItems plugin) {
		super(plugin);
		
		this.pluginPropertiesResource = PluginPropertiesBuild.getPluginPropertiesResource(plugin, plugin.getPluginType(), plugin.getPluginVersion());
		this.pluginPropertiesStream = PluginPropertiesBuild.getPluginPropertiesStream(plugin);
	}
	
	public final PluginPropertiesResourceBuild getPluginPropertiesResource() {
		return this.pluginPropertiesResource;
	}
	
	public final PluginPropertiesStreamBuild getPluginPropertiesStream() {
		return this.pluginPropertiesStream;
	}
	
	public final boolean isActivated() {
		return getPluginPropertiesStream().isActivated();
	}
	
	public final String getName() {
		return getPluginPropertiesStream().getName();
	}
	
	public final String getCompany() {
		return getPluginPropertiesStream().getCompany();
	}
	
	public final String getAuthor() {
		return getPluginPropertiesStream().getAuthor();
	}
	
	public final String getWebsite() {
		return getPluginPropertiesStream().getWebsite();
	}
	
	public final List<String> getDevelopers() {
		return getPluginPropertiesStream().getDevelopers();
	}
	
	public final List<String> getListType() {
		return SortUtil.toArray(getPluginPropertiesStream().getTypeProperties().keySet());
	}
	
	public final PluginTypePropertiesBuild getTypeProperties(String type) {
		if (type != null) {
			for (String key : getListType()) {
				if (key.equalsIgnoreCase(type)) {
					return getPluginPropertiesStream().getTypeProperties().get(key);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isTypeExists(String type) {
		return getTypeProperties(type) != null;
	}
	
	public final boolean isLatestVersion() {
		final String type = plugin.getPluginType();
		final String versionLatest = getPluginTypeVersion(type);
		final String versionCurrent = plugin.getPluginVersion();
		
		
		return versionLatest.equalsIgnoreCase(versionCurrent);
	}
	
	public final String getPluginTypeVersion(String type) {
		final PluginTypePropertiesBuild typeProperties = getTypeProperties(type);
		
		return typeProperties != null ? typeProperties.getVersion() : getPluginPropertiesResource().getVersion();
	}
	
	
	public final String getPluginTypeWebsite(String type) {
		final PluginTypePropertiesBuild typeProperties = getTypeProperties(type);
		
		return typeProperties != null ? typeProperties.getWebsite() : getPluginPropertiesResource().getWebsite();
	}
	
	@Deprecated
	public final String getLatestVersion(String type) {
		return getPluginTypeVersion(type);
	}
	
	@Deprecated
	public final List<String> getAnnouncement(String type) {
		final PluginTypePropertiesBuild typeProperties = getTypeProperties(type);
		
		return typeProperties != null ? typeProperties.getAnnouncement() : new ArrayList<String>();
	}
	
	@Deprecated
	public final List<String> getChangelog(String type) {
		final PluginTypePropertiesBuild typeProperties = getTypeProperties(type);
		
		return typeProperties != null ? typeProperties.getChangelog() : new ArrayList<String>();
	}
	
	@Deprecated
	public final List<String> getReasonBanned() {
		return SortUtil.toArray(getPluginPropertiesStream().getBannedProperties().keySet());
	}
	
	@Deprecated
	public final boolean isReasonBannedExists(String reason) {
		return getBannedProperties(reason) != null;
	}
	
	@Deprecated
	public final PluginBannedPropertiesBuild getBannedProperties(String reason) {
		for (String key : getPluginPropertiesStream().getBannedProperties().keySet()) {
			if (key.equalsIgnoreCase(reason)) {
				return getPluginPropertiesStream().getBannedProperties().get(key);
			}
		}
		
		return null;
	}
	
	public final void check() {		
		final PluginManager pluginManager = plugin.getPluginManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!isActivated()) {
			final String message = lang.getText("Plugin_Deactivated");
			
			disable(message);
			return;			
		} else if (!checkPluginName() || !checkPluginAuthor()) {
			final String message = lang.getText("Plugin_Information_Not_Match");
			
			disable(message);
			return;
		}
	}
	
	private final boolean checkPluginName() {
		if (getName() != null) {
			if (!getName().equalsIgnoreCase(getPluginPropertiesResource().getName())) {
				return false;
			}
		}
		
		return true;
	}
	
	private final boolean checkPluginAuthor() {
		if (getAuthor() != null) {
			if (!getAuthor().equalsIgnoreCase(getPluginPropertiesResource().getAuthor())) {
				return false;
			}
		}
		
		return true;
	}
	
	private final void disable(String message) {
		if (message != null) {
			SystemUtil.sendMessage(message);
		}
		
		plugin.getPluginLoader().disablePlugin(plugin);
	}
	
	@Deprecated
	public final boolean isBanned() {
		return getBannedReason() != null;
	}
	
	@Deprecated
	public final String getBannedReason() {
		final String serverIP = ServerUtil.getIP();
		
		for (String reason : pluginPropertiesStream.getBannedProperties().keySet()) {
			for (String ip : pluginPropertiesStream.getBannedProperties().get(reason).getServers()) {
				if (ip.equalsIgnoreCase(serverIP)) {
					return reason;
				}
			}
		}
		
		return null;
	}
}
