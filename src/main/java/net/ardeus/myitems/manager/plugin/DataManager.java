package net.ardeus.myitems.manager.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import core.praya.agarthalib.builder.main.DataBuild;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.DataConfig;
import net.ardeus.myitems.handler.HandlerManager;

public class DataManager extends HandlerManager {

	private final DataConfig dataConfig;
	
	protected DataManager(MyItems plugin) {
		super(plugin);
		
		this.dataConfig = new DataConfig(plugin);
	};
	
	public final DataConfig getDataConfig() {
		return this.dataConfig;
	}
	
	public final Collection<String> getDataIDs() {
		return getDataConfig().getDataIDs();
	}
	
	public final Collection<DataBuild> getDataBuilds() {
		return getDataConfig().getDataBuilds();
	}
	
	public final DataBuild getData(String id) {
		return getDataConfig().getData(id);
	}
	
	public final boolean isDataExists(String id) {
		return getData(id) != null;
	}
	
	public final List<String> getListPath(String id) {
		final DataBuild data = getData(id);
		
		return data != null ? data.getListPath() : new ArrayList<String>();
	}
	
	public final String getPath(String id) {
		final DataBuild data = getData(id);
		
		return data != null ? data.getPath() : "";
	}
}
