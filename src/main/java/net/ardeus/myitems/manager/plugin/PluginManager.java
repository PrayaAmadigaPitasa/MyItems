package net.ardeus.myitems.manager.plugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;

public class PluginManager extends HandlerManager {

	boolean initialize = false;
	
	private DataManager dataManager;
	private LanguageManager languageManager;
	private CommandManager commandManager;
	private PlaceholderManager placeholderManager;
	private PluginPropertiesManager pluginPropertiesManager;
	private MetricsManager metricsManager;
	
	public PluginManager(MyItems plugin) {
		super(plugin);
	};
	
	public final void initialize() {
		if (!initialize) {
			
			initialize = true;
			
			dataManager = new DataManager(plugin);
			placeholderManager = new PlaceholderManager(plugin);
			languageManager = new LanguageManager(plugin);
			commandManager = new CommandManager(plugin);			
			pluginPropertiesManager = new PluginPropertiesManager(plugin);
			metricsManager = new MetricsManager(plugin);
		}
	}
	
	public final CommandManager getCommandManager() {
		return this.commandManager;
	}
	
	public final DataManager getDataManager() {
		return this.dataManager;
	}
	
	public final LanguageManager getLanguageManager() {
		return this.languageManager;
	}
	
	public final PlaceholderManager getPlaceholderManager() {
		return this.placeholderManager;
	}
	
	public final PluginPropertiesManager getPluginPropertiesManager() {
		return this.pluginPropertiesManager;
	}
	
	public final MetricsManager getMetricsManager() {
		return this.metricsManager;
	}
}
