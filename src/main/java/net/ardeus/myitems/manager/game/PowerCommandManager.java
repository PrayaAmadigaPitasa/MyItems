package net.ardeus.myitems.manager.game;

import java.util.HashMap;
import java.util.List;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerCommand;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public abstract class PowerCommandManager extends HandlerManager {
	
	protected PowerCommandManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract List<String> getPowerCommandIds();
	public abstract List<PowerCommand> getAllPowerCommandProperties();
	public abstract PowerCommand getPowerCommand(String powerCommandId);
	public abstract PowerCommand getPowerCommandByKeyLore(String keyLore);
	
	public final PowerCommand getPowerCommandByLore(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (lore != null) {
			final String[] loreCheck = lore.split(MainConfig.KEY_COMMAND);
			
			if (loreCheck.length > 1) {
				final String colorPowerType = mainConfig.getPowerColorType();
				final String keyLore = loreCheck[1].replace(colorPowerType, "");
				
				return getPowerCommandByKeyLore(keyLore);
			}
		}
		
		return null;
	}
	
	public final boolean isPowerCommandExists(String powerCommandId) {
		return getPowerCommand(powerCommandId) != null;
	}
	
	public final boolean isLorePowerCommand(String lore) {
		return getPowerCommandByLore(lore) != null;
	}
	
	public final String getTextPowerCommand(PowerClickType click, PowerCommand powerCommand, double cooldown) {
		final PowerManager powerManager = plugin.getGameManager().getPowerManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (click != null && powerCommand != null) {
			final String keyCommand = getKeyCommand(powerCommand);
			final HashMap<String, String> map = new HashMap<String, String>();
			
			String format = mainConfig.getPowerFormat();
			
			cooldown = MathUtil.roundNumber(cooldown, 1);
			
			map.put("click", powerManager.getKeyClick(click));
			map.put("type", keyCommand);
			map.put("cooldown", powerManager.getKeyCooldown(cooldown));
			format = TextUtil.placeholder(map, format, "<", ">");
			
			return format;
		} else {
			return null;
		}
	}
	
	public final String getKeyCommand(PowerCommand powerCommand) {
		return getKeyCommand(powerCommand, false);
	}
	
	public final String getKeyCommand(PowerCommand powerCommand, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (powerCommand != null) {
			final String key = MainConfig.KEY_COMMAND;
			final String color = mainConfig.getPowerColorType();
			final String keyLore = powerCommand.getKeyLore();
			
			return justCheck ? key + color + keyLore : key + color + keyLore + key + color;
		} else {
			return null;
		}
	}
}
