package net.ardeus.myitems.manager.game;

import java.util.Collection;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.item.ItemTier;

public abstract class ItemTierManager extends HandlerManager {
	
	protected ItemTierManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract Collection<String> getItemTierIds();
	public abstract Collection<ItemTier> getItemTiers();
	public abstract ItemTier getItemTier(String id);
	
	public final boolean isItemTierExists(String id) {
		return getItemTier(id) != null;
	}
}