package net.ardeus.myitems.manager.game;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.SlotType;
import core.praya.agarthalib.enums.main.VersionNMS;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsOption;
import net.ardeus.myitems.lorestats.LoreStatsWeapon;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;

import com.praya.agarthalib.utility.EntityUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerUtil;
import com.praya.agarthalib.utility.TextUtil;

public class LoreStatsManager extends HandlerManager {

	protected LoreStatsManager(MyItems plugin) {
		super(plugin);
	};
	
	public final boolean hasLoreStats(Player player, String lorestats) {
		return hasLoreStats(Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND), LoreStatsEnum.get(lorestats));
	}
	
	public final boolean hasLoreStats(Player player, LoreStatsEnum lorestats) {
		return hasLoreStats(Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND), lorestats);
	}
	
	public final boolean hasLoreStats(ItemStack item, String lorestats) {
		return hasLoreStats(item, LoreStatsEnum.get(lorestats));
	}
		
	public final boolean hasLoreStats(ItemStack item, LoreStatsEnum loreStats) {
		if (EquipmentUtil.loreCheck(item)) {
			final String lores = EquipmentUtil.getLores(item).toString();
			
			return lores.contains(getKeyStats(loreStats, true));
		}
		
		return false;
	}

	public final String getTextLoreStats(LoreStatsEnum loreStats, double value) {
		return loreStats.equals(LoreStatsEnum.LEVEL) ? getTextLoreStats(loreStats, value, 0) : getTextLoreStats(loreStats, value, value);
	}
	
	public final String getTextLoreStats(LoreStatsEnum loreStats, double value1, double value2) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		String format = mainConfig.getStatsFormatValue();
		
		map.put("stats", getKeyStats(loreStats));
		map.put("value", statsValue(loreStats, value1, value2));
		format = TextUtil.placeholder(map, format, "<", ">");
		
		return format;
	}
	
	public final double getLoreValue(ItemStack item, LoreStatsEnum loreStats, LoreStatsOption option) {
		final int line = getLineLoreStats(item, loreStats);
		
		return line != -1 ? getLoreValue(item, loreStats, option, line) : 0; 
	}
	
	public final double getLoreValue(ItemStack item, LoreStatsEnum loreStats, LoreStatsOption option, int line) {
		final String lore = EquipmentUtil.getLores(item).get(line-1);
		
		return getLoreValue(loreStats, option, lore);
	}
		
	public final double getLoreValue(LoreStatsEnum loreStats, LoreStatsOption option, String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String[] textListValue = lore.split(MainConfig.KEY_STATS_VALUE);
		
		if (textListValue.length > 1) {
			final String positiveValue = mainConfig.getStatsLorePositiveValue();
			final String negativeValue = mainConfig.getStatsLoreNegativeValue();
			final String symbolDivide = mainConfig.getStatsLoreDividerSymbol();
			final String symbolMultiplier = mainConfig.getStatsLoreMultiplierSymbol();
			
			String textValue = textListValue[1];
			
			if (loreStats.equals(LoreStatsEnum.DAMAGE)) {
				final String symbolRange = mainConfig.getStatsLoreRangeSymbol();
				
				if (textValue.contains(symbolRange)) {
					final String[] valueList = textValue.split(symbolRange) ;
					final String textValueMin = valueList[0].replaceFirst(positiveValue, "").replaceFirst(negativeValue, "");
					final String textValueMax = valueList[1].replaceFirst(positiveValue, "").replaceFirst(negativeValue, "");
					
					double valueMin;
					double valueMax;
					double value;
						
					if (MathUtil.isNumber(textValueMin) && MathUtil.isNumber(textValueMax)) {
						
						valueMin = MathUtil.parseDouble(textValueMin);
						valueMax = MathUtil.parseDouble(textValueMax);
						
						value = valueMin + (Math.random() * (valueMax - valueMin));
						
						if (option == null) {
							return value;
						} else {
							return (option.equals(LoreStatsOption.MIN) || option.equals(LoreStatsOption.CURRENT)) ? valueMin : valueMax;
						}
					}
				} else {
					
					textValue = textValue.replaceFirst(positiveValue, "").replaceFirst(negativeValue, "");
											
					if (MathUtil.isNumber(textValue)) {
						return MathUtil.parseDouble(textValue);
					}
				}
			} else if (loreStats.equals(LoreStatsEnum.CRITICAL_CHANCE) || loreStats.equals(LoreStatsEnum.PENETRATION) || loreStats.equals(LoreStatsEnum.BLOCK_AMOUNT) || loreStats.equals(LoreStatsEnum.BLOCK_RATE) || loreStats.equals(LoreStatsEnum.HIT_RATE) || loreStats.equals(LoreStatsEnum.DODGE_RATE) || loreStats.equals(LoreStatsEnum.PVP_DAMAGE) || loreStats.equals(LoreStatsEnum.PVE_DAMAGE) || loreStats.equals(LoreStatsEnum.PVP_DEFENSE) || loreStats.equals(LoreStatsEnum.PVE_DEFENSE) || loreStats.equals(LoreStatsEnum.ATTACK_AOE_DAMAGE) || loreStats.equals(LoreStatsEnum.FISHING_SPEED) || loreStats.equals(LoreStatsEnum.LURES_ENDURANCE)) {
				
				textValue = textValue.replaceAll("%", "");
				textValue = textValue.replaceFirst(positiveValue, "").replaceFirst(negativeValue, "");
				
				if (MathUtil.isNumber(textValue)) {
					return MathUtil.parseDouble(textValue);
				}
			} else if (loreStats.equals(LoreStatsEnum.CRITICAL_DAMAGE) || loreStats.equals(LoreStatsEnum.FISHING_CHANCE)) {
				
				textValue = textValue.replaceAll(symbolMultiplier, "");
				textValue = textValue.replaceFirst(positiveValue, "").replaceFirst(negativeValue, "");
					
				if (MathUtil.isNumber(textValue)) {
					final double value = MathUtil.parseDouble(textValue);
					
					if (loreStats.equals(LoreStatsEnum.CRITICAL_DAMAGE)) {	
						return MathUtil.parseDouble(textValue) - 1;
					} else {
						return value;
					}
				}
			} else if (loreStats.equals(LoreStatsEnum.DURABILITY)) {
				if (textValue.contains(symbolDivide)) {
					final String[] valueList = textValue.split(symbolDivide) ;
					final String textValueMin = valueList[0].replaceFirst(positiveValue, "").replaceFirst(negativeValue, "");
					final String textValueMax = valueList[1].replaceFirst(positiveValue, "").replaceFirst(negativeValue, "");
					
					int valueMin;
					int valueMax;
						
					if (MathUtil.isNumber(textValueMin) && MathUtil.isNumber(textValueMax)) {
						valueMin = Integer.valueOf(textValueMin);
						valueMax = Integer.valueOf(textValueMax);
						
						if (option == null) {
							return valueMin;
						} else {
							return (option.equals(LoreStatsOption.MIN) || option.equals(LoreStatsOption.CURRENT)) ? valueMin : valueMax;
						}
					}
				}
			} else {
				
				textValue = textValue.replaceFirst(positiveValue, "").replaceFirst(negativeValue, "");
				
				if (MathUtil.isNumber(textValue)) {
					return MathUtil.parseDouble(textValue);
				}
			}
		}
		
		return 0;
	}
	
	public final void itemRepair(ItemStack item, int repair) {				
		final int line = getLineLoreStats(item, LoreStatsEnum.DURABILITY);
		
		if (line != -1) {
			final int nowDurability = (int) getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.CURRENT);
			final int maxDurability = (int) getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.MAX);
			
			int durability = nowDurability + repair;
			
			durability = MathUtil.limitInteger(durability, 0, maxDurability);
			
			if (maxDurability != 0) {
				final String newDurability = repair != -1 ? getTextLoreStats(LoreStatsEnum.DURABILITY, durability, maxDurability) : getTextLoreStats(LoreStatsEnum.DURABILITY, maxDurability);
				
				EquipmentUtil.setLore(item, line, newDurability);
			}
		}
	}
	
	public final String statsValue(LoreStatsEnum loreStats, double value1, double value2) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String positiveValue = mainConfig.getStatsLorePositiveValue();
		final String negativeValue = mainConfig.getStatsLoreNegativeValue();
		final String symbolDivide = mainConfig.getStatsLoreDividerSymbol();
		final String symbolMultiplier = mainConfig.getStatsLoreMultiplierSymbol();
		final String colorValue = mainConfig.getStatsColorValue();
		
		String textValue1;
		String textValue2;
		String statsValue;
		
		if (loreStats.equals(LoreStatsEnum.DURABILITY)) {
			
			value1 = MathUtil.limitDouble(value1, 1, value1);
			value2 = MathUtil.limitDouble(value2, 1, value2);
			
		} else if (loreStats.equals(LoreStatsEnum.LEVEL)) {
			final int maxLevel = mainConfig.getStatsMaxLevelValue();
			
			value1 = MathUtil.limitDouble(value1, 1, maxLevel);
			
		} else if (loreStats.equals(LoreStatsEnum.CRITICAL_CHANCE)) {
			
			value1 = MathUtil.limitDouble(value1, 0, 100);
		}
		
		if (loreStats.equals(LoreStatsEnum.DURABILITY)) {
			
			textValue1 = value1 < 0 ? (negativeValue + (int) value1) : (positiveValue + (int) value1);
			textValue2 = value2 < 0 ? (negativeValue + (int) value2) : (positiveValue + (int) value2);
			
		} else if (loreStats.equals(LoreStatsEnum.LEVEL)) {
			
			textValue1 = value1 < 0 ? (negativeValue + (int) value1) : (positiveValue + (int) value1);
			textValue2 = value2 < 0 ? "0" : String.valueOf(value2);
			
		} else {
			
			textValue1 = value1 < 0 ? (negativeValue + value1) : (positiveValue + value1);
			textValue2 = value2 < 0 ? (negativeValue + value2) : (positiveValue + value2);
		}
		
		if (loreStats.equals(LoreStatsEnum.DAMAGE)) {
			final String symbolRange = mainConfig.getStatsLoreRangeSymbol();
			
			if (value1 == value2) {
				statsValue = MainConfig.KEY_STATS_VALUE + textValue1 + MainConfig.KEY_STATS_VALUE + colorValue;
			} else {
				statsValue = value2 > value1 ? (MainConfig.KEY_STATS_VALUE + textValue1 + symbolRange + textValue2 + MainConfig.KEY_STATS_VALUE + colorValue) : (MainConfig.KEY_STATS_VALUE + textValue2 + symbolRange + textValue1 + MainConfig.KEY_STATS_VALUE + colorValue);
			}
		} else if (loreStats.equals(LoreStatsEnum.CRITICAL_CHANCE) || loreStats.equals(LoreStatsEnum.PENETRATION) || loreStats.equals(LoreStatsEnum.ATTACK_AOE_DAMAGE)) {
			statsValue = MainConfig.KEY_STATS_VALUE + textValue1 + "%" + MainConfig.KEY_STATS_VALUE + colorValue;
		} else if (loreStats.equals(LoreStatsEnum.CRITICAL_DAMAGE) || loreStats.equals(LoreStatsEnum.FISHING_CHANCE)) {
			statsValue = MainConfig.KEY_STATS_VALUE + textValue1 + symbolMultiplier + MainConfig.KEY_STATS_VALUE + colorValue;
		} else if (loreStats.equals(LoreStatsEnum.BLOCK_AMOUNT) || loreStats.equals(LoreStatsEnum.BLOCK_RATE) || loreStats.equals(LoreStatsEnum.HIT_RATE) || loreStats.equals(LoreStatsEnum.DODGE_RATE) || loreStats.equals(LoreStatsEnum.PVP_DAMAGE) || loreStats.equals(LoreStatsEnum.PVE_DAMAGE) || loreStats.equals(LoreStatsEnum.PVP_DEFENSE) || loreStats.equals(LoreStatsEnum.PVE_DEFENSE) || loreStats.equals(LoreStatsEnum.FISHING_SPEED) || loreStats.equals(LoreStatsEnum.LURES_ENDURANCE)) {
			statsValue = value1 > 0 ? (positiveValue + "+" + MainConfig.KEY_STATS_VALUE + textValue1 + "%" + MainConfig.KEY_STATS_VALUE + colorValue) : (MainConfig.KEY_STATS_VALUE + textValue1 + "%" + MainConfig.KEY_STATS_VALUE + colorValue);
		} else if (loreStats.equals(LoreStatsEnum.FISHING_POWER) || loreStats.equals(LoreStatsEnum.LURES_MAX_TENSION)) {
			statsValue = value1 > 0 ? (positiveValue + "+" + MainConfig.KEY_STATS_VALUE + textValue1 + MainConfig.KEY_STATS_VALUE + colorValue) : (MainConfig.KEY_STATS_VALUE + textValue1 + MainConfig.KEY_STATS_VALUE + colorValue);
		} else if (loreStats.equals(LoreStatsEnum.DURABILITY)) {
			statsValue = value2 > value1 ? (MainConfig.KEY_STATS_VALUE + textValue1 + symbolDivide + textValue2 + MainConfig.KEY_STATS_VALUE + colorValue) : (MainConfig.KEY_STATS_VALUE + textValue2 + symbolDivide + textValue1 + MainConfig.KEY_STATS_VALUE + colorValue);
		} else if (loreStats.equals(LoreStatsEnum.LEVEL)) {
			final String colorExpCurrent = mainConfig.getStatsColorExpCurrent();
			final String colorExpUp = mainConfig.getStatsColorExpUp();
			final HashMap<String, String> map = new HashMap<String, String>();
			
			String expValue = mainConfig.getStatsFormatExp();
			
			map.put("exp", MainConfig.KEY_EXP_CURRENT + colorExpCurrent + textValue2 + MainConfig.KEY_EXP_CURRENT + colorExpCurrent);
			map.put("up", MainConfig.KEY_EXP_UP + colorExpUp + getUpExp((int) value1) + MainConfig.KEY_EXP_UP + colorExpUp);
			expValue = TextUtil.placeholder(map, expValue, "<", ">");
			
			statsValue = MainConfig.KEY_STATS_VALUE + textValue1 + MainConfig.KEY_STATS_VALUE + colorValue + " " + expValue;
		} else {
			statsValue = MainConfig.KEY_STATS_VALUE + textValue1 + MainConfig.KEY_STATS_VALUE + colorValue;
		}
		
		return statsValue;
	}
	
	public final LoreStatsEnum getLoreStats(String lore) {
		for (LoreStatsEnum loreStats : LoreStatsEnum.values()) {
			if (lore.contains(getKeyStats(loreStats, true))) {
				return loreStats;
			}
		}
		
		return null;
	}
	
	public final LoreStatsEnum getLoreStats(ItemStack item, int line) {
		if (line > 0) {
			if (EquipmentUtil.hasLore(item)) {
				if (line <= EquipmentUtil.getLores(item).size()) {
					final String lore = EquipmentUtil.getLores(item).get(line-1);
					
					return getLoreStats(lore);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isLoreStats(String lore) {
		final LoreStatsEnum loreStats = getLoreStats(lore);
		
		return loreStats != null;
	}
	
	public final boolean isLoreStats(ItemStack item, int line) {
		final LoreStatsEnum loreStats = getLoreStats(item, line);
		
		return loreStats != null;
	}
	
	public final int getLineLoreStats(ItemStack item, LoreStatsEnum loreStats) {		
		return EquipmentUtil.loreGetLineKey(item, getKeyStats(loreStats, true));	
	}	

	public final double getUpExp(int level) {
		if (level < 1) {
			level = 1;
		}
			
		final double upExp = ((level^2)*25) + (level*50) + 100;
		
		return upExp;
	}
	
	public final boolean checkDurability(ItemStack item) {
		return checkDurability(item, (int) getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.CURRENT));
	}
	
	public final boolean checkDurability(ItemStack item, int durability) {
		final boolean hasLoreDurability = hasLoreStats(item, LoreStatsEnum.DURABILITY);
		
		return hasLoreDurability && durability < 1 ? false : true;
	}
	
	public final boolean checkLevel(ItemStack item, int requirement) {
		final int level = (int) getLoreValue(item, LoreStatsEnum.LEVEL, LoreStatsOption.CURRENT);
		
		return checkLevel(item, level, requirement);
	}
	
	public final boolean checkLevel(ItemStack item, int level, int requirement) {
		final boolean hasLoreLevel = hasLoreStats(item, LoreStatsEnum.LEVEL);
		
		return hasLoreLevel && level >= requirement ? true : false;
	}
	
	public boolean durability(LivingEntity livingEntity, Slot slot, int currentValue) {
		if (EntityUtil.isPlayer(livingEntity)) {
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(livingEntity, slot);
			
			return (item != null && !item.getType().equals(Material.BOW)) ? durability(livingEntity, item, currentValue, false) : true;
		} 
		
		return true;
	}
	
	public final boolean durability(LivingEntity livingEntity, ItemStack item, int currentValue, boolean damage) {
		final int line = getLineLoreStats(item, LoreStatsEnum.DURABILITY);
		final int check = damage ? 1 : 0;
		
		if (line != -1) {			
			if (!item.getType().equals(Material.BOW)) {				
				if (currentValue < check) {
					return false;
				} else {
					final int nextValue = currentValue - 1;
					
					if (damage) {
						damageDurability(item);
					}
					
					if (nextValue < check) {
						return false;
					}
				}
			}
		}
		
		return true;
	}
	
	public final void damageDurability(ItemStack item) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final int line = getLineLoreStats(item, LoreStatsEnum.DURABILITY);
		
		if (line != -1) {	
			final int currentValue = (int) getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.CURRENT, line);
			
			if (currentValue > 0) {
				final String positiveValue = mainConfig.getStatsLorePositiveValue();
				final String symbolDivide = mainConfig.getStatsLoreDividerSymbol();
				
				String loreDurability = positiveValue + currentValue + symbolDivide;
				String loreReplace = positiveValue + (currentValue-1) + symbolDivide;
				
				loreDurability = EquipmentUtil.getLores(item).get(line-1).replaceAll(loreDurability, loreReplace);
				
				EquipmentUtil.setLore(item, line, loreDurability);
			}
		}
		
	}
	
	public final LoreStatsWeapon getLoreStatsWeapon(LivingEntity attacker) {
		return getLoreStatsWeapon(attacker, false);
	}
	
	public final LoreStatsWeapon getLoreStatsWeapon(LivingEntity attacker, boolean reverse) {
		return getLoreStatsWeapon(attacker, true, reverse);
	}
	
	public final LoreStatsWeapon getLoreStatsWeapon(LivingEntity attacker, boolean checkDurability, boolean reverse) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final boolean isItemUniversal = mainConfig.isStatsEnableItemUniversal();
		
		double damage = 0;
		double penetration = 0;
		double pvpDamage = 0;
		double pveDamage = 0;
		double attackAoERadius = 0;
		double attackAoEDamage = 0;
		double criticalChance = 0;
		double criticalDamage = 0;
		double hitRate = 0;
		
		for (Slot slot : Slot.values()) {
			if (slot.getType().equals(SlotType.WEAPON) || isItemUniversal) {
				final ItemStack item = Bridge.getBridgeEquipment().getEquipment(attacker, slot);
				
				if (item != null) {
					final boolean itemReverse = reverse ? slot.getType().equals(SlotType.WEAPON) : false;
					final LoreStatsWeapon statsBuild = getLoreStatsWeapon(item, attacker, slot, checkDurability, itemReverse);
					
					damage = damage + statsBuild.getDamage();
					penetration = penetration + statsBuild.getPenetration();
					pvpDamage = pvpDamage + statsBuild.getPvPDamage();
					pveDamage = pveDamage + statsBuild.getPvEDamage();
					attackAoERadius = attackAoERadius + statsBuild.getAttackAoERadius();
					attackAoEDamage = attackAoEDamage + statsBuild.getAttackAoEDamage();
					criticalChance = criticalChance + statsBuild.getCriticalChance();
					criticalDamage = criticalDamage + statsBuild.getCriticalDamage();
					hitRate = hitRate + statsBuild.getHitRate();
				}
			}
		}
		
		return new LoreStatsWeapon(damage, penetration, pvpDamage, pveDamage, attackAoERadius, attackAoEDamage, criticalChance, criticalDamage, hitRate);
	}
	
	public final LoreStatsWeapon getLoreStatsWeapon(ItemStack item) {
		return getLoreStatsWeapon(item, null, Slot.MAINHAND, true, false);
	}
	
	public final LoreStatsWeapon getLoreStatsWeapon(ItemStack item, boolean reverse) {
		return getLoreStatsWeapon(item, null, Slot.MAINHAND, true, reverse);
	}
	
	public final LoreStatsWeapon getLoreStatsWeapon(ItemStack item, boolean checkDurability, boolean reverse) {
		return getLoreStatsWeapon(item, null, Slot.MAINHAND, checkDurability, reverse);
	}
	
	public final LoreStatsWeapon getLoreStatsWeapon(ItemStack item, Slot slot, boolean checkDurability, boolean reverse) {
		return getLoreStatsWeapon(item, null, slot, checkDurability, reverse);
	}
	
	public final LoreStatsWeapon getLoreStatsWeapon(ItemStack item, LivingEntity holder, Slot slot, boolean checkDurability, boolean reverse) {
		final GameManager gameManager = plugin.getGameManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final Slot secondarySlot = reverse ? Slot.MAINHAND : Slot.OFFHAND;
		
		if (EquipmentUtil.loreCheck(item)) {			
			if (!(checkDurability && !checkDurability(item))) {
				if (holder != null && holder instanceof Player && requirementManager.isAllowed((Player) holder, item)) {
					final double scaleValue = slot.equals(secondarySlot) ? mainConfig.getStatsScaleOffHandValue() : 1;
					final double damage = getLoreValue(item, LoreStatsEnum.DAMAGE, null) * scaleValue;
					final double penetration = getLoreValue(item, LoreStatsEnum.PENETRATION, null) * scaleValue;
					final double pvpDamage = getLoreValue(item, LoreStatsEnum.PVP_DAMAGE, null) * scaleValue;
					final double pveDamage = getLoreValue(item, LoreStatsEnum.PVE_DAMAGE, null) * scaleValue;
					final double attackAoERadius = getLoreValue(item, LoreStatsEnum.ATTACK_AOE_RADIUS, null) * scaleValue;
					final double attackAoEDamage = getLoreValue(item, LoreStatsEnum.ATTACK_AOE_DAMAGE, null) * scaleValue;
					final double criticalChance = getLoreValue(item, LoreStatsEnum.CRITICAL_CHANCE, null) * scaleValue;
					final double criticalDamage = getLoreValue(item, LoreStatsEnum.CRITICAL_DAMAGE, null) * scaleValue;
					final double hitRate = getLoreValue(item, LoreStatsEnum.HIT_RATE, null) * scaleValue;
					
					return new LoreStatsWeapon(damage, penetration, pvpDamage, pveDamage, attackAoERadius, attackAoEDamage, criticalChance, criticalDamage, hitRate);
				}
			}
		}
		
		return new LoreStatsWeapon(0, 0, 0, 0, 0, 0, 0, 0, 0);
	}
	
	public final LoreStatsArmor getLoreStatsArmor(LivingEntity victims) {
		return getLoreStatsArmor(victims, true);
	}
	
	public final LoreStatsArmor getLoreStatsArmor(LivingEntity victims, boolean checkDurability) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final boolean isItemUniversal = mainConfig.isStatsEnableItemUniversal();
		
		double defense = 0;
		double pvpDefense = 0;
		double pveDefense = 0;
		double health = 0;
		double healthRegen = 0;
		double staminaMax = 0;
		double staminaRegen = 0;
		double blockAmount = 0;
		double blockRate = 0;
		double dodgeRate = 0;
		
		for (Slot slot : Slot.values()) {
			final SlotType slotType = slot.getType();
			
			if (slotType.equals(SlotType.ARMOR) || isItemUniversal) {
				final ItemStack item = Bridge.getBridgeEquipment().getEquipment(victims, slot);
				
				if (EquipmentUtil.isSolid(item)) {
					final LoreStatsArmor statsBuild = getLoreStatsArmor(item, victims, slot, checkDurability);
					
					defense = defense + statsBuild.getDefense();
					pvpDefense = pvpDefense + statsBuild.getPvPDefense();
					pveDefense = pveDefense + statsBuild.getPvEDefense();
					health = health + statsBuild.getHealth();
					healthRegen = healthRegen + statsBuild.getHealthRegen();
					staminaMax = staminaMax + statsBuild.getStaminaMax();
					staminaRegen = staminaRegen + statsBuild.getStaminaRegen();
					blockAmount = blockAmount + statsBuild.getBlockAmount();
					blockRate = blockRate + statsBuild.getBlockRate();
					dodgeRate = dodgeRate + statsBuild.getDodgeRate();
				}
			} else {
				if (ServerUtil.isCompatible(VersionNMS.V1_9_R1)) {
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(victims, slot);
					
					if (EquipmentUtil.isSolid(item)) {
						final Material material = item.getType();
						
						if (material.equals(Material.SHIELD)) {
							final LoreStatsArmor statsBuild = getLoreStatsArmor(item, victims, slot, checkDurability);
							final double scale = mainConfig.getStatsScaleArmorShield();
							
							defense = defense + (statsBuild.getDefense() * scale);
							pvpDefense = pvpDefense + (statsBuild.getPvPDefense() * scale);
							pveDefense = pveDefense + (statsBuild.getPvEDefense() * scale);
							health = health + (statsBuild.getHealth() * scale);
							healthRegen = healthRegen + (statsBuild.getHealthRegen() * scale);
							staminaMax = staminaMax + (statsBuild.getStaminaMax() * scale);
							staminaRegen = staminaRegen + (statsBuild.getStaminaRegen() * scale);
							blockAmount = blockAmount + (statsBuild.getBlockAmount() * scale);
							blockRate = blockRate + (statsBuild.getBlockRate() * scale);
							dodgeRate = dodgeRate + (statsBuild.getDodgeRate() * scale);
						}
					}
				}
			}
		}
		
		return new LoreStatsArmor(defense, pvpDefense, pveDefense, health, healthRegen, staminaMax, staminaRegen, blockAmount, blockRate, dodgeRate);
	}
	
	public final LoreStatsArmor getLoreStatsArmor(ItemStack item) {
		return getLoreStatsArmor(item, null, Slot.MAINHAND, true);
	}
	
	public final LoreStatsArmor getLoreStatsArmor(ItemStack item, boolean checkDurability) {
		return getLoreStatsArmor(item, null, Slot.MAINHAND, checkDurability);
	}
	
	public final LoreStatsArmor getLoreStatsArmor(ItemStack item, Slot slot, boolean checkDurability) {
		return getLoreStatsArmor(item, null, slot, checkDurability);
	}
	
	public final LoreStatsArmor getLoreStatsArmor(ItemStack item, LivingEntity holder, Slot slot, boolean checkDurability) {
		final GameManager gameManager = plugin.getGameManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (EquipmentUtil.loreCheck(item)) {
			if (!(checkDurability && !checkDurability(item))) {
				if (!(holder != null && holder instanceof Player && !requirementManager.isAllowed((Player) holder, item))) {
					final double scale = slot.equals(Slot.OFFHAND) ? mainConfig.getStatsScaleOffHandValue() : 1;
					final double defense = getLoreValue(item, LoreStatsEnum.DEFENSE, null) * scale;
					final double pvpDefense = getLoreValue(item, LoreStatsEnum.PVP_DEFENSE, null) * scale;
					final double pveDefense = getLoreValue(item, LoreStatsEnum.PVE_DEFENSE, null) * scale;
					final double health = getLoreValue(item, LoreStatsEnum.HEALTH, null) * scale;
					final double healthRegen = getLoreValue(item, LoreStatsEnum.HEALTH_REGEN, null) * scale;
					final double staminaMax = getLoreValue(item, LoreStatsEnum.STAMINA_MAX, null) * scale;
					final double staminaRegen = getLoreValue(item, LoreStatsEnum.STAMINA_REGEN, null) * scale;
					final double blockAmount = getLoreValue(item, LoreStatsEnum.BLOCK_AMOUNT, null) * scale;
					final double blockRate = getLoreValue(item, LoreStatsEnum.BLOCK_RATE, null) * scale;
					final double dodgeRate = getLoreValue(item, LoreStatsEnum.DODGE_RATE, null) * scale;
					
					return new LoreStatsArmor(defense, pvpDefense, pveDefense, health, healthRegen, staminaMax, staminaRegen, blockAmount, blockRate, dodgeRate);
				}
			}
		}
		
		return new LoreStatsArmor(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	}
	
	public final void sendBrokenCode(LivingEntity livingEntity, Slot slot) {
		sendBrokenCode(livingEntity, slot, true);
	}
	
	public final void sendBrokenCode(LivingEntity livingEntity, Slot slot, boolean broken) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (EntityUtil.isPlayer(livingEntity)) {
			final Player player = EntityUtil.parsePlayer(livingEntity);
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, slot);
			final String message;
			
			switch (slot) {
			case MAINHAND : message = lang.getText(player, "Item_Broken_MainHand"); break;
			case OFFHAND : message = lang.getText(player, "Item_Broken_Offhand"); break;
			case HELMET : message = lang.getText(player, "Item_Broken_Helmet"); break;
			case CHESTPLATE : message = lang.getText(player, "Item_Broken_Chestplate"); break;
			case LEGGINGS : message = lang.getText(player, "Item_Broken_Leggings"); break;
			case BOOTS : message = lang.getText(player, "Item_Broken_Boots"); break;
			default : return;
			}

			SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
			SenderUtil.sendMessage(player, message);
			
			if (broken && mainConfig.isStatsEnableItemBroken()) {
				final boolean enableGradeCalculation = mainConfig.isPassiveEnableGradeCalculation();
				final Collection<PassiveEffectEnum> buffs = passiveEffectManager.getPassiveEffects(item);
				
				Bridge.getBridgeEquipment().setEquipment(livingEntity, null, slot);
				PlayerUtil.setMaxHealth(player);
				passiveEffectManager.reloadPassiveEffect(player, buffs, enableGradeCalculation);
			}
		}
	}
	
	public final String getKeyStats(LoreStatsEnum stats) {
		return getKeyStats(stats, false);
	}
	
	public final String getKeyStats(LoreStatsEnum stats, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = MainConfig.KEY_STATS;
		final String color = mainConfig.getStatsColor();
		final String text = stats.getText();
		
		return justCheck ? key + color + text : key + color + text + key + color;
	}
}
