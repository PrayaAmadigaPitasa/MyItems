package net.ardeus.myitems.manager.game;

import java.util.Collection;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeaponProperties;
import net.ardeus.myitems.handler.HandlerManager;

public abstract class AbilityWeaponPropertiesManager extends HandlerManager {
	
	protected AbilityWeaponPropertiesManager(MyItems plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getAbilityWeaponPropertiesIds();
	public abstract Collection<AbilityWeaponProperties> getAllAbilityWeaponProperties();
	public abstract AbilityWeaponProperties getAbilityWeaponProperties(String ability);
	
	public final boolean isExists(String ability) {
		return getAbilityWeaponProperties(ability) != null;
	}
}