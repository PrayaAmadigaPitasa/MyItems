package net.ardeus.myitems.manager.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityItemWeapon;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.item.ItemSet;
import net.ardeus.myitems.item.ItemSetBonus;
import net.ardeus.myitems.item.ItemSetBonusEffect;
import net.ardeus.myitems.item.ItemSetBonusEffectAbilityWeapon;
import net.ardeus.myitems.item.ItemSetBonusEffectEntity;
import net.ardeus.myitems.item.ItemSetBonusEffectStats;
import net.ardeus.myitems.item.ItemSetComponent;
import net.ardeus.myitems.item.ItemSetBonusEffectStats.ItemSetBonusEffectStatsBuilder;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.TextUtil;

public abstract class ItemSetManager extends HandlerManager {
	
	protected ItemSetManager(MyItems plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getItemSetIds();
	public abstract Collection<ItemSet> getAllItemSet();
	public abstract ItemSet getItemSet(String id);
	public abstract ItemSet getItemSetByComponentId(String componentId);
	public abstract ItemSet getItemSetByKeyLore(String keyLore);
	public abstract Collection<String> getItemComponentIds();
	public abstract ItemSetComponent getItemComponentByKeyLore(String keyLore);
	
	public final boolean isExists(String id) {
		return getItemSet(id) != null;
	}
	
	public final ItemSetComponent getItemComponentByLore(String lore) {
		if (lore != null) {
			final String keySetComponentSelf = MainConfig.KEY_SET_COMPONENT_SELF;
			
			if (lore.contains(keySetComponentSelf)) {
				final String[] partsComponent = lore.split(keySetComponentSelf);
				
				if (partsComponent.length > 1) {
					final String componentKeyLore = ChatColor.stripColor(partsComponent[1]);
					final ItemSetComponent itemSetComponent = getItemComponentByKeyLore(componentKeyLore);
					
					return itemSetComponent;
				}
			}
		}
		
		return null;
	}
	
	public final ItemSetComponent getItemComponent(ItemStack item) {
		if (item != null) {
			final List<String> lores = EquipmentUtil.getLores(item);
			
			for (String lore : lores) {
				final ItemSetComponent itemSetComponent = getItemComponentByLore(lore);
				
				if (itemSetComponent != null) {
					return itemSetComponent;
				}
			}
		}
		
		return null;
	}
	
	public final ItemSet getItemSetByLore(String lore) {
		if (lore != null) {
			final String keySetComponentSelf = MainConfig.KEY_SET_COMPONENT_SELF;
			
			if (lore.contains(keySetComponentSelf)) {
				final String[] partsComponent = lore.split(keySetComponentSelf);
				
				if (partsComponent.length > 1) {
					final String componentKeyLore = ChatColor.stripColor(partsComponent[1]);
					final ItemSet itemSet = getItemSetByKeyLore(componentKeyLore);
					
					return itemSet;
				}
			}
		}
		
		return null;
	}
	
	public final ItemSet getItemSet(ItemStack item) {
		if (item != null) {
			final List<String> lores = EquipmentUtil.getLores(item);
			
			for (String lore : lores) {
				final ItemSet itemSet = getItemSetByLore(lore);
				
				if (itemSet != null) {
					return itemSet;
				}
			}
		}
		
		return null;
	}
	
	public final boolean isItemSet(ItemStack item) {
		return getItemSet(item) != null;
	}
	
	public final HashMap<Slot, ItemSetComponent> getMapItemComponent(LivingEntity entity) {
		return getMapItemComponent(entity, true);
	}
	
	public final HashMap<Slot, ItemSetComponent> getMapItemComponent(LivingEntity entity, boolean checkSlot) {
		final HashMap<Slot, ItemSetComponent> mapItemSetComponent = new HashMap<Slot, ItemSetComponent>();
		
		if (entity != null) {
			for (Slot slot : Slot.values()) {
				final ItemStack item = Bridge.getBridgeEquipment().getEquipment(entity, slot);
				
				if (item != null) {
					final ItemSetComponent itemSetComponent = getItemComponent(item);
					
					if (itemSetComponent != null && itemSetComponent.isMatchSlot(slot)) {
						mapItemSetComponent.put(slot, itemSetComponent);
					}
				}
			}
		}
		
		return mapItemSetComponent;
	}
	
	public final HashMap<Slot, ItemSet> getMapItemSet(LivingEntity entity) {
		return getMapItemSet(entity, true);
	}
	
	public final HashMap<Slot, ItemSet> getMapItemSet(LivingEntity entity, boolean checkSlot) {
		final HashMap<Slot, ItemSet> mapItemSet = new HashMap<Slot, ItemSet>();
		
		if (entity != null) {
			final HashMap<Slot, ItemSetComponent> mapItemSetComponent = getMapItemComponent(entity, checkSlot);
			
			for (Slot slot : mapItemSetComponent.keySet()) {
				final ItemSetComponent itemSetComponent = mapItemSetComponent.get(slot);
				final ItemSet itemSet = itemSetComponent.getItemSet();
				
				if (itemSet != null) {
					mapItemSet.put(slot, itemSet);
				}
			}
		}
		
		return mapItemSet;
	}
	
	public final HashMap<ItemSet, Integer> getMapItemSetTotal(LivingEntity entity) {
		return getMapItemSetTotal(entity, true);
	}
	
	public final HashMap<ItemSet, Integer> getMapItemSetTotal(LivingEntity entity, boolean checkSlot) {
		final HashMap<ItemSet, Integer> mapItemSetTotal = new HashMap<ItemSet, Integer>();
		
		if (entity != null) {
			final HashMap<Slot, ItemSet> mapItemSet = getMapItemSet(entity, checkSlot);
			
			for (Slot slot : mapItemSet.keySet()) {
				final ItemSet itemSet = mapItemSet.get(slot);
				
				if (mapItemSetTotal.containsKey(itemSet)) {
					final int total = mapItemSetTotal.get(itemSet) + 1;
					
					mapItemSetTotal.put(itemSet, total);
				} else {
					mapItemSetTotal.put(itemSet, 1);
				}
			}
		}
		
		return mapItemSetTotal;
	}
	
	public final ItemSetBonusEffectEntity getItemSetBonusEffectEntity(LivingEntity entity) {
		return getItemSetBonusEffectEntity(entity, true);
	}
	
	public final ItemSetBonusEffectEntity getItemSetBonusEffectEntity(LivingEntity entity, boolean checkSlot) {
		return getItemSetBonusEffectEntity(entity, checkSlot, true);
	}
	
	public final ItemSetBonusEffectEntity getItemSetBonusEffectEntity(LivingEntity entity, boolean checkSlot, boolean checkChance) {
		final GameManager gameManager = plugin.getGameManager();
		final AbilityWeaponManager abilityWeaponManager = gameManager.getAbilityWeaponManager();
		final HashMap<AbilityWeapon, Integer> mapAbilityWeapon = new HashMap<AbilityWeapon, Integer>();
		
		double additionalDamage = 0;
		double percentDamage = 0;
		double penetration = 0;
		double pvpDamage = 0;
		double pveDamage = 0;
		double additionalDefense = 0;
		double percentDefense = 0;
		double health = 0;
		double healthRegen = 0;
		double staminaMax = 0;
		double staminaRegen = 0;
		double attackAoERadius = 0;
		double attackAoEDamage = 0;
		double pvpDefense = 0;
		double pveDefense = 0;
		double criticalChance = 0;
		double criticalDamage = 0;
		double blockAmount = 0;
		double blockRate = 0;
		double hitRate = 0;
		double dodgeRate = 0;
		
		if (entity != null) {
			final HashMap<ItemSet, Integer> mapItemSetTotal = getMapItemSetTotal(entity, checkSlot);
			
			for (ItemSet itemSet : mapItemSetTotal.keySet()) {
				final int total = mapItemSetTotal.get(itemSet);
				
				for (ItemSetBonus itemSetBonus : itemSet.getAllItemSetBonus()) {
					final int amountID = itemSetBonus.getAmountId();
					
					if (total >= amountID) {
						final ItemSetBonusEffect itemSetBonusEffect = itemSetBonus.getEffect();
						final ItemSetBonusEffectStats itemSetBonusEffectStats = itemSetBonusEffect.getEffectStats();
						final ItemSetBonusEffectAbilityWeapon itemSetBonusEffectAbilityWeapon = itemSetBonusEffect.getEffectAbilityWeapon();
						final Collection<AbilityItemWeapon> listAbilityItemWeapon = itemSetBonusEffectAbilityWeapon.getAllAbilityItemWeapon();
						final HashMap<AbilityWeapon, Integer> mapAbilityWeaponBonus = abilityWeaponManager.getMapAbilityWeapon(listAbilityItemWeapon, checkChance);
						
						additionalDamage = additionalDamage + itemSetBonusEffectStats.getAdditionalDamage();
						percentDamage = percentDamage + itemSetBonusEffectStats.getPercentDamage();
						penetration = penetration + itemSetBonusEffectStats.getPenetration();
						pvpDamage = pvpDamage + itemSetBonusEffectStats.getPvPDamage();
						pveDamage = pveDamage + itemSetBonusEffectStats.getPvEDamage();
						additionalDefense = additionalDefense + itemSetBonusEffectStats.getAdditionalDefense();
						percentDefense = percentDefense + itemSetBonusEffectStats.getPercentDefense();
						health = health + itemSetBonusEffectStats.getHealth();
						healthRegen = healthRegen + itemSetBonusEffectStats.getHealthRegen();
						staminaMax = staminaMax + itemSetBonusEffectStats.getStaminaMax();
						staminaRegen = staminaRegen + itemSetBonusEffectStats.getStaminaRegen();
						attackAoERadius = attackAoERadius + itemSetBonusEffectStats.getAttackAoERadius();
						attackAoEDamage = attackAoEDamage + itemSetBonusEffectStats.getAttackAoEDamage();
						pvpDefense = pvpDefense + itemSetBonusEffectStats.getPvPDefense();
						pveDefense = pveDefense + itemSetBonusEffectStats.getPvEDefense();
						criticalChance = criticalChance + itemSetBonusEffectStats.getCriticalChance();
						criticalDamage = criticalDamage + itemSetBonusEffectStats.getCriticalDamage();
						blockAmount = blockAmount + itemSetBonusEffectStats.getBlockAmount();
						blockRate = blockRate + itemSetBonusEffectStats.getBlockRate();
						hitRate = hitRate + itemSetBonusEffectStats.getHitRate();
						dodgeRate = dodgeRate + itemSetBonusEffectStats.getDodgeRate();
						
						for (AbilityWeapon abilityWeapon : mapAbilityWeaponBonus.keySet()) {
							final int grade = mapAbilityWeaponBonus.get(abilityWeapon);
							
							if (mapAbilityWeapon.containsKey(abilityWeapon)) {
								final int totalGrade = mapAbilityWeapon.get(abilityWeapon) + grade;
								
								mapAbilityWeapon.put(abilityWeapon, totalGrade);
							} else {
								mapAbilityWeapon.put(abilityWeapon, grade);
							}
						}
					}
				}
			}
		}
		
		final ItemSetBonusEffectStatsBuilder itemSetBonusEffectStatsBuilder = new ItemSetBonusEffectStatsBuilder();
		
		itemSetBonusEffectStatsBuilder.setAdditionalDamage(additionalDamage);
		itemSetBonusEffectStatsBuilder.setPercentDamage(percentDamage);
		itemSetBonusEffectStatsBuilder.setPenetration(penetration);
		itemSetBonusEffectStatsBuilder.setPvPDamage(pvpDamage);
		itemSetBonusEffectStatsBuilder.setPvEDamage(pveDamage);
		itemSetBonusEffectStatsBuilder.setAdditionalDefense(additionalDefense);
		itemSetBonusEffectStatsBuilder.setPercentDefense(percentDefense);
		itemSetBonusEffectStatsBuilder.setHealth(health);
		itemSetBonusEffectStatsBuilder.setHealthRegen(healthRegen);
		itemSetBonusEffectStatsBuilder.setStaminaMax(staminaMax);
		itemSetBonusEffectStatsBuilder.setStaminaRegen(staminaRegen);
		itemSetBonusEffectStatsBuilder.setAttackAoERadius(attackAoERadius);
		itemSetBonusEffectStatsBuilder.setAttackAoEDamage(attackAoEDamage);
		itemSetBonusEffectStatsBuilder.setPvPDefense(pvpDefense);
		itemSetBonusEffectStatsBuilder.setPvEDefense(pveDefense);
		itemSetBonusEffectStatsBuilder.setCriticalChance(criticalChance);
		itemSetBonusEffectStatsBuilder.setCriticalDamage(criticalDamage);
		itemSetBonusEffectStatsBuilder.setBlockAmount(blockAmount);
		itemSetBonusEffectStatsBuilder.setBlockRate(blockRate);
		itemSetBonusEffectStatsBuilder.setHitRate(hitRate);
		itemSetBonusEffectStatsBuilder.setDodgeRate(dodgeRate);
		
		final ItemSetBonusEffectStats itemSetBonusEffectStats = itemSetBonusEffectStatsBuilder.build();
		final ItemSetBonusEffectEntity itemSetBonusEffectEntity = new ItemSetBonusEffectEntity(itemSetBonusEffectStats, mapAbilityWeapon);
		
		return itemSetBonusEffectEntity;
	}
	
	public final void updateItemSet(LivingEntity entity) {
		updateItemSet(entity, true);
	}
	
	public final void updateItemSet(LivingEntity entity, boolean checkPlayerInventory) {
		updateItemSet(entity, checkPlayerInventory, null);
	}
	
	public final void updateItemSet(LivingEntity entity, boolean checkPlayerInventory, Inventory inventory) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (entity != null) {
			final String divider = "\n";
			final String keyLine = MainConfig.KEY_SET_LINE;
			final String keySetComponentSelf = MainConfig.KEY_SET_COMPONENT_SELF;
			final String keySetComponentOther = MainConfig.KEY_SET_COMPONENT_OTHER;
			final String loreBonusActive = mainConfig.getSetLoreBonusActive();
			final String loreBonusInactive = mainConfig.getSetLoreBonusInactive();
			final String loreComponentActive = mainConfig.getSetLoreComponentActive();
			final String loreComponentInactive = mainConfig.getSetLoreComponentInactive();
			final HashMap<Slot, ItemSetComponent> mapItemSetComponent = getMapItemComponent(entity);
			final HashMap<ItemSet, Integer> mapItemSetTotal = new HashMap<ItemSet, Integer>();
			final Collection<ItemSetComponent> allItemSetComponent = mapItemSetComponent.values();
			final Set<ItemStack> contents = new HashSet<ItemStack>();
			
			for (Slot slot : Slot.values()) {
				final ItemStack item = Bridge.getBridgeEquipment().getEquipment(entity, slot);
				
				if (item != null) {
					
					contents.add(item);
					
					if (mapItemSetComponent.containsKey(slot)) {
						final ItemSetComponent itemSetComponent = mapItemSetComponent.get(slot);
						final ItemSet itemSet = itemSetComponent.getItemSet();
						
						if (itemSet != null) {
							if (mapItemSetTotal.containsKey(itemSet)) {
								final int total = mapItemSetTotal.get(itemSet) + 1;
								
								mapItemSetTotal.put(itemSet, total);
							} else {
								mapItemSetTotal.put(itemSet, 1);
							}
						}
					}
				}
			}
			
			if (entity instanceof Player) {
				if (checkPlayerInventory) {
					final Player player = (Player) entity;
					final PlayerInventory playerInventory = player.getInventory();
					final ItemStack itemCursor = player.getItemOnCursor();
					
					if (itemCursor != null) {
						contents.add(itemCursor);
					}
					
					for (ItemStack content : playerInventory.getContents()) {
						if (content != null) {
							contents.add(content);
						}
					}
				}
				
				if (inventory != null) {
					for (ItemStack content : inventory.getContents()) {
						if (content != null) {
							contents.add(content);
						}
					}
				}
			}
			
			for (ItemStack item : contents) {
				final ItemSetComponent itemSetComponent = getItemComponent(item);
				
				if (itemSetComponent != null) {
					final ItemSet itemSet = itemSetComponent.getItemSet();
					
					if (itemSet != null) {
						final String name = itemSet.getName();
						final int total = mapItemSetTotal.containsKey(itemSet) ? mapItemSetTotal.get(itemSet) : 0;
						final int maxComponent = itemSet.getTotalComponent();
						final List<String> lores = EquipmentUtil.getLores(item);
						final List<String> loresBonus = new ArrayList<String>();
						final List<String> loresComponent = new ArrayList<String>();
						final List<Integer> bonusAmountIds = new ArrayList<Integer>(itemSet.getBonusAmountIds());
						final Iterator<String> iteratorLores = lores.iterator();
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						List<String> loresSet = mainConfig.getSetFormat();
						
						Collections.sort(bonusAmountIds);
						
						while (iteratorLores.hasNext()) {
							final String lore = iteratorLores.next();
							
							if (lore.contains(keyLine)) {
								iteratorLores.remove();
							}
						}
						
						for (ItemSetComponent partComponent : itemSet.getAllItemSetComponent()) {
							final String partComponentId = partComponent.getId();
							final String keyLore = partComponent.getKeyLore();
							final String replacementKeyLore;
							
							String formatComponent = mainConfig.getSetFormatComponent();
							
							if (allItemSetComponent.contains(partComponent)) {
								
								formatComponent = loreComponentActive + formatComponent;
								
								if (partComponent.equals(itemSetComponent)) {
									replacementKeyLore = keySetComponentSelf + loreComponentActive + keyLore + keySetComponentSelf + loreComponentActive;
								} else {
									replacementKeyLore = keySetComponentOther + loreComponentActive + keyLore + keySetComponentOther + loreComponentActive;
								}
							} else {
								
								formatComponent = loreComponentInactive + formatComponent;
								
								if (partComponent.equals(itemSetComponent)) {
									replacementKeyLore = keySetComponentSelf + loreComponentInactive + keyLore + keySetComponentSelf + loreComponentInactive;
								} else {
									replacementKeyLore = keySetComponentOther + loreComponentInactive + keyLore + keySetComponentOther + loreComponentInactive;
								}
							}
							
							mapPlaceholder.clear();
							mapPlaceholder.put("item_set_component_id", partComponentId);
							mapPlaceholder.put("item_set_component_keylore", replacementKeyLore);
							
							formatComponent = TextUtil.placeholder(mapPlaceholder, formatComponent, "<", ">");
							
							loresComponent.add(formatComponent);
						}
						
						for (int bonusAmountID : bonusAmountIds) {
							final ItemSetBonus partBonus = itemSet.getItemSetBonus(bonusAmountID);
							final List<String> listDescription = partBonus.getDescription();
							
							for (String description : listDescription) {
							
								String formatBonus = mainConfig.getSetFormatBonus();
								
								if (total >= bonusAmountID) {
									formatBonus = loreBonusActive + formatBonus;
								} else {
									formatBonus = loreBonusInactive + formatBonus;
								}
								
								mapPlaceholder.clear();
								mapPlaceholder.put("item_set_bonus_amount", String.valueOf(bonusAmountID));
								mapPlaceholder.put("item_set_bonus_description", String.valueOf(description));
								
								formatBonus = TextUtil.placeholder(mapPlaceholder, formatBonus, "<", ">");
								
								loresBonus.add(formatBonus);
							}
						}
						
						final String lineBonus = TextUtil.convertListToString(loresBonus, divider);
						final String lineComponent = TextUtil.convertListToString(loresComponent, divider);
						
						mapPlaceholder.clear();
						mapPlaceholder.put("item_set_name", name);
						mapPlaceholder.put("item_set_total", String.valueOf(total));
						mapPlaceholder.put("item_set_max", String.valueOf(maxComponent));
						mapPlaceholder.put("list_item_set_component", lineComponent);
						mapPlaceholder.put("list_item_set_bonus", lineBonus);
						
						loresSet = TextUtil.placeholder(mapPlaceholder, loresSet, "<", ">");
						loresSet = TextUtil.expandList(loresSet, divider);
						
						final ListIterator<String> iteratorLoresSet = loresSet.listIterator();
						
						while (iteratorLoresSet.hasNext()) {
							final String lore = keyLine + iteratorLoresSet.next();
							
							iteratorLoresSet.set(lore);
						}
						
						lores.addAll(loresSet);
						
						EquipmentUtil.setLores(item, lores);
					}
				}
			}
			
			if (entity instanceof Player) {
				final Player player = (Player) entity;
				final GameMode gameMode = player.getGameMode();
				final InventoryView inventoryView = player.getOpenInventory();
				final InventoryType inventoryType = inventoryView.getType();
				
				if (!gameMode.equals(GameMode.CREATIVE) || !inventoryType.equals(InventoryType.CREATIVE)) {
					player.updateInventory();
				}
			}
		}
	}
}