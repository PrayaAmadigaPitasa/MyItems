package net.ardeus.myitems.manager.game;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import api.praya.agarthalib.builder.support.main.SupportClass;
import api.praya.agarthalib.builder.support.main.SupportClass.SupportClassType;
import api.praya.agarthalib.builder.support.main.SupportLevel;
import api.praya.agarthalib.builder.support.main.SupportLevel.SupportLevelType;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.VersionNMS;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.ServerUtil;
import com.praya.agarthalib.utility.TextUtil;

public class RequirementManager extends HandlerManager {

	protected RequirementManager(MyItems plugin) {
		super(plugin);
	}
	
	public final String getTextSoulUnbound() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.getRequirementFormatSoulUnbound();
	}
	
	public final String getTextSoulBound(OfflinePlayer player) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		String format = mainConfig.getRequirementFormatSoulBound();
		
		map.put("player", getKeyReqSoulBound(player, false));
		format = TextUtil.placeholder(map, format, "<", ">");
		
		return format;
	}
	
	public final String getTextLevel(int level) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		String format = mainConfig.getRequirementFormatLevel();
		
		map.put("level", getKeyReqLevel(level, false));
		format = TextUtil.placeholder(map, format, "<", ">");
		
		return format;
	}
	
	public final String getTextPermission(String permission) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		String format = mainConfig.getRequirementFormatPermission();
		
		map.put("permission", getKeyReqPermission(permission, false));
		format = TextUtil.placeholder(map, format, "<", ">");
		
		return format;
	}
	
	public final String getTextClass(String playerClass) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		String format = mainConfig.getRequirementFormatClass();
		
		map.put("class", getKeyReqClass(playerClass, false));
		format = TextUtil.placeholder(map, format, "<", ">");
		
		return format;
	}
	
	public final Integer getLineRequirementSoulUnbound(ItemStack item) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = mainConfig.getRequirementFormatSoulUnbound();
		
		if (EquipmentUtil.hasLore(item)) {
			final int line = EquipmentUtil.loreGetLineKey(item, key);
			
			return line >= 0 ? line : null;
		} else {
			return null;
		}
	}
	
	public final Integer getLineRequirementSoulBound(ItemStack item) {
		final String key = getKeyReqSoulBound();
		
		if (EquipmentUtil.hasLore(item)) {
			final int line = EquipmentUtil.loreGetLineKey(item, key);
			
			return line >= 0 ? line : null;
		} else {
			return null;
		}
	}
	
	public final Integer getLineRequirementLevel(ItemStack item) {
		final String key = getKeyReqLevel();
		
		if (EquipmentUtil.hasLore(item)) {
			final int line = EquipmentUtil.loreGetLineKey(item, key);
			
			return line >= 0 ? line : null;
		} else {
			return null;
		}
	}
	
	public final Integer getLineRequirementPermission(ItemStack item) {
		final String key = getKeyReqPermission();
		
		if (EquipmentUtil.hasLore(item)) {
			final int line = EquipmentUtil.loreGetLineKey(item, key);
			
			return line >= 0 ? line : null;
		} else {
			return null;
		}
	}
	
	public final Integer getLineRequirementClass(ItemStack item) {
		final String key = getKeyReqClass();
		
		if (EquipmentUtil.hasLore(item)) {
			final int line = EquipmentUtil.loreGetLineKey(item, key);
			
			return line >= 0 ? line : null;
		} else {
			return null;
		}
	}
	
	public final boolean hasRequirementSoulUnbound(ItemStack item) {
		return getLineRequirementSoulUnbound(item) != null;
	}
	
	public final boolean hasRequirementSoulBound(ItemStack item) {
		return getLineRequirementSoulBound(item) != null;
	}
	
	public final boolean hasrequirementLevel(ItemStack item) {
		return getLineRequirementLevel(item) != null;
	}
	
	public final boolean hasRequirementPermission(ItemStack item) {
		return getLineRequirementPermission(item) != null;
	}
	
	public final boolean hasRequirementClass(ItemStack item) {
		return getLineRequirementClass(item) != null;
	}
	
	public final void setMetadataSoulbound(OfflinePlayer player, ItemStack item) {
		if (ServerUtil.isCompatible(VersionNMS.V1_8_R1)) {
			final String metadata = getMetadataSoulBound();
			final String bound = player.getUniqueId().toString();
			
			Bridge.getBridgeTagsNBT().setString(metadata, item, bound);
		}
	}
	
	public final String getRequirementSoulBound(ItemStack item) {
		final OfflinePlayer bound = getRequirementSoulBoundPlayer(item);
		
		return bound != null ? bound.getName() : null;
	}
	
	public final String getRequirementSoulBound(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = MainConfig.KEY_REQ_BOUND;
		final String[] textListValue = lore.split(key);
		
		if (textListValue.length > 1) {
			final String color = mainConfig.getRequirementColorSoulBound();
			final String value = textListValue[1].replaceAll(color, "");
			
			return value;
		};
		
		return null;
	}
	
	public final OfflinePlayer getRequirementSoulBoundPlayer(ItemStack item) {
		final Integer line = getLineRequirementSoulBound(item);
		
		if (line != null) {
			final List<String> lores = EquipmentUtil.getLores(item);
			final String lore = lores.get(line-1);
			
			if (ServerUtil.isCompatible(VersionNMS.V1_8_R1)) {
				final String metadata = getMetadataSoulBound();
				final String bound = Bridge.getBridgeTagsNBT().getString(metadata, item);
				
				if (bound != null) {
					final UUID playerID = UUID.fromString(bound);
					final OfflinePlayer player = PlayerUtil.getPlayer(playerID);
					
					if (player != null) {
						return player;
					}
				}
			}
			
			final String bound = getRequirementSoulBound(lore);
			
			return bound != null ? PlayerUtil.getPlayer(bound) : null;
		}
		
		return null;
	}
	
	public final Integer getRequirementLevel(ItemStack item) {
		final Integer line = getLineRequirementLevel(item);
		
		if (line != null) {
			final List<String> lores = EquipmentUtil.getLores(item);
			final String lore = lores.get(line-1);
			
			return getRequirementLevel(lore);
		}
		
		return null;
	}
	
	public final Integer getRequirementLevel(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = MainConfig.KEY_REQ_LEVEL;
		final String[] textListValue = lore.split(key);
		
		if (textListValue.length > 1) {
			final String color = mainConfig.getRequirementColorLevel();
			final String textValue = textListValue[1].replaceAll(color, "");
			
			if (MathUtil.isNumber(textValue)) {
				final int value = MathUtil.parseInteger(textValue);
				
				return value;
			}
		}
		
		return null;
	}
	
	public final String getRequirementPermission(ItemStack item) {
		final Integer line = getLineRequirementPermission(item);
		
		if (line != null) {
			final List<String> lores = EquipmentUtil.getLores(item);
			final String lore = lores.get(line-1);
			
			return getRequirementPermission(lore);
		}
		
		return null;
	}
	
	public final String getRequirementPermission(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = MainConfig.KEY_REQ_PERMISSION;
		final String[] textListValue = lore.split(key);
		
		if (textListValue.length > 1) {
			final String color = mainConfig.getRequirementColorPermission();
			final String value = textListValue[1].replaceAll(color, "");
			
			return value;
		}
		
		return null;
	}
	
	public final String getRequirementClass(ItemStack item) {
		final Integer line = getLineRequirementClass(item);
		
		if (line != null) {
			final List<String> lores = EquipmentUtil.getLores(item);
			final String lore = lores.get(line-1);
			
			return getRequirementClass(lore);
		}
		
		return null;
	}
	
	public final String getRequirementClass(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = MainConfig.KEY_REQ_CLASS;
		final String[] textListValue = lore.split(key);
		
		if (textListValue.length > 1) {
			final String color = mainConfig.getRequirementColorClass();
			final String value = textListValue[1].replaceAll(color, "");
			
			return value;
		}
		
		return null;
	}
	
	public final boolean isAllowed(Player player, ItemStack item) {
		final boolean allowSoulBound = isAllowedReqSoulBound(player, item);
		final boolean allowLevel = isAllowedReqLevel(player, item);
		final boolean allowPermission = isAllowedReqPermission(player, item);
		final boolean allowClass = isAllowedReqClass(player, item);
		
		return allowSoulBound && allowLevel && allowPermission && allowClass;
	}
	
	public final boolean isAllowedReqSoulBound(Player player, ItemStack item) {
		final String bound = getRequirementSoulBound(item);
		
		return bound != null ? isAllowedReqSoulBound(player, bound) : true; 
	}
	
	public final boolean isAllowedReqSoulBound(Player player, String bound) {
		final String name = player.getName();
		final String id = player.getUniqueId().toString();
		final boolean matchName = bound.equalsIgnoreCase(name);
		final boolean matchUUID = bound.equalsIgnoreCase(id);
		
		return matchName || matchUUID;
	}
	
	public final boolean isAllowedReqLevel(Player player, ItemStack item) {
		final Integer reqLevel = getRequirementLevel(item);
		
		return reqLevel != null ? isAllowedReqLevel(player, reqLevel) : true;
	}
	
	public final boolean isAllowedReqLevel(Player player, int reqLevel) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final String textLevelType = mainConfig.getSupportTypeLevel();
		final SupportLevel supportLevel;
		final int playerLevel;
		
		switch (textLevelType.toUpperCase()) {
		case "AUTO" : supportLevel = supportManagerAPI.getSupportLevel(); break;
		case "BATTLELEVELS" : supportLevel = supportManagerAPI.getSupportLevel(SupportLevelType.SKILL_API); break;
		case "SKILLAPI" : supportLevel = supportManagerAPI.getSupportLevel(SupportLevelType.SKILL_API); break;
		case "SKILLSPRO" : supportLevel = supportManagerAPI.getSupportLevel(SupportLevelType.SKILLS_PRO); break;
		case "HEROES" : supportLevel = supportManagerAPI.getSupportLevel(SupportLevelType.HEROES); break;
		default : supportLevel = null;
		}
		
		playerLevel = supportLevel != null ? supportLevel.getPlayerLevel(player) : player.getLevel(); 
		
		return reqLevel <= playerLevel;
	}
	
	public final boolean isAllowedReqPermission(Player player, ItemStack item) {
		final String reqPermission = getRequirementPermission(item);
		
		return reqPermission != null ? player.hasPermission(reqPermission) : true;
	}
	
	public final boolean isAllowedReqClass(Player player, ItemStack item) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final String itemClass = getRequirementClass(item);
		
		if (itemClass == null) {
			return true;
		} else if (!supportManagerAPI.isSupportClass()) {
			return true;
		} else {
			final SupportClass supportClass = getSupportReqClass();
			final String playerClass = supportClass.getPlayerMainClassName(player);
			
			return playerClass != null ? playerClass.equalsIgnoreCase(itemClass) : false;
		}
	}
	
	public final SupportClass getSupportReqClass() {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final String textClassType = mainConfig.getSupportTypeClass();
		final SupportClass supportClass;
		
		switch (textClassType.toUpperCase()) {
		case "AUTO" : supportClass = supportManagerAPI.getSupportClass(); break;
		case "SKILLAPI" : supportClass = supportManagerAPI.getSupportClass(SupportClassType.SKILL_API); break;
		case "SKILLSPRO" : supportClass = supportManagerAPI.getSupportClass(SupportClassType.SKILLS_PRO); break;
		case "HEROES" : supportClass = supportManagerAPI.getSupportClass(SupportClassType.HEROES); break;
		default : supportClass = null;
		}
		
		return supportClass;
	}
	
	public final boolean isSupportReqClass() {
		return getSupportReqClass() != null;
	}
	
	private final String getMetadataSoulBound() {
		return "SoulBound";
	}
	
	private final String getKeyReqSoulBound() {
		return getKeyReqSoulBound(null, true);
	}
	
	private final String getKeyReqSoulBound(OfflinePlayer player, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String bound = player != null ? player.getName() : null;
		final String colorSoulBound = mainConfig.getRequirementColorSoulBound();
		
		return justCheck ? MainConfig.KEY_REQ_BOUND + colorSoulBound : MainConfig.KEY_REQ_BOUND + colorSoulBound + bound + MainConfig.KEY_REQ_BOUND + colorSoulBound;
	}
	
	private final String getKeyReqLevel() {
		return getKeyReqLevel(0, true);
	}
	
	private final String getKeyReqLevel(int level, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String colorLevel = mainConfig.getRequirementColorLevel();
		
		return justCheck ? MainConfig.KEY_REQ_LEVEL + colorLevel : MainConfig.KEY_REQ_LEVEL + colorLevel + level + MainConfig.KEY_REQ_LEVEL + colorLevel;
	}
	
	private final String getKeyReqPermission() {
		return getKeyReqPermission(null, true);
	}
	
	private final String getKeyReqPermission(String permission, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String colorPermission = mainConfig.getRequirementColorPermission();
		
		return justCheck ? MainConfig.KEY_REQ_PERMISSION + colorPermission : MainConfig.KEY_REQ_PERMISSION + colorPermission + permission + MainConfig.KEY_REQ_PERMISSION + colorPermission;
	}
	
	private final String getKeyReqClass() {
		return getKeyReqClass(null, true);
	}
	
	private final String getKeyReqClass(String reqClass, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String colorClass = mainConfig.getRequirementColorClass();
		
		return justCheck ? MainConfig.KEY_REQ_CLASS + colorClass : MainConfig.KEY_REQ_CLASS + colorClass + reqClass + MainConfig.KEY_REQ_CLASS + colorClass;
	}
}
