package net.ardeus.myitems.manager.game;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeaponMemory;
import net.ardeus.myitems.ability.AbilityWeaponPropertiesMemory;
import net.ardeus.myitems.element.ElementMemory;
import net.ardeus.myitems.item.ItemGeneratorMemory;
import net.ardeus.myitems.item.ItemMemory;
import net.ardeus.myitems.item.ItemSetMemory;
import net.ardeus.myitems.item.ItemTierMemory;
import net.ardeus.myitems.item.ItemTypeMemory;
import net.ardeus.myitems.power.PowerCommandMemory;
import net.ardeus.myitems.power.PowerMemory;
import net.ardeus.myitems.power.PowerShootMemory;
import net.ardeus.myitems.power.PowerSpecialMemory;
import net.ardeus.myitems.power.PowerSpecialPropertiesMemory;
import net.ardeus.myitems.socket.SocketGemMemory;

public class GameManager {

	private final AbilityWeaponManager abilityWeaponManager;
	private final AbilityWeaponPropertiesManager abilityWeaponPropertiesManager;
	private final ElementManager elementManager;
	private final ItemManager itemManager;
	private final ItemTypeManager itemTypeManager;
	private final ItemTierManager itemTierManager;
	private final ItemGeneratorManager itemGeneratorManager;
	private final ItemSetManager itemSetManager;
	private final PassiveEffectManager passiveEffectManager;
	private final PowerManager powerManager;
	private final PowerCommandManager powerCommandManager;
	private final PowerShootManager powerShootManager;
	private final PowerSpecialManager powerSpecialManager;
	private final PowerSpecialPropertiesManager powerSpecialPropertiesManager;
	private final RequirementManager requirementManager;
	private final SocketGemManager socketManager;
	private final LoreStatsManager statsManager;
	private final MenuManager menuManager;
	
	public GameManager(MyItems plugin) {
		this.abilityWeaponManager = AbilityWeaponMemory.getInstance();
		this.abilityWeaponPropertiesManager = AbilityWeaponPropertiesMemory.getInstance();
		this.elementManager = ElementMemory.getInstance();
		this.itemManager = ItemMemory.getInstance();
		this.itemTypeManager = ItemTypeMemory.getInstance();
		this.itemTierManager = ItemTierMemory.getInstance();
		this.itemGeneratorManager = ItemGeneratorMemory.getInstance();
		this.itemSetManager = ItemSetMemory.getInstance();
		this.passiveEffectManager = new PassiveEffectManager(plugin);
		this.powerManager = PowerMemory.getInstance();
		this.powerCommandManager = PowerCommandMemory.getInstance();
		this.powerShootManager = PowerShootMemory.getInstance();
		this.powerSpecialManager = PowerSpecialMemory.getInstance();
		this.powerSpecialPropertiesManager = PowerSpecialPropertiesMemory.getInstance();
		this.requirementManager = new RequirementManager(plugin);
		this.socketManager = SocketGemMemory.getInstance();
		this.statsManager = new LoreStatsManager(plugin);
		this.menuManager = new MenuManager(plugin);
	};
	
	public final AbilityWeaponManager getAbilityWeaponManager() {
		return this.abilityWeaponManager;
	}
	
	public final AbilityWeaponPropertiesManager getAbilityWeaponPropertiesManager() {
		return this.abilityWeaponPropertiesManager;
	}
	
	public final ElementManager getElementManager() {
		return this.elementManager;
	}
	
	public final ItemManager getItemManager() {
		return this.itemManager;
	}
	
	public final ItemTypeManager getItemTypeManager() {
		return this.itemTypeManager;
	}
	
	public final ItemTierManager getItemTierManager() {
		return this.itemTierManager;
	}
	
	public final ItemGeneratorManager getItemGeneratorManager() {
		return this.itemGeneratorManager;
	}
	
	public final ItemSetManager getItemSetManager() {
		return this.itemSetManager;
	}
	
	public final PassiveEffectManager getPassiveEffectManager() {
		return this.passiveEffectManager;
	}
	
	public final PowerManager getPowerManager() {
		return this.powerManager;
	}
	
	public final PowerCommandManager getPowerCommandManager() {
		return this.powerCommandManager;
	}
	
	public final PowerShootManager getPowerShootManager() {
		return this.powerShootManager;
	}
	
	public final PowerSpecialManager getPowerSpecialManager() {
		return this.powerSpecialManager;
	}
	
	public final PowerSpecialPropertiesManager getPowerSpecialPropertiesManager() {
		return this.powerSpecialPropertiesManager;
	}
	
	public final RequirementManager getRequirementManager() {
		return this.requirementManager;
	}
	
	public final SocketGemManager getSocketManager() {
		return this.socketManager;
	}
	
	public final LoreStatsManager getStatsManager() {
		return this.statsManager;
	}
	
	public final MenuManager getMenuManager() {
		return this.menuManager;
	}
}
