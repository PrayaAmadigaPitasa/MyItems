package net.ardeus.myitems.manager.game;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.SlotType;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.element.Element;
import net.ardeus.myitems.element.ElementBoostWeapon;
import net.ardeus.myitems.element.ElementBoostStatsWeapon;
import net.ardeus.myitems.element.ElementPotion;
import net.ardeus.myitems.element.ElementBoostStatsWeapon.ElementBoostStatsWeaponBuilder;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.potion.PotionProperties;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public abstract class ElementManager extends HandlerManager {
	
	protected ElementManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract Collection<String> getElementIds();
	public abstract Collection<Element> getAllElement();
	public abstract Element getElement(String elementId);
	public abstract Element getElementByKeyLore(String keyLore);
	
	public final Element getElementByLore(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (lore != null) {
			final String[] part = lore.split(MainConfig.KEY_ELEMENT);
			
			if (part.length > 1) {
				final String loreElement = part[1].replace(mainConfig.getElementColor(), "");
				final Element element = getElementByKeyLore(loreElement);
				
				return element;
			}
		}
		
		return null;
	}
	
	public final boolean isExists(String elementId) {
		return getElement(elementId) != null;
	}
	
	public final boolean isElementLore(String lore) {
		return getElementByLore(lore) != null;
	}
	
	public final String getTextElement(String element, double value) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		String format = mainConfig.getElementFormat();
		
		map.put("element", getKeyElement(element));
		map.put("value", getKeyValue(value));
		format = TextUtil.placeholder(map, format, "<", ">");
		
		return format;		
	}
	
	public final HashMap<String, Double> getMapElement(LivingEntity livingEntity) {
		return getMapElement(livingEntity, SlotType.UNIVERSAL);
	}
	
	public final HashMap<String, Double> getMapElement(LivingEntity livingEntity, SlotType slotType) {
		return getMapElement(livingEntity, slotType, true);
	}
	
	public final HashMap<String, Double> getMapElement(LivingEntity livingEntity, SlotType slotType, boolean checkDurability) {
		final HashMap<String, Double> map = new HashMap<String, Double>();
		
		for (Slot slot : Slot.values()) {
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(livingEntity, slot);
			
			if (EquipmentUtil.isSolid(item)) {
				final Material itemMaterial = item.getType();
				final SlotType itemSlotType = SlotType.getSlotType(itemMaterial);
				
				if (slotType.equals(SlotType.UNIVERSAL) || slotType.equals(itemSlotType)) {
					final HashMap<String, Double> subMap = getMapElement(item, checkDurability);
					
					for (String key : subMap.keySet()) {
						final double value = subMap.get(key);
						
						map.put(key, map.containsKey(key) ? map.get(key) + value : value);
					}
				}
			}
		}
		
		return map;
	}
	
	public final HashMap<String, Double> getMapElement(ItemStack item, boolean checkDurability) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final HashMap<String, Double> map = new HashMap<String, Double>();
		
		if (EquipmentUtil.loreCheck(item)) {
			if (!(checkDurability && !statsManager.checkDurability(item))) {
				for (String elementId : getElementIds()) {
					final double value = getElementValue(item, elementId);
					
					if (value != 0) {
						map.put(elementId, value);
					}
				}
			}
		}
		
		return map;
	}
	
	public final HashMap<String, Double> getElementCalculation(HashMap<String, Double> elementAttacker, HashMap<String, Double> elementVictims) {
		final HashMap<String, Double> map = new HashMap<String, Double>();
		
		for (String key : elementAttacker.keySet()) {
			if (isExists(key)) {
				final double valueAttack = elementAttacker.get(key);
				final double minValue = MathUtil.negative(valueAttack);
				
				double resistance = 0;
				
				for (String resist : elementVictims.keySet()) {
					final Element element = getElement(resist);
					
					if (element != null) {
						final Double scaleResistance = element.getScaleResistance(key);
						
						if (scaleResistance != null) {
							final double valueDefense = elementVictims.get(resist);
							
							resistance = resistance + (scaleResistance * valueDefense);
						}
					}
				}
				
				final double value = valueAttack - resistance;
				
				map.put(key, MathUtil.limitDouble(value, minValue, value));
			}
		}
		
		return map;
	}
	
	public final void applyElementPotion(LivingEntity attacker, LivingEntity victims, HashMap<String, Double> mapElement) {
		for (String key : mapElement.keySet()) {
			final Element element = getElement(key);
			
			if (element != null) {
				final double value = mapElement.get(key);
				
				if (value > 0) {
					final ElementPotion elementPotion = element.getPotion();
					
					for (PotionEffectType potion : elementPotion.getPotionAttacker().keySet()) {
						final PotionProperties potionAttributes = elementPotion.getPotionAttacker().get(potion);
						final double chance = value * potionAttributes.getScaleChance();
						
						if (MathUtil.chanceOf(chance)) {
							final int grade = potionAttributes.getGrade();
							final int duration = (int) (value * potionAttributes.getScaleDuration());
							
							CombatUtil.applyPotion(attacker, potion, grade, duration);
						}
					}
					
					for (PotionEffectType potion : elementPotion.getPotionVictims().keySet()) {
						final PotionProperties potionAttributes = elementPotion.getPotionVictims().get(potion);
						final double chance = value * potionAttributes.getScaleChance();
						
						if (MathUtil.chanceOf(chance)) {
							final int grade = potionAttributes.getGrade();
							final int duration = (int) (value * potionAttributes.getScaleDuration());
							
							CombatUtil.applyPotion(victims, potion, grade, duration);
						}
					}
				}
			}
		}
	}
	
	public final ElementBoostStatsWeapon getElementBoostStats(HashMap<String, Double> mapElement) {
	
		double baseAdditionalDamage = 0;
		double basePercentDamage = 0;
		
		for (String key : mapElement.keySet()) {
			final Element element = getElement(key);
			
			if (element != null) {
				final double value = mapElement.get(key);
				final ElementBoostWeapon elementBoostWeapon = element.getBoostWeapon();
				
				baseAdditionalDamage = baseAdditionalDamage + (value * elementBoostWeapon.getScaleBaseAdditionalDamage());
				basePercentDamage = basePercentDamage + (value * elementBoostWeapon.getScaleBasePercentDamage());
			}
		}
		
		final ElementBoostStatsWeaponBuilder elementBoostStatsWeaponBuilder = new ElementBoostStatsWeaponBuilder();
		
		elementBoostStatsWeaponBuilder.setBaseAdditionalDamage(baseAdditionalDamage);
		elementBoostStatsWeaponBuilder.setBasePercentDamage(basePercentDamage);
		
		return elementBoostStatsWeaponBuilder.build();
	}
		
	public final double getElementValue(ItemStack item, String element) {
		final int line = getLineElement(item, element);
		
		return line != -1 ? getElementValue(item, line) : 0;
	}
	
	public final double getElementValue(ItemStack item, int line) {
		final String lore = EquipmentUtil.getLores(item).get(line-1);
		
		return getElementValue(lore);
	}
	
	public final double getElementValue(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String[] part = lore.split(MainConfig.KEY_ELEMENT_VALUE);
		
		if (part.length > 1) {
			final String positiveValue = mainConfig.getStatsLorePositiveValue();
			final String negativeValue = mainConfig.getStatsLoreNegativeValue();
			final String textValue = part[1].replaceAll(positiveValue, "").replaceAll(negativeValue, "");
			
			if (MathUtil.isNumber(textValue)) {
				return MathUtil.parseDouble(textValue);
			}
		}
		
		return 0;
	}
	
	public final String getElementKeyLore(String element) {
		if (isExists(element)) {
			return getElement(element).getKeyLore();
		}
		
		return null;
	}
	
	public final int getLineElement(ItemStack item, String element) {
		if (isExists(element)) {
			return EquipmentUtil.loreGetLineKey(item, getKeyElement(element, true));
		}
		
		return -1;
	}
	
	private final String getKeyElement(String element) {
		return getKeyElement(element, false);
	}
	
	private final String getKeyElement(String element, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return justCheck ? MainConfig.KEY_ELEMENT + mainConfig.getElementColor() + getElementKeyLore(element) : MainConfig.KEY_ELEMENT + mainConfig.getElementColor() + getElementKeyLore(element) + MainConfig.KEY_ELEMENT + mainConfig.getElementColor();
	}
	
	private final String getKeyValue(double value) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return MainConfig.KEY_ELEMENT_VALUE + (value > 0 ? mainConfig.getStatsLorePositiveValue() : mainConfig.getStatsLoreNegativeValue()) + value + MainConfig.KEY_ELEMENT_VALUE + mainConfig.getElementColor();
	}
}
