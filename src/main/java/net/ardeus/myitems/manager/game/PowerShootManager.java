package net.ardeus.myitems.manager.game;

import java.util.HashMap;

import core.praya.agarthalib.enums.branch.ProjectileEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.power.PowerClickType;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public abstract class PowerShootManager extends HandlerManager {

	protected PowerShootManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract String getPowerShootKeyLore(ProjectileEnum projectileEnum);
	public abstract ProjectileEnum getProjectileEnumByKeyLore(String keyLore);
	
	public final ProjectileEnum getProjectileEnumByLore(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (lore != null) {
			final String[] loreCheck = lore.split(MainConfig.KEY_SHOOT);
			
			if (loreCheck.length > 1) {
				final String colorPowerType = mainConfig.getPowerColorType();
				final String keyLore = loreCheck[1].replaceFirst(colorPowerType, "");
				
				return getProjectileEnumByKeyLore(keyLore);
			}
		}
		
		return null;
	}
	
	public final String getTextPowerShoot(PowerClickType click, ProjectileEnum projectile, double cooldown) {
		final GameManager gameManager = plugin.getGameManager();
		final PowerManager powerManager = gameManager.getPowerManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (click != null && projectile != null) {
			final String keyShoot = getKeyShoot(projectile);
			final HashMap<String, String> map = new HashMap<String, String>();
			
			String format = mainConfig.getPowerFormat();
			
			cooldown = MathUtil.roundNumber(cooldown, 1);
			
			map.put("click", powerManager.getKeyClick(click));
			map.put("type", keyShoot);
			map.put("cooldown", powerManager.getKeyCooldown(cooldown));
			format = TextUtil.placeholder(map, format, "<", ">");
	
			return format;
		} else {
			return null;
		}
	}
	
	public final String getKeyShoot(ProjectileEnum projectile) {
		return getKeyShoot(projectile, false);
	}
	
	public final String getKeyShoot(ProjectileEnum projectile, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (projectile != null) {
			final String key = MainConfig.KEY_SHOOT;
			final String color = mainConfig.getPowerColorType();
			final String text = getPowerShootKeyLore(projectile);
			
			return justCheck ? key + color + text : key + color + text + key + color;
		} else {
			return null;
		}
	}
}
