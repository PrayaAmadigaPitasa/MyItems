package net.ardeus.myitems.manager.game;

import java.util.List;

import org.bukkit.inventory.ItemStack;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerType;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;

public abstract class PowerManager extends HandlerManager {

	protected PowerManager(MyItems plugin) {
		super(plugin);
	};
	
	public final PowerClickType getPowerClickTypeByLore(String lore) {
		if (lore != null) {
			for (PowerClickType key : PowerClickType.values()) {
				final String keyClick = getKeyClick(key, true);
				
				if (lore.contains(keyClick)) {
					return key;
				}
			}
		}
		
		return null;
	}
	
	public final double getPowerCooldownByLore(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (lore != null) {
			final String[] loreCheck = lore.split(MainConfig.KEY_COOLDOWN);
			
			if (loreCheck.length > 1) {
				final String colorPowerCooldown = mainConfig.getPowerColorCooldown();
				final String loreCooldown = loreCheck[1].replaceFirst(colorPowerCooldown, "");
				
				if (MathUtil.isNumber(loreCooldown)) {
					return MathUtil.parseDouble(loreCooldown);
				}
			}
		}
		
		return 0;
	}
	
	public final Integer getLineClick(ItemStack item, PowerClickType click) {
		if (EquipmentUtil.hasLore(item) && click != null) {
			final String keyClick = getKeyClick(click);
			
			return EquipmentUtil.loreGetLineKey(item, keyClick);
		} else {
			return null;
		}
	}	
	
	public final PowerType getPowerType(ItemStack item, PowerClickType click) {
		final Integer line = getLineClick(item, click);
		
		if (line != null) {
			final List<String> lores = EquipmentUtil.getLores(item);
			final String lore = lores.get(line-1);
			
			return getPowerTypeByLore(lore);
		} else {
			return null;
		}
	}
	
	public final PowerType getPowerTypeByLore(String lore) {
		if (lore != null) {
			if (lore.contains(MainConfig.KEY_COMMAND)) {
				return PowerType.COMMAND;
			} else if (lore.contains(MainConfig.KEY_SHOOT)) {
				return PowerType.SHOOT;
			} else if (lore.contains(MainConfig.KEY_SPECIAL)) {
				return PowerType.SPECIAL;
			}
		}
		
		return null;
	}
	
	public final boolean hasPower(ItemStack item, PowerClickType click) {
		return getPowerType(item, click) != null;
	}
	
	public final boolean isLorePower(String lore) {
		return getPowerTypeByLore(lore) != null;
	}
	
	public final String getKeyClick(PowerClickType click) {
		return getKeyClick(click, false);
	}
	
	public final String getKeyClick(PowerClickType click, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (click != null) {
			final String key = MainConfig.KEY_CLICK;
			final String color = mainConfig.getPowerColorClick();
			final String text = click.getText();
			
			return justCheck ? key + color + text : key + color + text + key + color;
		} else {
			return null;
		}
	}
	
	public final String getKeyCooldown(double cooldown) {
		return getKeyCooldown(cooldown, false);
	}
	
	public final String getKeyCooldown(double cooldown, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = MainConfig.KEY_COOLDOWN;
		final String color = mainConfig.getPowerColorCooldown();
		
		return justCheck ? key + color : key + color + cooldown + key + color;
	}
}
