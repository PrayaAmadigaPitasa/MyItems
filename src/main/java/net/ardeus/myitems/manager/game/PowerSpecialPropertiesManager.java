package net.ardeus.myitems.manager.game;

import java.util.Collection;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.power.PowerSpecialProperties;

public abstract class PowerSpecialPropertiesManager extends HandlerManager {

	protected PowerSpecialPropertiesManager(MyItems plugin) {
		super(plugin);
	}

	public abstract Collection<String> getPowerSpecialPropertiesIds();
	public abstract Collection<PowerSpecialProperties> getAllPowerSpecialProperties();
	public abstract PowerSpecialProperties getPowerSpecialProperties(String powerSpecialId);
	
	public final boolean isExists(String powerSpecialId) {
		return getPowerSpecialProperties(powerSpecialId) != null;
	}
}