package net.ardeus.myitems.manager.game;

import java.util.Collection;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.item.ItemGenerator;

public abstract class ItemGeneratorManager extends HandlerManager {
	
	protected ItemGeneratorManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract Collection<String> getItemGeneratorIds();
	public abstract Collection<ItemGenerator> getAllItemGenerators();
	public abstract ItemGenerator getItemGenerator(String id);
	
	public final boolean isItemGeneratorExists(String id) {
		return getItemGenerator(id) != null;
	}
}
