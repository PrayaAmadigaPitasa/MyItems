package net.ardeus.myitems.manager.game;

import java.util.HashMap;
import java.util.List;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerSpecial;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public abstract class PowerSpecialManager extends HandlerManager {
	
	protected PowerSpecialManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract List<String> getPowerSpecialIds();
	public abstract List<PowerSpecial> getAllPowerSpecials();
	public abstract PowerSpecial getPowerSpecial(String powerSpecialId);
	public abstract PowerSpecial getPowerSpecialByKeyLore(String keyLore);
	
	public final PowerSpecial getPowerSpecialByLore(String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (lore != null) {
			final String[] loreCheck = lore.split(MainConfig.KEY_SPECIAL);
			
			if (loreCheck.length > 1) {
				final String colorPowerType = mainConfig.getPowerColorType();
				final String keyLore = loreCheck[1].replace(colorPowerType, "");
				
				return getPowerSpecialByKeyLore(keyLore);
			}
		}
			
		return null;
	}
	
	public final boolean isExists(String powerSpecialId) {
		return getPowerSpecial(powerSpecialId) != null;
	}
	
	public final boolean isLorePowerSpecial(String lore) {
		return getPowerSpecialByLore(lore) != null;
	}
	
	public final String getTextPowerSpecial(PowerClickType click, PowerSpecial powerSpecial, double cooldown) {
		final GameManager gameManager = plugin.getGameManager();
		final PowerManager powerManager = gameManager.getPowerManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (click != null && powerSpecial != null) {
			final String keyClick = powerManager.getKeyClick(click);
			final String keyCooldown = powerManager.getKeyCooldown(cooldown);
			final String keySpecial = getKeySpecial(powerSpecial);
			final HashMap<String, String> map = new HashMap<String, String>();
			
			String format = mainConfig.getPowerFormat();
			
			cooldown = MathUtil.roundNumber(cooldown, 1);
			
			map.put("click", keyClick);
			map.put("type", keySpecial);
			map.put("cooldown", keyCooldown);
			format = TextUtil.placeholder(map, format, "<", ">");
	
			return format;
		} else {
			return null;
		}
	}
	
	public final String getKeySpecial(PowerSpecial powerSpecial) {
		return getKeySpecial(powerSpecial, false);
	}
	
	public final String getKeySpecial(PowerSpecial powerSpecial, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (powerSpecial != null) {
			final String key = MainConfig.KEY_SPECIAL;
			final String color = mainConfig.getPowerColorType();
			final String keyLore = powerSpecial.getKeyLore();
			
			return justCheck ? key + color + keyLore : key + color + keyLore + key + color;
		} else {
			return null;
		}
	}
}
