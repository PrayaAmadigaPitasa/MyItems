package net.ardeus.myitems.manager.game;

import java.util.Collection;

import org.bukkit.inventory.ItemStack;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;

public abstract class ItemManager extends HandlerManager {
	
	protected ItemManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract Collection<String> getItemIds();
	public abstract Collection<ItemStack> getAllItems();
	public abstract ItemStack getItem(String id);
	public abstract String getRawName(String id);
	
	public final boolean isExist(String id) {
		return getItem(id) != null;
	}
}
