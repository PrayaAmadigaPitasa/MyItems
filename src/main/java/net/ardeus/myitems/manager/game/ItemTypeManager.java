package net.ardeus.myitems.manager.game;

import java.util.Collection;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.item.ItemType;

public abstract class ItemTypeManager extends HandlerManager {
	
	protected ItemTypeManager(MyItems plugin) {
		super(plugin);
	};

	public abstract Collection<String> getItemTypeIds();
	public abstract Collection<ItemType> getItemTypes();
	public abstract ItemType getItemType(String id);
	
	public final boolean isItemTypeExists(String id) {
		return getItemType(id) != null;
	}
}