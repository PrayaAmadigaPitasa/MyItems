package net.ardeus.myitems.manager.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.socket.SocketEnum;
import net.ardeus.myitems.socket.SocketGem;
import net.ardeus.myitems.socket.SocketGemProperties;
import net.ardeus.myitems.socket.SocketGemTree;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.TextUtil;

public abstract class SocketGemManager extends HandlerManager {
	
	protected SocketGemManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract Collection<String> getSocketGemIds();
	public abstract Collection<SocketGemTree> getAllSocketGemTrees();
	public abstract SocketGemTree getSocketGemTree(String socketGemId);
	public abstract SocketGem getSocketGemByKeyLore(String keyLore);
	public abstract SocketGem getSocketGemByItem(ItemStack item);
	
	public final SocketGem getSocketGemByLore(String lore) {
		if (lore != null) {
			final String[] part = lore.split(MainConfig.KEY_SOCKET_LORE_GEMS);
			
			if (part.length > 1) {
				final String keyLore = part[1];
				final SocketGem socketGem = getSocketGemByKeyLore(keyLore);
				
				return socketGem;
			}
		}
		
		return null;
	}
	
	public final boolean isExist(String socketGemId) {
		return getSocketGemTree(socketGemId) != null;
	}
	
	public final boolean isSocketGemKeyLore(String keyLore) {
		return getSocketGemByKeyLore(keyLore) != null;
	}
	
	public final boolean isSocketGemItem(ItemStack item) {
		return getSocketGemByItem(item) != null;
	}
	
	public final String getRawName(String socketGemId) {
		final SocketGemTree socketGemTree = getSocketGemTree(socketGemId);
		
		return socketGemTree != null ? socketGemTree.getId() : null;
	}	
	
	public final String getTextSocketGemSlotEmpty() {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		String format = mainConfig.getSocketFormatSlot();
		
		map.put("slot", getKeySocketEmpty());
		format = TextUtil.placeholder(map, format, "<", ">");
		
		return format;		
	}
	
	public final String getTextSocketGemSlotLocked() {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		String format = mainConfig.getSocketFormatSlot();
		
		map.put("slot", getKeySocketLocked());
		format = TextUtil.placeholder(map, format, "<", ">");
		
		return format;		
	}
	
	
	public final double getSocketValue(SocketGemProperties socketProperties, SocketEnum typeValue) {
		switch (typeValue) {
		case ADDITIONAL_DAMAGE : return socketProperties != null ? socketProperties.getAdditionalDamage() : 0D;
		case PERCENT_DAMAGE : return socketProperties != null ? socketProperties.getPercentDamage() : 0D;
		case PENETRATION : return socketProperties != null ? socketProperties.getPenetration() : 0D;
		case PVP_DAMAGE : return socketProperties != null ? socketProperties.getPvPDamage() : 0D;
		case PVE_DAMAGE : return socketProperties != null ? socketProperties.getPvEDamage() : 0D;
		case ADDITIONAL_DEFENSE : return socketProperties != null ? socketProperties.getAdditionalDefense() : 0D;
		case PERCENT_DEFENSE : return socketProperties != null ? socketProperties.getPercentDefense() : 0D;
		case HEALTH : return socketProperties != null ? socketProperties.getHealth() : 0D;
		case PVP_DEFENSE : return socketProperties != null ? socketProperties.getPvPDefense() : 0D;
		case PVE_DEFENSE : return socketProperties != null ? socketProperties.getPvEDefense() : 0D;
		case CRITICAL_CHANCE : return socketProperties != null ? socketProperties.getCriticalChance() : 0D;
		case CRITICAL_DAMAGE : return socketProperties != null ? socketProperties.getCriticalDamage() : 0D;
		case BLOCK_AMOUNT : return socketProperties != null ? socketProperties.getBlockAmount() : 0D;
		case BLOCK_RATE : return socketProperties != null ? socketProperties.getBlockRate() : 0D;
		case HIT_RATE : return socketProperties != null ? socketProperties.getHitRate() : 0D;
		case DODGE_RATE : return socketProperties != null ? socketProperties.getDodgeRate() : 0D;
		default : return 0;
		}
	}
	
	public final List<Integer> getLineLoresSocketEmpty(ItemStack item) {
		final List<Integer> list = new ArrayList<Integer>(); 
		
		if (EquipmentUtil.loreCheck(item)) {
			final List<String> lores = EquipmentUtil.getLores(item);
			
			for (int index = 0; index < lores.size(); index++) {
				final String lore = lores.get(index);
				
				if (isSocketEmptyLore(lore)) {
					list.add(index+1);
				}
			}
		}
		
		return list;
	}
	
	public final int getFirstLineSocketEmpty(ItemStack item) {
		final List<Integer> list = getLineLoresSocketEmpty(item);
		
		return list.size() > 0 ? list.get(0) : -1;
	}
	
	public final boolean containsSocketEmpty(ItemStack item) {
		return getLineLoresSocketEmpty(item).size() > 0;
	}
	
	public final List<Integer> getLineLoresSocketLocked(ItemStack item) {
		final List<Integer> list = new ArrayList<Integer>(); 
		
		if (EquipmentUtil.loreCheck(item)) {
			final List<String> lores = EquipmentUtil.getLores(item);
			
			for (int index = 0; index < lores.size(); index++) {
				final String lore = lores.get(index);
				
				if (isSocketLockedLore(lore)) {
					list.add(index+1);
				}
			}
		}
		
		return list;
	}
	
	public final int getFirstLineSocketLocked(ItemStack item) {
		final List<Integer> list = getLineLoresSocketLocked(item);
		
		return list.size() > 0 ? list.get(0) : -1;
	}
	
	public final boolean containsSocketLocked(ItemStack item) {
		return getLineLoresSocketLocked(item).size() > 0;
	}
	
	public final List<String> getLoresSocket(ItemStack item) {
		final List<String> list = new ArrayList<String>();
		
		if (EquipmentUtil.loreCheck(item)) {
			final List<String> lores = EquipmentUtil.getLores(item);
			
			for (String lore : lores) {
				if (isSocketGemLore(lore)) {
					list.add(lore);
				}
			}
		}
		
		return list;
	}
	
	public final List<Integer> getLineLoresSocket(ItemStack item) {
		final List<Integer> list = new ArrayList<Integer>(); 
		
		if (EquipmentUtil.loreCheck(item)) {
			final List<String> lores = EquipmentUtil.getLores(item);
			
			for (int index = 0; index < lores.size(); index++) {
				final String lore = lores.get(index);
				
				if (isSocketGemLore(lore)) {
					list.add(index+1);
				}
			}
		}
		
		return list;
	}
	
	public final boolean containsSocketGems(ItemStack item) {
		return getLineLoresSocket(item).size() > 0;
	}
	
	public final boolean isSocketGemLore(String lore) {
		return lore.contains(MainConfig.KEY_SOCKET_LORE_GEMS);
	}
	
	public final boolean isSocketEmptyLore(String lore) {
		final String key = getKeySocketEmpty(true);
		
		return lore.contains(key);
	}
	
	public final boolean isSocketLockedLore(String lore) {
		final String key = getKeySocketLocked(true);
		
		return lore.contains(key);
	}
	
	public final SocketGemProperties getSocketProperties(LivingEntity livingEntity) {
		return getSocketProperties(livingEntity, true);
	}
	
	public final SocketGemProperties getSocketProperties(LivingEntity livingEntity, boolean checkDurability) {
		
		double additionalDamage = 0;
		double percentDamage = 0;
		double penetration = 0;
		double pvpDamage = 0;
		double pveDamage = 0;
		double additionalDefense = 0;
		double percentDefense = 0;
		double health = 0;
		double healthRegen = 0;
		double staminaMax = 0;
		double staminaRegen = 0;
		double attackAoERadius = 0;
		double attackAoEDamage = 0;
		double pvpDefense = 0;
		double pveDefense = 0;
		double criticalChance = 0;
		double criticalDamage = 0;
		double blockAmount = 0;
		double blockRate = 0;
		double hitRate = 0;
		double dodgeRate = 0;
		
		for (Slot slot : Slot.values()) {
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(livingEntity, slot);
			
			if (EquipmentUtil.isSolid(item)) {
				final SocketGemProperties build = getSocketProperties(item, checkDurability);
				
				additionalDamage = additionalDamage + build.getAdditionalDamage();
				percentDamage = percentDamage + build.getPercentDamage();
				penetration = penetration + build.getPenetration();
				pvpDamage = pvpDamage + build.getPvPDamage();
				pveDamage = pveDamage + build.getPvEDamage();
				additionalDefense = additionalDefense + build.getAdditionalDefense();
				percentDefense = percentDefense + build.getPercentDefense();
				health = health + build.getHealth();
				healthRegen = healthRegen + build.getHealthRegen();
				staminaMax = staminaMax + build.getStaminaMax();
				staminaRegen = staminaRegen + build.getStaminaRegen();
				attackAoERadius = attackAoERadius + build.getAttackAoERadius();
				attackAoEDamage = attackAoEDamage + build.getAttackAoEDamage();
				pvpDefense = pvpDefense + build.getPvPDefense();
				pveDefense = pveDefense + build.getPvEDefense();
				criticalChance = criticalChance + build.getCriticalChance();
				criticalDamage = criticalDamage + build.getCriticalDamage();
				blockAmount = blockAmount + build.getBlockAmount();
				blockRate = blockRate + build.getBlockRate();
				hitRate = hitRate + build.getHitRate();
				dodgeRate = dodgeRate + build.getDodgeRate();
			}
		}
		
		return new SocketGemProperties(additionalDamage, percentDamage, penetration, pvpDamage, pveDamage, additionalDefense, percentDefense, health, healthRegen, staminaMax, staminaRegen, attackAoERadius, attackAoEDamage, pvpDefense, pveDefense, criticalChance, criticalDamage, blockAmount, blockRate, hitRate, dodgeRate);
	}
	
	public final SocketGemProperties getSocketProperties(ItemStack item) {
		return getSocketProperties(item, true);
	}
	
	public final SocketGemProperties getSocketProperties(ItemStack item, boolean checkDurability) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		
		double additionalDamage = 0;
		double percentDamage = 0;
		double penetration = 0;
		double pvpDamage = 0;
		double pveDamage = 0;
		double additionalDefense = 0;
		double percentDefense = 0;
		double health = 0;
		double healthRegen = 0;
		double staminaMax = 0;
		double staminaRegen = 0;
		double attackAoERadius = 0;
		double attackAoEDamage = 0;
		double pvpDefense = 0;
		double pveDefense = 0;
		double criticalChance = 0;
		double criticalDamage = 0;
		double blockAmount = 0;
		double blockRate = 0;
		double hitRate = 0;
		double dodgeRate = 0;
		
		if (EquipmentUtil.loreCheck(item)) {
			if (!(checkDurability && !statsManager.checkDurability(item))) {
				for (String lore : EquipmentUtil.getLores(item)) {
					final SocketGem socket = getSocketGemByLore(lore);
					
					if (socket != null) {
						final SocketGemProperties build = socket.getSocketProperties();
						
						additionalDamage = additionalDamage + build.getAdditionalDamage();
						percentDamage = percentDamage + build.getPercentDamage();
						penetration = penetration + build.getPenetration();
						pvpDamage = pvpDamage + build.getPvPDamage();
						pveDamage = pveDamage + build.getPvEDamage();
						additionalDefense = additionalDefense + build.getAdditionalDefense();
						percentDefense = percentDefense + build.getPercentDefense();
						health = health + build.getHealth();
						healthRegen = healthRegen + build.getHealthRegen();
						staminaMax = staminaMax + build.getStaminaMax();
						staminaRegen = staminaRegen + build.getStaminaRegen();
						attackAoERadius = attackAoERadius + build.getAttackAoERadius();
						attackAoEDamage = attackAoEDamage + build.getAttackAoEDamage();
						pvpDefense = pvpDefense + build.getPvPDefense();
						pveDefense = pveDefense + build.getPvEDefense();
						criticalChance = criticalChance + build.getCriticalChance();
						criticalDamage = criticalDamage + build.getCriticalDamage();
						blockAmount = blockAmount + build.getBlockAmount();
						blockRate = blockRate + build.getBlockRate();
						hitRate = hitRate + build.getHitRate();
						dodgeRate = dodgeRate + build.getDodgeRate();
					}
				}
			}
		}
		
		return new SocketGemProperties(additionalDamage, percentDamage, penetration, pvpDamage, pveDamage, additionalDefense, percentDefense, health, healthRegen, staminaMax, staminaRegen, attackAoERadius, attackAoEDamage, pvpDefense, pveDefense, criticalChance, criticalDamage, blockAmount, blockRate, hitRate, dodgeRate);
	}
	
	private String getKeySocketEmpty() {
		return getKeySocketEmpty(false);
	}
	
	private String getKeySocketEmpty(boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = MainConfig.KEY_SOCKET_SLOT;
		final String color = mainConfig.getSocketColorSlot();
		final String keylore = mainConfig.getSocketFormatSlotEmpty();
		
		return justCheck ? key + color + keylore : key + color + keylore + key + color;
	}
	
	private String getKeySocketLocked() {
		return getKeySocketLocked(false);
	}
	
	private String getKeySocketLocked(boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = MainConfig.KEY_SOCKET_SLOT;
		final String color = mainConfig.getSocketColorSlot();
		final String keylore = mainConfig.getSocketFormatSlotLocked();
		
		return justCheck ? key + color + keylore : key + color + keylore + key + color;
	}
	
	public final String getTextSocketGemsLore(String socket, int grade) {
		return getTextSocketGemsLore(socket, grade, false);
	}
	
	public final String getTextSocketGemsLore(String socketGemId, int grade, boolean justCheck) {
		final SocketGemTree socketGemTree = getSocketGemTree(socketGemId);
		
		if (socketGemTree != null) {
			final String key = MainConfig.KEY_SOCKET_LORE_GEMS;
			final SocketGem socketGem = socketGemTree.getSocketGem(grade);
			
			if (socketGem != null) {
				final String keyLore = socketGem.getKeyLore();
				
				return justCheck ? key + keyLore : key + keyLore + key;
			}
		}
		
		return null;
	}
}
