package net.ardeus.myitems.manager.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.RomanNumber;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.SlotType;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityItemSlotWeapon;
import net.ardeus.myitems.ability.AbilityItemWeapon;
import net.ardeus.myitems.ability.AbilityLore;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;

public abstract class AbilityWeaponManager extends HandlerManager {
	
	protected AbilityWeaponManager(MyItems plugin) {
		super(plugin);
	}
	
	public abstract Collection<String> getAbilityIds();
	public abstract Collection<AbilityWeapon> getAllAbilityWeapon();
	public abstract AbilityWeapon getAbilityWeapon(String ability);
	public abstract AbilityWeapon getAbilityWeaponByKeyLore(String keyLore);
	
	public final boolean isExists(String ability) {
		return getAbilityWeapon(ability) != null;
	}
	
	public final boolean isKeyLoreExists(String keyLore) {
		return getAbilityWeaponByKeyLore(keyLore) != null;
	}
	
	public final AbilityLore getAbiityLore(String lore) {
		if (lore != null) {
			final String keyAbility = getKeyAbility();
			final String keyChance = getKeyChance();
			
			if (lore.contains(keyAbility) && lore.contains(keyChance)) {
				final String[] partsAbility = lore.split(keyAbility);
				final int partsAbilityLength = partsAbility.length;
				
				if (partsAbilityLength > 1) {
					final String[] partsKeyLore = partsAbility[1].split(" ");
					final int partsKeyLoreLength = partsKeyLore.length;
					final String textGrade = partsKeyLore[partsKeyLoreLength-1];
					
					String keyLore = null;
					
					for (int index = 0; index < partsKeyLoreLength-1; index++) {
						final String part = partsKeyLore[index];
						
						if (index == 0) {
							keyLore = part;
						} else {
							keyLore = keyLore + " " + part;
						}
					}
					
					if (keyLore != null) {
						final int grade = RomanNumber.romanConvert(textGrade);
						
						if (grade > 0) {
							final String[] partsChance = lore.split(keyChance);
							final int partsChanceLength = partsChance.length;
							
							if (partsChanceLength > 1) {
								final String textChance = partsChance[1];
								
								if (MathUtil.isNumber(textChance)) {
									final double chance = MathUtil.parseDouble(textChance);
									final AbilityLore abilityLore = new AbilityLore(keyLore, grade, chance);
									
									return abilityLore;
								}
							}
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public final AbilityItemWeapon getAbilityItemWeapon(String lore) {
		final AbilityLore abilityLore = getAbiityLore(lore);
		
		if (abilityLore != null) {
			final String keyLore = abilityLore.getKeyLore();
			final AbilityWeapon abilityWeapon = getAbilityWeaponByKeyLore(keyLore);
			
			if (abilityWeapon != null) {
				final String ability = abilityWeapon.getId();
				final int grade = abilityLore.getGrade();
				final double chance = abilityLore.getChance();
				final AbilityItemWeapon abilityItemWeapon = new AbilityItemWeapon(ability, grade, chance);
				
				return abilityItemWeapon;
			}
		}
		
		return null;
	}
	
	public final boolean isAbilityItemWeapon(String lore) {
		return getAbilityItemWeapon(lore) != null;
	}
	
	public final Integer getLineAbilityItemWeapon(ItemStack item, String ability) {
		if (item != null && ability != null) {
			final AbilityWeapon abilityWeapon = getAbilityWeapon(ability);
			
			if (abilityWeapon != null) {
				final String keyAbility = getKeyAbility(abilityWeapon);
				
				if (keyAbility != null) {
					final List<String> lores = EquipmentUtil.getLores(item);
					
					for (int index = 0; index < lores.size(); index++) {
						final String lore = lores.get(index);
						
						if (lore.contains(keyAbility)) {
							final int line = index + 1;
							
							return line;
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public final List<AbilityItemWeapon> getListAbilityItemWeapon(ItemStack item) {
		final List<AbilityItemWeapon> listAbilityItemWeapon = new ArrayList<AbilityItemWeapon>();
		
		if (item != null) {
			final String keyAbilityCheck = getKeyAbility();
			final List<String> lores = EquipmentUtil.getLores(item);
			
			for (String lore : lores) {
				if (lore.contains(keyAbilityCheck)) {
					final AbilityItemWeapon abilityItemWeapon = getAbilityItemWeapon(lore);
					
					if (abilityItemWeapon != null) {
						listAbilityItemWeapon.add(abilityItemWeapon);
					}
				}
			}
		}
		
		return listAbilityItemWeapon;
	}
	
	public final boolean hasAbilityItemWeapon(ItemStack item) {
		return !getListAbilityItemWeapon(item).isEmpty();
	}
	
	public final HashMap<Slot, AbilityItemSlotWeapon> getMapAbilityItemSlotWeapon(LivingEntity entity) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<Slot, AbilityItemSlotWeapon> mapAbilityItemSlotWeapon = new HashMap<Slot, AbilityItemSlotWeapon>();
		
		if (entity != null) {
			final boolean enableAbilityOffHand = mainConfig.isAbilityWeaponEnableOffHand();
			
			for (Slot slot : Slot.values()) {
				if (slot.getType().equals(SlotType.WEAPON)) {
					if (!slot.equals(Slot.OFFHAND) || enableAbilityOffHand) {
						final ItemStack item = Bridge.getBridgeEquipment().getEquipment(entity, slot);
						final List<AbilityItemWeapon> listAbilityItemWeapon = getListAbilityItemWeapon(item);
						final AbilityItemSlotWeapon abilityItemSlotWeapon = new AbilityItemSlotWeapon(slot, listAbilityItemWeapon);
						
						mapAbilityItemSlotWeapon.put(slot, abilityItemSlotWeapon);
					}
				}
			}
		}
		
		return mapAbilityItemSlotWeapon;
	}
	
	public final HashMap<AbilityWeapon, Integer> getMapAbilityWeapon(Collection<AbilityItemWeapon> listAbilityItemWeapon) {
		return getMapAbilityWeapon(listAbilityItemWeapon, false);
	}
	
	public final HashMap<AbilityWeapon, Integer> getMapAbilityWeapon(Collection<AbilityItemWeapon> listAbilityItemWeapon, boolean checkChance) {
		final HashMap<AbilityWeapon, Integer> mapAbilityWeapon = new HashMap<AbilityWeapon, Integer>();
		
		if (listAbilityItemWeapon != null) {
			for (AbilityItemWeapon abilityItemWeapon : listAbilityItemWeapon) {
				final double chance = abilityItemWeapon.getChance();
				
				if (!checkChance || MathUtil.chanceOf(chance)) {
					final AbilityWeapon abilityWeapon = abilityItemWeapon.getAbilityWeapon();
					
					if (abilityWeapon != null) {
						final int grade = abilityItemWeapon.getGrade();
						
						if (mapAbilityWeapon.containsKey(abilityWeapon)) {
							final int totalGrade = mapAbilityWeapon.get(abilityWeapon) + grade;
							
							mapAbilityWeapon.put(abilityWeapon, totalGrade);
						} else {
							mapAbilityWeapon.put(abilityWeapon, grade);
						}
					}
				}
			}
		}
		
		return mapAbilityWeapon;
	}
	
	public final HashMap<AbilityWeapon, Integer> getMapAbilityWeapon(LivingEntity entity) {
		return getMapAbilityWeapon(entity, false);
	}
	
	public final HashMap<AbilityWeapon, Integer> getMapAbilityWeapon(LivingEntity entity, boolean checkChance) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<AbilityWeapon, Integer> mapAbilityWeapon = new HashMap<AbilityWeapon, Integer>();
		
		if (entity != null) {
			final boolean enableAbilityOffHand = mainConfig.isAbilityWeaponEnableOffHand();
			
			for (Slot slot : Slot.values()) {
				if (slot.getType().equals(SlotType.WEAPON)) {
					if (!slot.equals(Slot.OFFHAND) || enableAbilityOffHand) {
						final ItemStack item = Bridge.getBridgeEquipment().getEquipment(entity, slot);
						final List<AbilityItemWeapon> listAbilityItemWeapon = getListAbilityItemWeapon(item);
						
						for (AbilityItemWeapon abilityItemWeapon : listAbilityItemWeapon) {
							final double chance = abilityItemWeapon.getChance();
							
							if (!checkChance || MathUtil.chanceOf(chance)) {
								final AbilityWeapon abilityWeapon = abilityItemWeapon.getAbilityWeapon();
								
								if (abilityWeapon != null) {
									final int grade = abilityItemWeapon.getGrade();
								
									if (mapAbilityWeapon.containsKey(abilityWeapon)) {
										final int totalGrade = mapAbilityWeapon.get(abilityWeapon) + grade;
										
										mapAbilityWeapon.put(abilityWeapon, totalGrade);
									} else {
										mapAbilityWeapon.put(abilityWeapon, grade);
									}
								}
							}
						}
					}
				}
			}
		}
		
		return mapAbilityWeapon;
	}
	
	public final double getTotalBaseBonusDamage(HashMap<AbilityWeapon, Integer> mapAbilityWeapon) {
		
		double totalBaseBonusDamage = 0;
		
		if (mapAbilityWeapon != null) {
			for (AbilityWeapon abilityWeapon : mapAbilityWeapon.keySet()) {
				final int grade = mapAbilityWeapon.get(abilityWeapon);
				final double baseBonusDamage = abilityWeapon.getBaseBonusDamage(grade);
				
				totalBaseBonusDamage = totalBaseBonusDamage + baseBonusDamage;
			}
		}
		
		return totalBaseBonusDamage;
	}
	
	public final double getTotalBasePercentDamage(HashMap<AbilityWeapon, Integer> mapAbilityWeapon) {
		
		double totalBasePercentDamage = 0;
		
		if (mapAbilityWeapon != null) {
			for (AbilityWeapon abilityWeapon : mapAbilityWeapon.keySet()) {
				final int grade = mapAbilityWeapon.get(abilityWeapon);
				final double basePercentDamage = abilityWeapon.getBasePercentDamage(grade);
				
				totalBasePercentDamage = totalBasePercentDamage + basePercentDamage;
			}
		}
		
		return totalBasePercentDamage;
	}
	
	public final double getTotalCastBonusDamage(HashMap<AbilityWeapon, Integer> mapAbilityWeapon) {
		
		double totalCastBonusDamage = 0;
		
		if (mapAbilityWeapon != null) {
			for (AbilityWeapon abilityWeapon : mapAbilityWeapon.keySet()) {
				final int grade = mapAbilityWeapon.get(abilityWeapon);
				final double castBonusDamage = abilityWeapon.getCastBonusDamage(grade);
				
				totalCastBonusDamage = totalCastBonusDamage + castBonusDamage;
			}
		}
		
		return totalCastBonusDamage;
	}
	
	public final double getTotalCastPercentDamage(HashMap<AbilityWeapon, Integer> mapAbilityWeapon) {
		
		double totalCastPercentDamage = 0;
		
		if (mapAbilityWeapon != null) {
			for (AbilityWeapon abilityWeapon : mapAbilityWeapon.keySet()) {
				final int grade = mapAbilityWeapon.get(abilityWeapon);
				final double castPercentDamage = abilityWeapon.getCastPercentDamage(grade);
				
				totalCastPercentDamage = totalCastPercentDamage + castPercentDamage;
			}
		}
		
		return totalCastPercentDamage;
	}
	
	public final String getTextAbility(AbilityWeapon abilityWeapon, int grade) {
		return getTextAbility(abilityWeapon, grade, 100);
	}
	
	public final String getTextAbility(AbilityWeapon abilityWeapon, int grade, double chance) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String keyAbility = getKeyAbility(abilityWeapon, grade);
		final String keyChance = getKeyChance(chance);
		final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
		
		String format = mainConfig.getAbilityFormat();
		
		mapPlaceholder.put("ability", keyAbility);
		mapPlaceholder.put("chance", keyChance);
		format = TextUtil.placeholder(mapPlaceholder, format, "<", ">");
		
		return format;
	}
	
	private final String getKeyAbility() {
		return getKeyAbility(null);
	}
	
	private final String getKeyAbility(AbilityWeapon abilityWeapon) {
		return getKeyAbility(abilityWeapon, null);
	}
	
	private final String getKeyAbility(AbilityWeapon abilityWeapon, Integer grade) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String abilityKey = MainConfig.KEY_ABILITY_WEAPON;
		final String abilityColor = mainConfig.getAbilityColor();
		
		if (abilityWeapon != null) {	
			final String keyLore = TextUtil.colorful(abilityWeapon.getKeyLore());
			
			if (grade != null) {
				final int maxGrade = abilityWeapon.getMaxGrade();
				final int abilityGrade = MathUtil.limitInteger(grade, 1, maxGrade);
				final String keyAbility = abilityKey + abilityColor + keyLore + " " + RomanNumber.getRomanNumber(abilityGrade) + abilityKey + abilityColor;
				
				return keyAbility;
			} else {
				final String keyAbility = abilityKey + abilityColor + keyLore;
				
				return keyAbility;
			}
		} else {
			final String keyAbility = abilityKey + abilityColor;
			
			return keyAbility;
		}
	}
	
	private final String getKeyChance() {
		return getKeyChance(null);
	}
	
	private final String getKeyChance(Double chance) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String abilityKeyPercent = MainConfig.KEY_ABILITY_PERCENT;
		final String abilityColorPercent = mainConfig.getAbilityColorPercent(); 
		
		if (chance != null) {
			final double abilityChance = MathUtil.limitDouble(chance, 0, 100);
			final String keyChance = abilityKeyPercent + abilityColorPercent + abilityChance + abilityKeyPercent + abilityColorPercent + "%";
			
			return keyChance;
		} else {
			final String keyChance = abilityKeyPercent + abilityColorPercent;
			
			return keyChance;
		}
	}
}
