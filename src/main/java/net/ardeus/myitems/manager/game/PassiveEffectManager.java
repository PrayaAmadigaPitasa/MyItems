package net.ardeus.myitems.manager.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.RomanNumber;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.VersionNMS;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.passive.PassiveEffect;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.passive.PassiveTypeEnum;
import net.ardeus.myitems.passive.buff.BuffHealthBoost;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.ServerUtil;
import com.praya.agarthalib.utility.TextUtil;

public class PassiveEffectManager extends HandlerManager {

	protected PassiveEffectManager(MyItems plugin) {
		super(plugin);
	};
	
	public final String getTextPassiveEffect(PassiveEffectEnum effect, int grade) {	
		final MainConfig mainConfig = MainConfig.getInstance();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		String format = effect.getType().equals(PassiveTypeEnum.BUFF) ? mainConfig.getPassiveBuffFormat() : mainConfig.getPassiveDebuffFormat();
		
		map.put("buff", getKeyPassiveEffect(effect, grade));
		map.put("buffs", getKeyPassiveEffect(effect, grade));
		map.put("debuff", getKeyPassiveEffect(effect, grade));
		map.put("debuffs", getKeyPassiveEffect(effect, grade));
		format = TextUtil.placeholder(map, format, "<", ">");
		
		return format;		
	}
	
	public final int getHighestGradePassiveEffect(PassiveEffectEnum effect, LivingEntity livingEntity) {
		
		int grade = 0;
		
		for (Slot slot : Slot.values()) {
			if (checkAllowedSlot(slot)) {
				final ItemStack item = Bridge.getBridgeEquipment().getEquipment(livingEntity, slot);
					
				if (EquipmentUtil.loreCheck(item)) {
					final int passiveEffectGrade = passiveEffectGrade(item, effect);
					
					if (passiveEffectGrade > grade) {
						grade = passiveEffectGrade;
					}
				}
			}
		}
		
		return grade;
	}
	
	public final int getTotalGradePassiveEffect(PassiveEffectEnum effect, LivingEntity livingEntity) {
		
		int grade = 0;
		
		for (Slot slot : Slot.values()) {
			if (checkAllowedSlot(slot)) {
				final ItemStack item = Bridge.getBridgeEquipment().getEquipment(livingEntity, slot);
					
				if (EquipmentUtil.loreCheck(item)) {
					
					grade = grade + passiveEffectGrade(item, effect);
				}
			}
		}
		
		return grade;
	}
	
	public final PassiveEffectEnum getPassiveEffect(String lore) {
		for (PassiveEffectEnum passiveEffect : PassiveEffectEnum.values()) {
			if (lore.contains(getKeyPassiveEffect(passiveEffect, true))) {
				return passiveEffect;
			}
		}
		
		return null;
	}
	
	public final PassiveEffectEnum getPassiveEffect(ItemStack item, int line) {
		if (line > 0) {
			if (EquipmentUtil.hasLore(item)) {
				if (line <= EquipmentUtil.getLores(item).size()) {
					final String lore = EquipmentUtil.getLores(item).get(line-1);
					
					return getPassiveEffect(lore);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isPassiveEffect(String lore) {
		return getPassiveEffect(lore) != null;
	}
	
	public final boolean isPassiveEffect(ItemStack item, int line) {
		return getPassiveEffect(item, line) != null;
	}
	
	public final int passiveEffectGrade(LivingEntity livingEntity, PassiveEffectEnum effect, Slot slot) {
		return passiveEffectGrade(Bridge.getBridgeEquipment().getEquipment(livingEntity, slot), effect);
	}
	
	public final int passiveEffectGrade(ItemStack item, PassiveEffectEnum effect) {
		final int line = getLinePassiveEffect(item, effect);
		
		return line != -1 ? passiveEffectGrade(item, effect, line) : 0;
	}
	
	public final int passiveEffectGrade(ItemStack item, PassiveEffectEnum effect, int line) {
		return passiveEffectGrade(effect, EquipmentUtil.getLores(item).get(line-1));
	}
	
	public final int passiveEffectGrade(PassiveEffectEnum effect, String lore) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String[] textListValue = lore.split(MainConfig.KEY_PASSIVE_EFFECT);
		
		if (textListValue.length > 1) {
			final String regex = (effect.getType().equals(PassiveTypeEnum.BUFF) ? mainConfig.getPassiveBuffColor() : mainConfig.getPassiveDebuffColor()) + effect.getText() + " ";
			
			String textValue = textListValue[1];
			
			if (textValue.contains(regex)) {
			
				textValue = textValue.replaceAll(regex, "");
				
				return RomanNumber.romanConvert(textValue);
			}
		}
		
		return 0;
	}	
	
	public final int getLinePassiveEffect(ItemStack item, PassiveEffectEnum effect) {
		return EquipmentUtil.loreGetLineKey(item, getKeyPassiveEffect(effect, true));
	}	
	
	public Collection<PassiveEffectEnum> getPassiveEffects(ItemStack item) {
		final Collection<PassiveEffectEnum> listEffect = new ArrayList<PassiveEffectEnum>();
		
		if (EquipmentUtil.loreCheck(item)) {
			for (PassiveEffectEnum effect : PassiveEffectEnum.values()) {
				final int line = getLinePassiveEffect(item, effect);
				
				if (line != -1) {
					listEffect.add(effect);
				}
			}
		}
		
		return listEffect;
	}
	
	public final void reloadPassiveEffect(Player player, ItemStack item, boolean sum) {
		reloadPassiveEffect(player, getPassiveEffects(item), sum);
	}
	
	public final void reloadPassiveEffect(Player player, Collection<PassiveEffectEnum> effects, boolean sum) {
		for (PassiveEffectEnum effect : effects) {
			runPassiveEffect(player, effect, true, sum);
		}
	}
	
	public final void loadPassiveEffect(boolean sum) {
		for (Player player : PlayerUtil.getOnlinePlayers()) {
			runAllPassiveEffect(player, sum);
		}
	}
	
	public final void runAllPassiveEffect(Player player, boolean sum) {
		runAllPassiveEffect(player, false, sum);
	}
	
	public final void runAllPassiveEffect(Player player, boolean reset, boolean sum) {
		for (PassiveEffectEnum effect : PassiveEffectEnum.values()) {
			runPassiveEffect(player, effect, reset, sum);
		}
	}
	
	public final void runPassiveEffect(Player player, PassiveEffectEnum effect, boolean sum) {
		runPassiveEffect(player, effect, false, sum);
	}
	
	public final void runPassiveEffect(Player player, PassiveEffectEnum effect, boolean reset, boolean sum) {
		final int grade = sum ? getTotalGradePassiveEffect(effect, player) : getHighestGradePassiveEffect(effect, player);
		
		applyPassiveEffect(player, effect, grade, reset);
	}	
	
	public final void applyPassiveEffect(Player player, PassiveEffectEnum effect, int grade) {
		applyPassiveEffect(player, effect, grade, false);
	}
	
	public final void applyPassiveEffect(Player player, PassiveEffectEnum effect, int grade, boolean reset) {
		if (!ServerUtil.isCompatible(VersionNMS.V1_9_R1)) {			
			if (effect.equals(PassiveEffectEnum.LUCK)) {
				return;
			}
		}
		
		if (reset) {
			final PotionEffectType potion = effect.getPotion();
			
			if (potion != null) {
				if (potion.equals(PotionEffectType.HEALTH_BOOST)) {
					final BuffHealthBoost buffHealth = new BuffHealthBoost();
					
					buffHealth.reset(player);					
				} else {
					player.removePotionEffect(potion);
				}
			}
		}
		
		grade = MathUtil.limitInteger(grade, grade, effect.getMaxGrade());
		
		if (grade != 0) {
			final PassiveEffect passiveEffect = PassiveEffect.getPassiveEffect(effect, grade);
			
			passiveEffect.cast(player);
		}
	}
	
	public final String getKeyPassiveEffect(PassiveEffectEnum effect, boolean justCheck) {
		return getKeyPassiveEffect(effect, 1, justCheck);
	}
	
	public final String getKeyPassiveEffect(PassiveEffectEnum effect, int grade) {
		return getKeyPassiveEffect(effect, grade, false);
	}
	
	public final String getKeyPassiveEffect(PassiveEffectEnum effect, int grade, boolean justCheck) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final String key = MainConfig.KEY_PASSIVE_EFFECT;
		final String color = effect.getType().equals(PassiveTypeEnum.BUFF) ? mainConfig.getPassiveBuffColor() : mainConfig.getPassiveDebuffColor();
		final String text = effect.getText();
		final String roman = RomanNumber.getRomanNumber(grade);
		
		return justCheck ? key + color + text : key + color + text + " " + roman + key + color;
	}
	
	public final boolean checkAllowedSlot(Slot slot) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return mainConfig.isPassiveEnableHand() ? true : (!slot.equals(Slot.MAINHAND) && !slot.equals(Slot.OFFHAND));
	}
}
