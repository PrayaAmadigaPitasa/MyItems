package net.ardeus.myitems.manager.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.player.PlayerPowerCooldown;

public abstract class PlayerPowerManager extends HandlerManager {
	
	protected PlayerPowerManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract PlayerPowerCooldown getPlayerPowerCooldown(UUID playerId);
	public abstract void removePlayerPowerCooldown(UUID playerId);
	
	public final PlayerPowerCooldown getPlayerPowerCooldown(OfflinePlayer player) {
		return player != null ? getPlayerPowerCooldown(player.getUniqueId()) : null;
	}
	
	public final void removePlayerPowerCooldown(OfflinePlayer player) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			
			removePlayerPowerCooldown(playerId);
		}
	}
}
