package net.ardeus.myitems.manager.player;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.SlotType;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.element.ElementBoostStatsWeapon;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.item.ItemStatsArmor;
import net.ardeus.myitems.item.ItemStatsWeapon;
import net.ardeus.myitems.item.ItemStatsArmor.ItemStatsArmorBuilder;
import net.ardeus.myitems.item.ItemStatsWeapon.ItemStatsWeaponBuilder;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsOption;
import net.ardeus.myitems.lorestats.LoreStatsWeapon;
import net.ardeus.myitems.manager.game.ElementManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.socket.SocketGemProperties;

public abstract class PlayerItemStatsManager extends HandlerManager {

	protected PlayerItemStatsManager(MyItems plugin) {
		super(plugin);
	}
	
	public final ItemStatsWeapon getItemStatsWeapon(Player player) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final ElementManager elementManager = gameManager.getElementManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		final ItemStack itemEquipmentMainHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
		final ItemStack itemEquipmentOffHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.OFFHAND);
		
		final boolean enableItemUniversal = mainConfig.isStatsEnableItemUniversal();
		final boolean enableOffHand = mainConfig.isAbilityWeaponEnableOffHand();
		final boolean hasStatsCriticalChance = statsManager.hasLoreStats(itemEquipmentMainHand, LoreStatsEnum.CRITICAL_CHANCE) || (enableOffHand && statsManager.hasLoreStats(itemEquipmentOffHand, LoreStatsEnum.CRITICAL_CHANCE));
		final boolean hasStatsCriticalDamage = statsManager.hasLoreStats(itemEquipmentMainHand, LoreStatsEnum.CRITICAL_DAMAGE) || (enableOffHand && statsManager.hasLoreStats(itemEquipmentOffHand, LoreStatsEnum.CRITICAL_DAMAGE));
		
		final HashMap<String, Double> mapElementWeapon = elementManager.getMapElement(player, SlotType.WEAPON, false);
		final LoreStatsWeapon statsWeapon = statsManager.getLoreStatsWeapon(player, false, false);
		final SocketGemProperties socket = socketManager.getSocketProperties(player, false);
		final ElementBoostStatsWeapon elementWeapon = elementManager.getElementBoostStats(mapElementWeapon);
		
		final double scaleOffHandValue = mainConfig.getStatsScaleOffHandValue();
		
		double statsDamage = 0;
		double statsDamageDiff = 0;
		
		for (Slot slot : Slot.values()) {
			if (slot.getType().equals(SlotType.WEAPON) || enableItemUniversal) {
				final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, slot);
				
				if (item != null) {
					final double scaleValue = slot.equals(Slot.OFFHAND) ? (enableOffHand ? scaleOffHandValue : 0) : 1;
					final double statsItemDamageMin = statsManager.getLoreValue(item, LoreStatsEnum.DAMAGE, LoreStatsOption.MIN) * scaleValue;
					final double statsItemDamageMax = statsManager.getLoreValue(item, LoreStatsEnum.DAMAGE, LoreStatsOption.MAX) * scaleValue;
					final double statsItemDamageDiff = statsItemDamageMax - statsItemDamageMin;
					
					statsDamage = statsDamage + statsItemDamageMin;
					statsDamageDiff = statsDamageDiff + statsItemDamageDiff;
				}
			}
		}
		
		final double socketDamage = socket.getAdditionalDamage() + ((statsDamage * socket.getPercentDamage())/100);
		final double elementDamage = elementWeapon.getBaseAdditionalDamage() + ((statsDamage * elementWeapon.getBasePercentDamage())/100); 
		final double attributeDamage = statsDamage + socketDamage + elementDamage;
		
		final double baseDamage = 0;
		final double basePenetration = 0;
		final double basePvPDamage = 100;
		final double basePvEDamage = 100;
		final double baseCriticalChance = 5;
		final double baseCriticalDamage = 1.2;
		final double baseAttackAoERadius = 0;
		final double baseAttackAoEDamage = 0;
		final double baseHitRate = 100;
		
		final double totalDamageMin = baseDamage + attributeDamage;
		final double totalDamageMax = totalDamageMin + statsDamageDiff;
		final double totalPenetration = basePenetration + statsWeapon.getPenetration() + socket.getPenetration();
		final double totalPvPDamage = basePvPDamage + statsWeapon.getPvPDamage() + socket.getPvPDamage();
		final double totalPvEDamage = basePvEDamage + statsWeapon.getPvEDamage() + socket.getPvEDamage();
		final double totalCriticalChance = !hasStatsCriticalChance ? baseCriticalChance : statsWeapon.getCriticalChance() + socket.getCriticalChance();
		final double totalCriticalDamage = !hasStatsCriticalDamage ? baseCriticalDamage : 1 + statsWeapon.getCriticalDamage() + (socket.getCriticalDamage()/100);
		final double totalAttackAoERadius = baseAttackAoERadius + statsWeapon.getAttackAoERadius() + socket.getAttackAoERadius();
		final double totalAttackAoEDamage = baseAttackAoEDamage + statsWeapon.getAttackAoEDamage() + socket.getAttackAoEDamage();
		final double totalHitRate = baseHitRate  + statsWeapon.getHitRate() + socket.getHitRate();
		
		final ItemStatsWeaponBuilder itemStatsWeaponBuilder = new ItemStatsWeaponBuilder();
		
		itemStatsWeaponBuilder.setTotalDamageMin(totalDamageMin);
		itemStatsWeaponBuilder.setTotalDamageMax(totalDamageMax);
		itemStatsWeaponBuilder.setTotalPenetration(totalPenetration);
		itemStatsWeaponBuilder.setTotalPvPDamage(totalPvPDamage);
		itemStatsWeaponBuilder.setTotalPvEDamage(totalPvEDamage);
		itemStatsWeaponBuilder.setTotalCriticalChance(totalCriticalChance);
		itemStatsWeaponBuilder.setTotalCriticalDamage(totalCriticalDamage);
		itemStatsWeaponBuilder.setTotalAttackAoERadius(totalAttackAoERadius);
		itemStatsWeaponBuilder.setTotalAttackAoEDamage(totalAttackAoEDamage);
		itemStatsWeaponBuilder.setTotalHitRate(totalHitRate);
		itemStatsWeaponBuilder.setTotalSocketDamage(socketDamage);
		itemStatsWeaponBuilder.setTotalElementDamage(elementDamage);
		
		return itemStatsWeaponBuilder.build();
	}
	
	public final ItemStatsArmor getItemStatsArmor(Player player) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final LoreStatsArmor statsArmor = statsManager.getLoreStatsArmor(player, false);
		final SocketGemProperties socket = socketManager.getSocketProperties(player, false);
		
		final double statsDefense = statsArmor.getDefense();
		final double socketDefense = socket.getAdditionalDefense() + ((statsDefense * socket.getPercentDefense())/100); 
		final double attributeDefense = statsDefense + socketDefense;
		
		final double baseDefense = 0;
		final double basePvPDefense = 100;
		final double basePvEDefense = 100;
		final double baseHealth = 0;
		final double baseHealthRegen = 0;
		final double baseStaminaMax = 0;
		final double baseStaminaRegen = 0;
		final double baseBlockAmount = 25;
		final double baseBlockRate = 0;
		final double baseDodgeRate = 0;
		
		final double totalDefense = baseDefense + attributeDefense;
		final double totalPvPDefense = basePvPDefense + statsArmor.getPvPDefense() + socket.getPvPDefense();
		final double totalPvEDefense = basePvEDefense + statsArmor.getPvEDefense() + socket.getPvEDefense();
		final double totalHealth = baseHealth + statsArmor.getHealth() + socket.getHealth();
		final double totalHealthRegen = baseHealthRegen + statsArmor.getHealthRegen() + socket.getHealthRegen();
		final double totalStaminaMax = baseStaminaMax + statsArmor.getStaminaMax() + socket.getStaminaMax();
		final double totalStaminaRegen = baseStaminaRegen + statsArmor.getStaminaRegen() + socket.getStaminaRegen();
		final double totalBlockAmount = baseBlockAmount + statsArmor.getBlockAmount() + socket.getBlockAmount();
		final double totalBlockRate = baseBlockRate + statsArmor.getBlockRate() + socket.getBlockRate();
		final double totalDodgeRate = baseDodgeRate + statsArmor.getDodgeRate() + socket.getDodgeRate();
		
		final ItemStatsArmorBuilder itemStatsArmorBuilder = new ItemStatsArmorBuilder();
		
		itemStatsArmorBuilder.setTotalDefense(totalDefense);
		itemStatsArmorBuilder.setTotalPvPDefense(totalPvPDefense);
		itemStatsArmorBuilder.setTotalPvEDefense(totalPvEDefense);
		itemStatsArmorBuilder.setTotalHealth(totalHealth);
		itemStatsArmorBuilder.setTotalHealthRegen(totalHealthRegen);
		itemStatsArmorBuilder.setTotalStaminaMax(totalStaminaMax);
		itemStatsArmorBuilder.setTotalStaminaRegen(totalStaminaRegen);
		itemStatsArmorBuilder.setTotalBlockAmount(totalBlockAmount);
		itemStatsArmorBuilder.setTotalBlockRate(totalBlockRate);
		itemStatsArmorBuilder.setTotalDodgeRate(totalDodgeRate);
		itemStatsArmorBuilder.setSocketDefense(socketDefense);
		
		return itemStatsArmorBuilder.build();
	}
}
