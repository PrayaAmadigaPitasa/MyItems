package net.ardeus.myitems.manager.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.player.PlayerPassiveEffect;

public abstract class PlayerPassiveEffectManager extends HandlerManager {
	
	protected PlayerPassiveEffectManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract PlayerPassiveEffect getPlayerPassiveEffect(UUID playerId);
	public abstract void removePlayerPassiveEffect(UUID playerId);
	
	public final PlayerPassiveEffect getPlayerPassiveEffect(OfflinePlayer player) {
		return player != null ? getPlayerPassiveEffect(player.getUniqueId()) : null;
	}
	
	public final void removePlayerPassiveEffect(OfflinePlayer player) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			
			removePlayerPassiveEffect(playerId);
		}
	}
}
