package net.ardeus.myitems.manager.player;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;

public abstract class PlayerManager extends HandlerManager {
	
	public PlayerManager(MyItems plugin) {
		super(plugin);
	};
	
	public abstract PlayerItemStatsManager getPlayerItemStatsManager();
	public abstract PlayerPassiveEffectManager getPlayerPassiveEffectManager();
	public abstract PlayerPowerManager getPlayerPowerManager();
}
