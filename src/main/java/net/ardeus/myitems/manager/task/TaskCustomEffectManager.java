package net.ardeus.myitems.manager.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.task.TaskCustomEffect;

public class TaskCustomEffectManager extends HandlerManager {

	private BukkitTask taskCustomEffect;
	
	protected TaskCustomEffectManager(MyItems plugin) {
		super(plugin);
		
		reloadTaskCustomEffect();
	};
	
	public final void reloadTaskCustomEffect() {
		if (this.taskCustomEffect != null) {
			this.taskCustomEffect.cancel();
		}
		
		this.taskCustomEffect = createTaskCustomEffect();
	}
	
	private final BukkitTask createTaskCustomEffect() {
		final BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		final Runnable runnable = new TaskCustomEffect(plugin);
		final int delay = 2;
		final int period = 1;
		final BukkitTask task = scheduler.runTaskTimer(plugin, runnable, delay, period);
		
		return task;
	}
}
