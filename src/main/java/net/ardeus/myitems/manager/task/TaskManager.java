package net.ardeus.myitems.manager.task;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;

public class TaskManager extends HandlerManager {

	private final TaskCustomEffectManager taskCustomEffectManager;
	private final TaskPassiveEffectManager taskPassiveEffectManager;
	private final TaskPowerCooldownManager taskPowerCooldownManager;
	
	public TaskManager(MyItems plugin) {
		super(plugin);
		
		this.taskCustomEffectManager = new TaskCustomEffectManager(plugin);
		this.taskPassiveEffectManager = new TaskPassiveEffectManager(plugin);
		this.taskPowerCooldownManager = new TaskPowerCooldownManager(plugin);
	};
	
	public final TaskCustomEffectManager getTaskCustomEffectManager() {
		return this.taskCustomEffectManager;
	}
	
	public final TaskPassiveEffectManager getTaskPassiveEffectManager() {
		return this.taskPassiveEffectManager;
	}
	
	public final TaskPowerCooldownManager getTaskPowerCooldownManager() {
		return this.taskPowerCooldownManager;
	}
}
