package net.ardeus.myitems.manager.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.task.TaskPowerCooldown;

public class TaskPowerCooldownManager extends HandlerManager {

	private BukkitTask taskPowerCooldown;
	
	protected TaskPowerCooldownManager(MyItems plugin) {
		super(plugin);
		
		reloadTaskPowerCooldown();
	};
	
	public final void reloadTaskPowerCooldown() {
		if (taskPowerCooldown != null) {
			this.taskPowerCooldown.cancel();
		}
		
		this.taskPowerCooldown = createTaskPowerCooldown();
	}
	
	private final BukkitTask createTaskPowerCooldown() {
		final BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		final Runnable runnable = new TaskPowerCooldown(plugin);
		final int delay = 0;
		final int period = 1;
		final BukkitTask task = scheduler.runTaskTimer(plugin, runnable, delay, period);
		
		return task;
	}
}
