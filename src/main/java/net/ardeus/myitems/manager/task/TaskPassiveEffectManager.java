package net.ardeus.myitems.manager.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scheduler.BukkitTask;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerManager;
import net.ardeus.myitems.task.TaskPassiveEffect;

public class TaskPassiveEffectManager extends HandlerManager {

	private BukkitTask taskLoadPassiveEffect;
	
	protected TaskPassiveEffectManager(MyItems plugin) {
		super(plugin);
		
		reloadTaskLoadPassiveEffect();
	}
	
	public final void reloadTaskLoadPassiveEffect() {
		if (this.taskLoadPassiveEffect != null) {
			this.taskLoadPassiveEffect.cancel();
		}
		
		this.taskLoadPassiveEffect = createTaskLoadPassiveEffect();
	}
	
	private final BukkitTask createTaskLoadPassiveEffect() {
		final MainConfig mainConfig = MainConfig.getInstance();
		final BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		final Runnable runnable = new TaskPassiveEffect(plugin);
		final int delay = 2;
		final int period = mainConfig.getPassivePeriodEffect();
		final BukkitTask task = scheduler.runTaskTimer(plugin, runnable, delay, period);
		
		return task;
	}
}
