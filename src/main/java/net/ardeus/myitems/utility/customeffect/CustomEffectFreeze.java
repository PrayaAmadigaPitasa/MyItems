package net.ardeus.myitems.utility.customeffect;

import java.util.Collection;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.passive.PassiveEffectTypeEnum;
import net.ardeus.myitems.utility.main.CustomEffectUtil;

import com.praya.agarthalib.utility.EntityUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public class CustomEffectFreeze {
	
	protected static final PassiveEffectTypeEnum customEffect = PassiveEffectTypeEnum.FREEZE;

	public static final void cast(LivingEntity livingEntity) {
		if (EntityUtil.isPlayer(livingEntity)) {
			effect(PlayerUtil.parse(livingEntity));
		}
		
		display(livingEntity);
	}
	
	public static final void cast(Player player) {
		effect(player);
		display(player);
	}
	
	public static final void effect(Player player) {
		if (CustomEffectUtil.isRunCustomEffect(player, customEffect)) {
			if (player.getWalkSpeed() >= 0.05F) {
				
				CustomEffectUtil.setSpeedBase(player, player.getWalkSpeed());
				player.setWalkSpeed(0);
			}
		} else {
			if (CustomEffectUtil.hasSpeedBase(player)) {
				final float baseSpeed = CustomEffectUtil.getSpeedBase(player);
				
				CustomEffectUtil.removeSpeedBase(player);
				player.setWalkSpeed(baseSpeed);
			}
		}
	}
	
	public static final void display(LivingEntity livingEntity) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (CustomEffectUtil.isRunCustomEffect(livingEntity, customEffect)) {
			final Location loc = livingEntity.getLocation();
			final Collection<Player> players = PlayerUtil.getNearbyPlayers(loc, mainConfig.getEffectRange());
			
			Bridge.getBridgeParticle().playParticle(players, ParticleEnum.CLOUD, loc, 10, 0.25, 0.25, 0.25, 0.1F);
			Bridge.getBridgeSound().playSound(players, loc, SoundEnum.BLOCK_GLASS_BREAK, 1, 1);
		}
	}
}
