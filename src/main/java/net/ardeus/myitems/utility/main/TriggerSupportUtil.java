package net.ardeus.myitems.utility.main;

import org.bukkit.entity.Player;

import api.praya.agarthalib.builder.support.SupportCombatStamina;
import api.praya.agarthalib.builder.support.SupportLifeEssence;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import net.ardeus.myitems.config.plugin.MainConfig;

import com.praya.agarthalib.utility.PlayerUtil;

public class TriggerSupportUtil {
	
	public static final void updateSupport(Player player) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final SupportManagerAPI supportManagerAPI = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final boolean enableMaxHealth = mainConfig.isStatsEnableMaxHealth();
		
		if (enableMaxHealth) {
			PlayerUtil.setMaxHealth(player);
		}
		
		if (supportManagerAPI.isSupportCombatStamina()) {
			final SupportCombatStamina supportCombatStamina = supportManagerAPI.getSupportCombatStamina();
			
			supportCombatStamina.updateMaxStamina(player);
			supportCombatStamina.updateStaminaRegen(player);
		}
		
		if (supportManagerAPI.isSupportLifeEssence()) {
			final SupportLifeEssence supportLifeEssence = supportManagerAPI.getSupportLifeEssence();
			
			supportLifeEssence.updateHealthRegen(player);
		}
	}
}
