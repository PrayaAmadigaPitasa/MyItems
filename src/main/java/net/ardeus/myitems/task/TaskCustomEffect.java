package net.ardeus.myitems.task;

import org.bukkit.entity.Player;

import com.praya.agarthalib.utility.PlayerUtil;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerTask;
import net.ardeus.myitems.utility.customeffect.CustomEffectFreeze;

public class TaskCustomEffect extends HandlerTask implements Runnable {

	public TaskCustomEffect(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public void run() {
		for (Player loopPlayer : PlayerUtil.getOnlinePlayers())	 {
			CustomEffectFreeze.cast(loopPlayer);
		}
	}
}
