package net.ardeus.myitems.task;

import java.util.HashMap;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ProjectileEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerTask;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.PowerShootManager;
import net.ardeus.myitems.manager.player.PlayerManager;
import net.ardeus.myitems.manager.player.PlayerPowerManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.player.PlayerPowerCooldown;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.TextUtil;

public class TaskPowerCooldown extends HandlerTask implements Runnable {

	private final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
	
	public TaskPowerCooldown(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public void run() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PowerShootManager powerShootManager = gameManager.getPowerShootManager();
		final PlayerPowerManager playerPowerManager = playerManager.getPlayerPowerManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		for (Player player : Bukkit.getOnlinePlayers())	 {
			final PlayerPowerCooldown playerPowerCooldown = playerPowerManager.getPlayerPowerCooldown(player);
			final Set<String> cooldownCommandKeySet = playerPowerCooldown.getCooldownCommandKeySet();
			final Set<ProjectileEnum> cooldownProjectileKeySet = playerPowerCooldown.getCooldownProjectileKeySet();
			final Set<String> cooldownSpecialKeySet = playerPowerCooldown.getCooldownSpecialKeySet();
			
			for (String powerCommandId : cooldownCommandKeySet) {
				if (!playerPowerCooldown.isPowerCommandCooldown(powerCommandId)) {
					final Location location = player.getLocation();
					
					String message = lang.getText(player, "Power_Command_Refresh");
					
					mapPlaceholder.clear();
					mapPlaceholder.put("power", powerCommandId);
					message = TextUtil.placeholder(mapPlaceholder, message);
					
					playerPowerCooldown.removePowerCommandCooldown(powerCommandId);
					Bridge.getBridgeSound().playSound(player, location, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON, 1, 1);
					PlayerUtil.sendMessage(player, message);
				}
			}

			for (ProjectileEnum projectileEnum : cooldownProjectileKeySet) {
				if (!playerPowerCooldown.isPowerShootCooldown(projectileEnum)) {
					final String powerShootKeyLore = powerShootManager.getPowerShootKeyLore(projectileEnum);
					final Location location = player.getLocation();
					
					String message = lang.getText(player, "Power_Shoot_Refresh");
					
					mapPlaceholder.clear();
					mapPlaceholder.put("power", powerShootKeyLore);
					message = TextUtil.placeholder(mapPlaceholder, message);
					
					playerPowerCooldown.removePowerShootCooldown(projectileEnum);
					Bridge.getBridgeSound().playSound(player, location, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON, 1, 1);
					PlayerUtil.sendMessage(player, message);
				}
			}
			
			for (String powerSpecialId : cooldownSpecialKeySet) {
				if (!playerPowerCooldown.isPowerSpecialCooldown(powerSpecialId)) {
					final Location location = player.getLocation();
					
					String message = lang.getText(player, "Power_Special_Refresh");
					
					mapPlaceholder.clear();
					mapPlaceholder.put("power", powerSpecialId);
					message = TextUtil.placeholder(mapPlaceholder, message);
					
					playerPowerCooldown.removePowerSpecialCooldown(powerSpecialId);
					Bridge.getBridgeSound().playSound(player, location, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON, 1, 1);
					PlayerUtil.sendMessage(player, message);
				}
			}
		}
	}
}