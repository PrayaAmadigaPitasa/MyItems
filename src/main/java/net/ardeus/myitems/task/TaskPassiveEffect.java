package net.ardeus.myitems.task;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerTask;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;

public class TaskPassiveEffect extends HandlerTask implements Runnable {

	public TaskPassiveEffect(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public void run() {
		final GameManager gameManager = plugin.getGameManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final boolean enableGradeCalculation = mainConfig.isPassiveEnableGradeCalculation();
		
		passiveEffectManager.loadPassiveEffect(enableGradeCalculation);
	}
}