package net.ardeus.myitems.passive.buff;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.player.PlayerManager;
import net.ardeus.myitems.manager.player.PlayerPassiveEffectManager;
import net.ardeus.myitems.passive.PassiveEffect;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.player.PlayerPassiveEffect;
import net.ardeus.myitems.player.PlayerPassiveEffectCooldown;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PotionUtil;

public class BuffSaturation extends PassiveEffect {
	
	private static final PassiveEffectEnum buff = PassiveEffectEnum.SATURATION;
	
	public BuffSaturation() {
		super(buff, 1);
	}
	
	public BuffSaturation(int grade) {
		super(buff, grade);
	}

	@Override
	public final void cast(Player player) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerPassiveEffectManager playerPassiveEffectManager = playerManager.getPlayerPassiveEffectManager();
		final PlayerPassiveEffect playerPassiveEffect = playerPassiveEffectManager.getPlayerPassiveEffect(player);
		final PlayerPassiveEffectCooldown playerPassiveEffectCooldown = playerPassiveEffect.getCooldown();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!playerPassiveEffectCooldown.isPassiveEffectCooldown(buff)) {
			final PotionEffectType potionType = buff.getPotion();
			final long cooldown = getCooldown();
			final int duration = getDuration();
			final boolean isEnableParticle = mainConfig.isMiscEnableParticlePotion();
			final PotionEffect potion = PotionUtil.createPotion(potionType, duration, grade, true, isEnableParticle);
			
			player.addPotionEffect(potion);
			playerPassiveEffectCooldown.setPassiveEffectCooldown(buff, cooldown);
		}
	}
	
	private final int getDuration() {
		final int duration = (grade*5)/buff.getMaxGrade();
		
		return MathUtil.limitInteger(duration, 1, duration);
	}
	
	private final long getCooldown() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return MathUtil.convertTickToMilis(mainConfig.getPassivePeriodEffect())*buff.getMaxGrade();
	}
}
