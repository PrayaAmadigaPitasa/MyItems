package net.ardeus.myitems.passive.buff;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import core.praya.agarthalib.enums.main.VersionNMS;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.passive.PassiveEffect;
import net.ardeus.myitems.passive.PassiveEffectEnum;

import com.praya.agarthalib.utility.PotionUtil;
import com.praya.agarthalib.utility.ServerUtil;

public class BuffLuck extends PassiveEffect {

	private static final PassiveEffectEnum buff = PassiveEffectEnum.LUCK;
	
	public BuffLuck() {
		super(buff, 1);
	}
	
	public BuffLuck(int grade) {
		super(buff, grade);
	}

	@Override
	public final void cast(Player player) {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (ServerUtil.isCompatible(VersionNMS.V1_9_R1)) {
			final PotionEffectType potionType = getPotion();
			final boolean isEnableParticle = mainConfig.isMiscEnableParticlePotion();
			final PotionEffect potion = PotionUtil.createPotion(potionType, 96000, grade, true, isEnableParticle);
			
			player.addPotionEffect(potion);
		}
	}
}
