package net.ardeus.myitems.passive.buff;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.manager.player.PlayerManager;
import net.ardeus.myitems.manager.player.PlayerPassiveEffectManager;
import net.ardeus.myitems.passive.PassiveEffect;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.player.PlayerPassiveEffect;
import net.ardeus.myitems.player.PlayerPassiveEffectCooldown;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PotionUtil;

public class BuffInvisibility extends PassiveEffect {

	private static final PassiveEffectEnum buff = PassiveEffectEnum.INVISIBILITY;
	
	public BuffInvisibility() {
		super(buff, 1);
	}
	
	public BuffInvisibility(int grade) {
		super(buff, grade);
	}

	@Override
	public final void cast(Player player) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerPassiveEffectManager playerPassiveEffectManager = playerManager.getPlayerPassiveEffectManager();
		final PlayerPassiveEffect playerPassiveEffect = playerPassiveEffectManager.getPlayerPassiveEffect(player);
		final PlayerPassiveEffectCooldown playerPassiveEffectCooldown = playerPassiveEffect.getCooldown();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!playerPassiveEffectCooldown.isPassiveEffectCooldown(buff)) {
			final PotionEffectType potionType = getPotion();
			final int duration = getDuration();
			final long cooldown = getCooldown();
			final boolean isEnableParticle = mainConfig.isMiscEnableParticlePotion();
			final PotionEffect potion = PotionUtil.createPotion(potionType, duration, grade, true, isEnableParticle);
			
			player.addPotionEffect(potion);
			playerPassiveEffectCooldown.setPassiveEffectCooldown(buff, cooldown);
		}
	}
	
	private final int getDuration() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return (int) (((mainConfig.getPassivePeriodEffect()*grade)/2.5)+20);
	}
	
	private final long getCooldown() {
		final MainConfig mainConfig = MainConfig.getInstance();
		
		return (long) ((MathUtil.convertTickToMilis(mainConfig.getPassivePeriodEffect())*buff.getMaxGrade())/2.5);
	}
}
