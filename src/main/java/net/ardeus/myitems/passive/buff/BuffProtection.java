package net.ardeus.myitems.passive.buff;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.passive.PassiveEffect;
import net.ardeus.myitems.passive.PassiveEffectEnum;

import com.praya.agarthalib.utility.PotionUtil;

public class BuffProtection extends PassiveEffect {

	private static final PassiveEffectEnum buff = PassiveEffectEnum.PROTECTION;
	
	public BuffProtection() {
		super(buff, 1);
	}
	
	public BuffProtection(int grade) {
		super(buff, grade);
	}

	@Override
	public final void cast(Player player) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final PotionEffectType potionType = getPotion();
		final boolean isEnableParticle = mainConfig.isMiscEnableParticlePotion();
		final PotionEffect potion = PotionUtil.createPotion(potionType, 96000, grade, true, isEnableParticle);
		
		player.addPotionEffect(potion);
	}
}
