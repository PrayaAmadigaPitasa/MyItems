package net.ardeus.myitems.passive.buff;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.passive.PassiveEffect;
import net.ardeus.myitems.passive.PassiveEffectEnum;

import com.praya.agarthalib.utility.PotionUtil;

public class BuffHealthBoost extends PassiveEffect {
	
	private static final PassiveEffectEnum buff = PassiveEffectEnum.HEALTH_BOOST;
	
	public BuffHealthBoost() {
		super(buff, 1);
	}
	
	public BuffHealthBoost(int grade) {
		super(buff, grade);
	}

	@Override
	public final void cast(Player player) {
		final MainConfig mainConfig = MainConfig.getInstance();
		final PotionEffectType potionType = getPotion();
		final boolean isEnableParticle = mainConfig.isMiscEnableParticlePotion();
		final PotionEffect potion = PotionUtil.createPotion(potionType, 96000, grade, true, isEnableParticle);
		
		player.addPotionEffect(potion);
	}
	
	public final void reset(Player player) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PotionEffectType potion = getPotion();
		final double health = player.getHealth();
		
		player.removePotionEffect(potion);
		
		new BukkitRunnable() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				if (player.isOnline()) {
					final double maxHealth = player.getMaxHealth();
					
					player.setHealth(health > maxHealth ? maxHealth : health);
				}
			}
		}.runTaskLater(plugin, 1);
	}
}
