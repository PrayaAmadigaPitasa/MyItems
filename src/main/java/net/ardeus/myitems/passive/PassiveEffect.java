package net.ardeus.myitems.passive;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import net.ardeus.myitems.passive.buff.BuffAbsorb;
import net.ardeus.myitems.passive.buff.BuffFireResistance;
import net.ardeus.myitems.passive.buff.BuffHaste;
import net.ardeus.myitems.passive.buff.BuffHealthBoost;
import net.ardeus.myitems.passive.buff.BuffInvisibility;
import net.ardeus.myitems.passive.buff.BuffJump;
import net.ardeus.myitems.passive.buff.BuffLuck;
import net.ardeus.myitems.passive.buff.BuffProtection;
import net.ardeus.myitems.passive.buff.BuffRegeneration;
import net.ardeus.myitems.passive.buff.BuffSaturation;
import net.ardeus.myitems.passive.buff.BuffSpeed;
import net.ardeus.myitems.passive.buff.BuffStrength;
import net.ardeus.myitems.passive.buff.BuffVision;
import net.ardeus.myitems.passive.buff.BuffWaterBreathing;
import net.ardeus.myitems.passive.debuff.DebuffBlind;
import net.ardeus.myitems.passive.debuff.DebuffConfuse;
import net.ardeus.myitems.passive.debuff.DebuffFatigue;
import net.ardeus.myitems.passive.debuff.DebuffGlow;
import net.ardeus.myitems.passive.debuff.DebuffSlow;
import net.ardeus.myitems.passive.debuff.DebuffStarve;
import net.ardeus.myitems.passive.debuff.DebuffToxic;
import net.ardeus.myitems.passive.debuff.DebuffUnluck;
import net.ardeus.myitems.passive.debuff.DebuffWeak;
import net.ardeus.myitems.passive.debuff.DebuffWither;

public abstract class PassiveEffect {

	protected final PassiveEffectEnum buffEnum;
	protected final int grade;
	
	public PassiveEffect(PassiveEffectEnum buffEnum, int grade) {
		this.buffEnum = buffEnum;
		this.grade = grade;
	}
	
	public abstract void cast(Player player);
	
	public final PassiveEffectEnum getPassiveEffectEnum() {
		return this.buffEnum;
	}
	
	public final int getGrade() {
		return this.grade;
	}
	
	public final PotionEffectType getPotion() {
		return buffEnum.getPotion();
	}
	
	public static final PassiveEffect getPassiveEffect(PassiveEffectEnum buffEnum, int grade) {
		switch (buffEnum) {
		case ABSORB : return new BuffAbsorb(grade);
		case FIRE_RESISTANCE : return new BuffFireResistance(grade);
		case HASTE : return new BuffHaste(grade);
		case HEALTH_BOOST : return new BuffHealthBoost(grade);
		case INVISIBILITY : return new BuffInvisibility(grade);
		case JUMP : return new BuffJump(grade);
		case LUCK : return new BuffLuck(grade);
		case PROTECTION : return new BuffProtection(grade);
		case REGENERATION : return new BuffRegeneration(grade);
		case SATURATION : return new BuffSaturation(grade);
		case SPEED : return new BuffSpeed(grade);
		case STRENGTH : return new BuffStrength(grade);
		case VISION : return new BuffVision(grade);
		case WATER_BREATHING : return new BuffWaterBreathing(grade);
		case BLIND : return new DebuffBlind(grade);
		case CONFUSE : return new DebuffConfuse(grade);
		case FATIGUE : return new DebuffFatigue(grade);
		case SLOW : return new DebuffSlow(grade);
		case STARVE : return new DebuffStarve(grade);
		case TOXIC : return new DebuffToxic(grade);
		case GLOW : return new DebuffGlow(grade);
		case UNLUCK : return new DebuffUnluck(grade);
		case WEAK : return new DebuffWeak(grade);
		case WITHER : return new DebuffWither(grade);
		default : return null;
		}
	}
}
