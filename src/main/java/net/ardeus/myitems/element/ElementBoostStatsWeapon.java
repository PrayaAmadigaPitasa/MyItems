package net.ardeus.myitems.element;

public class ElementBoostStatsWeapon {

	private final double baseAdditionalDamage;
	private final double basePercentDamage;
	
	private ElementBoostStatsWeapon(double baseAdditionalDamage, double basePercentDamage) {
		this.baseAdditionalDamage = baseAdditionalDamage;
		this.basePercentDamage = basePercentDamage;
	}
	
	public final double getBaseAdditionalDamage() {
		return this.baseAdditionalDamage;
	}
	
	public final double getBasePercentDamage() {
		return this.basePercentDamage;
	}
	
	public static class ElementBoostStatsWeaponBuilder {
		
		private double baseAdditionalDamage;
		private double basePercentDamage;
		
		public ElementBoostStatsWeaponBuilder() {
			this.baseAdditionalDamage = 0;
			this.basePercentDamage = 0;
		}
		
		public final double getBaseAdditionalDamage() {
			return this.baseAdditionalDamage;
		}
		
		public final double getBasePercentDamage() {
			return this.basePercentDamage;
		}
		
		public final ElementBoostStatsWeaponBuilder setBaseAdditionalDamage(double baseAdditionalDamage) {
			this.baseAdditionalDamage = baseAdditionalDamage;
			return this;
		}
		
		public final ElementBoostStatsWeaponBuilder setBasePercentDamage(double basePercentDamage) {
			this.basePercentDamage = basePercentDamage;
			return this;
		}
		
		public final ElementBoostStatsWeapon build() {
			return new ElementBoostStatsWeapon(baseAdditionalDamage, basePercentDamage);
		}
	}
}
