package net.ardeus.myitems.element;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.potion.PotionEffectType;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.element.ElementBoostWeapon.ElementBoostWeaponBuilder;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.potion.PotionProperties;

import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PotionUtil;
import com.praya.agarthalib.utility.TextUtil;

public final class ElementConfig extends HandlerConfig {
	
	protected final Map<String, Element> mapElement = new HashMap<String, Element>();
	
	protected ElementConfig(MyItems plugin) {
		super(plugin);
		
		setup();
	};
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapElement.clear();
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String path = dataManager.getPath("Path_File_Element");
		final File file = FileUtil.getFile(plugin, path);		
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, path);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection section = config.getConfigurationSection(key);
			final Map<String, Double> mapResistance = new HashMap<String, Double>();
			final HashMap<PotionEffectType, PotionProperties> potionAttacker = new HashMap<PotionEffectType, PotionProperties>();
			final HashMap<PotionEffectType, PotionProperties> potionVictims = new HashMap<PotionEffectType, PotionProperties>();
			
			String keyLore = null;
			double scaleBaseAdditionalDamage = 0;
			double scaleBasePercentDamage = 0;			
			
			for (String keySection : section.getKeys(false)) {
				if (keySection.equalsIgnoreCase("Keylore")) {
					keyLore = TextUtil.colorful(section.getString(keySection));
				} else if (keySection.equalsIgnoreCase("Scale_Bonus_Additional_Damage") || keySection.equalsIgnoreCase("Scale_Base_Additional_Damage")) {
					scaleBaseAdditionalDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Scale_Bonus_Percent_Damage") || keySection.equalsIgnoreCase("Scale_Base_Percent_Damage")) {
					scaleBasePercentDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Resistance")) {
					final ConfigurationSection resistanceSection = section.getConfigurationSection(keySection);
					
					for (String keyResistance : resistanceSection.getKeys(false)) {
						final double resistance = resistanceSection.getDouble(keyResistance);
						
						mapResistance.put(keyResistance, resistance);
					}
				} else if (keySection.equalsIgnoreCase("Potion_To_Attacker") || keySection.equalsIgnoreCase("Potion_To_Attackers") || keySection.equalsIgnoreCase("Potion_To_Victim") || keySection.equalsIgnoreCase("Potion_To_Victims")) {
					final ConfigurationSection potionSection = section.getConfigurationSection(keySection);
					
					for (String keyPotion : potionSection.getKeys(false)) {
						final PotionEffectType potion = PotionUtil.getPotionEffectType(keyPotion);
						
						if (potion != null) {
							final ConfigurationSection attributePotionSection = potionSection.getConfigurationSection(keyPotion);
							
							int potionGrade = 1;
							double potionScaleChance = 0;
							double potionScaleDuration = 1;
							
							for (String keyAttribute : attributePotionSection.getKeys(false)) {
								if (keyAttribute.equalsIgnoreCase("Grade")) {
									potionGrade = attributePotionSection.getInt(keyAttribute);
								} else if (keyAttribute.equalsIgnoreCase("Scale_Chance")) {
									potionScaleChance = attributePotionSection.getDouble(keyAttribute);
								} else if (keyAttribute.equalsIgnoreCase("Scale_Duration")) {
									potionScaleDuration = attributePotionSection.getDouble(keyAttribute);
								}
							}
							
							potionGrade = MathUtil.limitInteger(potionGrade, 1, 10);
							potionScaleChance = MathUtil.limitDouble(potionScaleChance, 0.01, 100);
							potionScaleDuration = MathUtil.limitDouble(potionScaleDuration, 0.1, 100);
							
							final PotionProperties potionAttributes = new PotionProperties(potionGrade, potionScaleChance, potionScaleDuration);
							
							if (keySection.equalsIgnoreCase("Potion_To_Attacker") || keySection.equalsIgnoreCase("Potion_To_Attackers")) {
								potionAttacker.put(potion, potionAttributes);
							} else if (keySection.equalsIgnoreCase("Potion_To_Victim") || keySection.equalsIgnoreCase("Potion_To_Victims")) {
								potionVictims.put(potion, potionAttributes);
							}
						}
					}
				}
			}
			
			if (keyLore != null) {
				final ElementBoostWeaponBuilder elementBoostWeaponBuilder = new ElementBoostWeaponBuilder();
				
				elementBoostWeaponBuilder.setScaleBaseAdditionalDamage(scaleBaseAdditionalDamage);
				elementBoostWeaponBuilder.setScaleBasePercentDamage(scaleBasePercentDamage);
				
				final ElementBoostWeapon elementBoostWeapon = elementBoostWeaponBuilder.build();
				final ElementPotion elementPotion = new ElementPotion(potionAttacker, potionVictims);
				final Element element = new Element(key, keyLore, elementBoostWeapon, elementPotion, mapResistance);
				
				mapElement.put(key, element);
			}
		}
	}
	
	private final void moveOldFile() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathSource = "element.yml";
		final String pathTarget = dataManager.getPath("Path_File_Element");
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}