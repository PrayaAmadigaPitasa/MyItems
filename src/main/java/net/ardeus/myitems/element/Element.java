package net.ardeus.myitems.element;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.ardeus.myitems.element.ElementBoostWeapon.ElementBoostWeaponBuilder;

public class Element {

	private final String id;
	private final String keyLore;
	private final ElementBoostWeapon boostWeapon;
	private final ElementPotion potion;
	private final Map<String, Double> mapResistance;
	
	public Element(String id, String keyLore, ElementBoostWeapon boostWeapon, ElementPotion potion, Map<String, Double> mapResistance) {
		if (id == null || keyLore != null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.keyLore = keyLore;
			this.boostWeapon = boostWeapon != null ? boostWeapon : new ElementBoostWeaponBuilder().build();
			this.potion = potion != null ? potion : new ElementPotion();
			this.mapResistance = mapResistance != null ? mapResistance : new HashMap<String, Double>();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getKeyLore() {
		return this.keyLore;
	}
	
	public final ElementBoostWeapon getBoostWeapon() {
		return this.boostWeapon;
	}
	
	public final ElementPotion getPotion() {
		return this.potion;
	}
	
	public final Collection<String> getElementResistanceIds() {
		return new ArrayList<String>(this.mapResistance.keySet());
	}
	
	public final Double getScaleResistance(String elementId) {
		if (elementId != null) {
			for (String key : this.mapResistance.keySet()) {
				if (key.equalsIgnoreCase(elementId)) {
					return this.mapResistance.get(key);
				}
			}
		}
		
		return null;
	}
}
