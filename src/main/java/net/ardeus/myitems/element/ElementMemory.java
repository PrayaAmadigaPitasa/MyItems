package net.ardeus.myitems.element;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.element.Element;
import net.ardeus.myitems.element.ElementConfig;
import net.ardeus.myitems.manager.game.ElementManager;

public final class ElementMemory extends ElementManager {

	private final ElementConfig elementConfig;
	
	private ElementMemory(MyItems plugin) {
		super(plugin);
		
		this.elementConfig = new ElementConfig(plugin);
	};
	
	private static class ElementMemorySingleton {
		private static final ElementMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new ElementMemory(plugin);
		}
	}
	
	public static final ElementMemory getInstance() {
		return ElementMemorySingleton.INSTANCE;
	}
	
	protected final ElementConfig getElementConfig() {
		return this.elementConfig;
	}
	
	@Override
	public final Collection<String> getElementIds() {
		final Map<String, Element> mapElement = getElementConfig().mapElement;
		
		return new ArrayList<String>(mapElement.keySet());
	}
	
	@Override
	public final Collection<Element> getAllElement() {
		final Map<String, Element> mapElement = getElementConfig().mapElement;
		
		return new ArrayList<Element>(mapElement.values());
	}
	
	@Override
	public final Element getElement(String elementId) {
		if (elementId != null) {
			final Map<String, Element> mapElement = getElementConfig().mapElement;
			
			for (String key : mapElement.keySet()) {
				if (key.equalsIgnoreCase(elementId)) {
					return mapElement.get(key);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final Element getElementByKeyLore(String keyLore) {
		if (keyLore != null) {
			final Map<String, Element> mapElement = getElementConfig().mapElement;
			
			for (Element key : mapElement.values()) {
				if (key.getKeyLore().equalsIgnoreCase(keyLore)) {
					return key;
				}
			}
		}
		
		return null;
	}
}
