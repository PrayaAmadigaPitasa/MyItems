package net.ardeus.myitems.element;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.potion.PotionEffectType;

import net.ardeus.myitems.potion.PotionProperties;

public class ElementPotion {

	private final Map<PotionEffectType, PotionProperties> potionAttacker;
	private final Map<PotionEffectType, PotionProperties> potionVictims;
	
	public ElementPotion() {
		this(null, null);
	}
	
	public ElementPotion(Map<PotionEffectType, PotionProperties> potionAttacker, Map<PotionEffectType, PotionProperties> potionVictims) {
		this.potionAttacker = potionAttacker != null ? potionAttacker : new HashMap<PotionEffectType, PotionProperties>();
		this.potionVictims = potionVictims != null ? potionVictims : new HashMap<PotionEffectType, PotionProperties>();
	}
	
	public final Map<PotionEffectType, PotionProperties> getPotionAttacker() {
		return this.potionAttacker;
	}
	
	public final Map<PotionEffectType, PotionProperties> getPotionVictims() {
		return this.potionVictims;
	}
}
