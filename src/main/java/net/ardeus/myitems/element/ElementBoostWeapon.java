package net.ardeus.myitems.element;

public class ElementBoostWeapon {

	private final double scaleBaseAdditionalDamage;
	private final double scaleBasePercentDamage;
	
	private ElementBoostWeapon(double scaleBaseAdditionalDamage, double scaleBasePercentDamage) {
		this.scaleBaseAdditionalDamage = scaleBaseAdditionalDamage;
		this.scaleBasePercentDamage = scaleBasePercentDamage;
	}
	
	public final double getScaleBaseAdditionalDamage() {
		return this.scaleBaseAdditionalDamage;
	}
	
	public final double getScaleBasePercentDamage() {
		return this.scaleBasePercentDamage;
	}
	
	public static class ElementBoostWeaponBuilder {
		
		private double scaleBaseAdditionalDamage;
		private double scaleBasePercentDamage;
		
		public ElementBoostWeaponBuilder() {
			this.scaleBaseAdditionalDamage = 0;
			this.scaleBasePercentDamage = 0;
		}
		
		public final double getScaleBaseAdditionalDamage() {
			return this.scaleBaseAdditionalDamage;
		}
		
		public final double getScaleBasePercentDamage() {
			return this.scaleBasePercentDamage;
		}
		
		public final ElementBoostWeaponBuilder setScaleBaseAdditionalDamage(double scaleBaseAdditionalDamage) {
			this.scaleBaseAdditionalDamage = scaleBaseAdditionalDamage;
			return this;
		}
		
		public final ElementBoostWeaponBuilder setScaleBasePercentDamage(double scaleBasePercentDamage) {
			this.scaleBasePercentDamage = scaleBasePercentDamage;
			return this;
		}
		
		public final ElementBoostWeapon build() {
			return new ElementBoostWeapon(scaleBaseAdditionalDamage, scaleBasePercentDamage);
		}
	}
}
