package net.ardeus.myitems;

import java.util.List;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.TabCompleter;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.builder.face.Agartha;
import core.praya.agarthalib.enums.main.VersionNMS;
import net.ardeus.myitems.command.executor.CommandAttributes;
import net.ardeus.myitems.command.executor.CommandEnchant;
import net.ardeus.myitems.command.executor.CommandEnchantAdd;
import net.ardeus.myitems.command.executor.CommandEnchantClear;
import net.ardeus.myitems.command.executor.CommandEnchantRemove;
import net.ardeus.myitems.command.executor.CommandFlag;
import net.ardeus.myitems.command.executor.CommandFlagAdd;
import net.ardeus.myitems.command.executor.CommandFlagClear;
import net.ardeus.myitems.command.executor.CommandFlagRemove;
import net.ardeus.myitems.command.executor.CommandItemName;
import net.ardeus.myitems.command.executor.CommandLore;
import net.ardeus.myitems.command.executor.CommandLoreAdd;
import net.ardeus.myitems.command.executor.CommandLoreClear;
import net.ardeus.myitems.command.executor.CommandLoreInsert;
import net.ardeus.myitems.command.executor.CommandLoreRemove;
import net.ardeus.myitems.command.executor.CommandLoreSet;
import net.ardeus.myitems.command.executor.CommandMyItems;
import net.ardeus.myitems.command.executor.CommandNBTClear;
import net.ardeus.myitems.command.executor.CommandNotCompatible;
import net.ardeus.myitems.command.executor.CommandSocket;
import net.ardeus.myitems.command.executor.CommandUnbreakable;
import net.ardeus.myitems.listener.custom.ListenerCombatCriticalDamage;
import net.ardeus.myitems.listener.custom.ListenerMenuClose;
import net.ardeus.myitems.listener.custom.ListenerMenuOpen;
import net.ardeus.myitems.listener.custom.ListenerPowerCommandCast;
import net.ardeus.myitems.listener.custom.ListenerPowerPreCast;
import net.ardeus.myitems.listener.custom.ListenerPowerShootCast;
import net.ardeus.myitems.listener.custom.ListenerPowerSpecialCast;
import net.ardeus.myitems.listener.main.ListenerBlockBreak;
import net.ardeus.myitems.listener.main.ListenerBlockExplode;
import net.ardeus.myitems.listener.main.ListenerBlockPhysic;
import net.ardeus.myitems.listener.main.ListenerCommand;
import net.ardeus.myitems.listener.main.ListenerEntityDamage;
import net.ardeus.myitems.listener.main.ListenerEntityDamageByEntity;
import net.ardeus.myitems.listener.main.ListenerEntityDeath;
import net.ardeus.myitems.listener.main.ListenerEntityRegainHealth;
import net.ardeus.myitems.listener.main.ListenerEntityShootBow;
import net.ardeus.myitems.listener.main.ListenerHeldItem;
import net.ardeus.myitems.listener.main.ListenerInventoryClick;
import net.ardeus.myitems.listener.main.ListenerInventoryDrag;
import net.ardeus.myitems.listener.main.ListenerInventoryOpen;
import net.ardeus.myitems.listener.main.ListenerPlayerDropItem;
import net.ardeus.myitems.listener.main.ListenerPlayerInteract;
import net.ardeus.myitems.listener.main.ListenerPlayerInteractEntity;
import net.ardeus.myitems.listener.main.ListenerPlayerItemDamage;
import net.ardeus.myitems.listener.main.ListenerPlayerJoin;
import net.ardeus.myitems.listener.main.ListenerPlayerRespawn;
import net.ardeus.myitems.listener.main.ListenerPlayerSwapHandItems;
import net.ardeus.myitems.listener.main.ListenerProjectileHit;
import net.ardeus.myitems.listener.support.ListenerMythicMobDeath;
import net.ardeus.myitems.listener.support.ListenerMythicMobSpawn;
import net.ardeus.myitems.listener.support.ListenerPlayerHealthMaxChange;
import net.ardeus.myitems.listener.support.ListenerPlayerHealthRegenChange;
import net.ardeus.myitems.listener.support.ListenerPlayerLevelUp;
import net.ardeus.myitems.listener.support.ListenerPlayerStaminaMaxChange;
import net.ardeus.myitems.listener.support.ListenerPlayerStaminaRegenChange;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.player.PlayerManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.manager.task.TaskManager;
import net.ardeus.myitems.tabcompleter.TabCompleterAttributes;
import net.ardeus.myitems.tabcompleter.TabCompleterEnchantmentAdd;
import net.ardeus.myitems.tabcompleter.TabCompleterEnchantmentRemove;
import net.ardeus.myitems.tabcompleter.TabCompleterFlagAdd;
import net.ardeus.myitems.tabcompleter.TabCompleterFlagRemove;
import net.ardeus.myitems.tabcompleter.TabCompleterLoreRemove;
import net.ardeus.myitems.tabcompleter.TabCompleterMyItems;
import net.ardeus.myitems.tabcompleter.TabCompleterNotCompatible;
import net.ardeus.myitems.tabcompleter.TabCompleterSocket;
import net.ardeus.myitems.tabcompleter.TabCompleterUnbreakable;
import net.ardeus.myitems.utility.main.AntiBugUtil;

import com.praya.agarthalib.utility.PluginUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.agarthalib.utility.ServerUtil;

public class MyItems extends JavaPlugin implements Agartha {

	private final String type = "Premium";
	private final String placeholder = "myitems";
	
	private PluginManager pluginManager;
	private PlayerManager playerManager;
	private GameManager gameManager;
	private TaskManager taskManager;
	
	@Override
	public String getPluginName() {
		return this.getName();
	}

	@Override
	public String getPluginType() {
		return this.type;
	}
	
	@Override
	public String getPluginVersion() {
		return getDescription().getVersion();
	}
	
	@Override
	public String getPluginPlaceholder() {
		return this.placeholder;
	}

	@Override
	public String getPluginWebsite() {
		return getPluginManager().getPluginPropertiesManager().getWebsite();
	}

	@Override
	public String getPluginLatest() {
		return getPluginManager().getPluginPropertiesManager().getPluginTypeVersion(getPluginType());
	}
	
	@Override
	public List<String> getPluginDevelopers() {
		return getPluginManager().getPluginPropertiesManager().getDevelopers();
	}
	
	public final PluginManager getPluginManager() {
		return this.pluginManager;
	}
	
	public final GameManager getGameManager() {
		return this.gameManager;
	}
	
	public final PlayerManager getPlayerManager() {
		return this.playerManager;
	}
	
	public final TaskManager getTaskManager() {
		return this.taskManager;
	}
	
	@Override
	public void onEnable() {
				
		setPluginManager();
		setPlayerManager();
		setGameManager();
		setTaskManager();
			
		registerCommand();
		registerTabComplete();
		registerEvent();
		registerPlaceholder();
	}
	
	private final void setPluginManager() {
		pluginManager = new PluginManager(this);
		pluginManager.initialize();
	}
	
	private final void setGameManager() {
		this.gameManager = new GameManager(this);
	}
	
	private final void setPlayerManager() {
		this.playerManager = new MyItemsPlayerMemory(this);
	}
	
	private final void setTaskManager() {
		this.taskManager = new TaskManager(this);
	}
	
	private final void registerPlaceholder() {
		getPluginManager().getPlaceholderManager().registerAll();
	}
	
	private final void registerCommand() {
		final CommandExecutor commandMyItems = new CommandMyItems(this);
		final CommandExecutor commandAttributes = new CommandAttributes(this);
		final CommandExecutor commandEnchant = new CommandEnchant(this);
		final CommandExecutor commandEnchantAdd = new CommandEnchantAdd(this);
		final CommandExecutor commandEnchantClear = new CommandEnchantClear(this);
		final CommandExecutor commandEnchantRemove = new CommandEnchantRemove(this);
		final CommandExecutor commandItemName = new CommandItemName(this);
		final CommandExecutor commandLore = new CommandLore(this);
		final CommandExecutor commandLoreAdd = new CommandLoreAdd(this);
		final CommandExecutor commandLoreClear = new CommandLoreClear(this);
		final CommandExecutor commandLoreInsert = new CommandLoreInsert(this);
		final CommandExecutor commandLoreRemove = new CommandLoreRemove(this);
		final CommandExecutor commandLoreSet = new CommandLoreSet(this);
		final CommandExecutor commandNBTClear = new CommandNBTClear(this);
		final CommandExecutor commandSocket = new CommandSocket(this);
		final CommandExecutor commandUnbreakable = new CommandUnbreakable(this);
		final CommandExecutor commandNotCompatible = new CommandNotCompatible(this);
		final CommandExecutor commandFlag = ServerUtil.isCompatible(VersionNMS.V1_8_R1) ? new CommandFlag(this) : commandNotCompatible;
		final CommandExecutor commandAddFlag = ServerUtil.isCompatible(VersionNMS.V1_8_R1) ? new CommandFlagAdd(this) : commandNotCompatible;
		final CommandExecutor commandRemoveFlag = ServerUtil.isCompatible(VersionNMS.V1_8_R1) ? new CommandFlagRemove(this) : commandNotCompatible;
		final CommandExecutor commandClearFlag = ServerUtil.isCompatible(VersionNMS.V1_8_R1) ? new CommandFlagClear(this) : commandNotCompatible;
		
		getCommand("MyItems").setExecutor(commandMyItems);
		getCommand("ItemAtt").setExecutor(commandAttributes);
		getCommand("Enchant").setExecutor(commandEnchant);
		getCommand("EnchantAdd").setExecutor(commandEnchantAdd);
		getCommand("EnchantRemove").setExecutor(commandEnchantRemove);
		getCommand("EnchantClear").setExecutor(commandEnchantClear);
		getCommand("ItemName").setExecutor(commandItemName);
		getCommand("Lore").setExecutor(commandLore);
		getCommand("LoreSet").setExecutor(commandLoreSet);
		getCommand("LoreInsert").setExecutor(commandLoreInsert);
		getCommand("LoreAdd").setExecutor(commandLoreAdd);
		getCommand("LoreRemove").setExecutor(commandLoreRemove);
		getCommand("LoreClear").setExecutor(commandLoreClear);
		getCommand("NBTClear").setExecutor(commandNBTClear);
		getCommand("Socket").setExecutor(commandSocket);
		getCommand("Unbreakable").setExecutor(commandUnbreakable);
		getCommand("Flag").setExecutor(commandFlag);
		getCommand("FlagAdd").setExecutor(commandAddFlag);
		getCommand("FlagRemove").setExecutor(commandRemoveFlag);
		getCommand("FlagClear").setExecutor(commandClearFlag);
	}
	
	private final void registerTabComplete() {
		final TabCompleter tabCompleterMyItems = new TabCompleterMyItems(this);
		final TabCompleter tabCompleterAttributes = new TabCompleterAttributes(this);
		final TabCompleter tabCompleterEnchantmentAdd = new TabCompleterEnchantmentAdd(this);
		final TabCompleter tabCompleterEnchantmentRemove = new TabCompleterEnchantmentRemove(this);
		final TabCompleter tabCompleterLoreRemove = new TabCompleterLoreRemove(this);
		final TabCompleter tabCompleterSocket = new TabCompleterSocket(this);
		final TabCompleter tabCompleterUnbreakable = new TabCompleterUnbreakable(this);
		final TabCompleter tabCompleterNotCompatible = new TabCompleterNotCompatible(this); 
		final TabCompleter tabCompleterFlagAdd = ServerUtil.isCompatible(VersionNMS.V1_8_R1) ? new TabCompleterFlagAdd(this) : tabCompleterNotCompatible;
		final TabCompleter tabCompleterFlagRemove = ServerUtil.isCompatible(VersionNMS.V1_8_R1) ? new TabCompleterFlagRemove(this) : tabCompleterNotCompatible;
		
		getCommand("MyItems").setTabCompleter(tabCompleterMyItems);
		getCommand("ItemAtt").setTabCompleter(tabCompleterAttributes);
		getCommand("EnchantAdd").setTabCompleter(tabCompleterEnchantmentAdd);
		getCommand("EnchantRemove").setTabCompleter(tabCompleterEnchantmentRemove);
		getCommand("LoreRemove").setTabCompleter(tabCompleterLoreRemove);
		getCommand("Socket").setTabCompleter(tabCompleterSocket);
		getCommand("Unbreakable").setTabCompleter(tabCompleterUnbreakable);
		getCommand("FlagAdd").setTabCompleter(tabCompleterFlagAdd);
		getCommand("FlagRemove").setTabCompleter(tabCompleterFlagRemove);
	}
	
	private final void registerEvent() {
		final Listener listenerBlockBreak = new ListenerBlockBreak(this);
		final Listener listenerBlockPhysic = new ListenerBlockPhysic(this);
		final Listener listenerCommand = new ListenerCommand(this);
		final Listener listenerPlayerDropItem = new ListenerPlayerDropItem(this);
		final Listener listenerEntityDamage = new ListenerEntityDamage(this);
		final Listener listenerEntityDamageByEntity = new ListenerEntityDamageByEntity(this);
		final Listener listenerEntityDeath = new ListenerEntityDeath(this);
		final Listener listenerEntityRegainHealth = new ListenerEntityRegainHealth(this);
		final Listener listenerHeldItem = new ListenerHeldItem(this);
		final Listener listenerInventoryClick = new ListenerInventoryClick(this);
		final Listener listenerInventoryDrag = new ListenerInventoryDrag(this);
		final Listener listenerInventoryOpen = new ListenerInventoryOpen(this);
		final Listener listenerPlayerItemDamage = new ListenerPlayerItemDamage(this);
		final Listener listenerPlayerInteract = new ListenerPlayerInteract(this);
		final Listener listenerPlayerInteractEntity = new ListenerPlayerInteractEntity(this);
		final Listener listenerPlayerJoin = new ListenerPlayerJoin(this);
		final Listener listenerPlayerRespawn = new ListenerPlayerRespawn(this);
		final Listener listenerEntityShootBowEvent = new ListenerEntityShootBow(this);
		final Listener listenerProjectileHit = new ListenerProjectileHit(this);
		final Listener listenerCombatCriticalDamage = new ListenerCombatCriticalDamage(this);
		final Listener listenerMenuClose = new ListenerMenuClose(this);
		final Listener listenerMenuOpen = new ListenerMenuOpen(this);
		final Listener listenerPowerCommandCast = new ListenerPowerCommandCast(this);
		final Listener listenerPowerPreCast = new ListenerPowerPreCast(this);
		final Listener listenerPowerShootCast = new ListenerPowerShootCast(this);
		final Listener listenerPowerSpecialCast = new ListenerPowerSpecialCast(this);
		final Listener listenerPlayerHealthMaxChange = new ListenerPlayerHealthMaxChange(this);
		
		ServerEventUtil.registerEvent(this, listenerBlockBreak);
		ServerEventUtil.registerEvent(this, listenerBlockPhysic);
		ServerEventUtil.registerEvent(this, listenerCommand);
		ServerEventUtil.registerEvent(this, listenerEntityDamage);
		ServerEventUtil.registerEvent(this, listenerEntityDamageByEntity);
		ServerEventUtil.registerEvent(this, listenerPlayerDropItem);
		ServerEventUtil.registerEvent(this, listenerEntityDeath);
		ServerEventUtil.registerEvent(this, listenerEntityRegainHealth);
		ServerEventUtil.registerEvent(this, listenerHeldItem);
		ServerEventUtil.registerEvent(this, listenerInventoryClick);
		ServerEventUtil.registerEvent(this, listenerInventoryDrag);
		ServerEventUtil.registerEvent(this, listenerInventoryOpen);
		ServerEventUtil.registerEvent(this, listenerPlayerItemDamage);
		ServerEventUtil.registerEvent(this, listenerPlayerInteract);
		ServerEventUtil.registerEvent(this, listenerPlayerInteractEntity);
		ServerEventUtil.registerEvent(this, listenerPlayerJoin);
		ServerEventUtil.registerEvent(this, listenerPlayerRespawn);
		ServerEventUtil.registerEvent(this, listenerEntityShootBowEvent);
		ServerEventUtil.registerEvent(this, listenerProjectileHit);
		ServerEventUtil.registerEvent(this, listenerCombatCriticalDamage);
		ServerEventUtil.registerEvent(this, listenerMenuClose);
		ServerEventUtil.registerEvent(this, listenerMenuOpen);
		ServerEventUtil.registerEvent(this, listenerPowerCommandCast);
		ServerEventUtil.registerEvent(this, listenerPowerPreCast);
		ServerEventUtil.registerEvent(this, listenerPowerShootCast);
		ServerEventUtil.registerEvent(this, listenerPowerSpecialCast);
		ServerEventUtil.registerEvent(this, listenerPlayerHealthMaxChange);
		
		if (ServerUtil.isCompatible(VersionNMS.V1_9_R1)) {
			final Listener listenerBlockExplode = new ListenerBlockExplode(this);
			final Listener listenerPlayerSwapHandItems = new ListenerPlayerSwapHandItems(this);
			
			ServerEventUtil.registerEvent(this, listenerBlockExplode);
			ServerEventUtil.registerEvent(this, listenerPlayerSwapHandItems);
		}
		
		if (PluginUtil.isPluginInstalled("SkillAPI")) {
			final Listener listenerPlayerLevelUp = new ListenerPlayerLevelUp(this);
			
			ServerEventUtil.registerEvent(this, listenerPlayerLevelUp);
		}
		
		if (PluginUtil.isPluginInstalled("MythicMobs")) {
			final Listener listenerMythicMobSpawn = new ListenerMythicMobSpawn(this);
			final Listener listenerMythicMobDeath = new ListenerMythicMobDeath(this);
			
			ServerEventUtil.registerEvent(this, listenerMythicMobSpawn);
			ServerEventUtil.registerEvent(this, listenerMythicMobDeath);
		}
		
		if (PluginUtil.isPluginInstalled("LifeEssence")) {
			final Listener listenerPlayerHealthRegenChange = new ListenerPlayerHealthRegenChange(this);
			
			ServerEventUtil.registerEvent(this, listenerPlayerHealthRegenChange);
		}
		
		if (PluginUtil.isPluginInstalled("CombatStamina")) {
			final Listener listenerPlayerStaminaMaxChange = new ListenerPlayerStaminaMaxChange(this);
			final Listener listenerPlayerStaminaRegenChange = new ListenerPlayerStaminaRegenChange(this);
			
			ServerEventUtil.registerEvent(this, listenerPlayerStaminaMaxChange);
			ServerEventUtil.registerEvent(this, listenerPlayerStaminaRegenChange);
		}
	}
	
	@Override
	public void onDisable() {
		AntiBugUtil.antiBugCustomStats();
	}
}
