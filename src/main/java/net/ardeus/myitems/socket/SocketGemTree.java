package net.ardeus.myitems.socket;

import java.util.HashMap;
import java.util.Map;

import core.praya.agarthalib.enums.main.SlotType;

public class SocketGemTree {

	private final String id;
	private final int maxGrade;
	private final SlotType typeItem;
	protected final Map<Integer, SocketGem> mapSocket;
	
	public SocketGemTree(String id, int maxGrade, SlotType typeItem) {
		this(id, maxGrade, typeItem, null);
	}
	
	public SocketGemTree(String id, int maxGrade, SlotType typeItem, Map<Integer, SocketGem> mapSocket) {
		if (id == null || typeItem == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.maxGrade = maxGrade;
			this.typeItem = typeItem;
			this.mapSocket = mapSocket != null ? mapSocket : new HashMap<Integer, SocketGem>();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final int getMaxGrade() {
		return this.maxGrade;
	}
	
	public final SlotType getTypeItem() {
		return this.typeItem;
	}
	
	public final SocketGem getSocketGem(int grade) {
		return this.mapSocket.get(grade);
	}
	
	public final boolean isGemsAvailable(int grade) {
		return getSocketGem(grade) != null;
	}
}