package net.ardeus.myitems.socket;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.socket.SocketGem;
import net.ardeus.myitems.socket.SocketGemTree;

public final class SocketGemMemory extends SocketGemManager {

	private final SocketConfig socketConfig;
	
	private SocketGemMemory(MyItems plugin) {
		super(plugin);
		
		this.socketConfig = new SocketConfig(plugin);
	};
	
	private static class SocketMemorySingleton {
		private static final SocketGemMemory INSTANCE;
		
		static {
			final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
			
			INSTANCE = new SocketGemMemory(plugin);
		}
	}
	
	public static final SocketGemMemory getInstance() {
		return SocketMemorySingleton.INSTANCE;
	}
	
	protected final SocketConfig getSocketConfig() {
		return this.socketConfig;
	}
	
	@Override
	public final Collection<String> getSocketGemIds() {
		final Map<String, SocketGemTree> mapSocketGemTree = getSocketConfig().mapSocketGemTree;
		
		return new ArrayList<String>(mapSocketGemTree.keySet());
	}
	
	@Override
	public final Collection<SocketGemTree> getAllSocketGemTrees() {
		final Map<String, SocketGemTree> mapSocketGemTree = getSocketConfig().mapSocketGemTree;
		
		return new ArrayList<SocketGemTree>(mapSocketGemTree.values());
	}
	
	@Override
	public final SocketGemTree getSocketGemTree(String socketGemId) {
		if (socketGemId != null) {
			final Map<String, SocketGemTree> mapSocketGemTree = getSocketConfig().mapSocketGemTree;
			
			for (String key : mapSocketGemTree.keySet()) {
				if (key.equalsIgnoreCase(socketGemId)) {
					return mapSocketGemTree.get(socketGemId);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final SocketGem getSocketGemByKeyLore(String keyLore) {
		if (keyLore != null) {
			final Map<String, SocketGemTree> mapSocketGemTree = getSocketConfig().mapSocketGemTree;
			
			for (SocketGemTree socketGemTree : mapSocketGemTree.values()) {
				for (SocketGem key : socketGemTree.mapSocket.values()) {
					if (key.getKeyLore().equalsIgnoreCase(keyLore)) {
						return key;
					}
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final SocketGem getSocketGemByItem(ItemStack item) {
		if (item != null) {
			final Map<String, SocketGemTree> mapSocketGemTree = getSocketConfig().mapSocketGemTree;
			
			for (SocketGemTree socketGemTree : mapSocketGemTree.values()) {
				for (SocketGem key : socketGemTree.mapSocket.values()) {
					if (key.getItem().isSimilar(item)) {
						return key;
					}
				}
			}
		}
		
		return null;
	}
}
