package net.ardeus.myitems.socket;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.main.RomanNumber;
import core.praya.agarthalib.enums.main.SlotType;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerConfig;
import net.ardeus.myitems.manager.plugin.DataManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EnchantmentUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.FileUtil;
import com.praya.agarthalib.utility.MaterialUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public final class SocketConfig extends HandlerConfig {
	
	protected final Map<String, SocketGemTree> mapSocketGemTree = new HashMap<String, SocketGemTree>();
	
	protected SocketConfig(MyItems plugin) {
		super(plugin);
		
		setup();
	};
	
	public final void setup() {
		moveOldFile();
		reset();
		loadConfig();
	}
	
	private final void reset() {
		this.mapSocketGemTree.clear();
	}
	
	private final void loadConfig() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final String path = dataManager.getPath("Path_File_Socket");
		final File file = FileUtil.getFile(plugin, path);		
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, path);
		}
		
		final FileConfiguration config = FileUtil.getFileConfiguration(file);
		
		for (String key : config.getKeys(false)) {
			final ConfigurationSection section = config.getConfigurationSection(key);
			final List<String> lores = new ArrayList<String>();
			final List<String> flags = new ArrayList<String>();
			final HashMap<Enchantment, Integer> enchantments = new HashMap<Enchantment, Integer>();
			
			String keyLore = null;
			String display = null;
			Material material = null;
			SlotType typeItem = SlotType.UNIVERSAL;
			boolean shiny = false;
			boolean unbreakable = false;
			short data = 0;
			int maxGrade = 1;
			double baseSuccessRate = 100;
			double scaleSuccessRate = 0;
			double baseAdditionalDamage = 0;
			double scaleAdditionalDamage = 0;
			double basePercentDamage = 0;
			double scalePercentDamage = 0;
			double basePenetration = 0;
			double scalePenetration = 0;
			double basePvPDamage = 0;
			double scalePvPDamage = 0;
			double basePvEDamage = 0;
			double scalePvEDamage = 0;
			double baseAdditionalDefense = 0;
			double scaleAdditionalDefense = 0;
			double basePercentDefense = 0;
			double scalePercentDefense = 0;
			double baseMaxHealth = 0;
			double scaleMaxHealth = 0;
			double baseHealthRegen = 0;
			double scaleHealthRegen = 0;
			double baseStaminaMax = 0;
			double scaleStaminaMax = 0;
			double baseStaminaRegen = 0;
			double scaleStaminaRegen = 0;
			double baseAttackAoERadius = 0;
			double scaleAttackAoERadius = 0;
			double baseAttackAoEDamage = 0;
			double scaleAttackAoEDamage = 0;
			double basePvPDefense = 0;
			double scalePvPDefense = 0;
			double basePvEDefense = 0;
			double scalePvEDefense = 0;
			double baseCriticalChance = 0;
			double scaleCriticalChance = 0;
			double baseCriticalDamage = 0;
			double scaleCriticalDamage = 0;
			double baseBlockAmount = 0;
			double scaleBlockAmount = 0;
			double baseBlockRate = 0;
			double scaleBlockRate = 0;
			double baseHitRate = 0;
			double scaleHitRate = 0;
			double baseDodgeRate = 0;
			double scaleDodgeRate = 0;
			
			for (String keySection : section.getKeys(false)) {
				if (keySection.equalsIgnoreCase("KeyLore")) {
					keyLore = TextUtil.colorful(section.getString(keySection));
				} else if (keySection.equalsIgnoreCase("Display_Name") || keySection.equalsIgnoreCase("Display") || keySection.equalsIgnoreCase("Name")) {
					display = section.getString(keySection);
				} else if (keySection.equalsIgnoreCase("Material")) {
					material = MaterialUtil.getMaterial(section.getString(keySection));
				} else if (keySection.equalsIgnoreCase("Data")) {
					data = (short) section.getInt(keySection);
				} else if (keySection.equalsIgnoreCase("Shiny")) {
					shiny = section.getBoolean(keySection);
				} else if (keySection.equalsIgnoreCase("Unbreakable")) {
					unbreakable = section.getBoolean(keySection);
				} else if (keySection.equalsIgnoreCase("Max_Grade")) {
					maxGrade = section.getInt(keySection);
				} else if (keySection.equalsIgnoreCase("Type_Item")) {
					final String textTypeItem = section.getString(keySection);
					
					typeItem = SlotType.getSlotType(textTypeItem);
				} else if (keySection.equalsIgnoreCase("Base_Success_Rate")) {
					baseSuccessRate = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Scale_Success_Rate")) {
					scaleSuccessRate = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Lores") || keySection.equalsIgnoreCase("Lore")) {
					lores.addAll(section.getStringList(keySection));
				} else if (keySection.equalsIgnoreCase("Flags") || keySection.equalsIgnoreCase("ItemFlags")) {
					flags.addAll(section.getStringList(keySection));
				} else if (keySection.equalsIgnoreCase("Enchantments") || keySection.equalsIgnoreCase("Enchantment")) {
					for (String lineEnchant : section.getStringList(keySection)) {						
						final String[] parts = lineEnchant.replaceAll(" ", "").split(":");
						
						int grade = 1;
						
						if (parts.length > 0) {
							final String enchantmentKey = parts[0].toUpperCase();
							final Enchantment enchantment = EnchantmentUtil.getEnchantment(enchantmentKey);
							
							if (enchantment != null) {
								if (parts.length > 1) {
									final String textGrade = parts[1];
									
									if (MathUtil.isNumber(textGrade)) {
										grade = MathUtil.parseInteger(textGrade);
										grade = MathUtil.limitInteger(grade, 1, grade);
									}
								}
								
								enchantments.put(enchantment, grade);
							}
						}
					}
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Additional_Damage") || keySection.equalsIgnoreCase("Bonus_Additional_Damage")) {
					baseAdditionalDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Additional_Damage")) {
					scaleAdditionalDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Percent_Damage") || keySection.equalsIgnoreCase("Bonus_Percent_Damage")) {
					basePercentDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Percent_Damage")) {
					scalePercentDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Penetration") || keySection.equalsIgnoreCase("Bonus_Penetration")) {
					basePenetration = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Penetration")) {
					scalePenetration = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_PvP_Damage") || keySection.equalsIgnoreCase("Bonus_PvP_Damage")) {
					basePvPDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_PvP_Damage")) {
					scalePvPDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_PvE_Damage") || keySection.equalsIgnoreCase("Bonus_PvE_Damage")) {
					basePvEDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_PvE_Damage")) {
					scalePvEDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Additional_Defense") || keySection.equalsIgnoreCase("Bonus_Additional_Defense")) {
					baseAdditionalDefense = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Additional_Defense")) {
					scaleAdditionalDefense = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Percent_Defense") || keySection.equalsIgnoreCase("Bonus_Percent_Defense")) {
					basePercentDefense = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Percent_Defense")) {
					scalePercentDefense = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Max_Health") || keySection.equalsIgnoreCase("Bonus_Max_Health")) {
					baseMaxHealth = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Max_Health")) {
					scaleMaxHealth = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Health_Regen") || keySection.equalsIgnoreCase("Bonus_Health_Regen")) {
					baseHealthRegen = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Health_Regen")) {
					scaleHealthRegen = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Stamina_Max") || keySection.equalsIgnoreCase("Bonus_Stamina_Max")) {
					baseStaminaMax = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Stamina_Max")) {
					scaleStaminaMax = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Stamina_Regen") || keySection.equalsIgnoreCase("Bonus_Stamina_Regen")) {
					baseStaminaRegen = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Stamina_Regen")) {
					scaleStaminaRegen = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Attack_AoE_Radius") || keySection.equalsIgnoreCase("Bonus_Attack_AoE_Radius")) {
					baseAttackAoERadius = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Attack_AoE_Radius")) {
					scaleAttackAoERadius = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Attack_AoE_Damage") || keySection.equalsIgnoreCase("Bonus_Attack_AoE_Damage")) {
					baseAttackAoEDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Attack_AoE_Damage")) {
					scaleAttackAoEDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_PvP_Defense") || keySection.equalsIgnoreCase("Bonus_PvP_Defense")) {
					basePvPDefense = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_PvP_Defense")) {
					scalePvPDefense = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_PvE_Defense") || keySection.equalsIgnoreCase("Bonus_PvE_Defense")) {
					basePvEDefense = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_PvE_Defense")) {
					scalePvEDefense = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Critical_Chance") || keySection.equalsIgnoreCase("Bonus_Critical_Chance")) {
					baseCriticalChance = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Critical_Chance")) {
					scaleCriticalChance = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Critical_Damage") || keySection.equalsIgnoreCase("Bonus_Critical_Damage")) {
					baseCriticalDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Critical_Damage")) {
					scaleCriticalDamage = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Block_Amount") || keySection.equalsIgnoreCase("Bonus_Block_Amount")) {
					baseBlockAmount = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Block_Amount")) {
					scaleBlockAmount = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Block_Rate") || keySection.equalsIgnoreCase("Bonus_Block_Rate")) {
					baseBlockRate = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Block_Rate")) {
					scaleBlockRate = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Hit_Rate") || keySection.equalsIgnoreCase("Bonus_Hit_Rate")) {
					baseHitRate = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Hit_Rate")) {
					scaleHitRate = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Base_Dodge_Rate") || keySection.equalsIgnoreCase("Bonus_Dodge_Rate")) {
					baseDodgeRate = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Bonus_Scale_Dodge_Rate")) {
					scaleDodgeRate = section.getDouble(keySection);
				} else if (keySection.equalsIgnoreCase("Effect") || keySection.equalsIgnoreCase("Effects")) {
					final ConfigurationSection effectSection = section.getConfigurationSection(keySection);
					
					for (String effect : effectSection.getKeys(false)) {
						if (effect.equalsIgnoreCase("Bonus_Base_Additional_Damage") || effect.equalsIgnoreCase("Bonus_Additional_Damage")) {
							baseAdditionalDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Additional_Damage")) {
							scaleAdditionalDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Percent_Damage") || effect.equalsIgnoreCase("Bonus_Percent_Damage")) {
							basePercentDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Percent_Damage")) {
							scalePercentDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Penetration") || effect.equalsIgnoreCase("Bonus_Penetration")) {
							basePenetration = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Penetration")) {
							scalePenetration = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_PvP_Damage") || effect.equalsIgnoreCase("Bonus_PvP_Damage")) {
							basePvPDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_PvP_Damage")) {
							scalePvPDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_PvE_Damage") || effect.equalsIgnoreCase("Bonus_PvE_Damage")) {
							basePvEDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_PvE_Damage")) {
							scalePvEDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Additional_Defense") || effect.equalsIgnoreCase("Bonus_Additional_Defense")) {
							baseAdditionalDefense = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Additional_Defense")) {
							scaleAdditionalDefense = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Percent_Defense") || effect.equalsIgnoreCase("Bonus_Percent_Defense")) {
							basePercentDefense = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Percent_Defense")) {
							scalePercentDefense = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Max_Health") || effect.equalsIgnoreCase("Bonus_Max_Health")) {
							baseMaxHealth = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Max_Health")) {
							scaleMaxHealth = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Health_Regen") || effect.equalsIgnoreCase("Bonus_Health_Regen")) {
							baseHealthRegen = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Health_Regen")) {
							scaleHealthRegen = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Stamina_Max") || effect.equalsIgnoreCase("Bonus_Stamina_Max")) {
							baseStaminaMax = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Stamina_Max")) {
							scaleStaminaMax = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Stamina_Regen") || effect.equalsIgnoreCase("Bonus_Stamina_Regen")) {
							baseStaminaRegen = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Stamina_Regen")) {
							scaleStaminaRegen = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Attack_AoE_Radius") || effect.equalsIgnoreCase("Bonus_Attack_AoE_Radius")) {
							baseAttackAoERadius = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Attack_AoE_Radius")) {
							scaleAttackAoERadius = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Attack_AoE_Damage") || effect.equalsIgnoreCase("Bonus_Attack_AoE_Damage")) {
							baseAttackAoEDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Attack_AoE_Damage")) {
							scaleAttackAoEDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_PvP_Defense") || effect.equalsIgnoreCase("Bonus_PvP_Defense")) {
							basePvPDefense = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_PvP_Defense")) {
							scalePvPDefense = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_PvE_Defense") || effect.equalsIgnoreCase("Bonus_PvE_Defense")) {
							basePvEDefense = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_PvE_Defense")) {
							scalePvEDefense = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Critical_Chance") || effect.equalsIgnoreCase("Bonus_Critical_Chance")) {
							baseCriticalChance = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Critical_Chance")) {
							scaleCriticalChance = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Critical_Damage") || effect.equalsIgnoreCase("Bonus_Critical_Damage")) {
							baseCriticalDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Critical_Damage")) {
							scaleCriticalDamage = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Block_Amount") || effect.equalsIgnoreCase("Bonus_Block_Amount")) {
							baseBlockAmount = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Block_Amount")) {
							scaleBlockAmount = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Block_Rate") || effect.equalsIgnoreCase("Bonus_Block_Rate")) {
							baseBlockRate = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Block_Rate")) {
							scaleBlockRate = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Hit_Rate") || effect.equalsIgnoreCase("Bonus_Hit_Rate")) {
							baseHitRate = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Hit_Rate")) {
							scaleHitRate = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Base_Dodge_Rate") || effect.equalsIgnoreCase("Bonus_Dodge_Rate")) {
							baseDodgeRate = effectSection.getDouble(effect);
						} else if (effect.equalsIgnoreCase("Bonus_Scale_Dodge_Rate")) {
							scaleDodgeRate = effectSection.getDouble(effect);
						}
					}
				}
			}
			
			if (material != null && keyLore != null) {
				final MaterialEnum materialEnum = MaterialEnum.getMaterialEnum(material, data);
				
				if (materialEnum != null) {
					final HashMap<Integer, SocketGem> mapSocket = new HashMap<Integer, SocketGem>();
					final HashMap<String, String> map = new HashMap<String, String>();
					final String typeItemName;
					
					switch (typeItem) {
					case WEAPON : typeItemName = mainConfig.getSocketTypeItemWeapon(); break;
					case ARMOR : typeItemName = mainConfig.getSocketTypeItemArmor(); break;
					case UNIVERSAL : typeItemName = mainConfig.getSocketTypeItemUniversal(); break;
					default : typeItemName = "Unknown";
					}
					
					for (int grade = 1; grade <= maxGrade; grade++) {
						final double successRate = MathUtil.limitDouble(baseSuccessRate + ((grade-1) * scaleSuccessRate), 0, 100);
						final double failureRate = 100 - successRate;
						final double additionalDamage = baseAdditionalDamage + ((grade-1) * scaleAdditionalDamage);
						final double percentDamage = basePercentDamage + ((grade-1) * scalePercentDamage);
						final double penetration = basePenetration + ((grade-1) * scalePenetration);
						final double pvpDamage = basePvPDamage + ((grade-1) * scalePvPDamage);
						final double pveDamage = basePvEDamage + ((grade-1) * scalePvEDamage);
						final double additionalDefense = baseAdditionalDefense + ((grade-1) * scaleAdditionalDefense);
						final double percentDefense = basePercentDefense + ((grade-1) * scalePercentDefense);
						final double maxHealth = baseMaxHealth + ((grade-1) * scaleMaxHealth);
						final double healthRegen = baseHealthRegen + ((grade-1) * scaleHealthRegen);
						final double staminaMax = baseStaminaMax + ((grade-1) * scaleStaminaMax);
						final double staminaRegen = baseStaminaRegen + ((grade-1) * scaleStaminaRegen);
						final double attackAoERadius = baseAttackAoERadius + ((grade-1) * scaleAttackAoERadius);
						final double attackAoEDamage = baseAttackAoEDamage + ((grade-1) * scaleAttackAoEDamage);
						final double pvpDefense = basePvPDefense + ((grade-1) * scalePvPDefense);
						final double pveDefense = basePvEDefense + ((grade-1) * scalePvEDefense);
						final double criticalChance = baseCriticalChance + ((grade-1) * scaleCriticalChance);
						final double criticalDamage = baseCriticalDamage + ((grade-1) * scaleCriticalDamage);
						final double blockAmount = baseBlockAmount + ((grade-1) * scaleBlockAmount);
						final double blockRate = baseBlockRate + ((grade-1) * scaleBlockRate);
						final double hitRate = baseHitRate + ((grade-1) * scaleHitRate);
						final double dodgeRate = baseDodgeRate + ((grade-1) * scaleDodgeRate);
						;
						map.clear();
						map.put("gems", key);
						map.put("grade", RomanNumber.getRomanNumber(grade));
						map.put("type_item", typeItemName);
						map.put("success_rate", String.valueOf(MathUtil.roundNumber(successRate)));
						map.put("failure_rate", String.valueOf(MathUtil.roundNumber(failureRate)));
						map.put("additional_damage", String.valueOf(MathUtil.roundNumber(additionalDamage)));
						map.put("percent_damage", String.valueOf(MathUtil.roundNumber(percentDamage)));
						map.put("penetration", String.valueOf(MathUtil.roundNumber(penetration)));
						map.put("pvp_damage", String.valueOf(MathUtil.roundNumber(pvpDamage)));
						map.put("pve_damage", String.valueOf(MathUtil.roundNumber(pveDamage)));
						map.put("additional_defense", String.valueOf(MathUtil.roundNumber(additionalDefense)));
						map.put("percent_defense", String.valueOf(MathUtil.roundNumber(percentDefense)));
						map.put("max_health", String.valueOf(MathUtil.roundNumber(maxHealth)));
						map.put("health_max", String.valueOf(MathUtil.roundNumber(maxHealth)));
						map.put("health_regen", String.valueOf(MathUtil.roundNumber(healthRegen)));
						map.put("max_stamina", String.valueOf(MathUtil.roundNumber(staminaMax)));
						map.put("stamina_max", String.valueOf(MathUtil.roundNumber(staminaMax)));
						map.put("stamina_regen", String.valueOf(MathUtil.roundNumber(staminaRegen)));
						map.put("attack_aoe_radius", String.valueOf(MathUtil.roundNumber(attackAoERadius)));
						map.put("attack_aoe_damage", String.valueOf(MathUtil.roundNumber(attackAoEDamage)));
						map.put("pvp_defense", String.valueOf(MathUtil.roundNumber(pvpDefense)));
						map.put("pve_defense", String.valueOf(MathUtil.roundNumber(pveDefense)));
						map.put("critical_chance", String.valueOf(MathUtil.roundNumber(criticalChance)));
						map.put("critical_damage", String.valueOf(MathUtil.roundNumber(criticalDamage)));
						map.put("block_amount", String.valueOf(MathUtil.roundNumber(blockAmount)));
						map.put("block_rate", String.valueOf(MathUtil.roundNumber(blockRate)));
						map.put("hit_rate", String.valueOf(MathUtil.roundNumber(hitRate)));
						map.put("dodge_rate", String.valueOf(MathUtil.roundNumber(dodgeRate)));
						
						final String itemDisplay = TextUtil.placeholder(map, display);
						final String itemKeyLore = TextUtil.placeholder(map, keyLore);
						final ItemStack item = EquipmentUtil.createItem(materialEnum, itemDisplay, 1);
						final List<String> itemLores = TextUtil.placeholder(map, lores);
						
						if (shiny) {
							EquipmentUtil.shiny(item);
						}
						
						if (unbreakable) {
							Bridge.getBridgeTagsNBT().setUnbreakable(item, true);
						}
						
						if (!itemLores.isEmpty()) {
							EquipmentUtil.setLores(item, itemLores);
						}
						
						if (!flags.isEmpty()) {
							for (String flag : flags) {
								EquipmentUtil.addFlag(item, flag);
							}
						}
						
						if (!enchantments.isEmpty()) {
							for (Enchantment enchantment : enchantments.keySet()) {
								final int enchantmentGrade = enchantments.get(enchantment);
								
								EquipmentUtil.addEnchantment(item, enchantment, enchantmentGrade);
							}
						}
						
						EquipmentUtil.hookPlaceholderAPI(item);
						EquipmentUtil.colorful(item);
						
						if (EquipmentUtil.isSolid(item)) {
							final SocketGemProperties socketProperties = new SocketGemProperties(additionalDamage, percentDamage, penetration, pvpDamage, pveDamage, additionalDefense, percentDefense, maxHealth, healthRegen, staminaMax, staminaRegen, attackAoERadius, attackAoEDamage, pvpDefense, pveDefense, criticalChance, criticalDamage, blockAmount, blockRate, hitRate, dodgeRate);
							final SocketGem build = new SocketGem(item, itemKeyLore, key, grade, successRate, typeItem, socketProperties);
							
							mapSocket.put(grade, build);
						}
					}
					
					final SocketGemTree socketTree = new SocketGemTree(key, maxGrade, typeItem, mapSocket);
					
					mapSocketGemTree.put(key, socketTree);
				}
			}
		}
	}
	
	private final void moveOldFile() {
		final PluginManager pluginManager = plugin.getPluginManager();
		final DataManager dataManager = pluginManager.getDataManager();
		final String pathSource = "socket.yml";
		final String pathTarget = dataManager.getPath("Path_File_Socket");
		final File fileSource = FileUtil.getFile(plugin, pathSource);
		final File fileTarget = FileUtil.getFile(plugin, pathTarget);
		
		if (fileSource.exists()) {
			FileUtil.moveFileSilent(fileSource, fileTarget);
		}
	}
}