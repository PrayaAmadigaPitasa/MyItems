package net.ardeus.myitems.socket;

import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.enums.main.SlotType;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.SocketGemManager;


public class SocketGem {

	private final ItemStack item;
	private final String keyLore;
	private final String gem;
	private final int grade;
	private final double successRate;
	private final SlotType typeItem;
	private final SocketGemProperties socketProperties;
	
	public SocketGem(ItemStack item, String keyLore, String gem, int grade, double successRate, SlotType typeItem, SocketGemProperties socketProperties) {
		this.item = item;
		this.keyLore = keyLore;	
		this.gem = gem;
		this.grade = grade;
		this.successRate = successRate;
		this.typeItem = typeItem;
		this.socketProperties = socketProperties;
	}
	
	public final ItemStack getItem() {
		return this.item != null ? this.item.clone() : null;
	}
	
	public final String getKeyLore() {
		return this.keyLore;
	}
	
	public final String getGem() {
		return this.gem;
	}
	
	public final int getGrade() {
		return this.grade;
	}
	
	public final double getSuccessRate() {
		return this.successRate;
	}
	
	public final SlotType getTypeItem() {
		return this.typeItem;
	}
	
	public final SocketGemProperties getSocketProperties() {
		return this.socketProperties;
	}
	
	public final SocketGemTree getSocketTree() {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final GameManager gameManager = plugin.getGameManager();
		final SocketGemManager socketGemManager = gameManager.getSocketManager();
		
		return socketGemManager.getSocketGemTree(getGem());
	}
}
