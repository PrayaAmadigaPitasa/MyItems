package net.ardeus.myitems.listener.support;

import java.util.List;

import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.SlotType;
import com.praya.agarthalib.utility.EntityUtil;
import com.praya.agarthalib.utility.MathUtil;
import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMobSpawnEvent;
import io.lumine.xikage.mythicmobs.mobs.MythicMob;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.item.ItemGenerator;
import net.ardeus.myitems.item.ItemSet;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemGeneratorManager;
import net.ardeus.myitems.manager.game.ItemManager;
import net.ardeus.myitems.manager.game.ItemSetManager;

public class ListenerMythicMobSpawn extends HandlerEvent implements Listener {
	
	public ListenerMythicMobSpawn(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void eventMythicMobSpawn(MythicMobSpawnEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final ItemManager itemManager = gameManager.getItemManager();
		final ItemGeneratorManager itemGeneratorManager = gameManager.getItemGeneratorManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final MythicMob mythicMob = event.getMobType();
		final Entity entity = event.getEntity();
		final List<String> equipment = mythicMob.getEquipment();
		
		if (EntityUtil.isLivingEntity(entity)) {
			final LivingEntity livingEntity = EntityUtil.parseLivingEntity(entity);
			
			for (String lineEquipment : equipment) {
				final String[] args = lineEquipment.split(" ");
				final String textSlot = args[0];
				final Slot slot = Slot.get(textSlot);
				
				if (slot != null) {
					if (args.length > 1) {
						final String key = args[1];
						
						if (key.equalsIgnoreCase("MyItems") || key.equalsIgnoreCase("MyItem")) {
							if (args.length > 2) {
								final String category = args[2];
								
								if (category.equalsIgnoreCase("Custom") || category.equalsIgnoreCase("Items") || category.equalsIgnoreCase("Item")) {
									if (args.length > 3) {
										final String name = args[3];
										final ItemStack item = itemManager.getItem(name);
										
										if (item != null) {
											final double chance;
											
											if (args.length > 4) {
												final String textChance = args[4];
												
												if (textChance.contains("~")) {
													final String[] componentsChance = textChance.split("~");
													final String textChance1 = componentsChance[0];
													final String textChance2 = componentsChance[1];
													
													if (!MathUtil.isNumber(textChance1) || !MathUtil.isNumber(textChance2)) {
														continue;
													} else {
														final double chance1 = MathUtil.parseDouble(textChance1);
														final double chance2 = MathUtil.parseDouble(textChance2);
														
														chance = MathUtil.valueBetween(chance1, chance2);
													}
												} else {
													if (!MathUtil.isNumber(textChance)) {
														continue;
													} else {
														chance = MathUtil.roundNumber(MathUtil.parseDouble(textChance));
													}
												}
											} else {
												chance = 1;
											}
											
											if (MathUtil.chanceOf(chance, 1)) {
												Bridge.getBridgeEquipment().setEquipment(livingEntity, item, slot);
											}
										}
									}
								} else if (category.equalsIgnoreCase("Generator") || category.equalsIgnoreCase("Generate") || category.equalsIgnoreCase("Gen")) {
									if (args.length > 3) {
										final String textGenerator = args[3];
										final ItemGenerator itemGenerator = itemGeneratorManager.getItemGenerator(textGenerator);
										
										if (itemGenerator != null) {
											final SlotType slotType = slot.getType();
											final Slot limit = slotType.equals(SlotType.ARMOR) ? slot : null;
											final ItemStack item = itemGenerator.generateItem(limit);
											
											if (item != null) {
												final double chance;
												
												if (args.length > 4) {
													final String textChance = args[4];
													
													if (textChance.contains("~")) {
														final String[] componentsChance = textChance.split("~");
														final String textChance1 = componentsChance[0];
														final String textChance2 = componentsChance[1];
														
														if (!MathUtil.isNumber(textChance1) || !MathUtil.isNumber(textChance2)) {
															continue;
														} else {
															final double chance1 = MathUtil.parseDouble(textChance1);
															final double chance2 = MathUtil.parseDouble(textChance2);
															
															chance = MathUtil.valueBetween(chance1, chance2);
														}
													} else {
														if (!MathUtil.isNumber(textChance)) {
															continue;
														} else {
															chance = MathUtil.roundNumber(MathUtil.parseDouble(textChance));
														}
													}
												} else {
													chance = 1;
												}
												
												if (MathUtil.chanceOf(chance, 1)) {
													Bridge.getBridgeEquipment().setEquipment(livingEntity, item, slot);
												}
											}
										}
									}
								} else if (category.equalsIgnoreCase("Set") || category.equalsIgnoreCase("SetItem") || category.equalsIgnoreCase("ItemSet")) {
									if (args.length > 3) {
										final String componentId = args[3];
										final ItemSet itemSet = itemSetManager.getItemSetByComponentId(componentId);
										
										if (itemSet != null) {
											final ItemStack item = itemSet.generateItem(componentId);
										
											if (item != null) {
												final double chance;
												
												if (args.length > 4) {
													final String textChance = args[4];
													
													if (textChance.contains("~")) {
														final String[] componentsChance = textChance.split("~");
														final String textChance1 = componentsChance[0];
														final String textChance2 = componentsChance[1];
														
														if (!MathUtil.isNumber(textChance1) || !MathUtil.isNumber(textChance2)) {
															continue;
														} else {
															final double chance1 = MathUtil.parseDouble(textChance1);
															final double chance2 = MathUtil.parseDouble(textChance2);
															
															chance = MathUtil.valueBetween(chance1, chance2);
														}
													} else {
														if (!MathUtil.isNumber(textChance)) {
															continue;
														} else {
															chance = MathUtil.roundNumber(MathUtil.parseDouble(textChance));
														}
													}
												} else {
													chance = 1;
												}
												
												if (MathUtil.chanceOf(chance, 1)) {
													Bridge.getBridgeEquipment().setEquipment(livingEntity, item, slot);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}