package net.ardeus.myitems.listener.support;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import api.praya.combatstamina.builder.event.PlayerStaminaRegenChangeEvent;
import api.praya.combatstamina.builder.event.PlayerStaminaRegenChangeEvent.StaminaRegenModifierEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.socket.SocketGemProperties;

public class ListenerPlayerStaminaRegenChange extends HandlerEvent implements Listener {

	public ListenerPlayerStaminaRegenChange(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void eventPlayerStaminaMaxChange(PlayerStaminaRegenChangeEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final StaminaRegenModifierEnum staminaRegenType = StaminaRegenModifierEnum.BONUS;
			final LoreStatsArmor statsBuild = statsManager.getLoreStatsArmor(player);
			final SocketGemProperties socketBuild = socketManager.getSocketProperties(player);
			final double staminaRegenStats = statsBuild.getStaminaRegen();
			final double staminaRegenSocket = socketBuild.getStaminaRegen();
			final double staminaRegenBase = event.getOriginalStaminaRegen(staminaRegenType);
			final double staminaRegenResult = staminaRegenStats + staminaRegenSocket + staminaRegenBase;
			
			event.setStaminaRegen(staminaRegenType, staminaRegenResult);
		}
	}
}