package net.ardeus.myitems.listener.support;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import api.praya.combatstamina.builder.event.PlayerStaminaMaxChangeEvent;
import api.praya.combatstamina.builder.event.PlayerStaminaMaxChangeEvent.StaminaMaxModifierEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.socket.SocketGemProperties;

public class ListenerPlayerStaminaMaxChange extends HandlerEvent implements Listener {

	public ListenerPlayerStaminaMaxChange(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void eventPlayerStaminaMaxChange(PlayerStaminaMaxChangeEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final StaminaMaxModifierEnum staminaMaxType = StaminaMaxModifierEnum.BONUS;
			final LoreStatsArmor statsBuild = statsManager.getLoreStatsArmor(player);
			final SocketGemProperties socketBuild = socketManager.getSocketProperties(player);
			final double staminaMaxStats = statsBuild.getStaminaMax();
			final double staminaMaxSocket = socketBuild.getStaminaMax();
			final double staminaMaxBase = event.getOriginalMaxStamina(staminaMaxType);
			final double staminaMaxResult = staminaMaxStats + staminaMaxSocket + staminaMaxBase;
			
			event.setMaxStamina(staminaMaxType, staminaMaxResult);
		}
	}
}
