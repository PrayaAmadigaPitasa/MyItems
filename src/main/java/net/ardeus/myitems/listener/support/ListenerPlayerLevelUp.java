package net.ardeus.myitems.listener.support;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import com.sucy.skill.api.event.PlayerLevelUpEvent;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

public class ListenerPlayerLevelUp extends HandlerEvent implements Listener {

	public ListenerPlayerLevelUp(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void eventPlayerLevelUp(PlayerLevelUpEvent event) {
		final Player player = event.getPlayerData().getPlayer();
		
		TriggerSupportUtil.updateSupport(player);
	}
}
