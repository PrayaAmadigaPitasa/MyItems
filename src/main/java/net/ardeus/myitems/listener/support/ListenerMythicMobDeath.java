package net.ardeus.myitems.listener.support;

import java.util.List;
import java.util.Optional;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMobDeathEvent;
import io.lumine.xikage.mythicmobs.drops.MythicDropTable;
import io.lumine.xikage.mythicmobs.mobs.MythicMob;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.item.ItemGenerator;
import net.ardeus.myitems.item.ItemSet;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemGeneratorManager;
import net.ardeus.myitems.manager.game.ItemManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.socket.SocketGem;
import net.ardeus.myitems.socket.SocketGemTree;

public class ListenerMythicMobDeath extends HandlerEvent implements Listener {
	
	public ListenerMythicMobDeath(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void eventMythicMobDeath(MythicMobDeathEvent event) {
		final MythicMob mythicMob = event.getMobType();
		final Location location = event.getEntity().getLocation();
		final List<String> drops = mythicMob.getDrops();
		
		for (String lineDrops : drops) {
			final String[] args = lineDrops.split(" ");
			
			if (args.length > 0) {
				final String key = args[0];
				
				if (args.length == 1) {
					final Optional<MythicDropTable> mythicDropTable = MythicMobs.inst().getDropManager().getDropTable(key);
					
					if (mythicDropTable.isPresent()) {
						final List<String> dropTable = mythicDropTable.get().strDropItems;
						
						for (String drop : dropTable) {
							dropItems(drop, location);
						}
					}
				} else {
					dropItems(lineDrops, location);
				}
			}
		}
	}
	
	private final void dropItems(String drop, Location location) {
		final GameManager gameManager = plugin.getGameManager();
		final ItemManager itemManager = gameManager.getItemManager();
		final ItemGeneratorManager itemGeneratorManager = gameManager.getItemGeneratorManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final String[] args = drop.split(" ");
		final String key = args[0];
		
		if (key.equalsIgnoreCase("MyItems") || key.equalsIgnoreCase("MyItem")) {
			if (args.length > 1) {
				final String category = args[1];
				
				if (category.equalsIgnoreCase("Custom") || category.equalsIgnoreCase("Items") || category.equalsIgnoreCase("Item")) {
					if (args.length > 2) {
						final String name = args[2];
						final ItemStack item = itemManager.getItem(name);
						
						if (item != null) {
							final int amount;
							final double chance;
							
							if (args.length > 3) {
								final String textAmount = args[3];
								
								if (textAmount.contains("~")) {
									final String[] componentsAmount = textAmount.split("~");
									final String textAmount1 = componentsAmount[0];
									final String textAmount2 = componentsAmount[1];
									
									if (!MathUtil.isNumber(textAmount1) || !MathUtil.isNumber(textAmount2)) {
										return;
									} else {
										final int amount1 = MathUtil.parseInteger(textAmount1);
										final int amount2 = MathUtil.parseInteger(textAmount2); 
										
										amount = (int) MathUtil.valueBetween(amount1, amount2);
									}
								} else {
									if (!MathUtil.isNumber(textAmount)) {
										return;
									} else {
										amount = MathUtil.parseInteger(textAmount);
									}
								}
							} else {
								amount = 1;
							}
							
							if (args.length > 4) {
								final String textChance = args[4];
								
								if (textChance.contains("~")) {
									final String[] componentsChance = textChance.split("~");
									final String textChance1 = componentsChance[0];
									final String textChance2 = componentsChance[1];
									
									if (!MathUtil.isNumber(textChance1) || !MathUtil.isNumber(textChance2)) {
										return;
									} else {
										final double chance1 = MathUtil.parseDouble(textChance1);
										final double chance2 = MathUtil.parseDouble(textChance2);
										
										chance = MathUtil.valueBetween(chance1, chance2);
									}
								} else {
									if (!MathUtil.isNumber(textChance)) {
										return;
									} else {
										chance = MathUtil.roundNumber(MathUtil.parseDouble(textChance));
									}
								}
							} else {
								chance = 1;
							}
							
							EquipmentUtil.setAmount(item, amount);
							
							if (MathUtil.chanceOf(chance, 1)) {
								final World world = location.getWorld();
								
								world.dropItem(location, item);
							}
						}
					}
				} else if (category.equalsIgnoreCase("Generator") || category.equalsIgnoreCase("Generate") || category.equalsIgnoreCase("Gen")) {
					if (args.length > 2) {
						final String textGenerator = args[2];
						final ItemGenerator itemGenerator = itemGeneratorManager.getItemGenerator(textGenerator);
						
						if (itemGenerator != null) {
							final int loop;
							final int amount;
							final double chance;
							
							if (args.length > 3) {
								final String textLoop = args[3];
								
								if (textLoop.contains("~")) {
									final String[] componentsLoop = textLoop.split("~");
									final String textLoop1 = componentsLoop[0];
									final String textLoop2 = componentsLoop[1];
									
									if (!MathUtil.isNumber(textLoop1) || !MathUtil.isNumber(textLoop2)) {
										return;
									} else {
										final int loop1 = MathUtil.parseInteger(textLoop1);
										final int loop2 = MathUtil.parseInteger(textLoop2); 
										
										loop = (int) MathUtil.valueBetween(loop1, loop2);
									}
								} else {
									if (!MathUtil.isNumber(textLoop)) {
										return;
									} else {
										loop = MathUtil.parseInteger(textLoop);
									}
								}
							} else {
								loop = 1;
							}
							
							if (args.length > 4) {
								final String textAmount = args[4];
								
								if (textAmount.contains("~")) {
									final String[] componentsAmount = textAmount.split("~");
									final String textAmount1 = componentsAmount[0];
									final String textAmount2 = componentsAmount[1];
									
									if (!MathUtil.isNumber(textAmount1) || !MathUtil.isNumber(textAmount2)) {
										return;
									} else {
										final int amount1 = MathUtil.parseInteger(textAmount1);
										final int amount2 = MathUtil.parseInteger(textAmount2); 
										
										amount = (int) MathUtil.valueBetween(amount1, amount2);
									}
								} else {
									if (!MathUtil.isNumber(textAmount)) {
										return;
									} else {
										amount = MathUtil.parseInteger(textAmount);
									}
								}
							} else {
								amount = 1;
							}
							
							if (args.length > 5) {
								final String textChance = args[5];
								
								if (textChance.contains("~")) {
									final String[] componentsChance = textChance.split("~");
									final String textChance1 = componentsChance[0];
									final String textChance2 = componentsChance[1];
									
									if (!MathUtil.isNumber(textChance1) || !MathUtil.isNumber(textChance2)) {
										return;
									} else {
										final double chance1 = MathUtil.parseDouble(textChance1);
										final double chance2 = MathUtil.parseDouble(textChance2);
										
										chance = MathUtil.valueBetween(chance1, chance2);
									}
								} else {
									if (!MathUtil.isNumber(textChance)) {
										return;
									} else {
										chance = MathUtil.roundNumber(MathUtil.parseDouble(textChance));
									}
								}
							} else {
								chance = 1;
							}
							
							for (int t = 0; t < loop; t++) {
								final ItemStack item = itemGenerator.generateItem();
								
								EquipmentUtil.setAmount(item, amount);
								
								if (MathUtil.chanceOf(chance, 1)) {
									final World world = location.getWorld();
									
									world.dropItem(location, item);
								}
							}
						}
					}
				} else if (category.equalsIgnoreCase("Set") || category.equalsIgnoreCase("SetItem") || category.equalsIgnoreCase("ItemSet")) {
					if (args.length > 2) {
						final String componentId = args[2];
						final ItemSet itemSet = itemSetManager.getItemSetByComponentId(componentId);
						
						if (itemSet != null) {
							final int loop;
							final int amount;
							final double chance;
							
							if (args.length > 3) {
								final String textLoop = args[3];
								
								if (textLoop.contains("~")) {
									final String[] componentsLoop = textLoop.split("~");
									final String textLoop1 = componentsLoop[0];
									final String textLoop2 = componentsLoop[1];
									
									if (!MathUtil.isNumber(textLoop1) || !MathUtil.isNumber(textLoop2)) {
										return;
									} else {
										final int loop1 = MathUtil.parseInteger(textLoop1);
										final int loop2 = MathUtil.parseInteger(textLoop2); 
										
										loop = (int) MathUtil.valueBetween(loop1, loop2);
									}
								} else {
									if (!MathUtil.isNumber(textLoop)) {
										return;
									} else {
										loop = MathUtil.parseInteger(textLoop);
									}
								}
							} else {
								loop = 1;
							}
							
							if (args.length > 4) {
								final String textAmount = args[4];
								
								if (textAmount.contains("~")) {
									final String[] componentsAmount = textAmount.split("~");
									final String textAmount1 = componentsAmount[0];
									final String textAmount2 = componentsAmount[1];
									
									if (!MathUtil.isNumber(textAmount1) || !MathUtil.isNumber(textAmount2)) {
										return;
									} else {
										final int amount1 = MathUtil.parseInteger(textAmount1);
										final int amount2 = MathUtil.parseInteger(textAmount2); 
										
										amount = (int) MathUtil.valueBetween(amount1, amount2);
									}
								} else {
									if (!MathUtil.isNumber(textAmount)) {
										return;
									} else {
										amount = MathUtil.parseInteger(textAmount);
									}
								}
							} else {
								amount = 1;
							}
							
							if (args.length > 5) {
								final String textChance = args[5];
								
								if (textChance.contains("~")) {
									final String[] componentsChance = textChance.split("~");
									final String textChance1 = componentsChance[0];
									final String textChance2 = componentsChance[1];
									
									if (!MathUtil.isNumber(textChance1) || !MathUtil.isNumber(textChance2)) {
										return;
									} else {
										final double chance1 = MathUtil.parseDouble(textChance1);
										final double chance2 = MathUtil.parseDouble(textChance2);
										
										chance = MathUtil.valueBetween(chance1, chance2);
									}
								} else {
									if (!MathUtil.isNumber(textChance)) {
										return;
									} else {
										chance = MathUtil.roundNumber(MathUtil.parseDouble(textChance));
									}
								}
							} else {
								chance = 1;
							}
							
							for (int t = 0; t < loop; t++) {
								final ItemStack item = itemSet.generateItem(componentId);
								
								EquipmentUtil.setAmount(item, amount);
								
								if (MathUtil.chanceOf(chance, 1)) {
									final World world = location.getWorld();
									
									world.dropItem(location, item);
								}
							}
						}
					}
				} else if (category.equalsIgnoreCase("Gems") || category.equalsIgnoreCase("Gem")) {
					if (args.length > 2) {
						final String textSocket = args[2];
						final SocketGemTree socketTree = socketManager.getSocketGemTree(textSocket);
						
						if (socketTree != null) {
							final int maxGrade = socketTree.getMaxGrade();
							final int grade;
							final int amount;
							final double chance;
							
							if (args.length > 3) {
								final String textGrade = args[3];
								
								if (textGrade.contains("~")) {
									final String[] componentsGrade = textGrade.split("~");
									final String textGrade1 = componentsGrade[0];
									final String textGrade2 = componentsGrade[1];
									
									if (!MathUtil.isNumber(textGrade1) || !MathUtil.isNumber(textGrade2)) {
										return;
									} else {
										final int grade1 = MathUtil.parseInteger(textGrade1);
										final int grade2 = MathUtil.parseInteger(textGrade2);
										final int rawGrade = (int) MathUtil.valueBetween(grade1, grade2);
										
										grade = MathUtil.limitInteger(rawGrade, 1, maxGrade);
									}
								} else {
									if (!MathUtil.isNumber(textGrade)) {
										return;
									} else {
										final int rawGrade = MathUtil.parseInteger(textGrade);
										
										grade = MathUtil.limitInteger(rawGrade, 1, maxGrade);
									}
								}
							} else {
								grade = 1;
							}
							
							if (args.length > 4) {
								final String textAmount = args[4];
								
								if (textAmount.contains("~")) {
									final String[] componentsAmount = textAmount.split("~");
									final String textAmount1 = componentsAmount[0];
									final String textAmount2 = componentsAmount[1];
									
									if (!MathUtil.isNumber(textAmount1) || !MathUtil.isNumber(textAmount2)) {
										return;
									} else {
										final int amount1 = MathUtil.parseInteger(textAmount1);
										final int amount2 = MathUtil.parseInteger(textAmount2); 
										
										amount = (int) MathUtil.valueBetween(amount1, amount2);
									}
								} else {
									if (!MathUtil.isNumber(textAmount)) {
										return;
									} else {
										amount = MathUtil.parseInteger(textAmount);
									}
								}
							} else {
								amount = 1;
							}
							
							if (args.length > 5) {
								final String textChance = args[5];
								
								if (textChance.contains("~")) {
									final String[] componentsChance = textChance.split("~");
									final String textChance1 = componentsChance[0];
									final String textChance2 = componentsChance[1];
									
									if (!MathUtil.isNumber(textChance1) || !MathUtil.isNumber(textChance2)) {
										return;
									} else {
										final double chance1 = MathUtil.parseDouble(textChance1);
										final double chance2 = MathUtil.parseDouble(textChance2);
										
										chance = MathUtil.valueBetween(chance1, chance2);
									}
								} else {
									if (!MathUtil.isNumber(textChance)) {
										return;
									} else {
										chance = MathUtil.roundNumber(MathUtil.parseDouble(textChance));
									}
								}
							} else {
								chance = 1;
							}
							
							final SocketGem socketGem = socketTree.getSocketGem(grade);
							
							if (socketGem != null) {
								final ItemStack item = socketGem.getItem();
								
								EquipmentUtil.setAmount(item, amount);
								
								if (MathUtil.chanceOf(chance, 1)) {
									final World world = location.getWorld();
									
									world.dropItem(location, item);
								}
							}
						}
					}
				}
			}
		}
	}
}
