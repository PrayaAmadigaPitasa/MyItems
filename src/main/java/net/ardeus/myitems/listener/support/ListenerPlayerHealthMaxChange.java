package net.ardeus.myitems.listener.support;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import api.praya.agarthalib.builder.event.PlayerHealthMaxChangeEvent;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.socket.SocketGemProperties;

public class ListenerPlayerHealthMaxChange extends HandlerEvent implements Listener {

	public ListenerPlayerHealthMaxChange(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void eventPlayerHealthMaxChange(PlayerHealthMaxChangeEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final boolean enableMaxHealth = mainConfig.isStatsEnableMaxHealth();
		
		if (!event.isCancelled() && enableMaxHealth) {
			final Player player = event.getPlayer();
			final LoreStatsArmor statsBuild = statsManager.getLoreStatsArmor(player);
			final SocketGemProperties socketBuild = socketManager.getSocketProperties(player);
			final double healthStats = statsBuild.getHealth();
			final double healthSocket = socketBuild.getHealth();
			final double healthBase = event.getMaxHealth();
			final double healthResult = healthStats + healthSocket + healthBase;
			
			event.setMaxHealth(healthResult);
		}
	}
}
