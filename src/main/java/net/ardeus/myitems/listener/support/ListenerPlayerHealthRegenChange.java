package net.ardeus.myitems.listener.support;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import api.praya.lifeessence.builder.event.PlayerHealthRegenChangeEvent;
import api.praya.lifeessence.builder.event.PlayerHealthRegenChangeEvent.HealthRegenModifierEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.socket.SocketGemProperties;

public class ListenerPlayerHealthRegenChange extends HandlerEvent implements Listener {

	public ListenerPlayerHealthRegenChange(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void eventPlayerHealthRegenChange(PlayerHealthRegenChangeEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final HealthRegenModifierEnum healthRegenType = HealthRegenModifierEnum.BONUS;
			final LoreStatsArmor statsBuild = statsManager.getLoreStatsArmor(player);
			final SocketGemProperties socketBuild = socketManager.getSocketProperties(player);
			final double healthRegenStats = statsBuild.getHealthRegen();
			final double healthRegenSocket = socketBuild.getHealthRegen();
			final double healthRegenBase = event.getOriginalHealthRegen(healthRegenType);
			final double healthRegenResult = healthRegenStats + healthRegenSocket + healthRegenBase;
			
			event.setHealthRegen(healthRegenType, healthRegenResult);
		}
	}
}
