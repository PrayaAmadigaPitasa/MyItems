package net.ardeus.myitems.listener.custom;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import api.praya.agarthalib.builder.event.MenuOpenEvent;
import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuExecutor;
import core.praya.agarthalib.builder.menu.MenuGUI;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.menu.MenuSocket;
import net.ardeus.myitems.menu.MenuStats;

import com.praya.agarthalib.utility.SenderUtil;

public class ListenerMenuOpen extends HandlerEvent implements Listener {

	public ListenerMenuOpen(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
    public void menuOpenEvent(MenuOpenEvent event) {
		final Menu menu = event.getMenu();
		final Player player = event.getPlayer();
		final String id = menu.getID();
		
		if (!event.isCancelled()) {
			if (menu instanceof MenuGUI) {
				final MenuGUI menuGUI = (MenuGUI) menu;
				
				if (id.startsWith("MyItems")) {
					SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
				}
				
				if (id.equalsIgnoreCase("MyItems Socket")) {
					final MenuExecutor executor = menu.getExecutor();
					
					if (executor instanceof MenuSocket) {
						final MenuSocket executorSocket = (MenuSocket) executor;
						
						executorSocket.updateSocketMenu(menuGUI, player);
					}
				} else if (id.equalsIgnoreCase("MyItems Stats")) {
					final MenuExecutor executor = menu.getExecutor();
					
					if (executor instanceof MenuStats) {
						final MenuStats executorStats = (MenuStats) executor;
						
						executorStats.updateStatsMenu(menuGUI, player);
					}
				}
			}
		}
	}
}
