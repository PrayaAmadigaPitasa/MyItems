package net.ardeus.myitems.listener.custom;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.enums.branch.ProjectileEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.event.PowerShootCastEvent;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsOption;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.player.PlayerPowerManager;
import net.ardeus.myitems.player.PlayerPowerCooldown;

import com.praya.agarthalib.utility.CombatUtil;
import com.praya.agarthalib.utility.MathUtil;

public class ListenerPowerShootCast extends HandlerEvent implements Listener {
	
	public ListenerPowerShootCast(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void eventPowerShootCast(PowerShootCastEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final PlayerPowerManager playerPowerManager = plugin.getPlayerManager().getPlayerPowerManager();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final ItemStack item = event.getItem();
			final ProjectileEnum projectile = event.getProjectile();
			final double cooldown = event.getCooldown();
			final double speed = event.getSpeed();
			final long timeCooldown = MathUtil.convertSecondsToMilis(cooldown);
			final int durability = (int) statsManager.getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.CURRENT);
			final PlayerPowerCooldown powerCooldown = playerPowerManager.getPlayerPowerCooldown(player);
			
			CombatUtil.launchProjectile(player, projectile, speed, false, true);
			
			if (timeCooldown > 0) {
				powerCooldown.setPowerShootCooldown(projectile, timeCooldown);
			}
			
			if (!statsManager.durability(player, item, durability, true)) {
				statsManager.sendBrokenCode(player, Slot.MAINHAND);
			}
		}
	}	
}