package net.ardeus.myitems.listener.custom;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.ParticleEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.event.CombatCriticalDamageEvent;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.plugin.LanguageManager;

import com.praya.agarthalib.utility.EntityUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;

public class ListenerCombatCriticalDamage extends HandlerEvent implements Listener {
	
	public ListenerCombatCriticalDamage(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void eventCombatCriticalDamage(CombatCriticalDamageEvent event) {
		final LanguageManager lang = plugin.getPluginManager().getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			final LivingEntity attacker = event.getAttacker();
			final LivingEntity victims = event.getVictims();
			final String criticalAttackType = mainConfig.getModifierCriticalAttackType();
			
			if (criticalAttackType != null) {
				if (criticalAttackType.equalsIgnoreCase("Effect")) {
					final Location loc = victims.getEyeLocation();
					final Collection<Player> players = PlayerUtil.getNearbyPlayers(loc, mainConfig.getEffectRange());
					
					Bridge.getBridgeParticle().playParticle(players, ParticleEnum.EXPLOSION_LARGE, loc, 3, 0.1, 0.1, 0.1, 0.5F);
					Bridge.getBridgeSound().playSound(players, loc, SoundEnum.ENTITY_GENERIC_EXPLODE, 0.5F, 1F);
				} else if (criticalAttackType.equalsIgnoreCase("Messages")) {
					if (EntityUtil.isPlayer(attacker)) {
						final Player player = EntityUtil.parsePlayer(attacker);
						final String message = lang.getText(player, "Attack_Critical");
						
						SenderUtil.sendMessage(player, message);
					}
				}
			}
		}
	}
}
