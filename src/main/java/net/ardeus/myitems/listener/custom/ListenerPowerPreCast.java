package net.ardeus.myitems.listener.custom;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.ProjectileEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.event.PowerCommandCastEvent;
import net.ardeus.myitems.event.PowerPreCastEvent;
import net.ardeus.myitems.event.PowerShootCastEvent;
import net.ardeus.myitems.event.PowerSpecialCastEvent;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsOption;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PowerCommandManager;
import net.ardeus.myitems.manager.game.PowerShootManager;
import net.ardeus.myitems.manager.game.PowerSpecialManager;
import net.ardeus.myitems.manager.game.RequirementManager;
import net.ardeus.myitems.manager.player.PlayerManager;
import net.ardeus.myitems.manager.player.PlayerPowerManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.player.PlayerPowerCooldown;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerCommand;
import net.ardeus.myitems.power.PowerSpecial;
import net.ardeus.myitems.power.PowerType;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.agarthalib.utility.TimeUtil;

public class ListenerPowerPreCast extends HandlerEvent implements Listener {
	
	public ListenerPowerPreCast(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void eventPowerPreCast(PowerPreCastEvent event) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final PowerCommandManager powerCommandManager = gameManager.getPowerCommandManager();
		final PowerShootManager powerShootManager = gameManager.getPowerShootManager();
		final PowerSpecialManager powerSpecialManager = gameManager.getPowerSpecialManager();
		final PlayerPowerManager playerPowerManager = playerManager.getPlayerPowerManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final PowerType power = event.getPower();
			final PowerClickType click = event.getClick();
			final ItemStack item = event.getItem();
			final String lore = event.getLore();
			final int durability = (int) statsManager.getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.CURRENT);
			final PlayerPowerCooldown powerCooldown = playerPowerManager.getPlayerPowerCooldown(player);
			final String[] cooldownList = lore.split(MainConfig.KEY_COOLDOWN);
			
			if (requirementManager.isAllowed(player, item)) {
				if (cooldownList.length > 1) {
					final String keyCooldown = cooldownList[1].replace(mainConfig.getPowerColorCooldown(), "");
					
					if (!MathUtil.isNumber(keyCooldown)) {
						return;
					} else {
						final double cooldown = Math.max(0, Double.valueOf(keyCooldown));
						
						if (!statsManager.checkDurability(item, durability)) {
							statsManager.sendBrokenCode(player, Slot.MAINHAND, false);
						} else {
							final boolean enableMessageCooldown = mainConfig.isPowerEnableMessageCooldown();
							
							if (power.equals(PowerType.COMMAND)) {
								final PowerCommand powerCommand = powerCommandManager.getPowerCommandByLore(lore);
								
								if (powerCommand != null) {
									if (powerCooldown.isPowerCommandCooldown(powerCommand)) {
										if (enableMessageCooldown) {
											final String powerCommandId = powerCommand.getId();
											final long timeLeft = powerCooldown.getPowerCommandTimeLeft(powerCommand);
											final MessageBuild message = lang.getMessage("Power_Command_Cooldown");
											final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
											
											mapPlaceholder.put("power", powerCommandId);
											mapPlaceholder.put("time", TimeUtil.getTextTime(timeLeft));
		
											message.sendMessage(player, mapPlaceholder);
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										}
									} else {
										final PowerCommandCastEvent powerCommandCastEvent = new PowerCommandCastEvent(player, power, click, item, lore, powerCommand, cooldown);
										
										ServerEventUtil.callEvent(powerCommandCastEvent);
									}
								}
							} else if (power.equals(PowerType.SHOOT)) {
								final ProjectileEnum projectileEnum = powerShootManager.getProjectileEnumByLore(lore);
									
								if (projectileEnum != null) {
									if (powerCooldown.isPowerShootCooldown(projectileEnum)) {
										if (enableMessageCooldown) {
											final String powerShootKeyLore = powerShootManager.getPowerShootKeyLore(projectileEnum);
											final long timeLeft = powerCooldown.getPowerShootTimeLeft(projectileEnum);
											final MessageBuild message = lang.getMessage("Power_Shoot_Cooldown");
											final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
											
											mapPlaceholder.put("power", powerShootKeyLore);
											mapPlaceholder.put("time", TimeUtil.getTextTime(timeLeft));
		
											message.sendMessage(player, mapPlaceholder);
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										}
									} else {
										final PowerShootCastEvent powerShootCastEvent = new PowerShootCastEvent(player, power, click, item, lore, projectileEnum, cooldown);
										
										ServerEventUtil.callEvent(powerShootCastEvent);
									}
								}
							} else if (power.equals(PowerType.SPECIAL)) {
								final PowerSpecial powerSpecial = powerSpecialManager.getPowerSpecialByLore(lore);
								
								if (powerSpecial != null) {
									if (powerCooldown.isPowerSpecialCooldown(powerSpecial)) {
										if (enableMessageCooldown) {
											final long timeLeft = powerCooldown.getPowerSpecialTimeLeft(powerSpecial);
											final MessageBuild message = lang.getMessage("Power_Special_Cooldown");
											final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
											
											mapPlaceholder.put("power", powerSpecial.getId());
											mapPlaceholder.put("time", TimeUtil.getTextTime(timeLeft));
		
											message.sendMessage(player, mapPlaceholder);
											SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
											return;
										}
									} else {
										final PowerSpecialCastEvent powerSpecialCastEvent = new PowerSpecialCastEvent(player, power, click, item, lore, powerSpecial, cooldown);
										
										ServerEventUtil.callEvent(powerSpecialCastEvent);
									}
								}
							}
						}
					}
				}		
			}
		}
	}
}
