package net.ardeus.myitems.listener.custom;

import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.event.PowerCommandCastEvent;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsOption;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.player.PlayerManager;
import net.ardeus.myitems.manager.player.PlayerPowerManager;
import net.ardeus.myitems.player.PlayerPowerCooldown;
import net.ardeus.myitems.power.PowerCommand;

import com.praya.agarthalib.utility.CommandUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public class ListenerPowerCommandCast extends HandlerEvent implements Listener {
	
	public ListenerPowerCommandCast(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void eventPowerCommandCast(PowerCommandCastEvent event) {
		final PlayerManager playerManager = plugin.getPlayerManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerPowerManager playerPowerManager = playerManager.getPlayerPowerManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final ItemStack item = event.getItem();
			final PowerCommand powerCommand = event.getPowerCommand();
			final PlayerPowerCooldown powerCooldown = playerPowerManager.getPlayerPowerCooldown(player);
			final boolean consume = powerCommand.isConsume();
			final double cooldown = event.getCooldown();
			final long timeCooldown = MathUtil.convertSecondsToMilis(cooldown);
			final int durability = (int) statsManager.getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.CURRENT);
			final List<String> commandOP = powerCommand.getCommandOP();
			final List<String> commandConsole = powerCommand.getCommandConsole();
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			mapPlaceholder.put("player", player.getName());
			
			for (String command : commandOP) {
				
				command = TextUtil.placeholder(mapPlaceholder, command);
				command = TextUtil.placeholder(mapPlaceholder, command, "<", ">");
				command = TextUtil.hookPlaceholderAPI(player, command);
				
				CommandUtil.sudoCommand(player, command, true);
			}
			
			for (String command : commandConsole) {
				
				command = TextUtil.placeholder(mapPlaceholder, command);
				command = TextUtil.placeholder(mapPlaceholder, command, "<", ">");
				command = TextUtil.hookPlaceholderAPI(player, command);
				
				CommandUtil.consoleCommand(command);
			}				
			
			if (timeCooldown > 0) {
				powerCooldown.setPowerCommandCooldown(powerCommand, timeCooldown);
			}
			
			if (consume) {
				final int amount = item.getAmount();
				
				if (amount > 1) {
					EquipmentUtil.setAmount(item, amount-1);
				} else {
					Bridge.getBridgeEquipment().setEquipment(player, null, Slot.MAINHAND);
				}
			} else {
				if (!statsManager.durability(player, item, durability, true)) {
					statsManager.sendBrokenCode(player, Slot.MAINHAND);
				}
			}
		}
	}	
}