package net.ardeus.myitems.listener.custom;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import api.praya.agarthalib.builder.event.MenuCloseEvent;
import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuGUI;
import core.praya.agarthalib.builder.menu.MenuGUI.SlotCell;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.PlayerUtil;

public class ListenerMenuClose extends HandlerEvent implements Listener {

	public ListenerMenuClose(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority = EventPriority.NORMAL)
    public void menuCloseEvent(MenuCloseEvent event) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final Menu menu = event.getMenu();
		final Player player = event.getPlayer();
		final String id = menu.getID();
		
		if (menu instanceof MenuGUI) {
			final MenuGUI menuGUI = (MenuGUI) menu;
			
			if (id.equalsIgnoreCase("MyItems Socket")) {
				final Inventory inventory = menuGUI.getInventory();
				final SlotCell cellItemInput = SlotCell.B3;
				final SlotCell cellSocketInput = SlotCell.C3;
				final String headerItemInput = lang.getText(player, "Menu_Item_Header_Socket_Item_Input");
				final String headerSocketInput = lang.getText(player, "Menu_Item_Header_Socket_Socket_Input");
				final ItemStack itemItemInput = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, headerItemInput, 1);
				final ItemStack itemSocketInput = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, headerSocketInput, 1);
				final ItemStack itemItem = inventory.getItem(cellItemInput.getIndex());
				final ItemStack itemSocket = inventory.getItem(cellSocketInput.getIndex());
				
				if (!itemItem.isSimilar(itemItemInput)) {
					PlayerUtil.addItem(player, itemItem);
				}
				
				if (!itemSocket.isSimilar(itemSocketInput)) {
					PlayerUtil.addItem(player, itemSocket);
				}
			}
		}
	}
}
