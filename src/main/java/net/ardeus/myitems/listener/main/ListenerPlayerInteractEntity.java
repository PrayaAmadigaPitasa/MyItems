package net.ardeus.myitems.listener.main;

import java.util.Collection;

import org.bukkit.entity.Entity;
import org.bukkit.entity.ItemFrame;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

public class ListenerPlayerInteractEntity extends HandlerEvent implements Listener {
	
	public ListenerPlayerInteractEntity(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void eventPutItemFrame(PlayerInteractEntityEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final Entity entity = event.getRightClicked();
			final boolean enableGradeCalculation = mainConfig.isPassiveEnableGradeCalculation();
					
			if (entity instanceof ItemFrame) {
				final ItemStack itemMainHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
				final ItemStack itemOffHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.OFFHAND);
				final Collection<PassiveEffectEnum> mainPassiveEffects = passiveEffectManager.getPassiveEffects(itemMainHand);
				final Collection<PassiveEffectEnum> offPassiveEffects = passiveEffectManager.getPassiveEffects(itemOffHand);
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
										
						if (itemSetManager.isItemSet(itemMainHand) || itemSetManager.isItemSet(itemOffHand)) {
							itemSetManager.updateItemSet(player);
						}
						
						passiveEffectManager.reloadPassiveEffect(player, mainPassiveEffects, enableGradeCalculation);
						passiveEffectManager.reloadPassiveEffect(player, offPassiveEffects, enableGradeCalculation);
					}
				}.runTaskLater(plugin, 1);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void triggerSupport(PlayerInteractEntityEvent event) {
		if (!event.isCancelled()) {
			final Entity entity = event.getRightClicked();
					
			if (entity instanceof ItemFrame) {
				final Player player = event.getPlayer();
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						TriggerSupportUtil.updateSupport(player);
					}
				}.runTaskLater(plugin, 2);
			}
		}
	}
}