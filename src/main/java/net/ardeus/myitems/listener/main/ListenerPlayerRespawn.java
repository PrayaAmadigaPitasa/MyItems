package net.ardeus.myitems.listener.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

public class ListenerPlayerRespawn extends HandlerEvent implements Listener {
	
	public ListenerPlayerRespawn(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void triggerSupport(PlayerRespawnEvent event) {
		final Player player = event.getPlayer();
		
		TriggerSupportUtil.updateSupport(player);
	}
}
