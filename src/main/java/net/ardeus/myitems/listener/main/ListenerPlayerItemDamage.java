package net.ardeus.myitems.listener.main;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.bridge.unity.BridgeTagsNBT;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;

public class ListenerPlayerItemDamage extends HandlerEvent implements Listener {
	
	public ListenerPlayerItemDamage(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void itemDamageEvent(PlayerItemDamageEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		
		if (!event.isCancelled()) {
			final BridgeTagsNBT bridgeTagsNBT = Bridge.getBridgeTagsNBT();
			final ItemStack item = event.getItem();
			
			if (bridgeTagsNBT.isUnbreakable(item) || statsManager.hasLoreStats(item, LoreStatsEnum.DURABILITY)) {
				event.setCancelled(true);
			}
		}
	}
}
