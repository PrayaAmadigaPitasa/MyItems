package net.ardeus.myitems.listener.main;

import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.SlotType;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.event.PowerPreCastEvent;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsOption;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PowerManager;
import net.ardeus.myitems.manager.game.RequirementManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerType;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerEventUtil;
import com.praya.agarthalib.utility.TextUtil;

public class ListenerPlayerInteract extends HandlerEvent implements Listener {
	
	public ListenerPlayerInteract(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void checkRequirement(PlayerInteractEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PluginManager pluginManager = plugin.getPluginManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final Player player = event.getPlayer();
		final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
		
		if (EquipmentUtil.loreCheck(item)) {
			if (!requirementManager.isAllowedReqSoulBound(player, item)) {
				final String reqBound = requirementManager.getRequirementSoulBound(item);
				final String message = TextUtil.placeholder(lang.getText(player, "Requirement_Not_Allowed_Bound"), "Player", reqBound);
				
				event.setCancelled(true);
				SenderUtil.sendMessage(player, message);
				SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else if (!requirementManager.isAllowedReqPermission(player, item)) {
				final String reqPermission = requirementManager.getRequirementPermission(item);
				final String message = TextUtil.placeholder(lang.getText(player, "Requirement_Not_Allowed_Permission"), "Permission", reqPermission);
				
				event.setCancelled(true);
				SenderUtil.sendMessage(player, message);
				SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else if (!requirementManager.isAllowedReqLevel(player, item)) {
				final int reqLevel = requirementManager.getRequirementLevel(item);
				final String message = TextUtil.placeholder(lang.getText(player, "Requirement_Not_Allowed_Level"), "Level", String.valueOf(reqLevel));
				
				event.setCancelled(true);
				SenderUtil.sendMessage(player, message);
				SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else if (!requirementManager.isAllowedReqClass(player, item)) {
				final String reqClass = requirementManager.getRequirementClass(item);
				final String message = TextUtil.placeholder(lang.getText(player, "Requirement_Not_Allowed_Class"), "Class", reqClass);
				
				event.setCancelled(true);
				SenderUtil.sendMessage(player, message);
				SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void checkBound(PlayerInteractEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PluginManager pluginManager = plugin.getPluginManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (EquipmentUtil.loreCheck(item)) {
				final Integer lineUnbound = requirementManager.getLineRequirementSoulUnbound(item);
				
				if (lineUnbound != null) {
					final String loreBound = requirementManager.getTextSoulBound(player);
					final Integer lineOld = requirementManager.getLineRequirementSoulBound(item);
					final HashMap<String, String> map = new HashMap<String, String>();
					
					if (lineOld != null) {
						EquipmentUtil.removeLore(item, lineOld);
					}
					
					String message = lang.getText(player, "Item_Bound");
					
					map.put("item", EquipmentUtil.getDisplayName(item));
					map.put("player", player.getName());
					message = TextUtil.placeholder(map, message);
					
					requirementManager.setMetadataSoulbound(player, item);
					EquipmentUtil.setLore(item, lineUnbound, loreBound);
					SenderUtil.sendMessage(player, message);
					return;
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void triggerEquipmentChangeEvent(PlayerInteractEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final Action action = event.getAction();
		
		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			final Player player = event.getPlayer();
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (itemSetManager.isItemSet(item)) {
				final SlotType slotType = SlotType.getSlotType(item);
				
				if (slotType.equals(SlotType.ARMOR)) {
					new BukkitRunnable() {
						
						@Override
						public void run() {
							itemSetManager.updateItemSet(player);
						}
					}.runTaskLater(plugin, 0);
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void triggerSupport(PlayerInteractEvent event) {		
		final Action action = event.getAction();
		
		if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
			final Player player = event.getPlayer();
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			final SlotType slotType = SlotType.getSlotType(item);
			
			if (slotType.equals(SlotType.ARMOR)) {
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						TriggerSupportUtil.updateSupport(player);
					}
				}.runTaskLater(plugin, 2);
			}
		}
	}
	
	@EventHandler
	public void powerCheckEvent(PlayerInteractEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PowerManager powerManager = gameManager.getPowerManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final Player player = event.getPlayer();
		final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
		
		if (EquipmentUtil.loreCheck(item)) {
			final int durability = (int) statsManager.getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.CURRENT);
			
			if (statsManager.checkDurability(item, durability)) {
				final Action action = event.getAction();
				final PowerClickType click;
									
				if (action.equals(Action.LEFT_CLICK_AIR) || action.equals(Action.LEFT_CLICK_BLOCK)) {
					if (player.isSneaking()) {
						click = PowerClickType.SHIFT_LEFT;
						
					} else {
						click = PowerClickType.LEFT;
					}
				} else if (action.equals(Action.RIGHT_CLICK_AIR) || action.equals(Action.RIGHT_CLICK_BLOCK)) {
					if (player.isSneaking()) {
						click = PowerClickType.SHIFT_RIGHT;
						
					} else {
						click = PowerClickType.RIGHT;
					}
				} else {
					return;
				}
				
				final Integer line = powerManager.getLineClick(item, click);
				
				if (line != null) {
					final List<String> lores = EquipmentUtil.getLores(item); 
					final String lore = lores.get(line-1);
					final PowerType power = powerManager.getPowerTypeByLore(lore);
					
					if (power != null) {
						final PowerPreCastEvent powerPreCastEvent = new PowerPreCastEvent(player, power, click, item, lore);
						
						ServerEventUtil.callEvent(powerPreCastEvent);
					}
				}
			}
		}
	}
}