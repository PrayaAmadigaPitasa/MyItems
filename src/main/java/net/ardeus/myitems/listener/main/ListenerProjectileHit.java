package net.ardeus.myitems.listener.main;

import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

import com.praya.agarthalib.utility.ProjectileUtil;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;

public class ListenerProjectileHit extends HandlerEvent implements Listener {
	
	public ListenerProjectileHit(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		final Entity projectile = event.getEntity();
		
		if (ProjectileUtil.isDisappear(projectile)) {
			if (!projectile.isDead()) {
				projectile.remove();
			}
		}
	}
}