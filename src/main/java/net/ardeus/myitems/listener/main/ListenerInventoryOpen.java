package net.ardeus.myitems.listener.main;

import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;

public class ListenerInventoryOpen extends HandlerEvent implements Listener {

	public ListenerInventoryOpen(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void inventoryOpenEvent(InventoryOpenEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		
		if (!event.isCancelled()) {
			final Inventory inventory = event.getInventory();
			final HumanEntity human = event.getPlayer();
			
			if (human instanceof Player) {
				final Player player = (Player) human;
				
				itemSetManager.updateItemSet(player, false, inventory);
			}
		}
	}
}
