package net.ardeus.myitems.listener.main;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPhysicsEvent;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;

public class ListenerBlockPhysic extends HandlerEvent implements Listener {
	
	public ListenerBlockPhysic(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void eventBlockPhysic(BlockPhysicsEvent event) {
		if (!event.isCancelled()) {
			if (event.getBlock().hasMetadata("Anti_Block_Physic")) {
				event.setCancelled(true);
			}
		}
	}
}