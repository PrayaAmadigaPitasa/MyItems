package net.ardeus.myitems.listener.main;

import java.util.Collection;

import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

import com.praya.agarthalib.utility.EquipmentUtil;

public class ListenerPlayerDropItem extends HandlerEvent implements Listener {
	
	public ListenerPlayerDropItem(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void playerDropItemEvent(PlayerDropItemEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final Player player = event.getPlayer();
		
		if (!EquipmentUtil.isSolid(Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND))) {
			final Item drop = event.getItemDrop();
			final ItemStack item = drop.getItemStack();
			final boolean enableGradeCalculation = mainConfig.isPassiveEnableGradeCalculation();
			final Collection<PassiveEffectEnum> passiveEffects = passiveEffectManager.getPassiveEffects(item);
			
			new BukkitRunnable() {
				
				@Override
				public void run() {
					
					if (itemSetManager.isItemSet(item)) {
						itemSetManager.updateItemSet(player);
					}
					
					passiveEffectManager.reloadPassiveEffect(player, passiveEffects, enableGradeCalculation);
				}
			}.runTaskLater(plugin, 1);		
		}
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void triggerSupport(PlayerDropItemEvent event) {
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			
			new BukkitRunnable() {
				
				@Override
				public void run() {
					TriggerSupportUtil.updateSupport(player);
				}
			}.runTaskLater(plugin, 2);
		}
	}
}
