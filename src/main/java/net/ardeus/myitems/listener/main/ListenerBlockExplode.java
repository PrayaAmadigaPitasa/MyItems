package net.ardeus.myitems.listener.main;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;

import com.praya.agarthalib.utility.BlockUtil;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;

public class ListenerBlockExplode extends HandlerEvent implements Listener {
	
	public ListenerBlockExplode(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void onBlockExplode(BlockExplodeEvent event) {
		if (!event.isCancelled()) {
			if (BlockUtil.isSet(event.getBlock())) {
				BlockUtil.remove(event.getBlock());
			}
		}
	}
}
