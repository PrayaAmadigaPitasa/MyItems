package net.ardeus.myitems.listener.main;

import java.util.HashMap;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.main.MainBridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.utility.EntityUtil;
import core.praya.agarthalib.utility.EquipmentUtil;
import core.praya.agarthalib.utility.MathUtil;
import core.praya.agarthalib.utility.SenderUtil;
import core.praya.agarthalib.utility.TextUtil;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.RequirementManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.socket.SocketGemProperties;

@SuppressWarnings("deprecation")
public class ListenerEntityDamage extends HandlerEvent implements Listener {
	
	public ListenerEntityDamage(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void checkBound(EntityDamageEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PluginManager pluginManager = plugin.getPluginManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!event.isCancelled()) {
			final Entity entity = event.getEntity();
			
			if (entity instanceof Player) {
				final Player player = (Player) entity;
				
				for (Slot slot : Slot.values()) {
					if (slot.getID() > 1) {
						final ItemStack item = MainBridge.getBridgeEquipment().getEquipment(player, slot);
						
						if (EquipmentUtil.loreCheck(item)) {
							if (!requirementManager.isAllowed(player, item)) {
								final String message = TextUtil.placeholder(lang.getText("Item_Lack_Requirement"), "Item", EquipmentUtil.getDisplayName(item));
								
								SenderUtil.sendMessage(player, message);
								SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
							} else {
								final Integer lineUnbound = requirementManager.getLineRequirementSoulUnbound(item);
								
								if (lineUnbound != null) {
									final String loreBound = requirementManager.getTextSoulBound(player);
									final Integer lineOld = requirementManager.getLineRequirementSoulBound(item);
									final HashMap<String, String> map = new HashMap<String, String>();
									
									if (lineOld != null) {
										EquipmentUtil.removeLore(item, lineOld);
									}
									
									String message = lang.getText("Item_Bound");
									
									map.put("item", EquipmentUtil.getDisplayName(item));
									map.put("player", player.getName());
									message = TextUtil.placeholder(map, message);
									
									requirementManager.setMetadataSoulbound(player, item);
									EquipmentUtil.setLore(item, lineUnbound, loreBound);
									SenderUtil.sendMessage(player, message);
								}
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void damageEvent(EntityDamageEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			final boolean customDamage = mainConfig.isModifierEnableCustomModifier();
			
			if (customDamage) {
				final Entity getEntity = event.getEntity();
				
				if (EntityUtil.isLivingEntity(getEntity)) {
					if (MainBridge.getBridgeLivingEntity().isLivingEntity(getEntity)) {
						final LivingEntity victims = EntityUtil.parseLivingEntity(getEntity);
						final boolean defenseAffect;
						
						switch (event.getCause()) {
						case ENTITY_EXPLOSION : defenseAffect = mainConfig.isModifierDefenseAffectEntityExplosion(); break;
						case BLOCK_EXPLOSION : defenseAffect = mainConfig.isModifierDefenseAffectBlockExplosion(); break;
						case CUSTOM : defenseAffect = mainConfig.isModifierDefenseAffectCustom(); break;
						case CONTACT : defenseAffect = mainConfig.isModifierDefenseAffectContact(); break;
						case FALL : defenseAffect = mainConfig.isModifierDefenseAffectFall(); break;
						case FALLING_BLOCK : defenseAffect = mainConfig.isModifierDefenseAffectFallingBlock(); break;
						case THORNS : defenseAffect = mainConfig.isModifierDefenseAffectThorn(); break;
						default : defenseAffect = false;
						}
						
						if (defenseAffect) {
							final LoreStatsArmor loreStatsVictims = statsManager.getLoreStatsArmor(victims);
							final SocketGemProperties socketVictims = socketManager.getSocketProperties(victims);
							final double scaleDefenseOverall = mainConfig.getModifierScaleDefenseOverall();
							final double scaleDefensePhysic = mainConfig.getModifierScaleDefensePhysic();
							
							double damage = event.getDamage();
							double defense = loreStatsVictims.getDefense() + socketVictims.getAdditionalDefense() + ((damage * socketVictims.getPercentDefense())/100);
							
							defense = defense * scaleDefenseOverall * scaleDefensePhysic;
							damage = damage - defense;
							damage = MathUtil.limitDouble(damage, 1, damage);
							
							event.setDamage(damage);
						}
					}
				}
			}
		}
	}
}
		
