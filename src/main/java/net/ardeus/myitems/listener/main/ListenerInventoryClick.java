package net.ardeus.myitems.listener.main;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.menu.MenuGUI;
import core.praya.agarthalib.builder.menu.MenuGUI.SlotCell;
import core.praya.agarthalib.builder.menu.MenuSlot;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.VersionNMS;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.game.MenuManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.menu.MenuSocket;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.socket.SocketGem;
import net.ardeus.myitems.socket.SocketGemTree;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerUtil;

public class ListenerInventoryClick extends HandlerEvent implements Listener {

	public ListenerInventoryClick(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void socket(InventoryClickEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final MenuManager menuManager = gameManager.getMenuManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final LanguageManager lang = plugin.getPluginManager().getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			final Inventory inventory = event.getInventory();
			final InventoryType type = inventory.getType();
			
			if (type.equals(InventoryType.CRAFTING)) {
				final InventoryHolder holder = inventory.getHolder();
				
				if (!(holder instanceof MenuGUI)) {
					final Player player = PlayerUtil.parse(event.getWhoClicked());
					final ItemStack cursorItem = event.getCursor();
					final ItemStack currentItem = event.getCurrentItem();
					
					if (EquipmentUtil.isSolid(cursorItem) && EquipmentUtil.isSolid(currentItem)) {
						final GameMode gameMode = player.getGameMode();
						final InventoryAction inventoryAction = event.getAction();
						final boolean checkSocket = gameMode.equals(GameMode.CREATIVE) ? inventoryAction.equals(InventoryAction.PLACE_ALL) : inventoryAction.equals(InventoryAction.SWAP_WITH_CURSOR);
						
						if (checkSocket) {
							final ItemStack itemRodUnlock = mainConfig.getSocketItemRodUnlock();
							final ItemStack itemRodRemove = mainConfig.getSocketItemRodRemove();
							final boolean containsSocketEmpty = socketManager.containsSocketEmpty(currentItem);
							final boolean containsSocketLocked = socketManager.containsSocketLocked(currentItem);
							final boolean containsSocketGems = socketManager.containsSocketGems(currentItem);
							final boolean isSocketGems = socketManager.isSocketGemItem(cursorItem);
							final boolean isSocketRodUnlock = cursorItem.isSimilar(itemRodUnlock);
							final boolean isSocketRodRemove = cursorItem.isSimilar(itemRodRemove);
							
							if (isSocketGems) {
								if (!containsSocketEmpty) {
									final String message = lang.getText(player, "Socket_Slot_Empty_Failure");
									
									SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
									SenderUtil.sendMessage(player, message);
									return;
								} else {
									final SocketGem socketGem = socketManager.getSocketGemByItem(cursorItem);
									final SocketGemTree socketTree = socketGem.getSocketTree();
									final core.praya.agarthalib.enums.main.SlotType typeItem = socketTree.getTypeItem();
									final core.praya.agarthalib.enums.main.SlotType typeDefault = core.praya.agarthalib.enums.main.SlotType.getSlotType(currentItem);
									
									if (!typeItem.equals(typeDefault) && !typeItem.equals(core.praya.agarthalib.enums.main.SlotType.UNIVERSAL)) {
										final String message = lang.getText(player, "Socket_Type_Not_Match");
										
										SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
										SenderUtil.sendMessage(player, message);
										return;
									}
								}
							} else if (isSocketRodUnlock) {
								if (!containsSocketLocked) {
									final String message = lang.getText(player, "Socket_Slot_Locked_Failure");
									
									SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
									SenderUtil.sendMessage(player, message);
									return;
								}
							} else if (isSocketRodRemove) {
								if (!containsSocketGems) {
									final String message = lang.getText(player, "Socket_Slot_Gems_Failure");
									
									SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
									SenderUtil.sendMessage(player, message);
									return;
								}
							} else {
								return;
							}
							
							if (!ServerUtil.isCompatible(VersionNMS.V1_10_R1)) {
								event.setCancelled(true);
							}
							
							menuManager.openMenuSocket(player);
							
							final InventoryView view = player.getOpenInventory();
							final Inventory topInventory = view.getTopInventory();
							final InventoryHolder topHolder = topInventory.getHolder();
							
							if (topHolder instanceof MenuGUI) {
								final MenuGUI menuGUI = (MenuGUI) topHolder;
								final MenuSocket executor = (MenuSocket) menuGUI.getExecutor();
								
								final SlotCell cellItemInput = SlotCell.B3;
								final SlotCell cellSocketInput = SlotCell.C3;
								final MenuSlot menuSlotItemInput = menuGUI.getMenuSlot(cellItemInput.getIndex());
								final MenuSlot menuSlotSocketInput = menuGUI.getMenuSlot(cellSocketInput.getIndex());
								
								menuSlotItemInput.setItem(currentItem);
								menuSlotSocketInput.setItem(cursorItem);
								
								menuGUI.setMenuSlot(menuSlotItemInput);
								menuGUI.setMenuSlot(menuSlotSocketInput);
								
								executor.updateSocketMenu(menuGUI, player);
								
								player.setItemOnCursor(null);
								
								if (gameMode.equals(GameMode.CREATIVE)) {
									new BukkitRunnable() {
										
										@Override
										public void run() {
											event.setCurrentItem(null);
										}
									}.runTaskLater(plugin, 0);
								} else {
									event.setCurrentItem(null);
								}
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void triggerSupport(InventoryClickEvent event) {		
		if (!event.isCancelled()) {
			final Inventory inventory = event.getInventory();
			final InventoryHolder holder = inventory.getHolder();
			
			if (!(holder instanceof MenuGUI)) {
				final Player player = PlayerUtil.parse(event.getWhoClicked());
				final ItemStack oldHelmet = Bridge.getBridgeEquipment().getEquipment(player, Slot.HELMET);
				final ItemStack oldChestplate = Bridge.getBridgeEquipment().getEquipment(player, Slot.CHESTPLATE);
				final ItemStack oldLeggings = Bridge.getBridgeEquipment().getEquipment(player, Slot.LEGGINGS);
				final ItemStack oldBoots = Bridge.getBridgeEquipment().getEquipment(player, Slot.BOOTS);
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						final ItemStack newHelmet = Bridge.getBridgeEquipment().getEquipment(player, Slot.HELMET);
						final ItemStack newChestplate = Bridge.getBridgeEquipment().getEquipment(player, Slot.CHESTPLATE);
						final ItemStack newLeggings = Bridge.getBridgeEquipment().getEquipment(player, Slot.LEGGINGS);
						final ItemStack newBoots = Bridge.getBridgeEquipment().getEquipment(player, Slot.BOOTS);
						
						if (oldHelmet != newHelmet || oldChestplate != newChestplate || oldLeggings != newLeggings || oldBoots != newBoots) {
							TriggerSupportUtil.updateSupport(player);
						}
					}
				}.runTaskLater(plugin, 0);
			}
		}
	}
		
	@EventHandler(priority=EventPriority.MONITOR)
	public void eventChangeEquipment(InventoryClickEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			final Player player = PlayerUtil.parse(event.getWhoClicked());
			final SlotType slotType = event.getSlotType();
			final ClickType click = event.getClick();
			final ItemStack itemCurrent = event.getCurrentItem();
			final ItemStack itemCursor = event.getCursor();
			final boolean enableGradeCalculation = mainConfig.isPassiveEnableGradeCalculation();
			final Collection<PassiveEffectEnum> currentPassiveEffects = passiveEffectManager.getPassiveEffects(itemCurrent);
			final Collection<PassiveEffectEnum> cursorPassiveEffects = passiveEffectManager.getPassiveEffects(itemCursor);
			final HashMap<Slot, ItemStack> mapItemSlot = new HashMap<Slot, ItemStack>();
			
			for (Slot slot : Slot.values()) {
				final ItemStack itemSlot = Bridge.getBridgeEquipment().getEquipment(player, slot);
				
				mapItemSlot.put(slot, itemSlot);
			}
			
			new BukkitRunnable() {
				
				@Override
				public void run() {
					final InventoryView inventoryView = player.getOpenInventory();
					final InventoryType inventoryType = inventoryView.getType();
					final Inventory inventory = !inventoryType.equals(InventoryType.CREATIVE) ? inventoryView.getTopInventory() : null;
					
					for (Slot slot : Slot.values()) {
						final ItemStack itemBefore = mapItemSlot.get(slot);
						final ItemStack itemAfter = Bridge.getBridgeEquipment().getEquipment(player, slot);
						final boolean isSolidBefore = itemBefore != null;
						final boolean isSolidAfter = itemAfter != null;
						
						if (isSolidBefore && isSolidAfter ? !itemBefore.equals(itemAfter) : isSolidBefore != isSolidAfter) {
							if (itemSetManager.isItemSet(itemBefore) || itemSetManager.isItemSet(itemAfter)) {
								itemSetManager.updateItemSet(player, true, inventory);
								break;
							}
						}
					}
				}
			}.runTaskLater(plugin, 0);
			
			if (click.equals(ClickType.NUMBER_KEY) || click.equals(ClickType.LEFT) || click.equals(ClickType.SHIFT_LEFT) || click.equals(ClickType.RIGHT) || click.equals(ClickType.SHIFT_RIGHT)) {
				final ItemStack itemMainHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
				final ItemStack itemOffHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.OFFHAND);
				final Collection<PassiveEffectEnum> mainPassiveEffects = passiveEffectManager.getPassiveEffects(itemMainHand);
				final Collection<PassiveEffectEnum> offPassiveEffects = passiveEffectManager.getPassiveEffects(itemOffHand);
				
				new BukkitRunnable() {
					
					@Override
					public void run() {
						
						passiveEffectManager.reloadPassiveEffect(player, mainPassiveEffects, enableGradeCalculation);
						passiveEffectManager.reloadPassiveEffect(player, offPassiveEffects, enableGradeCalculation);
						passiveEffectManager.reloadPassiveEffect(player, cursorPassiveEffects, enableGradeCalculation);
						passiveEffectManager.reloadPassiveEffect(player, currentPassiveEffects, enableGradeCalculation);
						
						if (click.equals(ClickType.NUMBER_KEY)) {
							TriggerSupportUtil.updateSupport(player);
						}
					}
				}.runTaskLater(plugin, 0);
			} else {
				if (slotType.equals(SlotType.ARMOR) || slotType.equals(SlotType.QUICKBAR)) {
					
					new BukkitRunnable() {
						
						@Override
						public void run() {
							
							passiveEffectManager.reloadPassiveEffect(player, cursorPassiveEffects, enableGradeCalculation);
							passiveEffectManager.reloadPassiveEffect(player, currentPassiveEffects, enableGradeCalculation);
							
							TriggerSupportUtil.updateSupport(player);
						}
					}.runTaskLater(plugin, 0);
				}
			}
		}
	}
}
