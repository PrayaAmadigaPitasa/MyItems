package net.ardeus.myitems.listener.main;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityRegainHealthEvent;

import core.praya.agarthalib.bridge.unity.Bridge;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.passive.PassiveEffectTypeEnum;
import net.ardeus.myitems.utility.main.CustomEffectUtil;

public class ListenerEntityRegainHealth extends HandlerEvent implements Listener {

	public ListenerEntityRegainHealth(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void eventEntityRegainHealth(EntityRegainHealthEvent event) {
		if (!event.isCancelled()) {
			if (Bridge.getBridgeLivingEntity().isLivingEntity(event.getEntity())) {
				final LivingEntity entity = (LivingEntity) event.getEntity();
				
				if (CustomEffectUtil.isRunCustomEffect(entity, PassiveEffectTypeEnum.CURSE)) {
					event.setCancelled(true);
				}
			}
		}
	}
}
