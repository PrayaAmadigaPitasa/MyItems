package net.ardeus.myitems.listener.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.passive.PassiveEffectTypeEnum;
import net.ardeus.myitems.utility.main.CustomEffectUtil;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

import com.praya.agarthalib.utility.PlayerUtil;

public class ListenerPlayerJoin extends HandlerEvent implements Listener {

	public ListenerPlayerJoin(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void eventPlayerJoin(PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if (player.isOnline()) {
					if (player.getWalkSpeed() < 0.05F) {
						player.setWalkSpeed(0.2F);
					}
				}
			}
		}.runTaskLater(plugin, 1);
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if (!PlayerUtil.isOnline(player)) {
					this.cancel();
					return;
				} else {
					if (CustomEffectUtil.isRunCustomEffect(player, PassiveEffectTypeEnum.ROOTS)) {
						final Vector vector = player.getVelocity().setY(-1);
						
						player.setVelocity(vector);
					}
				}
			}
		}.runTaskTimer(plugin, 0, 1);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void triggerSupport(PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		
		TriggerSupportUtil.updateSupport(player);
	}
}
