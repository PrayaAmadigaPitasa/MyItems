package net.ardeus.myitems.listener.main;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsOption;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;

import com.praya.agarthalib.utility.BlockUtil;
import com.praya.agarthalib.utility.EquipmentUtil;

public class ListenerBlockBreak extends HandlerEvent implements Listener {
	
	public ListenerBlockBreak(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void eventBlockBreak(BlockBreakEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		
		if (!event.isCancelled()) {
			if (BlockUtil.isSet(event.getBlock())) {
				event.setCancelled(true);
				return;
			} else {
				final Player player = event.getPlayer();
				final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
				
				if (EquipmentUtil.hasLore(item)) {
					final int durability = (int) statsManager.getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.CURRENT);
					
					if (!statsManager.durability(player, item, durability, true)) {
						
						event.setCancelled(true);
						statsManager.sendBrokenCode(player, Slot.MAINHAND);
					}
				}
			}
		}
	}
}