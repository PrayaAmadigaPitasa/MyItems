package net.ardeus.myitems.listener.main;

import java.util.HashMap;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsOption;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.manager.game.RequirementManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EntityUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public class ListenerEntityShootBow extends HandlerEvent implements Listener {
	
	public ListenerEntityShootBow(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void checkRequirement(EntityShootBowEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PluginManager pluginManager = plugin.getPluginManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final LivingEntity shooter = event.getEntity();
		
		if (shooter instanceof Player) {
			final Player player = (Player) shooter;
			final ItemStack item = event.getBow();
			
			if (EquipmentUtil.loreCheck(item)) {
				if (!requirementManager.isAllowedReqSoulBound(player, item)) {
					final String reqBound = requirementManager.getRequirementSoulBound(item);
					final String message = TextUtil.placeholder(lang.getText(player, "Requirement_Not_Allowed_Bound"), "Player", reqBound);
					
					event.setCancelled(true);
					SenderUtil.sendMessage(player, message);
					SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else if (!requirementManager.isAllowedReqPermission(player, item)) {
					final String reqPermission = requirementManager.getRequirementPermission(item);
					final String message = TextUtil.placeholder(lang.getText(player, "Requirement_Not_Allowed_Permission"), "Permission", reqPermission);
					
					event.setCancelled(true);
					SenderUtil.sendMessage(player, message);
					SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else if (!requirementManager.isAllowedReqLevel(player, item)) {
					final int reqLevel = requirementManager.getRequirementLevel(item);
					final String message = TextUtil.placeholder(lang.getText(player, "Requirement_Not_Allowed_Level"), "Level", String.valueOf(reqLevel));
					
					event.setCancelled(true);
					SenderUtil.sendMessage(player, message);
					SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else if (!requirementManager.isAllowedReqClass(player, item)) {
					final String reqClass = requirementManager.getRequirementClass(item);
					final String message = TextUtil.placeholder(lang.getText(player, "Requirement_Not_Allowed_Class"), "Class", reqClass);
					
					event.setCancelled(true);
					SenderUtil.sendMessage(player, message);
					SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.NORMAL)
	public void checkUnbound(EntityShootBowEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PluginManager pluginManager = plugin.getPluginManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!event.isCancelled()) {
			final LivingEntity shooter = event.getEntity();
			
			if (shooter instanceof Player) {
				final Player player = (Player) shooter;
				final ItemStack item = event.getBow();
				
				if (EquipmentUtil.loreCheck(item)) {
					final Integer lineUnbound = requirementManager.getLineRequirementSoulUnbound(item);
					
					if (lineUnbound != null) {
						final String loreBound = requirementManager.getTextSoulBound(player);
						final Integer lineOld = requirementManager.getLineRequirementSoulBound(item);
						final HashMap<String, String> map = new HashMap<String, String>();
						
						if (lineOld != null) {
							EquipmentUtil.removeLore(item, lineOld);
						}
						
						String message = lang.getText(player, "Item_Bound");
						
						map.put("item", EquipmentUtil.getDisplayName(item));
						map.put("player", player.getName());
						message = TextUtil.placeholder(map, message);
						
						requirementManager.setMetadataSoulbound(player, item);
						EquipmentUtil.setLore(item, lineUnbound, loreBound);
						SenderUtil.sendMessage(player, message);
						return;
					}
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGH)
	public void entityShootBowEvent(EntityShootBowEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final LanguageManager lang = plugin.getPluginManager().getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			if (EntityUtil.isPlayer(event.getEntity())) {
				final Player player = PlayerUtil.parse(event.getEntity());
				final ItemStack item = event.getBow();
				
				if (EquipmentUtil.loreCheck(item)) {
					if (statsManager.hasLoreStats(item, LoreStatsEnum.DURABILITY)) {
						final int durability = (int) statsManager.getLoreValue(item, LoreStatsEnum.DURABILITY, LoreStatsOption.CURRENT);	
						final int nextValue = durability - 1;			
												
						if (nextValue < 1) {
							final boolean enableItemBroken = mainConfig.isStatsEnableItemBroken();
							final boolean enableItemBrokenMessage = mainConfig.isStatsEnableItemBrokenMessage();
							
							if (enableItemBrokenMessage) {
								final String message = lang.getText(player, "Item_Broken_Bow");
	
								SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
								SenderUtil.sendMessage(player, message);
							}
							
							if (enableItemBroken) {
								final boolean enableGradeCalculation = mainConfig.isPassiveEnableGradeCalculation();
								
								passiveEffectManager.reloadPassiveEffect(player, item, enableGradeCalculation);
								
								if (Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND).equals(item)) {
									Bridge.getBridgeEquipment().setEquipment(player, null, Slot.MAINHAND);
								} else if (Bridge.getBridgeEquipment().getEquipment(player, Slot.OFFHAND).equals(item)) {
									Bridge.getBridgeEquipment().setEquipment(player, null, Slot.OFFHAND);
								}
								
								itemSetManager.updateItemSet(player);
							}
						}
						
						if (durability < 1) {
							event.setCancelled(true);
						} else {
							statsManager.damageDurability(item);
						}
					}
				}
			}
		}
	}
}