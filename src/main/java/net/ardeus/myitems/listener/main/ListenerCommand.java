package net.ardeus.myitems.listener.main;

import java.util.Collection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;

import com.praya.agarthalib.utility.EquipmentUtil;

public class ListenerCommand extends HandlerEvent implements Listener {
	
	public ListenerCommand(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final Player player = event.getPlayer();
		final ItemStack itemBefore = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
		final Collection<PassiveEffectEnum> passiveEffectBefore = passiveEffectManager.getPassiveEffects(itemBefore);
		final int idBefore = EquipmentUtil.isSolid(itemBefore) ? itemBefore.hashCode() : 0;
		final boolean enableGradeCalculation = mainConfig.isPassiveEnableGradeCalculation();
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if (player.isOnline()) {
					final ItemStack itemAfter = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
					final Collection<PassiveEffectEnum> passiveEffectAfter = passiveEffectManager.getPassiveEffects(itemAfter);
					final int idAfter = EquipmentUtil.isSolid(itemAfter) ? itemAfter.hashCode() : 0;
					
					if (idBefore != idAfter) {				
						passiveEffectManager.reloadPassiveEffect(player, passiveEffectBefore, enableGradeCalculation);
						passiveEffectManager.reloadPassiveEffect(player, passiveEffectAfter, enableGradeCalculation);
					}
				}
			}
		}.runTaskLater(plugin, 1);
	}
}
