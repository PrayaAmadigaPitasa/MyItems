package net.ardeus.myitems.listener.main;

import java.util.HashMap;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.SlotType;
import core.praya.agarthalib.utility.CombatUtil;
import core.praya.agarthalib.utility.EntityUtil;
import core.praya.agarthalib.utility.EquipmentUtil;
import core.praya.agarthalib.utility.MathUtil;
import core.praya.agarthalib.utility.ProjectileUtil;
import core.praya.agarthalib.utility.SenderUtil;
import core.praya.agarthalib.utility.ServerEventUtil;
import core.praya.agarthalib.utility.ServerUtil;
import core.praya.agarthalib.utility.TextUtil;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.element.ElementBoostStatsWeapon;
import net.ardeus.myitems.event.CombatCriticalDamageEvent;
import net.ardeus.myitems.event.CombatPreCriticalEvent;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.item.ItemSetBonusEffectEntity;
import net.ardeus.myitems.item.ItemSetBonusEffectStats;
import net.ardeus.myitems.lorestats.LoreStatsArmor;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.lorestats.LoreStatsWeapon;
import net.ardeus.myitems.manager.game.AbilityWeaponManager;
import net.ardeus.myitems.manager.game.ElementManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.RequirementManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.socket.SocketGemProperties;

@SuppressWarnings("deprecation")
public class ListenerEntityDamageByEntity extends HandlerEvent implements Listener {
	
	public ListenerEntityDamageByEntity(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void checkBoundAttacker(EntityDamageByEntityEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PluginManager pluginManager = plugin.getPluginManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!event.isCancelled()) {
			final Entity attacker = event.getDamager();
			
			if (attacker instanceof Player) {
				final Player player = (Player) attacker;
				
				for (Slot slot : Slot.values()) {
					if (slot.getID() < 2) {
						final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, slot);
						
						if (EquipmentUtil.loreCheck(item)) {
							if (!requirementManager.isAllowed(player, item)) {
								final String message = TextUtil.placeholder(lang.getText("Item_Lack_Requirement"), "Item", EquipmentUtil.getDisplayName(item));
								
								event.setCancelled(true);
								SenderUtil.sendMessage(player, message);
								SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
							} else {
								final Integer lineUnbound = requirementManager.getLineRequirementSoulUnbound(item);
								
								if (lineUnbound != null) {
									final String loreBound = requirementManager.getTextSoulBound(player);
									final Integer lineOld = requirementManager.getLineRequirementSoulBound(item);
									final HashMap<String, String> map = new HashMap<String, String>();
									
									if (lineOld != null) {
										EquipmentUtil.removeLore(item, lineOld);
									}
									
									String message = lang.getText("Item_Bound");
									
									map.put("item", EquipmentUtil.getDisplayName(item));
									map.put("player", player.getName());
									message = TextUtil.placeholder(map, message);
									
									requirementManager.setMetadataSoulbound(player, item);
									EquipmentUtil.setLore(item, lineUnbound, loreBound);
									SenderUtil.sendMessage(player, message);
								}
							}
						}
					}
				}
			}
		}
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void entityDamageByEntityEvent(EntityDamageByEntityEvent event) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final ElementManager elementManager = gameManager.getElementManager();
		final AbilityWeaponManager abilityWeaponManager = gameManager.getAbilityWeaponManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			final Entity entityAttacker = event.getDamager();
			final Entity entityVictims = event.getEntity();
			final boolean reverse;
			final LivingEntity attacker;
			final LivingEntity victims;
			 
			if (ProjectileUtil.isProjectile(entityAttacker)) {
				final Projectile projectile = ProjectileUtil.parseProjectile(entityAttacker);
				final ProjectileSource source = projectile.getShooter();
				
				if (source instanceof LivingEntity) {
					
					attacker = EntityUtil.parseLivingEntity(source);
					reverse = EquipmentUtil.holdBow(attacker) ? EquipmentUtil.getActiveSlotBow(attacker).equals(Slot.OFFHAND) : false;
				} else {
					return;
				}
			} else if (EntityUtil.isLivingEntity(entityAttacker)) {
				attacker = EntityUtil.parseLivingEntity(entityAttacker);
				reverse = false;
			} else {
				return;
			}
			
			if (EntityUtil.isLivingEntity(entityVictims)) {
				victims = EntityUtil.parseLivingEntity(entityVictims);
			} else {
				return;
			}
			
			if (!Bridge.getBridgeLivingEntity().isLivingEntity(attacker)) {
				return;
			} else if (!Bridge.getBridgeLivingEntity().isLivingEntity(victims)) {
				return;
			} else {
				final boolean isSkillDamage = CombatUtil.isSkillDamage(victims);
				final boolean isAreaDamage = CombatUtil.isAreaDamage(victims);
				
				if (CombatUtil.hasMetadataInstantDamage(victims)) {
					
					CombatUtil.removeMetadataInstantDamage(victims);
					return;
				} else {
					final DamageCause damageCause = event.getCause();
					final ItemStack itemMainHand = Bridge.getBridgeEquipment().getEquipment(attacker, Slot.MAINHAND);
					final ItemStack itemOffHand = Bridge.getBridgeEquipment().getEquipment(attacker, Slot.OFFHAND);
					final double baseDamage = !ServerUtil.isVersion_1_12() ? event.getOriginalDamage(DamageModifier.BASE) : 0;
					final boolean enableVanillaDamage = mainConfig.isModifierEnableVanillaDamage();
					
					double damage = event.getDamage() - baseDamage;
					boolean customDamage = true;		
							
					if (!(EntityUtil.isPlayer(attacker))) {
						final boolean enableCustomMobDamage = mainConfig.isModifierEnableCustomMobDamage();
						
						customDamage = enableCustomMobDamage;
					} else {
						final boolean enableCustomDamage = mainConfig.isModifierEnableCustomModifier();
						
						customDamage = enableCustomDamage;
					}
					
					if (!damageCause.equals(DamageCause.PROJECTILE)) {
						if (EquipmentUtil.isSolid(itemMainHand)) {
							final Material materialMainHand = itemMainHand.getType();
							
							if (materialMainHand.equals(Material.BOW)) {
								if (!isSkillDamage && !isAreaDamage) {
									customDamage = false;
								}
							}
						}
					}
					
					if (enableVanillaDamage) {
						final double scaleDamageVanilla = mainConfig.getModifierScaleDamageVanilla();
						
						if (scaleDamageVanilla > 0) {
							final double vanillaDamage = (scaleDamageVanilla*baseDamage);
							
							damage = damage + vanillaDamage;
						} 
					} else {
						damage = damage - 1;
					}
					
					if (customDamage) {
						final LoreStatsWeapon loreStatsAttacker = statsManager.getLoreStatsWeapon(attacker, reverse);
						final LoreStatsArmor loreStatsVictims = statsManager.getLoreStatsArmor(victims);
						final SocketGemProperties socketAttacker = socketManager.getSocketProperties(attacker);
						final SocketGemProperties socketVictims = socketManager.getSocketProperties(victims);
						final HashMap<String, Double> elementAttacker = elementManager.getMapElement(attacker, SlotType.WEAPON);
						final HashMap<String, Double> elementVictims = elementManager.getMapElement(victims, SlotType.ARMOR);
						final HashMap<String, Double> mapElement = elementManager.getElementCalculation(elementAttacker, elementVictims);
						final ElementBoostStatsWeapon elementBoostStatsAttacker = elementManager.getElementBoostStats(mapElement);
						final HashMap<AbilityWeapon, Integer> mapAbilityWeapon = abilityWeaponManager.getMapAbilityWeapon(attacker, true);
						final ItemSetBonusEffectEntity itemSetBonusEffectEntityAttacker = itemSetManager.getItemSetBonusEffectEntity(attacker);
						final ItemSetBonusEffectEntity itemSetBonusEffectEntityVictims = itemSetManager.getItemSetBonusEffectEntity(victims);
						final ItemSetBonusEffectStats itemSetBonusEffectStatsAttacker = itemSetBonusEffectEntityAttacker.getEffectStats();
						final ItemSetBonusEffectStats itemSetBonusEffectStatsVictims = itemSetBonusEffectEntityVictims.getEffectStats();
						
						for (AbilityWeapon abilityWeaponItemSet : itemSetBonusEffectEntityAttacker.getAllAbilityWeapon()) {
							final int maxGrade = abilityWeaponItemSet.getMaxGrade(); 
									
							if (mapAbilityWeapon.containsKey(abilityWeaponItemSet)) {
								final int totalGrade = mapAbilityWeapon.get(abilityWeaponItemSet) + itemSetBonusEffectEntityAttacker.getGradeAbilityWeapon(abilityWeaponItemSet);
								
								mapAbilityWeapon.put(abilityWeaponItemSet, Math.min(maxGrade, totalGrade));
							} else {
								final int grade = itemSetBonusEffectEntityAttacker.getGradeAbilityWeapon(abilityWeaponItemSet);
								
								mapAbilityWeapon.put(abilityWeaponItemSet, Math.min(maxGrade, grade));
							}
						}
						
						final double scaleDamageCustom = mainConfig.getModifierScaleDamageCustom();
						
						final double statsDamage = loreStatsAttacker.getDamage();
						final double socketDamage = socketAttacker.getAdditionalDamage() + (statsDamage * (socketAttacker.getPercentDamage() / 100));
						final double abilityDamage = abilityWeaponManager.getTotalBaseBonusDamage(mapAbilityWeapon) + (statsDamage * (abilityWeaponManager.getTotalBasePercentDamage(mapAbilityWeapon) / 100));
						final double elementDamage = elementBoostStatsAttacker.getBaseAdditionalDamage() + (statsDamage * (elementBoostStatsAttacker.getBasePercentDamage() / 100));
						final double itemSetDamage = itemSetBonusEffectStatsAttacker.getAdditionalDamage() + (statsDamage * (itemSetBonusEffectStatsAttacker.getPercentDamage() / 100));
						final double attributeDamage = scaleDamageCustom * (statsDamage + socketDamage + abilityDamage + elementDamage + itemSetDamage);
						
						final double statsDefense = loreStatsVictims.getDefense();
						final double socketDefense = socketVictims.getAdditionalDefense() + (statsDefense * (socketVictims.getPercentDefense() / 100));
						final double itemSetDefense = itemSetBonusEffectStatsVictims.getAdditionalDefense() + (statsDefense * (itemSetBonusEffectStatsVictims.getPercentDefense() / 100));
						final double attributeDefense = statsDefense + socketDefense + itemSetDefense;
						
						final boolean enableOffHand = mainConfig.isAbilityWeaponEnableOffHand();
						final boolean hasStatsCriticalChance = statsManager.hasLoreStats(itemMainHand, LoreStatsEnum.CRITICAL_CHANCE) || (enableOffHand && statsManager.hasLoreStats(itemOffHand, LoreStatsEnum.CRITICAL_CHANCE));
						final boolean hasStatsCriticalDamage = statsManager.hasLoreStats(itemMainHand, LoreStatsEnum.CRITICAL_DAMAGE) || (enableOffHand && statsManager.hasLoreStats(itemOffHand, LoreStatsEnum.CRITICAL_DAMAGE));
						
						double penetration = 0;
						double pvpDamage = 0;
						double pveDamage = 0;
						double defense = 0;
						double pvpDefense = 0;
						double pveDefense = 0;
						double cc = 5;
						double cd = 1.2;
						double attackAoERadius = 0;
						double attackAoEDamage = 0;
						double blockAmount = 25;
						double blockRate = 0;
						double accuration = 100;
						
						damage = isSkillDamage || isAreaDamage ? damage : damage + attributeDamage;
						penetration = penetration + loreStatsAttacker.getPenetration() + socketAttacker.getPenetration() + itemSetBonusEffectStatsAttacker.getPenetration();
						pvpDamage = pvpDamage + loreStatsAttacker.getPvPDamage() + socketAttacker.getPvPDamage() + itemSetBonusEffectStatsAttacker.getPvPDamage();
						pveDamage = pveDamage + loreStatsAttacker.getPvEDamage() + socketAttacker.getPvEDamage() + itemSetBonusEffectStatsAttacker.getPvEDamage();
						defense = defense + attributeDefense;
						pvpDefense = pvpDefense + loreStatsVictims.getPvPDefense() + socketVictims.getPvPDefense() + itemSetBonusEffectStatsVictims.getPvPDefense();
						pveDefense = pveDefense + loreStatsVictims.getPvEDefense() + socketVictims.getPvEDefense() + itemSetBonusEffectStatsVictims.getPvEDefense();
						cc = !hasStatsCriticalChance ? cc : loreStatsAttacker.getCriticalChance() + socketAttacker.getCriticalChance() + itemSetBonusEffectStatsAttacker.getCriticalChance();
						cd = !hasStatsCriticalDamage ? cd : 1 + loreStatsAttacker.getCriticalDamage() + ((socketAttacker.getCriticalDamage() + itemSetBonusEffectStatsAttacker.getCriticalDamage()) / 100);
						attackAoERadius = attackAoERadius + loreStatsAttacker.getAttackAoERadius() + socketAttacker.getAttackAoERadius() + itemSetBonusEffectStatsAttacker.getAttackAoERadius();
						attackAoEDamage = attackAoEDamage + loreStatsAttacker.getAttackAoEDamage() + socketAttacker.getAttackAoEDamage() + itemSetBonusEffectStatsAttacker.getAttackAoEDamage();
						blockAmount = blockAmount + loreStatsVictims.getBlockAmount() + socketVictims.getBlockAmount() + itemSetBonusEffectStatsVictims.getBlockAmount();
						blockRate = blockRate + loreStatsVictims.getBlockRate() + socketVictims.getBlockRate() + itemSetBonusEffectStatsVictims.getBlockRate();
						accuration = accuration  + (loreStatsAttacker.getHitRate() + socketAttacker.getHitRate() + itemSetBonusEffectStatsAttacker.getHitRate()) - (loreStatsVictims.getDodgeRate() + socketVictims.getDodgeRate() + itemSetBonusEffectStatsVictims.getDodgeRate());
						
						if (MathUtil.chanceOf(100 - accuration)) {
							if (EntityUtil.isPlayer(attacker)) {
								final Player player = EntityUtil.parsePlayer(attacker);
								final String message = lang.getText("Attack_Miss");
								
								SenderUtil.playSound(player, SoundEnum.ENTITY_BAT_TAKEOFF);
								SenderUtil.sendMessage(player, message);
							}
							
							if (EntityUtil.isPlayer(victims)) {
								final Player player = EntityUtil.parsePlayer(victims);
								final String message = lang.getText("Attack_Dodge");
								
								SenderUtil.playSound(player, SoundEnum.ENTITY_BAT_TAKEOFF);
								SenderUtil.sendMessage(player, message);
							}
							
							event.setCancelled(true);
							return;
						}
						
						final boolean enableCustomCritical = mainConfig.isModifierEnableCustomCritical();
					
						accuration = MathUtil.limitDouble(accuration, 0, accuration);							
						damage = damage * (1 + Math.log10(accuration/100));
						
						if (MathUtil.chanceOf(blockRate)) {
							
							blockAmount = MathUtil.limitDouble(blockAmount, 0, 100);
							damage = damage * (100 - blockAmount) / 100;
							
							if (EntityUtil.isPlayer(attacker)) {
								final Player player = EntityUtil.parsePlayer(attacker);
								final String message = lang.getText("Attack_Block");
								
								SenderUtil.playSound(player, SoundEnum.BLOCK_ANVIL_PLACE);
								SenderUtil.sendMessage(player, message);
							}
							
							if (EntityUtil.isPlayer(victims)) {
								final Player player = EntityUtil.parsePlayer(victims);
								final String message = lang.getText("Attack_Blocked");
								
								SenderUtil.playSound(player, SoundEnum.BLOCK_ANVIL_PLACE);
								SenderUtil.sendMessage(player, message);
							}
						}
						
						if (enableCustomCritical) {
							final CombatPreCriticalEvent combatPreCriticalEvent = new CombatPreCriticalEvent(attacker, victims, cc);
							
							ServerEventUtil.callEvent(combatPreCriticalEvent);
							
							if (!combatPreCriticalEvent.isCancelled() && combatPreCriticalEvent.isCritical()) {
								final CombatCriticalDamageEvent combatCriticalDamageEvent = new CombatCriticalDamageEvent(attacker, victims, damage, cd, 0);
								
								ServerEventUtil.callEvent(combatCriticalDamageEvent);
								
								if (!combatCriticalDamageEvent.isCancelled()) {
									damage = combatCriticalDamageEvent.getCalculationDamage();
								}
							}
						}
						
						if (!isAreaDamage && !isSkillDamage) {
							for (Slot slot : Slot.values()) {
								final LivingEntity holder = slot.getType().equals(SlotType.WEAPON) ? attacker : victims;
								final ItemStack item = Bridge.getBridgeEquipment().getEquipment(holder, slot);
								
								if (EquipmentUtil.isSolid(item)) {
									if (!item.getType().equals(Material.BOW)) {
										final boolean enableItemBrokenMessage = mainConfig.isStatsEnableItemBrokenMessage();
										
										statsManager.damageDurability(item);
										
										if (enableItemBrokenMessage && !statsManager.checkDurability(item)) {
											statsManager.sendBrokenCode(holder, slot);
										}
									}
								}
							}
						}
						
						if (!isAreaDamage) {
							
							elementManager.applyElementPotion(attacker, victims, mapElement);
														
							for (AbilityWeapon abilityWeapon : mapAbilityWeapon.keySet()) {
								final int grade = mapAbilityWeapon.get(abilityWeapon);
								
								abilityWeapon.onDamage(entityAttacker, entityVictims, grade, damage);
							}
						}
						
						final double bonusPercentDamage = EntityUtil.isPlayer(victims) ? pvpDamage : pveDamage;
						final double bonusPercentDefense = EntityUtil.isPlayer(attacker) ? pvpDefense : pveDefense;
						final double scaleDefenseOverall = mainConfig.getModifierScaleDefenseOverall();
						final double scaleDamageMobReceive = mainConfig.getModifierScaleMobDamageReceive();
						final boolean enableFlatDamage = mainConfig.isModifierEnableFlatDamage();
						
						penetration = MathUtil.limitDouble(penetration, 0, 100);
						defense = defense * scaleDefenseOverall;
						defense = defense * (100 + bonusPercentDefense) / 100;
						defense = defense * (100 - penetration) / 100;
						damage = damage * (100 + bonusPercentDamage) / 100;
						damage = damage - defense;
						damage = MathUtil.limitDouble(damage, 0, damage);
						damage = EntityUtil.isPlayer(victims) ? damage : damage * scaleDamageMobReceive;
						
						if (!isAreaDamage && !isSkillDamage) {
							if (attackAoERadius > 0 && attackAoEDamage > 0) {
								final double attackAoEDamageEntity = damage * (attackAoEDamage/100);
								final Location attackAoELocation = victims.getLocation();
								
								for (LivingEntity attackAoEVictim : CombatUtil.getNearbyUnits(attackAoELocation, attackAoERadius)) {
									if (!attackAoEVictim.equals(attacker) && !attackAoEVictim.equals(victims)) {
										CombatUtil.areaDamage(attacker, attackAoEVictim, attackAoEDamageEntity);
									}
								}
							}
						}
						
						if (!enableFlatDamage) {
							final boolean enableBalancingSystem = mainConfig.isModifierEnableBalancingSystem();
							final boolean enableBalancingMob = mainConfig.isModifierEnableBalancingMob();
							
							if (EntityUtil.isPlayer(victims) ? enableBalancingSystem : enableBalancingMob) {
								final double modusValue = mainConfig.getModifierModusValue();
								final double divider = 1+Math.log10(1+(10*defense/modusValue));
							
								damage = damage / divider;
								damage = damage * 10 / modusValue;
							}
						}
					}
						
					if (!event.isCancelled()) {
						final double scaleDamageOverall = mainConfig.getModifierScaleDamageOverall();
						final boolean enableVanillaModifier = mainConfig.isModifierEnableVanillaDamage();
						
						damage = damage * scaleDamageOverall;
						
						if (enableVanillaModifier) {
							if (attacker.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
								for (PotionEffect potion : attacker.getActivePotionEffects()) {
									if (potion.getType().equals(PotionEffectType.INCREASE_DAMAGE)) {
										damage = damage + (damage*potion.getAmplifier()/10);
										break;
									}
								}
							}
							
							if (victims.hasPotionEffect(PotionEffectType.DAMAGE_RESISTANCE)) {
								for (PotionEffect potion : victims.getActivePotionEffects()) {
									if (potion.getType().equals(PotionEffectType.DAMAGE_RESISTANCE)) {
										damage = damage-(damage*potion.getAmplifier()/20);
										break;
									}
								}
							}
							
							if (!ServerUtil.isVersion_1_12()) {
								if (event.isApplicable(DamageModifier.ABSORPTION)) {
									if (event.getDamage(DamageModifier.ABSORPTION) < 0) {
										final double scaleAbsorbEffect = mainConfig.getModifierScaleAbsorbEffect();
										
										if (scaleAbsorbEffect > 1) {
											event.setDamage(DamageModifier.ABSORPTION, -damage);
										} else if (scaleAbsorbEffect < 0.1) {
											event.setDamage(DamageModifier.ABSORPTION, -(damage * 0.1));
										} else {
											event.setDamage(DamageModifier.ABSORPTION, -(damage * scaleAbsorbEffect));
										}
									}
								}
							}
						}
						
						if (!ServerUtil.isVersion_1_12()) {
							final boolean enableVanillaFormulaArmor = mainConfig.isModifierEnableVanillaFormulaArmor();
							
							if (enableVanillaFormulaArmor && customDamage) {
								
								double modDamage = 0;
								double modArmor = 0;
								
								if (event.isApplicable(DamageModifier.BASE)) {
									modDamage = event.getDamage(DamageModifier.BASE);
								}
								
								if (event.isApplicable(DamageModifier.ARMOR)) {
									modArmor = event.getDamage(DamageModifier.ARMOR);
								}
								
								final double calculation = modDamage != 0 ? ((modDamage + modArmor) / modDamage) : 0;
								
								damage = damage * calculation;
							}
						}
						
						event.setDamage(damage);
						return;
					}
				}
			}
		}
	}
}
		
