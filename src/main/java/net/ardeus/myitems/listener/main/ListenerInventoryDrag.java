package net.ardeus.myitems.listener.main;

import java.util.HashMap;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;

import com.praya.agarthalib.utility.PlayerUtil;

public class ListenerInventoryDrag extends HandlerEvent implements Listener {

	public ListenerInventoryDrag(MyItems plugin) {
		super(plugin);
	}
		
	@EventHandler(priority=EventPriority.MONITOR)
	public void inventoryDragEvent(InventoryDragEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		
		if (!event.isCancelled()) {
			final Player player = PlayerUtil.parse(event.getWhoClicked());
			final HashMap<Slot, ItemStack> mapItemSlot = new HashMap<Slot, ItemStack>();
			
			for (Slot slot : Slot.values()) {
				final ItemStack itemSlot = Bridge.getBridgeEquipment().getEquipment(player, slot);
				
				mapItemSlot.put(slot, itemSlot);
			}
			
			new BukkitRunnable() {
				
				@Override
				public void run() {
					final InventoryView inventoryView = player.getOpenInventory();
					final InventoryType inventoryType = inventoryView.getType();
					final Inventory inventory = !inventoryType.equals(InventoryType.CREATIVE) ? inventoryView.getTopInventory() : null;
					
					for (Slot slot : Slot.values()) {
						final ItemStack itemBefore = mapItemSlot.get(slot);
						final ItemStack itemAfter = Bridge.getBridgeEquipment().getEquipment(player, slot);
						final boolean isSolidBefore = itemBefore != null;
						final boolean isSolidAfter = itemAfter != null;
						
						if (isSolidBefore && isSolidAfter ? !itemBefore.equals(itemAfter) : isSolidBefore != isSolidAfter) {
							if (itemSetManager.isItemSet(itemBefore) || itemSetManager.isItemSet(itemAfter)) {
								itemSetManager.updateItemSet(player, true, inventory);
								break;
							}
						}
					}
				}
			}.runTaskLater(plugin, 0);
		}
	}
}
