package net.ardeus.myitems.listener.main;

import java.util.Collection;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

import com.praya.agarthalib.utility.EquipmentUtil;

public class ListenerPlayerSwapHandItems extends HandlerEvent implements Listener {
	
	public ListenerPlayerSwapHandItems(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void triggerEquipmentChangeEvent(PlayerSwapHandItemsEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final ItemStack itemMainHand = event.getMainHandItem();
			final ItemStack itemOffHand = event.getOffHandItem();
			
			if (itemSetManager.isItemSet(itemMainHand) || itemSetManager.isItemSet(itemOffHand)) {
				new BukkitRunnable() {
					
					@Override
					public void run() {
						itemSetManager.updateItemSet(player);
					}
				}.runTaskLater(plugin, 0);
			}
		}
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void playerSwapHandItemsEvent(PlayerSwapHandItemsEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final ItemStack itemMainHand = event.getMainHandItem();
			final ItemStack itemOffHand = event.getOffHandItem();
			final boolean enableItemUniversal = mainConfig.isStatsEnableItemUniversal();
			final boolean enableGradeCalculation = mainConfig.isPassiveEnableGradeCalculation();;
			final boolean isShieldMainHand;
			final boolean isShieldOffHand;
			
			if (EquipmentUtil.isSolid(itemMainHand)) {
				final Material materialMainHand = itemMainHand.getType();
				final Collection<PassiveEffectEnum> passiveEffectsMainHand = passiveEffectManager.getPassiveEffects(itemMainHand);
				
				isShieldMainHand = materialMainHand.toString().equalsIgnoreCase("SHIELD");
				passiveEffectManager.reloadPassiveEffect(player, passiveEffectsMainHand, enableGradeCalculation);
			} else {
				isShieldMainHand = false;
			}
			
			if (EquipmentUtil.isSolid(itemOffHand)) {
				final Material materialOffHand = itemOffHand.getType();
				final Collection<PassiveEffectEnum> passiveEffectsOffHand = passiveEffectManager.getPassiveEffects(itemOffHand);
				
				isShieldOffHand = materialOffHand.toString().equalsIgnoreCase("SHIELD");
				passiveEffectManager.reloadPassiveEffect(player, passiveEffectsOffHand, enableGradeCalculation);
			} else {
				isShieldOffHand = false;
			}
			
			if (enableItemUniversal || isShieldMainHand || isShieldOffHand) {
				new BukkitRunnable() {
					
					@Override
					public void run() {
						TriggerSupportUtil.updateSupport(player);
					}
				}.runTaskLater(plugin, 0);
			}
		}
	}
}