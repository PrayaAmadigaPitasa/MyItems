package net.ardeus.myitems.listener.main;

import java.util.Collection;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerEvent;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

import com.praya.agarthalib.utility.EquipmentUtil;

public class ListenerHeldItem extends HandlerEvent implements Listener {
	
	public ListenerHeldItem(MyItems plugin) {
		super(plugin);
	}
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void playerItemHeldEvent(PlayerItemHeldEvent event) {
		final GameManager gameManager = plugin.getGameManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (!event.isCancelled()) {
			final Player player = event.getPlayer();
			final int slotPrevious = event.getPreviousSlot();
			final int slotAfter = event.getNewSlot();
			final Inventory inventory = player.getInventory();
			final ItemStack itemBefore = inventory.getItem(slotPrevious);
			final ItemStack itemAfter = inventory.getItem(slotAfter);
			final boolean enableItemUniversal = mainConfig.isStatsEnableItemUniversal();
			final boolean enableGradeCalculation = mainConfig.isPassiveEnableGradeCalculation();
			final boolean isSolidBefore = EquipmentUtil.isSolid(itemBefore);
			final boolean isSolidAfter = EquipmentUtil.isSolid(itemAfter);		
			
			if (isSolidBefore || isSolidAfter) {
				new BukkitRunnable() {
					
					@Override
					public void run() {
						final boolean isShieldBefore = isSolidBefore ? itemBefore.getType().toString().equalsIgnoreCase("SHIELD") : false;
						final boolean isShieldAfter = isSolidAfter ? itemAfter.getType().toString().equalsIgnoreCase("SHIELD") : false;
						
						if (enableItemUniversal || isShieldBefore || isShieldAfter) {
							TriggerSupportUtil.updateSupport(player);
						}
						
						if (itemSetManager.isItemSet(itemBefore) || itemSetManager.isItemSet(itemAfter)) {
							itemSetManager.updateItemSet(player);
						}
						
						if (isSolidBefore) {
							final Collection<PassiveEffectEnum> passiveEffectBefore = passiveEffectManager.getPassiveEffects(itemBefore);
							
							passiveEffectManager.reloadPassiveEffect(player, passiveEffectBefore, enableGradeCalculation);
						}
						
						if (isSolidAfter) {
							final Collection<PassiveEffectEnum> passiveEffectAfter = passiveEffectManager.getPassiveEffects(itemAfter);
							
							passiveEffectManager.reloadPassiveEffect(player, passiveEffectAfter, enableGradeCalculation);
						}
					}
				}.runTaskLater(plugin, 0);
			}
		}
	}
}