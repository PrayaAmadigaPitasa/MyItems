package net.ardeus.myitems.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuExecutor;
import core.praya.agarthalib.builder.menu.MenuGUI;
import core.praya.agarthalib.builder.menu.MenuSlot;
import core.praya.agarthalib.builder.menu.MenuGUI.SlotCell;
import core.praya.agarthalib.builder.menu.MenuSlotAction.ActionType;
import core.praya.agarthalib.enums.branch.FlagEnum;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.main.RomanNumber;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.SlotType;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.element.ElementBoostStatsWeapon;
import net.ardeus.myitems.handler.HandlerMenu;
import net.ardeus.myitems.item.ItemSetBonusEffectEntity;
import net.ardeus.myitems.item.ItemSetBonusEffectStats;
import net.ardeus.myitems.item.ItemStatsArmor;
import net.ardeus.myitems.item.ItemStatsWeapon;
import net.ardeus.myitems.manager.game.AbilityWeaponManager;
import net.ardeus.myitems.manager.game.ElementManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.ItemSetManager;
import net.ardeus.myitems.manager.player.PlayerItemStatsManager;
import net.ardeus.myitems.manager.player.PlayerManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.TextUtil;

public class MenuStats extends HandlerMenu implements MenuExecutor {

	public MenuStats(MyItems plugin) {
		super(plugin);
	}

	@Override
	public void onClick(Player player, Menu menu, ActionType actionType, String... args) {
		
	}
	
	public final void updateStatsMenu(MenuGUI menuGUI, Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final AbilityWeaponManager abilityWeaponManager = gameManager.getAbilityWeaponManager();
		final ElementManager elementManager = gameManager.getElementManager();
		final ItemSetManager itemSetManager = gameManager.getItemSetManager();
		final PlayerItemStatsManager playerItemStatsManager = playerManager.getPlayerItemStatsManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final String divider = "\n";
		final String headerSlotHelmet = lang.getText(player, "Menu_Item_Header_Stats_Slot_Helmet");
		final String headerSlotChestplate = lang.getText(player, "Menu_Item_Header_Stats_Slot_Chestplate");
		final String headerSlotLeggings = lang.getText(player, "Menu_Item_Header_Stats_Slot_Leggings");
		final String headerSlotBoots = lang.getText(player, "Menu_Item_Header_Stats_Slot_Boots");
		final String headerSlotMainHand = lang.getText(player, "Menu_Item_Header_Stats_Slot_MainHand");
		final String headerSlotOffHand = lang.getText(player, "Menu_Item_Header_Stats_Slot_OffHand");
		final String headerInformation = lang.getText(player, "Menu_Item_Header_Stats_Information");
		final String headerAttack = lang.getText(player, "Menu_Item_Header_Stats_Attack");
		final String headerDefense = lang.getText(player, "Menu_Item_Header_Stats_Defense");
		final String headerAbility = lang.getText(player, "Menu_Item_Header_Stats_Ability");
		final String headerElement = lang.getText(player, "Menu_Item_Header_Stats_Element");
		final SlotCell cellEquipmentHelmet = SlotCell.C2;
		final SlotCell cellEquipmentChestplate = SlotCell.C3;
		final SlotCell cellEquipmentLeggings = SlotCell.C4;
		final SlotCell cellEquipmentBoots = SlotCell.C5;
		final SlotCell cellEquipmentMainHand = SlotCell.E2;
		final SlotCell cellEquipmentOffHand = SlotCell.E3;
		final SlotCell cellInformation = SlotCell.E5;
		final SlotCell cellAttack = SlotCell.G2;
		final SlotCell cellDefense = SlotCell.G3;
		final SlotCell cellAbility = SlotCell.G4;
		final SlotCell cellElement = SlotCell.G5;
		final MenuSlot menuSlotEquipmentHelmet = new MenuSlot(cellEquipmentHelmet.getIndex());
		final MenuSlot menuSlotEquipmentChestplate = new MenuSlot(cellEquipmentChestplate.getIndex());
		final MenuSlot menuSlotEquipmentLeggings = new MenuSlot(cellEquipmentLeggings.getIndex());
		final MenuSlot menuSlotEquipmentBoots = new MenuSlot(cellEquipmentBoots.getIndex());
		final MenuSlot menuSlotEquipmentMainHand = new MenuSlot(cellEquipmentMainHand.getIndex());
		final MenuSlot menuSlotEquipmentOffHand = new MenuSlot(cellEquipmentOffHand.getIndex());
		final MenuSlot menuSlotInformation = new MenuSlot(cellInformation.getIndex());
		final MenuSlot menuSlotAttack = new MenuSlot(cellAttack.getIndex());
		final MenuSlot menuSlotDefense = new MenuSlot(cellDefense.getIndex());
		final MenuSlot menuSlotAbility = new MenuSlot(cellAbility.getIndex());
		final MenuSlot menuSlotElement = new MenuSlot(cellElement.getIndex());
		final HashMap<String, String> map = new HashMap<String, String>();
		final HashMap<String, String> mapData = new HashMap<String, String>();
		
		final ItemStack itemEquipmentHelmet = Bridge.getBridgeEquipment().getEquipment(player, Slot.HELMET);
		final ItemStack itemEquipmentChestplate = Bridge.getBridgeEquipment().getEquipment(player, Slot.CHESTPLATE);
		final ItemStack itemEquipmentLeggings = Bridge.getBridgeEquipment().getEquipment(player, Slot.LEGGINGS);
		final ItemStack itemEquipmentBoots = Bridge.getBridgeEquipment().getEquipment(player, Slot.BOOTS);
		final ItemStack itemEquipmentMainHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
		final ItemStack itemEquipmentOffHand = Bridge.getBridgeEquipment().getEquipment(player, Slot.OFFHAND);
		
		final ItemStatsWeapon itemStatsWeapon = playerItemStatsManager.getItemStatsWeapon(player);
		final ItemStatsArmor itemStatsArmor = playerItemStatsManager.getItemStatsArmor(player);
		final HashMap<AbilityWeapon, Integer> mapAbilityWeapon = abilityWeaponManager.getMapAbilityWeapon(player);
		final HashMap<String, Double> mapElementWeapon = elementManager.getMapElement(player, SlotType.WEAPON, false);
		final HashMap<String, Double> mapElementArmor = elementManager.getMapElement(player, SlotType.ARMOR, false);
		final ItemSetBonusEffectEntity itemSetBonusEffectEntity = itemSetManager.getItemSetBonusEffectEntity(player, true, false);
		final ItemSetBonusEffectStats itemSetBonusEffectStats = itemSetBonusEffectEntity.getEffectStats();
		final ElementBoostStatsWeapon elementWeapon = elementManager.getElementBoostStats(mapElementWeapon);
		
		final double elementAdditionalDamage = elementWeapon.getBaseAdditionalDamage();
		final double elementPercentDamage = elementWeapon.getBasePercentDamage();
		
		final double abilityBaseBonusDamage = abilityWeaponManager.getTotalBaseBonusDamage(mapAbilityWeapon);
		final double abilityBasePercentDamage = abilityWeaponManager.getTotalBasePercentDamage(mapAbilityWeapon);
		final double abilityCastBonusDamage = abilityWeaponManager.getTotalCastBonusDamage(mapAbilityWeapon);
		final double abilityCastPercentDamage = abilityWeaponManager.getTotalCastPercentDamage(mapAbilityWeapon);
		
		final double totalDamageMin = (itemStatsWeapon.getTotalDamageMin() + itemSetBonusEffectStats.getAdditionalDamage()) * ((100 + itemSetBonusEffectStats.getPercentDamage()) / 100);
		final double totalDamageMax = (itemStatsWeapon.getTotalDamageMax() + itemSetBonusEffectStats.getAdditionalDamage()) * ((100 + itemSetBonusEffectStats.getPercentDamage()) / 100);
		final double totalPenetration = itemStatsWeapon.getTotalPenetration() + itemSetBonusEffectStats.getPenetration();
		final double totalPvPDamage = itemStatsWeapon.getTotalPvPDamage() + itemSetBonusEffectStats.getPvPDamage();
		final double totalPvEDamage = itemStatsWeapon.getTotalPvEDamage() + itemSetBonusEffectStats.getPvEDamage();
		final double totalCriticalChance = itemStatsWeapon.getTotalCriticalChance() + itemSetBonusEffectStats.getCriticalChance();
		final double totalCriticalDamage = itemStatsWeapon.getTotalCriticalDamage() + itemSetBonusEffectStats.getCriticalDamage();
		final double totalAttackAoERadius = itemStatsWeapon.getTotalAttackAoERadius() + itemSetBonusEffectStats.getAttackAoERadius();
		final double totalAttackAoEDamage = itemStatsWeapon.getTotalAttackAoEDamage() + itemSetBonusEffectStats.getAttackAoEDamage();
		final double totalHitRate = itemStatsWeapon.getTotalHitRate() + itemSetBonusEffectStats.getHitRate();
		final double totalDefense = (itemStatsArmor.getTotalDefense() + itemSetBonusEffectStats.getAdditionalDefense()) * ((100 + itemSetBonusEffectStats.getPercentDefense()) / 100);
		final double totalPvPDefense = itemStatsArmor.getTotalPvPDefense() + itemSetBonusEffectStats.getPvPDefense();
		final double totalPvEDefense = itemStatsArmor.getTotalPvEDefense() + itemSetBonusEffectStats.getPvEDefense();
		final double totalHealth = itemStatsArmor.getTotalHealth() + itemSetBonusEffectStats.getHealth();
		final double totalHealthRegen = itemStatsArmor.getTotalHealthRegen() + itemSetBonusEffectStats.getHealthRegen();
		final double totalStaminaMax = itemStatsArmor.getTotalStaminaMax() + itemSetBonusEffectStats.getStaminaMax();
		final double totalStaminaRegen = itemStatsArmor.getTotalStaminaRegen() + itemSetBonusEffectStats.getStaminaRegen();
		final double totalBlockAmount = itemStatsArmor.getTotalBlockAmount() + itemSetBonusEffectStats.getBlockAmount();
		final double totalBlockRate = itemStatsArmor.getTotalBlockRate() + itemSetBonusEffectStats.getBlockRate();
		final double totalDodgeRate = itemStatsArmor.getTotalDodgeRate() + itemSetBonusEffectStats.getDodgeRate();
		
		final List<String> loreAbilityDataWeapon = new ArrayList<String>();
		final List<String> loreElementDataWeapon = new ArrayList<String>();
		final List<String> loreElementDataArmor = new ArrayList<String>();
		
		String loreDamage = String.valueOf(MathUtil.roundNumber(totalDamageMin));
		
		List<String> loreInformation = lang.getListText(player, "Menu_Item_Lores_Stats_Information");
		List<String> loreAttack = lang.getListText(player, "Menu_Item_Lores_Stats_Attack");
		List<String> loreDefense = lang.getListText(player, "Menu_Item_Lores_Stats_Defense");
		List<String> loreAbility = lang.getListText(player, "Menu_Item_Lores_Stats_Ability");
		List<String> loreElement = lang.getListText(player, "Menu_Item_Lores_Stats_Element");
		
		for (AbilityWeapon abilityWeaponItemSet : itemSetBonusEffectEntity.getAllAbilityWeapon()) {
			final int grade = itemSetBonusEffectEntity.getGradeAbilityWeapon(abilityWeaponItemSet);
			final int maxGrade = abilityWeaponItemSet.getMaxGrade();
					
			if (mapAbilityWeapon.containsKey(abilityWeaponItemSet)) {
				final int totalGrade = mapAbilityWeapon.get(abilityWeaponItemSet) + grade;
				
				mapAbilityWeapon.put(abilityWeaponItemSet, Math.min(maxGrade, totalGrade));
			} else {
				mapAbilityWeapon.put(abilityWeaponItemSet, Math.min(maxGrade, grade));
			}
		}
		
		for (AbilityWeapon abilityWeapon : mapAbilityWeapon.keySet()) {
			final String abilityType = abilityWeapon.getId();
			final int abilityGrade = mapAbilityWeapon.get(abilityWeapon);
			
			String formatAbilityData = lang.getText(player, "Menu_Item_Format_Stats_Ability_Data");
			
			mapData.clear();
			mapData.put("ability_type", abilityType);
			mapData.put("ability_grade", RomanNumber.getRomanNumber(abilityGrade));
			
			formatAbilityData = TextUtil.placeholder(mapData, formatAbilityData);
			
			loreAbilityDataWeapon.add(formatAbilityData);
		}
		
		for (String keyElement : mapElementWeapon.keySet()) {
			final double elementValue = mapElementWeapon.get(keyElement);
			
			String formatElementData = lang.getText(player, "Menu_Item_Format_Stats_Element_Data");
			
			mapData.clear();
			mapData.put("element_type", keyElement);
			mapData.put("element_value", String.valueOf(elementValue));
			
			formatElementData = TextUtil.placeholder(mapData, formatElementData);
			
			loreElementDataWeapon.add(formatElementData);
		}
		
		for (String keyElement : mapElementArmor.keySet()) {
			final double elementValue = mapElementArmor.get(keyElement);
			
			String formatElementData = lang.getText(player, "Menu_Item_Format_Stats_Element_Data");
			
			mapData.clear();
			mapData.put("element_type", keyElement);
			mapData.put("element_value", String.valueOf(elementValue));
			
			formatElementData = TextUtil.placeholder(mapData, formatElementData);
			
			loreElementDataArmor.add(formatElementData);
		}
		
		if (totalDamageMin != totalDamageMax) {
			
			loreDamage = lang.getText(player, "Menu_Item_Format_Stats_Attack_Damage_Range");
			
			mapData.clear();
			mapData.put("stats_damage_min", String.valueOf(MathUtil.roundNumber(totalDamageMin)));
			mapData.put("stats_damage_max", String.valueOf(MathUtil.roundNumber(totalDamageMax)));
			
			loreDamage = TextUtil.placeholder(mapData, loreDamage);
		}
		
		final String lineAbilityDataWeapon = TextUtil.convertListToString(loreAbilityDataWeapon, divider);
		final String lineElementDataWeapon = TextUtil.convertListToString(loreElementDataWeapon, divider);
		final String lineElementDataArmor = TextUtil.convertListToString(loreElementDataArmor, divider);
		final String lineAbilityEmpty = lang.getText(player, "Menu_Item_Format_Stats_Ability_Empty");
		final String lineElementEmpty = lang.getText(player, "Menu_Item_Format_Stats_Element_Empty");
		
		map.put("stats_damage", loreDamage);
		map.put("stats_penetration", String.valueOf(MathUtil.roundNumber(totalPenetration)));
		map.put("stats_pvp_damage", String.valueOf(MathUtil.roundNumber(totalPvPDamage)));
		map.put("stats_pve_damage", String.valueOf(MathUtil.roundNumber(totalPvEDamage)));
		map.put("stats_critical_chance", String.valueOf(MathUtil.roundNumber(totalCriticalChance)));
		map.put("stats_critical_damage", String.valueOf(MathUtil.roundNumber(totalCriticalDamage)));
		map.put("stats_aoe_radius", String.valueOf(MathUtil.roundNumber(totalAttackAoERadius)));
		map.put("stats_aoe_damage", String.valueOf(MathUtil.roundNumber(totalAttackAoEDamage)));
		map.put("stats_hit_rate", String.valueOf(MathUtil.roundNumber(totalHitRate)));
		map.put("stats_defense", String.valueOf(MathUtil.roundNumber(totalDefense)));
		map.put("stats_pvp_defense", String.valueOf(MathUtil.roundNumber(totalPvPDefense)));
		map.put("stats_pve_defense", String.valueOf(MathUtil.roundNumber(totalPvEDefense)));
		map.put("stats_health", String.valueOf(MathUtil.roundNumber(totalHealth)));
		map.put("stats_health_regen", String.valueOf(MathUtil.roundNumber(totalHealthRegen)));
		map.put("stats_stamina_max", String.valueOf(MathUtil.roundNumber(totalStaminaMax)));
		map.put("stats_stamina_regen", String.valueOf(MathUtil.roundNumber(totalStaminaRegen)));
		map.put("stats_block_amount", String.valueOf(MathUtil.roundNumber(totalBlockAmount)));
		map.put("stats_block_rate", String.valueOf(MathUtil.roundNumber(totalBlockRate)));
		map.put("stats_dodge_rate", String.valueOf(MathUtil.roundNumber(totalDodgeRate)));
		map.put("ability_base_additional_damage", String.valueOf(MathUtil.roundNumber(abilityBaseBonusDamage)));
		map.put("ability_base_percent_damage", String.valueOf(MathUtil.roundNumber(abilityBasePercentDamage)));
		map.put("ability_cast_additional_damage", String.valueOf(MathUtil.roundNumber(abilityCastBonusDamage)));
		map.put("ability_cast_percent_damage", String.valueOf(MathUtil.roundNumber(abilityCastPercentDamage)));
		map.put("element_additional_damage", String.valueOf(MathUtil.roundNumber(elementAdditionalDamage)));
		map.put("element_percent_damage", String.valueOf(MathUtil.roundNumber(elementPercentDamage)));
		map.put("ability_data", mapAbilityWeapon.isEmpty() ? lineAbilityEmpty : lineAbilityDataWeapon);
		map.put("ability_data_weapon", mapAbilityWeapon.isEmpty() ? lineAbilityEmpty : lineAbilityDataWeapon);
		map.put("element_data_weapon", mapElementWeapon.isEmpty() ? lineElementEmpty : lineElementDataWeapon);
		map.put("element_data_armor", mapElementArmor.isEmpty() ? lineElementEmpty : lineElementDataArmor);
		
		loreInformation = TextUtil.placeholder(map, loreInformation);
		loreAttack = TextUtil.placeholder(map, loreAttack);
		loreDefense = TextUtil.placeholder(map, loreDefense);
		loreAbility = TextUtil.placeholder(map, loreAbility);
		loreElement = TextUtil.placeholder(map, loreElement);
		
		loreInformation = TextUtil.expandList(loreInformation, divider);
		loreAttack = TextUtil.expandList(loreAttack, divider);
		loreDefense = TextUtil.expandList(loreDefense, divider);
		loreAbility = TextUtil.expandList(loreAbility, divider);
		loreElement = TextUtil.expandList(loreElement, divider);
		
		final ItemStack itemSlotHelmet = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerSlotHelmet, 1);
		final ItemStack itemSlotChestplate = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerSlotChestplate, 1);
		final ItemStack itemSlotLeggings = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerSlotLeggings, 1);
		final ItemStack itemSlotBoots = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerSlotBoots, 1);
		final ItemStack itemSlotMainHand = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerSlotMainHand, 1);
		final ItemStack itemSlotOffHand = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerSlotOffHand, 1);
		final ItemStack itemInformation = EquipmentUtil.createItem(MaterialEnum.SIGN, headerInformation, 1, loreInformation);
		final ItemStack itemAttack = EquipmentUtil.createItem(MaterialEnum.IRON_SWORD, headerAttack, 1, loreAttack);
		final ItemStack itemDefense = EquipmentUtil.createItem(MaterialEnum.IRON_CHESTPLATE, headerDefense, 1, loreDefense);
		final ItemStack itemAbility = EquipmentUtil.createItem(MaterialEnum.BLAZE_POWDER, headerAbility, 1, loreAbility);
		final ItemStack itemElement = EquipmentUtil.createItem(MaterialEnum.MAGMA_CREAM, headerElement, 1, loreElement);
		
		EquipmentUtil.addFlag(itemAttack, FlagEnum.HIDE_ATTRIBUTES);
		EquipmentUtil.addFlag(itemDefense, FlagEnum.HIDE_ATTRIBUTES);
		
		menuSlotEquipmentHelmet.setItem(EquipmentUtil.isSolid(itemEquipmentHelmet) ? itemEquipmentHelmet : itemSlotHelmet);
		menuSlotEquipmentChestplate.setItem(EquipmentUtil.isSolid(itemEquipmentChestplate) ? itemEquipmentChestplate : itemSlotChestplate);
		menuSlotEquipmentLeggings.setItem(EquipmentUtil.isSolid(itemEquipmentLeggings) ? itemEquipmentLeggings : itemSlotLeggings);
		menuSlotEquipmentBoots.setItem(EquipmentUtil.isSolid(itemEquipmentBoots) ? itemEquipmentBoots : itemSlotBoots);
		menuSlotEquipmentMainHand.setItem(EquipmentUtil.isSolid(itemEquipmentMainHand) ? itemEquipmentMainHand : itemSlotMainHand);
		menuSlotEquipmentOffHand.setItem(EquipmentUtil.isSolid(itemEquipmentOffHand) ? itemEquipmentOffHand : itemSlotOffHand);
		menuSlotInformation.setItem(itemInformation);
		menuSlotAttack.setItem(itemAttack);
		menuSlotDefense.setItem(itemDefense);
		menuSlotAbility.setItem(itemAbility);
		menuSlotElement.setItem(itemElement);
		
		menuGUI.setMenuSlot(menuSlotEquipmentHelmet);
		menuGUI.setMenuSlot(menuSlotEquipmentChestplate);
		menuGUI.setMenuSlot(menuSlotEquipmentLeggings);
		menuGUI.setMenuSlot(menuSlotEquipmentBoots);
		menuGUI.setMenuSlot(menuSlotEquipmentMainHand);
		menuGUI.setMenuSlot(menuSlotEquipmentOffHand);
		menuGUI.setMenuSlot(menuSlotInformation);
		menuGUI.setMenuSlot(menuSlotAttack);
		menuGUI.setMenuSlot(menuSlotDefense);
		menuGUI.setMenuSlot(menuSlotAbility);
		menuGUI.setMenuSlot(menuSlotElement);
		
		player.updateInventory();
	}
}
