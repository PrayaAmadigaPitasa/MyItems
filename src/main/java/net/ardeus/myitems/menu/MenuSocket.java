package net.ardeus.myitems.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.MetadataValue;

import api.praya.agarthalib.builder.support.SupportVault;
import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;
import core.praya.agarthalib.builder.menu.Menu;
import core.praya.agarthalib.builder.menu.MenuExecutor;
import core.praya.agarthalib.builder.menu.MenuGUI;
import core.praya.agarthalib.builder.menu.MenuSlot;
import core.praya.agarthalib.builder.menu.MenuGUI.SlotCell;
import core.praya.agarthalib.builder.menu.MenuSlotAction.ActionCategory;
import core.praya.agarthalib.builder.menu.MenuSlotAction.ActionType;
import core.praya.agarthalib.enums.branch.MaterialEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.RomanNumber;
import core.praya.agarthalib.enums.main.SlotType;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerMenu;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.socket.SocketGem;
import net.ardeus.myitems.socket.SocketGemTree;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.MetadataUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public class MenuSocket extends HandlerMenu implements MenuExecutor {

	public MenuSocket(MyItems plugin) {
		super(plugin);
	}

	@Override
	public void onClick(Player player, Menu menu, ActionType actionType, String... args) {
		final AgarthaLibAPI agarthaLibAPI = AgarthaLibAPI.getInstance();
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final SupportManagerAPI supportManager = agarthaLibAPI.getPluginManagerAPI().getSupportManager();
		final SupportVault supportVault = supportManager.getSupportVault();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (menu instanceof MenuGUI) {
			final MenuGUI menuGUI = (MenuGUI) menu;
			
			if (args.length > 0) {
				final String label = args[0];
				
				if (label.equalsIgnoreCase("MyItems")) {
					if (args.length > 1) {
						final String key = args[1];
						
						if (key.equalsIgnoreCase("Socket")) {
							if (args.length > 2) {
								final String subArg = args[2];
								final String metadataID = "MyItems Socket Line_Selector";
								
								if (subArg.equalsIgnoreCase("Item_Input") || subArg.equalsIgnoreCase("Socket_Input")) {
									if (args.length > 3) {
										final String textCell = args[3];
										final SlotCell cell = SlotCell.valueOf(textCell);
										final Inventory inventory = menu.getInventory();
										final int index = cell.getIndex();
										final ItemStack itemCurrent = inventory.getItem(index);
										final ItemStack itemCursor = player.getItemOnCursor();
										final boolean isSocketSlot = subArg.equalsIgnoreCase("Socket_Input");
										
										if (EquipmentUtil.isSolid(itemCursor)) {
											if (isSocketSlot) {
												final ItemStack itemRodUnlock = mainConfig.getSocketItemRodUnlock();
												final ItemStack itemRodRemove = mainConfig.getSocketItemRodRemove();
												final boolean isGems = socketManager.isSocketGemItem(itemCursor);
												final boolean isRodUnlock = itemCursor.isSimilar(itemRodUnlock);
												final boolean isRodRemove = itemCursor.isSimilar(itemRodRemove);
												
												if (!(isGems || isRodUnlock || isRodRemove)) {
													return;
												}
											} else {
												final boolean containsSocketEmpty = socketManager.containsSocketEmpty(itemCursor);
												final boolean containsSocketGems = socketManager.containsSocketGems(itemCursor);
												final boolean containsSocketLocked = socketManager.containsSocketLocked(itemCursor);
												
												if (!(containsSocketEmpty || containsSocketGems || containsSocketLocked)) {
													return;
												}
											}
										}
										
										if (EquipmentUtil.isSolid(itemCurrent)) {
											final String headerItemInput = lang.getText(player, "Menu_Item_Header_Socket_Item_Input");
											final String headerSocketInput = lang.getText(player, "Menu_Item_Header_Socket_Socket_Input");
											final ItemStack itemItemInput = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, headerItemInput, 1);
											final ItemStack itemSocketInput = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, headerSocketInput, 1);
											
											if (itemCurrent.isSimilar(itemItemInput) || itemCurrent.isSimilar(itemSocketInput)) {
												if (EquipmentUtil.isSolid(itemCursor)) {
													inventory.setItem(index, itemCursor);
													player.setItemOnCursor(null);
												} else {
													return;
												}
											} else {
												if (!EquipmentUtil.isSolid(itemCursor)) {
													final ItemStack itemReplace = isSocketSlot ? itemSocketInput : itemItemInput;
													
													player.setItemOnCursor(itemCurrent);
													inventory.setItem(index, itemReplace);
												} else {
													if (itemCurrent.isSimilar(itemCursor)) {
														final int amountCurrent = itemCurrent.getAmount();
														final int amountCursor = itemCursor.getAmount();
														final int amountTotal = amountCurrent + amountCursor;
														final int maxStack = itemCurrent.getMaxStackSize();
														final int newCurrent = MathUtil.limitInteger(amountTotal, 1, maxStack);
														final int newCursor = amountTotal - newCurrent;
														
														itemCurrent.setAmount(newCurrent);
														itemCursor.setAmount(newCursor);
														
														inventory.setItem(index, itemCurrent);
														
														if (newCursor == 0) {
															player.setItemOnCursor(null);
														} else {
															player.setItemOnCursor(itemCursor);
														}
													} else {
														inventory.setItem(index, itemCursor);
														player.setItemOnCursor(itemCurrent);
													}
												}
											}
											
											MetadataUtil.removeMetadata(player, metadataID);
											updateSocketMenu(menuGUI, player);
											return;
										}
									}
								} else if (subArg.equalsIgnoreCase("Line_Selector")) {
									if (args.length > 3) {
										final String textLine = args[3];
										final int line = MathUtil.parseInteger(textLine);
										final MetadataValue metadata = MetadataUtil.createMetadata(line);
										
										player.setMetadata(metadataID, metadata);
										updateSocketMenu(menuGUI, player);
										return;
									}
								} else if (subArg.equalsIgnoreCase("Accept")) {
									final SlotCell cellItemResult = SlotCell.G3;
									final Inventory inventory = menu.getInventory();
									final String headerItemItemResult = lang.getText(player, "Menu_Item_Header_Socket_Item_Result");
									final ItemStack itemItemResult = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerItemItemResult, 1);
									final ItemStack itemResult = inventory.getItem(cellItemResult.getIndex());
									
									if (!itemResult.isSimilar(itemItemResult)) {
										final SlotCell cellItemInput = SlotCell.B3;
										final SlotCell cellSocketInput = SlotCell.C3;
										final String headerItemInput = lang.getText(player, "Menu_Item_Header_Socket_Item_Input");
										final String headerSocketInput = lang.getText(player, "Menu_Item_Header_Socket_Socket_Input");
										final ItemStack itemItemInput = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, headerItemInput, 1);
										final ItemStack itemSocketInput = EquipmentUtil.createItem(MaterialEnum.WHITE_STAINED_GLASS_PANE, headerSocketInput, 1);
										final ItemStack itemItem = inventory.getItem(cellItemInput.getIndex());
										final ItemStack itemSocket = inventory.getItem(cellSocketInput.getIndex());
										final ItemStack itemRodUnlock = mainConfig.getSocketItemRodUnlock();
										final ItemStack itemRodRemove = mainConfig.getSocketItemRodRemove();
										final int line = MetadataUtil.getMetadata(player, metadataID).asInt();
										final int amountItem = itemItem.getAmount();
										final int amountSocket = itemSocket.getAmount();
										final int actionId;
										final double actionCost;
										
										if (socketManager.isSocketGemItem(itemSocket)) {
											actionId = 0;
											actionCost = mainConfig.getSocketCostSocket();
										} else if (itemSocket.isSimilar(itemRodUnlock)) {
											actionId = 1;
											actionCost = mainConfig.getSocketCostUnlock();
										} else if (itemSocket.isSimilar(itemRodRemove)) {
											actionId = 2;
											actionCost = mainConfig.getSocketCostDesocket();
										} else {
											actionId = -1;
											actionCost = 0;
										}
										
										if (supportVault != null) {
											final double playerBalance = supportVault.getBalance(player);
											
											if (playerBalance < actionCost) {
												final String message = lang.getText(player, "Argument_Lack_Money");
												
												SenderUtil.sendMessage(player, message);
												SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
												return;
											} else {
												supportVault.remBalance(player, actionCost);
											}
										}
										
										if (amountSocket > 1) {
											final int amountSocketLeft = amountSocket - 1;
											
											itemSocket.setAmount(amountSocketLeft);
											inventory.setItem(cellSocketInput.getIndex(), itemSocket);
										} else {
											inventory.setItem(cellSocketInput.getIndex(), itemSocketInput);
										}
										
										if (actionId == 0) {
											final SocketGem socketBuild = socketManager.getSocketGemByItem(itemSocket);
											final String socketId = socketBuild.getGem();
											final double chance = socketBuild.getSuccessRate();
											final int grade = socketBuild.getGrade();
											
											if (!MathUtil.chanceOf(chance)) {
												final String message = lang.getText(player, "Socket_Input_Failure");
												
												SenderUtil.sendMessage(player, message);
												SenderUtil.playSound(player, SoundEnum.ENTITY_BLAZE_DEATH);
												return;
											} else {
												final HashMap<String, String> map = new HashMap<String, String>();
												
												String message = lang.getText(player, "Socket_Input_Success");
												
												map.put("socket", socketId);
												map.put("grade", RomanNumber.getRomanNumber(grade));
												map.put("line", String.valueOf(line));
												
												message = TextUtil.placeholder(map, message);
												
												PlayerUtil.addItem(player, itemResult);
												SenderUtil.sendMessage(player, message);
												SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
											}
										} else if (actionId == 1) {
											final HashMap<String, String> map = new HashMap<String, String>();
											
											String message = lang.getText(player, "Socket_Unlock_Success");
											
											map.put("line", String.valueOf(line));
											
											message = TextUtil.placeholder(map, message);
											
											PlayerUtil.addItem(player, itemResult);
											SenderUtil.sendMessage(player, message);
											SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
										} else if (actionId == 2) {
											final HashMap<String, String> map = new HashMap<String, String>();
											
											String message = lang.getText(player, "Socket_Remove_Success");
											
											map.put("line", String.valueOf(line));
											
											message = TextUtil.placeholder(map, message);
											
											PlayerUtil.addItem(player, itemResult);
											SenderUtil.sendMessage(player, message);
											SenderUtil.playSound(player, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
										}
										
										if (amountItem > 1) {
											final int amountItemLeft = amountItem - 1;
											
											itemItem.setAmount(amountItemLeft);
											inventory.setItem(cellItemInput.getIndex(), itemItem);
										} else {
											inventory.setItem(cellItemInput.getIndex(), itemItemInput);
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public final void updateSocketMenu(MenuGUI menuGUI, Player player) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		final String metadataID = "MyItems Socket Line_Selector";
		final String headerLineSelector = lang.getText(player, "Menu_Item_Header_Socket_Line_Selector");
		final SlotCell cellItemInput = SlotCell.B3;
		final SlotCell cellSocketInput = SlotCell.C3;
		final SlotCell cellItemResult = SlotCell.G3;
		final SlotCell[] cellsLineSelector = new SlotCell[] {SlotCell.F2, SlotCell.G2, SlotCell.H2};
		final MenuSlot menuSlotItemResult = new MenuSlot(cellItemResult.getIndex());
		final Inventory inventory = menuGUI.getInventory();
		final ItemStack itemItemInput = inventory.getItem(cellItemInput.getIndex());
		final ItemStack itemSocketInput = inventory.getItem(cellSocketInput.getIndex());		
		final HashMap<String, String> map = new HashMap<String, String>();
		
		int socketActionID = -1;
		int socketLine = -1;
		int socketLinePrevious = -1;
		int socketLineNext = -1;
		
		List<String> loreLineSelector = lang.getListText(player, "Menu_Item_Lores_Socket_Line_Selector");
		
		if (EquipmentUtil.isSolid(itemItemInput) && EquipmentUtil.isSolid(itemSocketInput)) {
			final ItemStack itemRodUnlock = mainConfig.getSocketItemRodUnlock();
			final ItemStack itemRodRemove = mainConfig.getSocketItemRodRemove();
			final boolean containsSocketEmpty = socketManager.containsSocketEmpty(itemItemInput);
			final boolean containsSocketLocked = socketManager.containsSocketLocked(itemItemInput);
			final boolean containsSocketGems = socketManager.containsSocketGems(itemItemInput);
			final boolean isSocketGems = socketManager.isSocketGemItem(itemSocketInput);
			final boolean isSocketRodUnlock = itemSocketInput.isSimilar(itemRodUnlock);
			final boolean isSocketRodRemove = itemSocketInput.isSimilar(itemRodRemove);
			
			if (containsSocketEmpty && isSocketGems) {
				final SocketGem socket = socketManager.getSocketGemByItem(itemSocketInput);
				final SocketGemTree socketTree = socket.getSocketTree();
				final SlotType typeItem = socketTree.getTypeItem();
				final SlotType typeDefault = core.praya.agarthalib.enums.main.SlotType.getSlotType(itemItemInput);
				
				if (typeItem.equals(typeDefault) || typeItem.equals(SlotType.UNIVERSAL)) {
					socketActionID = 0;
				}
			} else if (containsSocketLocked && isSocketRodUnlock) {
				socketActionID = 1;
			} else if (containsSocketGems && isSocketRodRemove) {
				socketActionID = 2;
			}
			
			if (socketActionID != -1) {
				final List<Integer> listLineSocket;
				
				switch (socketActionID) {
				case 0 : listLineSocket = socketManager.getLineLoresSocketEmpty(itemItemInput); break;
				case 1 : listLineSocket = socketManager.getLineLoresSocketLocked(itemItemInput); break;
				case 2 : listLineSocket = socketManager.getLineLoresSocket(itemItemInput); break;
				default : listLineSocket = new ArrayList<>();
				}
				
				final int size = listLineSocket.size();
				final boolean hasMetadataLine = MetadataUtil.hasMetadata(player, metadataID);
				
				if (hasMetadataLine) {
					final int metadataLine = MetadataUtil.getMetadata(player, metadataID).asInt();
					
					int order = 0;
					
					for (int index = 0; index < size; index++) {
						if (listLineSocket.get(index) == metadataLine) {
							order = index;
							break;
						}
					}
					
					socketLine = listLineSocket.contains(metadataLine) ? metadataLine : listLineSocket.get(0);
					socketLinePrevious = order == 0 ? listLineSocket.get(size-1) : listLineSocket.get(order-1);
					socketLineNext = order == (size-1) ? listLineSocket.get(0) : listLineSocket.get(order+1);
				} else {
					socketLine = listLineSocket.get(0);
					socketLinePrevious = listLineSocket.get(size-1);
					socketLineNext = size > 1 ? listLineSocket.get(1) : listLineSocket.get(0);
				}
			}
		}
		
		final String socketAction;
		final double socketCost;
		
		if (socketActionID == 0) {
			socketAction = lang.getText(player, "Socket_Action_Gems");
			socketCost = mainConfig.getSocketCostSocket();
		} else if (socketActionID == 1) {
			socketAction = lang.getText(player, "Socket_Action_Unlock");
			socketCost = mainConfig.getSocketCostUnlock();
		} else if (socketActionID == 2) {
			socketAction = lang.getText(player, "Socket_Action_Desocket");
			socketCost = mainConfig.getSocketCostDesocket();
		} else {
			socketAction = lang.getText(player, "Socket_Action_Unknown");
			socketCost = 0;
		}
		
		map.put("socket_line", socketLine == -1 ? "None" : String.valueOf(socketLine));
		map.put("socket_action", String.valueOf(socketAction));
		map.put("socket_cost", String.valueOf(socketCost));
		map.put("symbol_currency", mainConfig.getUtilityCurrency());
		loreLineSelector = TextUtil.placeholder(map, loreLineSelector);
		
		final ItemStack itemLineSelector = EquipmentUtil.createItem(MaterialEnum.SIGN, headerLineSelector, 1, loreLineSelector);
				
		for (SlotCell cell : cellsLineSelector) {
			final int slot = cell.getIndex();
			final MenuSlot menuSlot = new MenuSlot(slot);
			final String actionLinePrevious = "MyItems Socket Line_Selector " + socketLinePrevious;
			final String actionLineNext = "MyItems Socket Line_Selector " + socketLineNext;
			
			menuSlot.setItem(itemLineSelector);
			menuSlot.setActionArguments(ActionCategory.ALL_LEFT_CLICK, actionLinePrevious);
			menuSlot.setActionArguments(ActionCategory.ALL_RIGHT_CLICK, actionLineNext);
			menuGUI.setMenuSlot(menuSlot);
		}
		
		if (socketActionID == -1) {
			final String headerItemItemResult = lang.getText(player, "Menu_Item_Header_Socket_Item_Result");
			final ItemStack itemItemResult = EquipmentUtil.createItem(MaterialEnum.RED_STAINED_GLASS_PANE, headerItemItemResult, 1);
			
			menuSlotItemResult.setItem(itemItemResult);
			menuGUI.setMenuSlot(menuSlotItemResult);
		} else {
			final ItemStack itemItemResult = itemItemInput.clone();
			final MetadataValue metadata = MetadataUtil.createMetadata(socketLine);
			
			if (socketActionID == 0) {
				final SocketGem socketBuild = socketManager.getSocketGemByItem(itemSocketInput);
				final String gemsId = socketBuild.getGem();
				final int gemsGrade = socketBuild.getGrade();
				final String loreGems = socketManager.getTextSocketGemsLore(gemsId, gemsGrade);
				
				EquipmentUtil.setLore(itemItemResult, socketLine, loreGems);
			} else if (socketActionID == 1 || socketActionID == 2) {
				final String loreEmpty = socketManager.getTextSocketGemSlotEmpty();
				
				EquipmentUtil.setLore(itemItemResult, socketLine, loreEmpty);
			}
			
			itemItemResult.setAmount(1);
			player.setMetadata(metadataID, metadata);
			menuSlotItemResult.setItem(itemItemResult);
			menuGUI.setMenuSlot(menuSlotItemResult);
		}
		
		player.updateInventory();
	}
}
