package net.ardeus.myitems.command.executor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;

public class CommandLoreClear extends HandlerCommand implements CommandExecutor {

	public CommandLoreClear(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		return clearLore(sender, command, label, args);
	}
	
	protected static final boolean clearLore(CommandSender sender, Command command, String label, String[] args) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!commandManager.checkPermission(sender, "Lore_Clear")) {
			final String permission = commandManager.getPermission("Lore_Clear");
			final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else if (!SenderUtil.isPlayer(sender)) {
			final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else {
			final Player player = PlayerUtil.parse(sender);
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (!EquipmentUtil.isSolid(item)) {
				final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			} else if (!EquipmentUtil.hasLore(item)) {
				final MessageBuild message = lang.getMessage(sender, "Item_Lore_Empty");
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			} else {		
				final MessageBuild message = lang.getMessage(sender, "MyItems_ClearLore_Success");
				
				EquipmentUtil.clearLore(item);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
				
				player.updateInventory();
				return true;
			}
		}
	}
}
