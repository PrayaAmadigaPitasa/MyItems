package net.ardeus.myitems.command.executor;

import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EnchantmentUtil;
import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public class CommandEnchantAdd extends HandlerCommand implements CommandExecutor {

	public CommandEnchantAdd(MyItems plugin) {
		super(plugin);
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		return addEnchant(sender, command, label, args);
	}
	
	@SuppressWarnings("deprecation")
	protected static final boolean addEnchant(CommandSender sender, Command command, String label, String[] args) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!commandManager.checkPermission(sender, "Enchant_Add")) {
			final String permission = commandManager.getPermission("Enchant_Add");
			final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else if (!SenderUtil.isPlayer(sender)) {
			final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else {
			final Player player = PlayerUtil.parse(sender);
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (!EquipmentUtil.isSolid(item)) {
				final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			} else {		
				if (args.length < 2) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Enchant_Add"));
					final MessageBuild message = lang.getMessage(sender, "Argument_AddEnchant");
					
					message.sendMessage(sender, "tooltip_enchant_add", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final String enchantmentName = args[0];
					final String textGrade = args[1];
					final Enchantment enchantment = EnchantmentUtil.getEnchantment(enchantmentName);
					
					if (!MathUtil.isNumber(textGrade)) {
						final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else if (enchantment == null) {
						final MessageBuild message = lang.getMessage(sender, "Item_Enchantment_Not_Exist");
						
						message.sendMessage(sender, "enchantment", enchantmentName);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						
						final MessageBuild message = lang.getMessage(sender, "MyItems_AddEnchant_Success");
						final int grade = Math.max(1, MathUtil.parseInteger(textGrade));
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						mapPlaceholder.put("enchantment", enchantment.getName());
						mapPlaceholder.put("grade", String.valueOf(grade));
						
						EquipmentUtil.addEnchantment(item, enchantment, grade);
						
						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						
						player.updateInventory();
						return true;
					}
				}
			}
		}
	}
}
