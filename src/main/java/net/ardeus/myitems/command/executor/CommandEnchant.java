package net.ardeus.myitems.command.executor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.praya.agarthalib.utility.TextUtil;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.CommandManager;

public class CommandEnchant extends HandlerCommand implements CommandExecutor {

	public CommandEnchant(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		final CommandManager commandManager = plugin.getPluginManager().getCommandManager();
		
		if (args.length > 0) {
			final String subCommand = args[0];
			
			if (commandManager.checkCommand(subCommand, "Enchant_Add")) {
				final String[] fullArgs = TextUtil.pressList(args, 2);
				
				return CommandEnchantAdd.addEnchant(sender, command, label, fullArgs);
			} else if (commandManager.checkCommand(subCommand, "Enchant_Remove")) {
				final String[] fullArgs = TextUtil.pressList(args, 2);
				
				return CommandEnchantRemove.removeEnchant(sender, command, label, fullArgs);
			} else if (commandManager.checkCommand(subCommand, "Enchant_Clear")) {
				final String[] fullArgs = TextUtil.pressList(args, 2);
				
				return CommandEnchantClear.clearEnchant(sender, command, label, fullArgs);
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
}
