package net.ardeus.myitems.command.executor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.RomanNumber;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.SocketGemManager;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.socket.SocketGem;
import net.ardeus.myitems.socket.SocketGemTree;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.SortUtil;
import com.praya.agarthalib.utility.TextUtil;
import com.praya.agarthalib.utility.WorldUtil;

public class CommandSocket extends HandlerCommand implements CommandExecutor {

	public CommandSocket(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final SocketGemManager socketManager = gameManager.getSocketManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (args.length > 0) {
			final String subCommand = args[0];
			
			if (commandManager.checkCommand(subCommand, "Socket_Add")) {
				if (!commandManager.checkPermission(sender, "Socket_Add")) {
					final String permission = commandManager.getPermission("Socket_Add");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!SenderUtil.isPlayer(sender)) {
					final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 2) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Add"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Add");
					
					message.sendMessage(sender, "tooltip_socket_add", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final String slotType = args[1];
					final Player player = PlayerUtil.parse(sender);
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
					
					if (!EquipmentUtil.isSolid(item)) {
						final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						final String lore;
						final String type;
						
						int line = 1;
						
						if (slotType.equalsIgnoreCase("Empty") || slotType.equalsIgnoreCase("Unlock")) {
							lore = socketManager.getTextSocketGemSlotEmpty();
							type = lang.getText(sender, "Socket_Slot_Type_Empty");
						} else if (slotType.equalsIgnoreCase("Locked") || slotType.equalsIgnoreCase("Lock")) {
							lore = socketManager.getTextSocketGemSlotLocked();
							type = lang.getText(sender, "Socket_Slot_Type_Locked");
						} else {
							final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Add"));
							final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Add");
							
							message.sendMessage(sender, "tooltip_socket_add", tooltip);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						}
						
						if (EquipmentUtil.loreCheck(item)) {
							line = EquipmentUtil.getLores(item).size() + 1;
						}
						
						if (args.length > 2) {
							final String textLine = args[2];
							
							if (!MathUtil.isNumber(textLine)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								
								line = MathUtil.parseInteger(textLine);
								
								if (line < 1) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								}
							}
						}
						
						final MessageBuild message = lang.getMessage(sender, "MyItems_Socket_Add_Slot_Success");
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						mapPlaceholder.put("line", String.valueOf(line));
						mapPlaceholder.put("type", type);
						
						EquipmentUtil.setLore(item, line, lore);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						message.sendMessage(sender, mapPlaceholder);
						player.updateInventory();
						return true;
					}
				}
			} else if (commandManager.checkCommand(subCommand, "Socket_Drop")) {
				if (!commandManager.checkPermission(sender, "Socket_Drop")) {
					final String permission = commandManager.getPermission("Socket_Drop");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 2) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Drop"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Drop");
					
					message.sendMessage(sender, "tooltip_socket_drop", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final String dropType = args[1];
					
					if (commandManager.checkCommand(dropType, "Socket_Drop_Rod")) {
						if (!commandManager.checkPermission(sender, "Socket_Drop_Rod")) {
							final String permission = commandManager.getPermission("Socket_Drop_Rod");
							final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
							
							message.sendMessage(sender, "permission", permission);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else if (args.length < (sender instanceof Player ? 3 : 7)) {
							final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Drop_Rod"));
							final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Drop_Rod");
							
							message.sendMessage(sender, "tooltip_socket_drop_rod", tooltip);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final String textRod = args[2];
							final ItemStack itemRod;
							final World world;
							
							if (textRod.equalsIgnoreCase("Unlock")) {
								itemRod = mainConfig.getSocketItemRodUnlock();
							} else if (textRod.equalsIgnoreCase("Remove")) {
								itemRod = mainConfig.getSocketItemRodRemove();
							} else {
								final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Drop_Rod"));
								final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Drop_Rod");
								
								message.sendMessage(sender, "tooltip_socket_drop_rod", tooltip);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							}
							
							if (args.length > 3) {
								final String textWorld = args[3];
								
								if (textWorld.equalsIgnoreCase("~") && sender instanceof Player) {
									final Player player = (Player) sender;
									
									world = player.getWorld();
								} else {
									world = WorldUtil.getWorld(textWorld);
								}
							} else {
								final Player player = (Player) sender;
								
								world = player.getWorld(); 
							}
							
							if (world == null) {
								final MessageBuild message = lang.getMessage(sender, "MyItems_World_Not_Exists");
								
								message.sendMessage(sender, "world", args[3]);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								final double x;
								final double y;
								final double z;
								final int amount;
								 
								if (args.length > 4) {
									final String textX = args[4];
									
									if (!MathUtil.isNumber(textX)) {
										if (textX.equalsIgnoreCase("~") && sender instanceof Player) {
											final Player player = (Player) sender;
											final Location location = player.getLocation();
											
											x = location.getX();
										} else {
											final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
											
											message.sendMessage(sender);
											SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
											return true;
										}
									} else {
										x = MathUtil.parseDouble(textX);
									}
								} else {
									final Player player = (Player) sender;
									final Location location = player.getLocation();
									
									x = location.getX();
								}

								if (args.length > 5) {
									final String textY = args[5];
									
									if (!MathUtil.isNumber(textY)) {
										if (textY.equalsIgnoreCase("~") && sender instanceof Player) {
											final Player player = (Player) sender;
											final Location location = player.getLocation();
											
											y = location.getY();
										} else {
											final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
											
											message.sendMessage(sender);
											SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
											return true;
										}
									} else {
										y = MathUtil.parseDouble(textY);
									}
								} else {
									final Player player = (Player) sender;
									final Location location = player.getLocation();
									
									y = location.getY();
								}
								
								if (args.length > 6) {
									final String textZ = args[6];
									
									if (!MathUtil.isNumber(textZ)) {
										if (textZ.equalsIgnoreCase("~") && sender instanceof Player) {
											final Player player = (Player) sender;
											final Location location = player.getLocation();
											
											z = location.getZ();
										} else {
											final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
											
											message.sendMessage(sender);
											SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
											return true;
										}
									} else {
										z = MathUtil.parseDouble(textZ);
									}
								} else {
									final Player player = (Player) sender;
									final Location location = player.getLocation();
									
									z = location.getZ();
								}
									
								if (args.length > 7) {
									final String textAmount = args[7];
									
									if (!MathUtil.isNumber(textAmount)) {
										final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
										
										message.sendMessage(sender);
										SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
										return true;
									} else {
										amount = MathUtil.parseInteger(textAmount);
									}
								} else {
									amount = 1;
								}
								
								final Location location = new Location(world, x, y, z);
									
								if (SenderUtil.isPlayer(sender)) {
									final MessageBuild message = lang.getMessage(sender, "MyItems_Socket_Drop_Rod_Success");
									final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
									
									mapPlaceholder.put("rod", EquipmentUtil.getDisplayName(itemRod));
									mapPlaceholder.put("amount", String.valueOf(amount));
									mapPlaceholder.put("world", world.getName());
									mapPlaceholder.put("x", String.valueOf(x));
									mapPlaceholder.put("y", String.valueOf(y));
									mapPlaceholder.put("z", String.valueOf(z));
									
									message.sendMessage(sender, mapPlaceholder);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
								}
								
								itemRod.setAmount(amount);
								world.dropItem(location, itemRod);
								return true;
							}
						}
					} else if (commandManager.checkCommand(dropType, "Socket_Drop_Gems")) {
						if (!commandManager.checkPermission(sender, "Socket_Drop_Gems")) {
							final String permission = commandManager.getPermission("Socket_Drop_Gems");
							final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
							
							message.sendMessage(sender, "permission", permission);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else if (args.length < 8) {
							final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Drop"));
							final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Drop_Gems");
							
							message.sendMessage(sender, "tooltip_socket_drop_gems", tooltip);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else if (args[2].contains(".")) {
							final MessageBuild message = lang.getMessage(sender, "Character_Special");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final SocketGemTree socketTree = socketManager.getSocketGemTree(args[2]);
							
							if (socketTree == null) {
								final MessageBuild message = lang.getMessage(sender, "Item_Not_Exist");
								
								message.sendMessage(sender, "nameid", args[1]);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								final String textGrade = args[3];
								final World world;
								final int grade;
								
								if (!MathUtil.isNumber(textGrade)) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									final int maxGrade = socketTree.getMaxGrade();
									
									grade = MathUtil.limitInteger(MathUtil.parseInteger(textGrade), 1, maxGrade);
								}
								
								if (args.length > 4) {
									final String textWorld = args[4];
									
									if (textWorld.equalsIgnoreCase("~") && sender instanceof Player) {
										final Player player = (Player) sender;
										
										world = player.getWorld();
									} else {
										world = WorldUtil.getWorld(textWorld);
									}
								} else {
									final Player player = (Player) sender;
									
									world = player.getWorld(); 
								}
								
								if (world == null) {
									final MessageBuild message = lang.getMessage(sender, "MyItems_World_Not_Exists");
									
									message.sendMessage(sender, "world", args[4]);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									final double x;
									final double y;
									final double z;
									final int amount;
									
									if (args.length > 5) {
										final String textX = args[5];
										
										if (!MathUtil.isNumber(textX)) {
											if (textX.equalsIgnoreCase("~") && sender instanceof Player) {
												final Player player = (Player) sender;
												final Location location = player.getLocation();
												
												x = location.getX();
											} else {
												final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
												
												message.sendMessage(sender);
												SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
												return true;
											}
										} else {
											x = MathUtil.parseDouble(textX);
										}
									} else {
										final Player player = (Player) sender;
										final Location location = player.getLocation();
										
										x = location.getX();
									}

									if (args.length > 6) {
										final String textY = args[6];
										
										if (!MathUtil.isNumber(textY)) {
											if (textY.equalsIgnoreCase("~") && sender instanceof Player) {
												final Player player = (Player) sender;
												final Location location = player.getLocation();
												
												y = location.getY();
											} else {
												final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
												
												message.sendMessage(sender);
												SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
												return true;
											}
										} else {
											y = MathUtil.parseDouble(textY);
										}
									} else {
										final Player player = (Player) sender;
										final Location location = player.getLocation();
										
										y = location.getY();
									}
									
									if (args.length > 7) {
										final String textZ = args[7];
										
										if (!MathUtil.isNumber(textZ)) {
											if (textZ.equalsIgnoreCase("~") && sender instanceof Player) {
												final Player player = (Player) sender;
												final Location location = player.getLocation();
												
												z = location.getZ();
											} else {
												final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
												
												message.sendMessage(sender);
												SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
												return true;
											}
										} else {
											z = MathUtil.parseDouble(textZ);
										}
									} else {
										final Player player = (Player) sender;
										final Location location = player.getLocation();
										
										z = location.getZ();
									}
									
									if (args.length > 8) {
										final String textAmount = args[8];
										
										if (!MathUtil.isNumber(textAmount)) {
											final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
											
											message.sendMessage(sender);
											SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
											return true;
										} else {
											amount = MathUtil.parseInteger(textAmount);
										}
									} else {
										amount = 1;
									}
									
									final SocketGem socketGem = socketTree.getSocketGem(grade);
									final ItemStack item = socketGem.getItem();
									final Location location = new Location(world, x, y, z);
									
									if (SenderUtil.isPlayer(sender)) {
										final MessageBuild message = lang.getMessage(sender, "MyItems_Socket_Drop_Gems_Success");
										final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
										
										mapPlaceholder.put("amount", String.valueOf(amount));
										mapPlaceholder.put("nameid", socketTree.getId());
										mapPlaceholder.put("grade", String.valueOf(grade));
										mapPlaceholder.put("world", world.getName());
										mapPlaceholder.put("x", String.valueOf(x));
										mapPlaceholder.put("y", String.valueOf(y));
										mapPlaceholder.put("z", String.valueOf(z));
										
										message.sendMessage(sender, mapPlaceholder);
										SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
									}
									
									item.setAmount(amount);
									world.dropItem(location, item);
									return true;
								}
							}
						}
					} else {
						final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Drop"));
						final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Drop");
						
						message.sendMessage(sender, "tooltip_socket_drop", tooltip);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					}
				}
			} else if (commandManager.checkCommand(subCommand, "Socket_Load")) {
				if (!commandManager.checkPermission(sender, "Socket_Load")) {
					final String permission = commandManager.getPermission("Socket_Load");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 2) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Load"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Load");
					
					message.sendMessage(sender, "tooltip_socket_load", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final String loadType = args[1];
					
					if (commandManager.checkCommand(loadType, "Socket_Load_Gems")) {
						if (!commandManager.checkPermission(sender, "Socket_Load_Gems")) {
							final String permission = commandManager.getPermission("Socket_Load_Gems");
							final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
							
							message.sendMessage(sender, "permission", permission);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else if (args.length < (SenderUtil.isPlayer(sender) ? 3 : 5)) {
							final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Load_Gems"));
							final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Load_Gems");
							
							message.sendMessage(sender, "tooltip_socket_load_gems", tooltip);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else if (args[2].contains(".")) {
							final MessageBuild message = lang.getMessage(sender, "Contains_Special_Character");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final String textSocketGemId = args[2];
							final SocketGemTree socketGemTree = socketManager.getSocketGemTree(textSocketGemId);
							
							if (socketGemTree == null) {
								final MessageBuild message = lang.getMessage(sender, "Item_Not_Exist");
								
								message.sendMessage(sender, "nameid", textSocketGemId);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								final Player target;
								final int grade;
								final int amount;
								
								if (args.length > 3) {
									final String textGrade = args[3];
									
									if (!MathUtil.isNumber(textGrade)) {
										final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
										
										message.sendMessage(sender);
										SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
										return true;
									} else {
										final int maxGrade = socketGemTree.getMaxGrade();
										
										grade = MathUtil.limitInteger(MathUtil.parseInteger(textGrade), 1, maxGrade);
									}
								} else {
									grade = 1;
								}
								
								if (args.length > 4) {
									final String nameTarget = args[4];
									
									if (!PlayerUtil.isOnline(nameTarget)) {
										final MessageBuild message = lang.getMessage(sender, "Player_Target_Offline");
										
										message.sendMessage(sender);
										SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
										return true;
									} else {
										target = PlayerUtil.getOnlinePlayer(nameTarget);
									}
								} else {
									target = PlayerUtil.parse(sender);
								}
								
								if (args.length > 5) {
									final String textAmount = args[5];
									
									if (!MathUtil.isNumber(textAmount)) {
										final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
										
										message.sendMessage(sender);
										SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
										return true;
									} else {		
										amount = Math.max(1, MathUtil.parseInteger(textAmount));
									}
								} else {
									amount = 1;
								}
								
								final SocketGem socketGem = socketGemTree.getSocketGem(grade);
								final ItemStack item = socketGem.getItem().clone();
								
								if (target.equals(sender)) {
									final MessageBuild message = lang.getMessage(sender, "MyItems_Socket_Load_Gems_Success_Self");
									final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
															
									mapPlaceholder.put("amount", String.valueOf(amount));
									mapPlaceholder.put("nameId", socketGemTree.getId());
									mapPlaceholder.put("grade", String.valueOf(RomanNumber.getRomanNumber(grade)));
									
									EquipmentUtil.setAmount(item, amount);
									PlayerUtil.addItem(target, item);
									
									message.sendMessage(sender, mapPlaceholder);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
									return true;
								} else {
									final MessageBuild messageToSender = lang.getMessage(sender, "MyItems_Socket_Load_Gems_Success_To_Sender");
									final MessageBuild messageToTarget = lang.getMessage(target, "MyItems_Socket_Load_Gems_Success_To_Target");
									final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
									
									mapPlaceholder.put("nameId", socketGemTree.getId());
									mapPlaceholder.put("grade", String.valueOf(RomanNumber.getRomanNumber(grade)));
									mapPlaceholder.put("amount", String.valueOf(amount));
									mapPlaceholder.put("target", target.getName());
									mapPlaceholder.put("sender", sender.getName());
									
									EquipmentUtil.setAmount(item, amount);
									PlayerUtil.addItem(target, item);
									
									messageToSender.sendMessage(sender, mapPlaceholder);
									messageToTarget.sendMessage(target, mapPlaceholder);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
									SenderUtil.playSound(target, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
									return true;
								}
							}
						}
					} else if (commandManager.checkCommand(loadType, "Socket_Load_Rod")) {
						if (!commandManager.checkPermission(sender, "Socket_Load_Rod")) {
							final String permission = commandManager.getPermission("Socket_Load_Rod");
							final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
							
							message.sendMessage(sender, "permission", permission);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else if (args.length < (SenderUtil.isPlayer(sender) ? 3 : 4)) {
							final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Load_Rod"));
							final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Load_Rod");
							
							message.sendMessage(sender, "tooltip_socket_load_rod", tooltip);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final String textRod = args[2];
							final ItemStack rod;
							final Player target;
							final int amount;
							
							if (textRod.equalsIgnoreCase("Unlock")) {
								rod = mainConfig.getSocketItemRodUnlock();
							} else if (textRod.equalsIgnoreCase("Remove")) {
								rod = mainConfig.getSocketItemRodRemove();
							} else {
								final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Load_Rod"));
								final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Load_Rod");
								
								message.sendMessage(sender, "tooltip_socket_load_rod", tooltip);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							}
							
							if (args.length > 3) {
								final String nameTarget = args[3];
								
								if (!PlayerUtil.isOnline(nameTarget)) {
									final MessageBuild message = lang.getMessage(sender, "Player_Target_Offline");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									target = PlayerUtil.getOnlinePlayer(nameTarget);
								}
							} else {
								target = PlayerUtil.parse(sender);
							}
							
							if (args.length > 4) {
								final String textAmount = args[4];
								
								if (!MathUtil.isNumber(textAmount)) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									final int rawAmount = MathUtil.parseInteger(textAmount);
									
									amount = MathUtil.limitInteger(rawAmount, 1, rawAmount);
								}
							} else {
								amount = 1;
							}
							
							if (target.equals(sender)) {
								final MessageBuild message = lang.getMessage(sender, "MyItems_Socket_Load_Rod_Success_Self");
								final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
							
								mapPlaceholder.put("socket_rod", EquipmentUtil.getDisplayName(rod));
								mapPlaceholder.put("amount", String.valueOf(amount));
								
								EquipmentUtil.setAmount(rod, amount);
								PlayerUtil.addItem(target, rod);
								
								message.sendMessage(sender, mapPlaceholder);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
								return true;
							} else {
								final MessageBuild messageToSender = lang.getMessage(sender, "MyItems_Socket_Load_Rod_Success_To_Sender");
								final MessageBuild messageToTarget = lang.getMessage(sender, "MyItems_Socket_Load_Rod_Success_To_Target");
								final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
								
								mapPlaceholder.put("socket_rod", EquipmentUtil.getDisplayName(rod));
								mapPlaceholder.put("amount", String.valueOf(amount));
								
								EquipmentUtil.setAmount(rod, amount);
								PlayerUtil.addItem(target, rod);
								
								messageToSender.sendMessage(sender, mapPlaceholder);
								messageToTarget.sendMessage(target, mapPlaceholder);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
								SenderUtil.playSound(target, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
								return true;
							}
						}
					} else {
						final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Socket_Load"));
						final MessageBuild message = lang.getMessage(sender, "Argument_Socket_Load");
						
						message.sendMessage(sender, "tooltip_socket_load", tooltip);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					}
				}
				
			} else if (commandManager.checkCommand(subCommand, "Socket_List")) {
				final String[] fullArgs = TextUtil.pressList(args, 2);
				
				list(sender, command, label, fullArgs);
				return true;
			} else {
				final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Command");
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			}
		} else {
			final String[] fullArgs = TextUtil.pressList(args, 2);
			
			return CommandMyItems.help(sender, command, label, fullArgs);
		}
	}
	
	private static final List<String> list(CommandSender sender, Command command, String label, String[] args) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final SocketGemManager socketManager = plugin.getGameManager().getSocketManager();
		final PluginManager pluginManager = plugin.getPluginManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final List<String> list = new ArrayList<String>();
		
		if (!commandManager.checkPermission(sender, "Socket_List")) {
			final String permission = commandManager.getPermission("Socket_List");
			final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return list;
		} else if (socketManager.getSocketGemIds().isEmpty()) {
			final MessageBuild message = lang.getMessage(sender, "Item_Database_Empty");
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
			return list;
		} else {
			final List<String> keyList = SortUtil.toList(socketManager.getSocketGemIds());
			final int size = keyList.size();
			final int maxRow = 5;
			final int maxPage = MathUtil.isDividedBy(size, maxRow) ? size/maxRow : (size/maxRow)+1;
			
			int page = 1;
			
			if (args.length > 0) {
				final String textPage = args[0];
				
				if (MathUtil.isNumber(textPage)) {
					page = MathUtil.parseInteger(textPage);
					page = MathUtil.limitInteger(page, 1, maxPage);
				}
			}
			
			final HashMap<String, String> map = new HashMap<String, String>();
			
			String listHeaderMessage = lang.getText(sender, "List_Header");
			
			map.put("page", String.valueOf(page));
			map.put("maxpage", String.valueOf(maxPage));
			listHeaderMessage = TextUtil.placeholder(map, listHeaderMessage);

			SenderUtil.sendMessage(sender, listHeaderMessage);

			final int addNum = (page-1)*maxRow;
			
			for (int t = 0; t < maxRow && (t+addNum) < size; t++) {
				final int index = t + addNum;
				final String key = keyList.get(index);
				final HashMap<String, String> subMap = new HashMap<String, String>();
				
				String listItemMessage = lang.getText(sender, "List_Container");
				
				subMap.put("index", String.valueOf(index+1));
				subMap.put("container", key);
				subMap.put("maxpage", String.valueOf(page));
				listItemMessage = TextUtil.placeholder(subMap, listItemMessage);

				list.add(key);
				SenderUtil.sendMessage(sender, listItemMessage);
			}
			
			SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
			return list;
		}
	}
}