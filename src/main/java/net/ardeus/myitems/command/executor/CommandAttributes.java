package net.ardeus.myitems.command.executor;

import java.util.HashMap;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.ProjectileEnum;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.RomanNumber;
import core.praya.agarthalib.enums.main.Slot;
import core.praya.agarthalib.enums.main.TagsAttribute;
import core.praya.agarthalib.enums.main.VersionNMS;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.ability.AbilityWeapon;
import net.ardeus.myitems.config.plugin.MainConfig;
import net.ardeus.myitems.element.Element;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.lorestats.LoreStatsEnum;
import net.ardeus.myitems.manager.game.AbilityWeaponManager;
import net.ardeus.myitems.manager.game.ElementManager;
import net.ardeus.myitems.manager.game.GameManager;
import net.ardeus.myitems.manager.game.LoreStatsManager;
import net.ardeus.myitems.manager.game.PassiveEffectManager;
import net.ardeus.myitems.manager.game.PowerCommandManager;
import net.ardeus.myitems.manager.game.PowerManager;
import net.ardeus.myitems.manager.game.PowerShootManager;
import net.ardeus.myitems.manager.game.PowerSpecialManager;
import net.ardeus.myitems.manager.game.RequirementManager;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;
import net.ardeus.myitems.passive.PassiveEffectEnum;
import net.ardeus.myitems.passive.PassiveTypeEnum;
import net.ardeus.myitems.power.PowerClickType;
import net.ardeus.myitems.power.PowerCommand;
import net.ardeus.myitems.power.PowerSpecial;
import net.ardeus.myitems.power.PowerType;
import net.ardeus.myitems.requirement.RequirementEnum;
import net.ardeus.myitems.utility.main.TriggerSupportUtil;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.ServerUtil;
import com.praya.agarthalib.utility.TextUtil;

public class CommandAttributes extends HandlerCommand implements CommandExecutor {

	public CommandAttributes(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final GameManager gameManager = plugin.getGameManager();
		final PowerManager powerManager = gameManager.getPowerManager();
		final PowerCommandManager powerCommandManager = gameManager.getPowerCommandManager();
		final PowerShootManager powerShootManager = gameManager.getPowerShootManager();
		final PowerSpecialManager powerSpecialManager = gameManager.getPowerSpecialManager();
		final AbilityWeaponManager abilityWeaponManager = gameManager.getAbilityWeaponManager();
		final ElementManager elementManager = gameManager.getElementManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final RequirementManager requirementManager = gameManager.getRequirementManager();
		final PassiveEffectManager passiveEffectManager = gameManager.getPassiveEffectManager();
		final LoreStatsManager statsManager = gameManager.getStatsManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final MainConfig mainConfig = MainConfig.getInstance();
		
		if (args.length > 0) {
			final String subCommand = args[0];
			
			if (commandManager.checkCommand(subCommand, "Attribute_Stats")) {
				if (!commandManager.checkPermission(sender, "Attribute_Stats")) {
					final String permission = commandManager.getPermission("Attribute_Stats");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!SenderUtil.isPlayer(sender)) {
					final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 3) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Stats"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Stats");
					
					message.sendMessage(sender, "tooltip_att_stats", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final Player player = PlayerUtil.parse(sender);
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
					final LoreStatsEnum loreStats = LoreStatsEnum.get(args[1]);
					
					if (!EquipmentUtil.isSolid(item)) {
						final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else if (loreStats == null) {
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Stats_Not_Exists");

						message.sendMessage(sender, "stats", args[1]);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						final String textValue = args[2];
						
						double value1;
						double value2;
						int line;
						
						if (args[2].contains(":")) {
							final String[] valueBuild = textValue.split(":");
							
							if (MathUtil.isNumber(valueBuild[0]) && MathUtil.isNumber(valueBuild[1])) {
								value1 = MathUtil.parseDouble(valueBuild[0]);;
								value2 = MathUtil.parseDouble(valueBuild[1]);
							} else {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							}
						} else {
							if (!MathUtil.isNumber(textValue)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								value1 = MathUtil.parseDouble(args[2]);
								
								if (loreStats.equals(LoreStatsEnum.LEVEL)) {
									value2 = 0;
								} else {
									value2 = value1;
								}
							}
						}
						
						if (args.length > 3) {
							final String textLine = args[3];
							
							if (!MathUtil.isNumber(textLine)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								line = MathUtil.parseInteger(args[3]);
								
								if (line < 1) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									if (EquipmentUtil.hasLore(item)) {
										final int lastLine = statsManager.getLineLoreStats(item, loreStats);
										if (lastLine != -1) {
											if (lastLine != line) {
												EquipmentUtil.setLore(item, lastLine, "");
											}
										}
									}
								}
							}
						} else {
							if (EquipmentUtil.hasLore(item)) {
								line = statsManager.getLineLoreStats(item, loreStats);
								
								if (line == -1) {
									line = EquipmentUtil.getLores(item).size() + 1;
								}
							} else {
								line = 1;
							}
						}
						
						final String lore = statsManager.getTextLoreStats(loreStats, value1, value2);
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Stats_Success");
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						mapPlaceholder.put("stats", loreStats.getText());
						mapPlaceholder.put("value", statsManager.statsValue(loreStats, value1, value2));
						
						EquipmentUtil.setLore(item, line, lore);
						TriggerSupportUtil.updateSupport(player);
						
						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						
						player.updateInventory();
						return true;
					}
				}
			} else if (commandManager.checkCommand(subCommand, "Attribute_Element")) {
				if (!commandManager.checkPermission(sender, "Attribute_Element")) {
					final String permission = commandManager.getPermission("Attribute_Element");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!SenderUtil.isPlayer(sender)) {
					final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 3) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Element"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Element");
					
					message.sendMessage(sender, "tooltip_att_element", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final String textElement = args[1];
					final Player player = PlayerUtil.parse(sender);
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND); 
					final Element element = elementManager.getElement(textElement);
					
					if (!EquipmentUtil.isSolid(item)) {
						final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else if (element == null) {
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Element_Not_Exists");

						message.sendMessage(sender, "element", textElement);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						final String textValue = args[2];
						final String elementId = element.getId();
						
						double value;
						int line;
						
						if (!MathUtil.isNumber(textValue)) {
							final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							value = MathUtil.parseDouble(textValue);
						}
						
						if (args.length > 3) {
							final String textLine = args[3];
							
							if (!MathUtil.isNumber(textLine)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								line = MathUtil.parseInteger(args[3]);
								
								if (line < 1) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									if (EquipmentUtil.hasLore(item)) {
										final int lastLine = elementManager.getLineElement(item, elementId);
										
										if (lastLine != -1) {
											if (lastLine != line) {
												EquipmentUtil.setLore(item, lastLine, "");
											}
										}
									}
								}
							}
						} else {
							if (EquipmentUtil.hasLore(item)) {
								line = elementManager.getLineElement(item, elementId);
								
								if (line == -1) {
									line = EquipmentUtil.getLores(item).size() + 1;
								}
							} else {
								line = 1;
							}
						}						
						
						final String lore = elementManager.getTextElement(elementId, value);
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Element_Success");
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						mapPlaceholder.put("element", elementId);
						mapPlaceholder.put("value", String.valueOf(value));
						
						EquipmentUtil.setLore(item, line, lore);
						
						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						
						player.updateInventory();
						return true;
					}
				}
			} else if (commandManager.checkCommand(subCommand, "Attribute_Buff")) {
				if (!commandManager.checkPermission(sender, "Attribute_Buff")) {
					final String permission = commandManager.getPermission("Attribute_Buff");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!SenderUtil.isPlayer(sender)) {
					final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 3) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Buffs"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Buffs");
					
					message.sendMessage(sender, "tooltip_att_buffs", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final Player player = PlayerUtil.parse(sender);
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
					final PassiveEffectEnum effect = PassiveEffectEnum.get(args[1]);
					
					if (!EquipmentUtil.isSolid(item)) {
						final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else if (effect == null ? true : !effect.getType().equals(PassiveTypeEnum.BUFF)) {
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Buffs_Not_Exists");

						message.sendMessage(sender, "buff", args[1]);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						final String textGrade = args[2];
						
						int grade;
						int line;
						
						if (!MathUtil.isNumber(textGrade)) {
							final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							grade = MathUtil.parseInteger(textGrade);
							grade = MathUtil.limitInteger(grade, 1, 10);
						}
						
						if (args.length > 3) {
							final String textLine = args[3];
							
							if (!MathUtil.isNumber(textLine)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								line = MathUtil.parseInteger(args[3]);
								
								if (line < 1) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									if (EquipmentUtil.hasLore(item)) {
										final int lastLine = passiveEffectManager.getLinePassiveEffect(item, effect);
										
										if (lastLine != -1) {
											if (lastLine != line) {
												EquipmentUtil.setLore(item, lastLine, "");
											}
										}
									}
								}
							}
						} else {
							if (EquipmentUtil.hasLore(item)) {
								
								line = passiveEffectManager.getLinePassiveEffect(item, effect);
								
								if (line == -1) {
									line = EquipmentUtil.getLores(item).size() + 1;
								}
							} else {
								line = 1;
							}
						}
						
						final String lore = passiveEffectManager.getTextPassiveEffect(effect, grade);
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Buffs_Success");
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						mapPlaceholder.put("buff", effect.getText());
						mapPlaceholder.put("buffs", effect.getText());
						mapPlaceholder.put("grade", RomanNumber.getRomanNumber(grade));
						
						EquipmentUtil.setLore(item, line, lore);
						
						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						
						player.updateInventory();
						return true;
					}
				}
			} else if (commandManager.checkCommand(subCommand, "Attribute_Debuff")) {
				if (!commandManager.checkPermission(sender, "Attribute_Debuff")) {
					final String permission = commandManager.getPermission("Attribute_Debuff");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!SenderUtil.isPlayer(sender)) {
					final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 3) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Debuffs"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Debuffs");
					
					message.sendMessage(sender, "tooltip_att_debuffs", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final Player player = PlayerUtil.parse(sender);
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
					final PassiveEffectEnum effect = PassiveEffectEnum.get(args[1]);
					
					if (!EquipmentUtil.isSolid(item)) {
						final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else if (effect == null ? true : !effect.getType().equals(PassiveTypeEnum.DEBUFF)) {
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Debuffs_Not_Exists");

						message.sendMessage(sender, "debuff", args[1]);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						final String textGrade = args[2];
						
						int grade;
						int line;
						
						if (!MathUtil.isNumber(textGrade)) {
							final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							grade = MathUtil.parseInteger(textGrade);
							grade = MathUtil.limitInteger(grade, 1, 10);
						}
						
						if (args.length > 3) {
							final String textLine = args[3];
							
							if (!MathUtil.isNumber(textLine)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								line = MathUtil.parseInteger(args[3]);
								
								if (line < 1) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									if (EquipmentUtil.hasLore(item)) {
										final int lastLine = passiveEffectManager.getLinePassiveEffect(item, effect);
										
										if (lastLine != -1) {
											if (lastLine != line) {
												EquipmentUtil.setLore(item, lastLine, "");
											}
										}
									}
								}
							}
						} else {
							if (EquipmentUtil.hasLore(item)) {
								
								line = passiveEffectManager.getLinePassiveEffect(item, effect);
								
								if (line == -1) {
									line = EquipmentUtil.getLores(item).size() + 1;
								}
							} else {
								line = 1;
							}
						}
						
						final String lore = passiveEffectManager.getTextPassiveEffect(effect, grade);
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Debuffs_Success");
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						mapPlaceholder.put("debuff", effect.getText());
						mapPlaceholder.put("debuffs", effect.getText());
						mapPlaceholder.put("grade", RomanNumber.getRomanNumber(grade));
						
						EquipmentUtil.setLore(item, line, lore);
						
						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						
						player.updateInventory();
						return true;
					}
				}
			} else if (commandManager.checkCommand(subCommand, "Attribute_NBT")) {
				if (!ServerUtil.isCompatible(VersionNMS.V1_8_R1)) {
					final MessageBuild message = lang.getMessage(sender, "MyItems_Not_Compatible");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!commandManager.checkPermission(sender, "Attribute_NBT")) {
					final String permission = commandManager.getPermission("Attribute_NBT");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!SenderUtil.isPlayer(sender)) {
					final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 3) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_NBT"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_NBT");
					
					message.sendMessage(sender, "tooltip_att_nbt", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final Player player = PlayerUtil.parse(sender);
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
					final TagsAttribute tags = TagsAttribute.getTagsAttribute(args[1]);
					
					if (!EquipmentUtil.isSolid(item)) {
						final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else if (tags == null) {
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_NBT_Not_Exists");
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						mapPlaceholder.put("NBT", args[1]);
						mapPlaceholder.put("Tags", args[1]);

						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						final String textValue = args[2];
						
						if (!MathUtil.isNumber(textValue)) {
							final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final double value = MathUtil.parseDouble(textValue);
							
							Slot slot;
							
							if (args.length > 3) {
								final String textSlot = args[3];
								
								slot = Slot.get(textSlot);
								
								if (slot == null) {
									slot = Slot.getDefault(item.getType());
								}
							} else {
								slot = Slot.getDefault(item.getType());
							}
							
							final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_NBT_Success");
							final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
							
							mapPlaceholder.put("NBT", TextUtil.firstSolidCharacter(String.valueOf(tags)));
							mapPlaceholder.put("Value", String.valueOf(value));
							
							Bridge.getBridgeTagsNBT().addNBT(item, tags, value, slot);
							
							message.sendMessage(sender, mapPlaceholder);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
							
							player.updateInventory();
							return true;
						}
					}
				}
			} else if (commandManager.checkCommand(subCommand, "Attribute_Ability")) {
				if (!commandManager.checkPermission(sender, "Attribute_Ability")) {
					final String permission = commandManager.getPermission("Attribute_Ability");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!SenderUtil.isPlayer(sender)) {
					final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 3) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Ability"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Ability");
					
					message.sendMessage(sender, "tooltip_att_ability", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final String ability = args[1];
					final Player player = PlayerUtil.parse(sender);
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
					final AbilityWeapon abilityWeapon = abilityWeaponManager.getAbilityWeapon(ability);
					
					if (!EquipmentUtil.isSolid(item)) {
						final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else if (abilityWeapon == null) {
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Ability_Not_Exists");
						
						message.sendMessage(sender, "ability", args[1]);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						final String textGrade = args[2];
						
						double chance;
						int grade;
						int line;
						
						if (args.length > 3) {
							final String textChance = args[3];
							
							if (!MathUtil.isNumber(textChance)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								chance = MathUtil.parseDouble(textChance);
								chance = MathUtil.roundNumber(chance);
								chance = MathUtil.limitDouble(chance, 0, 100);
							}
						} else {
							chance = 100;
						}
						
						if (!MathUtil.isNumber(textGrade)) {
							final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final int maxGrade = abilityWeapon.getMaxGrade();
							
							grade = MathUtil.parseInteger(textGrade);
							grade = MathUtil.limitInteger(grade, 1, maxGrade);
						}
						
						if (args.length > 4) {
							final String textLine = args[4];
							
							if (!MathUtil.isNumber(textLine)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								
								line = MathUtil.parseInteger(args[4]);
								
								if (line < 1) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else if (EquipmentUtil.hasLore(item)) {
									final Integer abilityLine = abilityWeaponManager.getLineAbilityItemWeapon(item, ability);
									
									if (abilityLine != null && abilityLine != line) {
										EquipmentUtil.setLore(item, abilityLine, "");
									}
								}
							}
						} else {
							if (EquipmentUtil.hasLore(item)) {
								final Integer abilityLine = abilityWeaponManager.getLineAbilityItemWeapon(item, ability);
								
								line = abilityLine != null ? abilityLine : EquipmentUtil.getLores(item).size() + 1;
							} else {
								line = 1;
							}
						}
						
						final String lore = abilityWeaponManager.getTextAbility(abilityWeapon, grade, chance);
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Ability_Success");
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						mapPlaceholder.put("ability", abilityWeapon.getKeyLore());
						mapPlaceholder.put("grade", RomanNumber.getRomanNumber(grade));
						
						EquipmentUtil.setLore(item, line, lore);
						
						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						
						player.updateInventory();
						return true;
					}
				}
			} else if (commandManager.checkCommand(subCommand, "Attribute_Power")) {
				if (!commandManager.checkPermission(sender, "Attribute_Power")) {
					final String permission = commandManager.getPermission("Attribute_Power");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!SenderUtil.isPlayer(sender)) {
					final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 4) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Power"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Power");
					
					message.sendMessage(sender, "tooltip_att_power", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final String textPowerType = args[1];
					final String textPowerClickType = args[2];
					final Player player = PlayerUtil.parse(sender);
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
					final PowerType power = PowerType.getPowerType(textPowerType);
					final PowerClickType click = PowerClickType.getPowerClickType(textPowerClickType);
					
					double cooldown;
					int line;
					
					if (!EquipmentUtil.isSolid(item)) {
						final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else if (power == null) {
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Power_Not_Exists");

						message.sendMessage(sender, "Power", textPowerType);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else if (click == null) {
						final MessageBuild message = lang.getMessage(sender, "Utility_Slot_Not_Exists");
						
						message.sendMessage(sender, "slot", textPowerClickType);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						if (args.length > 4) {
							final String textCooldown = args[4];
							
							if (!MathUtil.isNumber(textCooldown)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								cooldown = MathUtil.parseDouble(textCooldown);
								cooldown = MathUtil.limitDouble(cooldown, 0, cooldown);
							}
						} else {
							cooldown = 0;
						}
						
						if (args.length > 5) {
							final String textLine = args[5];
							
							if (!MathUtil.isNumber(textLine)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								
								line = MathUtil.parseInteger(textLine);
								
								if (line < 1) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									if (EquipmentUtil.hasLore(item)) {
										final Integer lineClick = powerManager.getLineClick(item, click);
										
										if (lineClick != null && lineClick != line) {
											EquipmentUtil.setLore(item, lineClick, "");
										}
									}
								}
							}
						} else {
							if (EquipmentUtil.hasLore(item)) {
								final Integer lineClick = powerManager.getLineClick(item, click); 
								
								line = lineClick != null ? lineClick : EquipmentUtil.getLores(item).size() + 1;
							} else {
								line = 1;
							}
						}
						
						if (power.equals(PowerType.COMMAND)) {
							final String textPowerCommand = args[3];
							final PowerCommand powerCommand = powerCommandManager.getPowerCommand(textPowerCommand);
							
							if (powerCommand == null) {
								final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Power_Command_Not_Exists");
								
								message.sendMessage(sender, "Command", textPowerCommand);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								final String powerCommandId = powerCommand.getId();
								final String lore = powerCommandManager.getTextPowerCommand(click, powerCommand, cooldown);
								final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Power_Command_Success");		
								
								EquipmentUtil.setLore(item, line, lore);
								
								message.sendMessage(sender, "command", powerCommandId);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
								
								player.updateInventory();
								return true;
							}
						} else if (power.equals(PowerType.SHOOT)) {
							final String powerShoot = args[3];
							final ProjectileEnum projectileEnum = ProjectileEnum.getProjectileEnum(powerShoot);
							
							if (projectileEnum == null) {
								final MessageBuild message = lang.getMessage(sender, "Myitems_Attribute_Power_Shoot_Not_Exists");

								message.sendMessage(sender, "projectile", powerShoot);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								final String powerShootKeyLore = powerShootManager.getPowerShootKeyLore(projectileEnum);
								final String lore = powerShootManager.getTextPowerShoot(click, projectileEnum, cooldown);
								final MessageBuild message = lang.getMessage(sender, "Myitems_Attribute_Power_Shoot_Success");		
								
								EquipmentUtil.setLore(item, line, lore);
								
								message.sendMessage(sender, "projectile", powerShootKeyLore);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
								
								player.updateInventory();
								return true;
							}
						} else if (power.equals(PowerType.SPECIAL)) {
							final String textPowerSpecial = args[3];
							final PowerSpecial powerSpecial = powerSpecialManager.getPowerSpecial(textPowerSpecial);
							
							if (powerSpecial == null) {
								final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Power_Special_Not_Exists");

								message.sendMessage(sender, "special", textPowerSpecial);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								final String lore = powerSpecialManager.getTextPowerSpecial(click, powerSpecial, cooldown);
								final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Power_Special_Success");		
								
								EquipmentUtil.setLore(item, line, lore);
								
								message.sendMessage(sender, "special", powerSpecial.getId());
								SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
								
								player.updateInventory();
								return true;
							}
						} else {
							final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Command");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						}
					}
				}
			} else if (commandManager.checkCommand(subCommand, "Attribute_Requirement")) {
				if (!commandManager.checkPermission(sender, "Attribute_Requirement")) {
					final String permission = commandManager.getPermission("Attribute_Requirement");
					final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
					
					message.sendMessage(sender, "permission", permission);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (!SenderUtil.isPlayer(sender)) {
					final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else if (args.length < 2) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Req"));
					final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Requirement");
					
					message.sendMessage(sender, "tooltip_att_req", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final Player player = PlayerUtil.parse(sender);
					final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
					final RequirementEnum requirementEnum = RequirementEnum.getRequirement(args[1]);
					
					if (requirementEnum.equals(RequirementEnum.REQUIREMENT_SOUL_UNBOUND)) {
						final String format = mainConfig.getRequirementFormatSoulUnbound();
						
						int line;
						
						if (args.length > 2) {
							final String textLine = args[2];
							
							if (!MathUtil.isNumber(textLine)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								line = MathUtil.parseInteger(textLine);
								
								if (line < 1) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									if (EquipmentUtil.hasLore(item)) {
										final Integer lastLine = requirementManager.getLineRequirementSoulUnbound(item);
										
										if (lastLine != null) {
											if (lastLine != line) {
												EquipmentUtil.setLore(item, lastLine, "");
											}
										}
									}
								}
							}
						} else {
							if (EquipmentUtil.hasLore(item)) {
								final Integer lineReq = requirementManager.getLineRequirementSoulUnbound(item);
								final int loreSize = EquipmentUtil.getLores(item).size();
								
								line = lineReq != null ? lineReq : loreSize + 1;
							} else {
								line = 1;
							}
						}
						
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Requirement_Unbound_Success");
						
						EquipmentUtil.setLore(item, line, format);
						
						message.sendMessage(sender, "line", String.valueOf(line));
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						
						player.updateInventory();
						return true;
					} else if (requirementEnum.equals(RequirementEnum.REQUIREMENT_SOUL_BOUND)) {
						final OfflinePlayer bound;
						
						int line;
						
						if (args.length > 2) {
							final String textBound = args[2];
							
							bound = PlayerUtil.isOnline(textBound) ? PlayerUtil.getOnlinePlayer(textBound) : PlayerUtil.getPlayer(textBound);
							
							if (bound == null) {
								final MessageBuild message = lang.getMessage(sender, "Player_Not_Exists");
										
								message.sendMessage(sender, "Player", textBound);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							}
						} else {
							bound = player;
						}
						
						if (args.length > 3) {
							final String textLine = args[3];
							
							if (!MathUtil.isNumber(textLine)) {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							} else {
								
								line = MathUtil.parseInteger(textLine);
								
								if (line < 1) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									if (EquipmentUtil.hasLore(item)) {
										final Integer lastLine = requirementManager.getLineRequirementSoulBound(item);
										
										if (lastLine != null) {
											if (lastLine != line) {
												EquipmentUtil.setLore(item, lastLine, "");
											}
										}
									}
								}
							}
						} else {
							if (EquipmentUtil.hasLore(item)) {
								final Integer lineReq = requirementManager.getLineRequirementSoulBound(item);
								final int loreSize = EquipmentUtil.getLores(item).size();
								
								line = lineReq != null ? lineReq : loreSize + 1;
							} else {
								line = 1;
							}
						}
						
						final String lore = requirementManager.getTextSoulBound(bound);
						final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Requirement_Bound_Success");
						final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
						
						mapPlaceholder.put("player", bound.getName());
						mapPlaceholder.put("line", String.valueOf(line));
						
						requirementManager.setMetadataSoulbound(bound, item);
						EquipmentUtil.setLore(item, line, lore);
						
						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						
						player.updateInventory();
						return true;
					} else if (requirementEnum.equals(RequirementEnum.REQUIREMENT_PERMISSION)) {
						if (args.length < 3) {
							final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Req_Permission"));
							final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Requirement_Permission");
							
							message.sendMessage(sender, "tooltip_att_req_permission", tooltip);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final String permission = args[2];
							
							int line;
							
							if (args.length > 3) {
								final String textLine = args[3];
								
								if (!MathUtil.isNumber(textLine)) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									
									line = MathUtil.parseInteger(textLine);
									
									if (line < 1) {
										final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
										
										message.sendMessage(sender);
										SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
										return true;
									} else {
										if (EquipmentUtil.hasLore(item)) {
											final Integer lastLine = requirementManager.getLineRequirementPermission(item);
											
											if (lastLine != null) {
												if (lastLine != line) {
													EquipmentUtil.setLore(item, lastLine, "");
												}
											}
										}
									}
								}
							} else {
								if (EquipmentUtil.hasLore(item)) {
									final Integer lineReq = requirementManager.getLineRequirementPermission(item);
									final int loreSize = EquipmentUtil.getLores(item).size();
									
									line = lineReq != null ? lineReq : loreSize + 1;
								} else {
									line = 1;
								}
							}
							
							
							final String lore = requirementManager.getTextPermission(permission);
							final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Requirement_Permission_Success");
							final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
							
							mapPlaceholder.put("permission", permission);
							mapPlaceholder.put("line", String.valueOf(line));
							
							EquipmentUtil.setLore(item, line, lore);
							
							message.sendMessage(sender, mapPlaceholder);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
							
							player.updateInventory();
							return true;
						}
					} else if (requirementEnum.equals(RequirementEnum.REQUIREMENT_LEVEL)) {
						if (args.length < 3) {
							final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Req_Level"));
							final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Level_Permission");
							
							message.sendMessage(sender, "tooltip_att_req_level", tooltip);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final String textLevel = args[2];
							
							int level;
							int line;
							
							if (MathUtil.isNumber(textLevel)) {
								level = MathUtil.parseInteger(textLevel);
								level = MathUtil.limitInteger(level, 0, level);
							} else {
								final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
								
								message.sendMessage(sender);
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return true;
							}
							
							if (args.length > 3) {
								final String textLine = args[3];
								
								if (!MathUtil.isNumber(textLine)) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									
									line = MathUtil.parseInteger(textLine);
									
									if (line < 1) {
										final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
										
										message.sendMessage(sender);
										SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
										return true;
									} else {
										if (EquipmentUtil.hasLore(item)) {
											final Integer lastLine = requirementManager.getLineRequirementLevel(item);
											
											if (lastLine != null) {
												if (lastLine != line) {
													EquipmentUtil.setLore(item, lastLine, "");
												}
											}
										}
									}
								}
							} else {
								if (EquipmentUtil.hasLore(item)) {
									final Integer lineReq = requirementManager.getLineRequirementLevel(item);
									final int loreSize = EquipmentUtil.getLores(item).size();
									
									line = lineReq != null ? lineReq : loreSize + 1;
								} else {
									line = 1;
								}
							}
							
							final String lore = requirementManager.getTextLevel(level);
							final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Requirement_Level_Success");
							final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
							
							mapPlaceholder.put("level", String.valueOf(level));
							mapPlaceholder.put("line", String.valueOf(line));
							
							EquipmentUtil.setLore(item, line, lore);
							
							message.sendMessage(sender, mapPlaceholder);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
							
							player.updateInventory();
							return true;
						}
					} else if (requirementEnum.equals(RequirementEnum.REQUIREMENT_CLASS)) {
						if (!requirementManager.isSupportReqClass()) {
							final MessageBuild message = lang.getMessage(sender, "Argument_Not_Support_Class");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else if (args.length < 3) {
							final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Req_Class"));
							final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Class_Permission");
							
							message.sendMessage(sender, "tooltip_att_req_class", tooltip);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final String textClass = args[2];
							
							int line;
							
							if (args.length > 3) {
								final String textLine = args[3];
								
								if (!MathUtil.isNumber(textLine)) {
									final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
									
									message.sendMessage(sender);
									SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
									return true;
								} else {
									
									line = MathUtil.parseInteger(textLine);
									
									if (line < 1) {
										final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
										
										message.sendMessage(sender);
										SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
										return true;
									} else {
										if (EquipmentUtil.hasLore(item)) {
											final Integer lastLine = requirementManager.getLineRequirementClass(item);
											
											if (lastLine != null) {
												if (lastLine != line) {
													EquipmentUtil.setLore(item, lastLine, "");
												}
											}
										}
									}
								}
							} else {
								if (EquipmentUtil.hasLore(item)) {
									final Integer lineReq = requirementManager.getLineRequirementClass(item);
									final int loreSize = EquipmentUtil.getLores(item).size();
									
									line = lineReq != null ? lineReq : loreSize + 1;
								} else {
									line = 1;
								}
							}
							
							final String lore = requirementManager.getTextClass(textClass);
							final MessageBuild message = lang.getMessage(sender, "MyItems_Attribute_Requirement_Class_Success");
							final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
							
							mapPlaceholder.put("class", textClass);
							mapPlaceholder.put("line", String.valueOf(line));
							
							EquipmentUtil.setLore(item, line, lore);
							
							message.sendMessage(sender, mapPlaceholder);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
							
							player.updateInventory();
							return true;
						}
					} else {
						final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Attribute_Req"));
						final MessageBuild message = lang.getMessage(sender, "Argument_Attribute_Requirement");
						
						message.sendMessage(sender, "tooltip_att_req", tooltip);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					}
				}
			} else {
				final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Command");
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			}
		} else {
			final String[] fullArgs = TextUtil.pressList(args, 2);
			
			return CommandMyItems.help(sender, command, label, fullArgs);
		}
	}
}
