package net.ardeus.myitems.command.executor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.bridge.unity.BridgeTagsNBT;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;

public class CommandUnbreakable extends HandlerCommand implements CommandExecutor {

	public CommandUnbreakable(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		return unbreakable(sender, command, label, args);
	}
	
	protected static final boolean unbreakable(CommandSender sender, Command command, String label, String[] args) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!commandManager.checkPermission(sender, "MyItems_Unbreakable")) {
			final String permission = commandManager.getPermission("MyItems_Unbreakable");
			final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else if (!SenderUtil.isPlayer(sender)) {
			final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else {
			final Player player = PlayerUtil.parse(sender);
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (!EquipmentUtil.isSolid(item)) {
				final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			} else {		
				final BridgeTagsNBT bridgeTagsNBT = Bridge.getBridgeTagsNBT();
				final boolean isUnbreakable = bridgeTagsNBT.isUnbreakable(item);
				
				if (args.length > 0) {
					final String textBoolean = args[0];
					
					if (textBoolean.equalsIgnoreCase("true") || textBoolean.equalsIgnoreCase("on")) {
						final MessageBuild message = lang.getMessage(sender, "MyItems_Unbreakable_Turn_On");
						
						bridgeTagsNBT.setUnbreakable(item, true);
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						return true;
					} else if (textBoolean.equalsIgnoreCase("false") || textBoolean.equalsIgnoreCase("off")) {
						final MessageBuild message = lang.getMessage(sender, "MyItems_Unbreakable_Turn_Off");
						
						bridgeTagsNBT.setUnbreakable(item, false);
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						return true;
					}
				}
					
				if (isUnbreakable) {
					final MessageBuild message = lang.getMessage(sender, "MyItems_Unbreakable_Turn_Off");
					
					bridgeTagsNBT.setUnbreakable(item, false);
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
					return true;
				} else {
					final MessageBuild message = lang.getMessage(sender, "MyItems_Unbreakable_Turn_On");
					
					bridgeTagsNBT.setUnbreakable(item, true);
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
					
					player.updateInventory();
					return true;
				}
			}
		}
	}
}
