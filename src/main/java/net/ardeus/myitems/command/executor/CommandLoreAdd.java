package net.ardeus.myitems.command.executor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public class CommandLoreAdd extends HandlerCommand implements CommandExecutor {

	public CommandLoreAdd(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		return addLore(sender, command, label, args);
	}
	
	protected static final boolean addLore(CommandSender sender, Command command, String label, String[] args) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!commandManager.checkPermission(sender, "Lore_Add")) {
			final String permission = commandManager.getPermission("Lore_Add");
			final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else if (!SenderUtil.isPlayer(sender)) {
			final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else {
			final Player player = PlayerUtil.parse(sender);
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (!EquipmentUtil.isSolid(item)) {
				final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			} else {
				final String lore = args.length < 1 ? "" : TextUtil.hookPlaceholderAPI(player, TextUtil.concatenate(args)); 			
				final MessageBuild message = lang.getMessage(sender, "MyItems_AddLore_Success");
				
				EquipmentUtil.addLore(item, lore);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
				
				player.updateInventory();
				return true;
			}
		}
	}
}
