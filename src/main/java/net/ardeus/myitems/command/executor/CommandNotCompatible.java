package net.ardeus.myitems.command.executor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.SenderUtil;

public class CommandNotCompatible extends HandlerCommand implements CommandExecutor {

	public CommandNotCompatible(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		final PluginManager pluginManager = plugin.getPluginManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		final MessageBuild message = lang.getMessage(sender, "MyItems_Not_Compatible");
		
		message.sendMessage(sender);
		SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
		return true;
	}
}
