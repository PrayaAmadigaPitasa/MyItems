package net.ardeus.myitems.command.executor;

import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public class CommandLoreSet extends HandlerCommand implements CommandExecutor {

	public CommandLoreSet(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		return setLore(sender, command, label, args);
	}
	
	protected static final boolean setLore(CommandSender sender, Command command, String label, String[] args) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!commandManager.checkPermission(sender, "Lore_Set")) {
			final String permission = commandManager.getPermission("Lore_Set");
			final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else if (!SenderUtil.isPlayer(sender)) {
			final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else {
			final Player player = PlayerUtil.parse(sender);
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (!EquipmentUtil.isSolid(item)) {
				final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			} else {
				if (args.length < 1) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Lore_Set"));
					final MessageBuild message = lang.getMessage(sender, "Argument_SetLore");
					
					message.sendMessage(sender, "tooltip_lore_set", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final String textLine = args[0];
					
					if (!MathUtil.isNumber(textLine)) {
						final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Value");
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						final int line = MathUtil.parseInteger(textLine);
						
						if (line < 1) {
							final MessageBuild message = lang.getMessage(sender, "Argument_Invalid_Line");
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return true;
						} else {
							final String lore = args.length < 2 ? "" : TextUtil.hookPlaceholderAPI(player, TextUtil.concatenate(args, 2));
							final MessageBuild message = lang.getMessage(sender, "MyItems_SetLore_Success");
							final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
							
							mapPlaceholder.put("line", String.valueOf(line));
							mapPlaceholder.put("lore", lore);
							
							EquipmentUtil.setLore(item, line, lore);
							
							message.sendMessage(sender, mapPlaceholder);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
							
							player.updateInventory();
							return true;
						}
					}
				}
			}
		}
	}
}
