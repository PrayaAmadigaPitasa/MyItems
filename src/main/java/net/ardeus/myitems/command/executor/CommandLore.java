package net.ardeus.myitems.command.executor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.praya.agarthalib.utility.TextUtil;

import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.CommandManager;

public class CommandLore extends HandlerCommand implements CommandExecutor {

	public CommandLore(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		final CommandManager commandManager = plugin.getPluginManager().getCommandManager();
		
		if (args.length > 0) {
			final String subCommand = args[0];
			
			if (commandManager.checkCommand(subCommand, "Lore_Set")) {
				final String[] fullArgs = TextUtil.pressList(args, 2);
				
				return CommandLoreSet.setLore(sender, command, label, fullArgs);
			} else if (commandManager.checkCommand(subCommand, "Lore_Insert")) {
				final String[] fullArgs = TextUtil.pressList(args, 2);
				
				return CommandLoreInsert.insertLore(sender, command, label, fullArgs);
			} else if (commandManager.checkCommand(subCommand, "Lore_Add")) {
				final String[] fullArgs = TextUtil.pressList(args, 2);
				
				return CommandLoreAdd.addLore(sender, command, label, fullArgs);
			} else if (commandManager.checkCommand(subCommand, "Lore_Remove")) {
				final String[] fullArgs = TextUtil.pressList(args, 2);
				
				return CommandLoreRemove.removeLore(sender, command, label, fullArgs);
			} else if (commandManager.checkCommand(subCommand, "Lore_Clear")) {
				final String[] fullArgs = TextUtil.pressList(args, 2);
				
				return CommandLoreClear.clearLore(sender, command, label, fullArgs);
			} else {
				return true;
			}
		} else {
			return true;
		}
	}
}
