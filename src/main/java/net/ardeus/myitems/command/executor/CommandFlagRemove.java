package net.ardeus.myitems.command.executor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import core.praya.agarthalib.enums.main.Slot;
import net.ardeus.myitems.MyItems;
import net.ardeus.myitems.handler.HandlerCommand;
import net.ardeus.myitems.manager.plugin.CommandManager;
import net.ardeus.myitems.manager.plugin.LanguageManager;
import net.ardeus.myitems.manager.plugin.PluginManager;

import com.praya.agarthalib.utility.EquipmentUtil;
import com.praya.agarthalib.utility.ItemFlagUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

public class CommandFlagRemove extends HandlerCommand implements CommandExecutor {

	public CommandFlagRemove(MyItems plugin) {
		super(plugin);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		return removeFlag(sender, command, label, args);
	}
	
	protected static final boolean removeFlag(CommandSender sender, Command command, String label, String[] args) {
		final MyItems plugin = JavaPlugin.getPlugin(MyItems.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final CommandManager commandManager = pluginManager.getCommandManager();
		final LanguageManager lang = pluginManager.getLanguageManager();
		
		if (!commandManager.checkPermission(sender, "Flag_Remove")) {
			final String permission = commandManager.getPermission("Flag_Remove");
			final MessageBuild message = lang.getMessage(sender, "Permission_Lack");
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else if (!SenderUtil.isPlayer(sender)) {
			final MessageBuild message = lang.getMessage(sender, "Console_Command_Forbiden");
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else {
			final Player player = PlayerUtil.parse(sender);
			final ItemStack item = Bridge.getBridgeEquipment().getEquipment(player, Slot.MAINHAND);
			
			if (!EquipmentUtil.isSolid(item)) {
				final MessageBuild message = lang.getMessage(sender, "Item_MainHand_Empty");
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			} else {		
				if (args.length < 1) {
					final String tooltip = TextUtil.getJsonTooltip(lang.getText(sender, "Tooltip_Flag_Remove"));
					final MessageBuild message = lang.getMessage(sender, "Argument_RemoveFlag");
					
					message.sendMessage(sender, "tooltip_flag_remove", tooltip);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return true;
				} else {
					final String flagName = args[0];
					
					if (!ItemFlagUtil.isExist(flagName)) {
						final MessageBuild message = lang.getMessage(sender, "Item_Flag_Not_Exist");
						
						message.sendMessage(sender, "flag", flagName);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return true;
					} else {
						final ItemFlag flag = ItemFlagUtil.getFlag(flagName);
						final MessageBuild message = lang.getMessage(sender, "MyItems_RemoveFlag_Success");
						
						ItemFlagUtil.removeFlag(item, flag);
						
						message.sendMessage(sender, "flag", flag.toString());
						SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
						
						player.updateInventory();
						return true;
					}
				}
			}
		}
	}
}
